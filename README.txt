
GENERAL PURPOSE APPLICATION DEVELOPMENT FRAMEWORK

Java 8 is required from:
http://www.oracle.com/technetwork/java/javase/downloads/index.html

Download Vaadin All-In-One Archive from
https://vaadin.com/releases
Unzip vaadin-all.7.x.x.zip package to directory vaadin
Then give a command:
ant vaadin
In directory compile/lib should be files:
vaadin-client-7.x.x.jar
vaadin-client-compiler-7.x.x.jar
In directory lib should be other vaadin-*.jar files.

USER INTERFACE

Rapid application with user interface generator using reflection and
Apache BCEL Byte Code Engineering Library from

http://commons.apache.org/proper/commons-bcel/

The user interface upports tabsheets, menus, sub- and application windows, standard- and custom user interface
components, themes and localization with resource bundles.
The user interface is generated from a tree of Java objects having methods and types related
to application logic without direct reference to Vaadin-interfaces.

PERSISTENT STORAGE

Automatic persistence engine using Apache Jena Resource Description Framework database from:
http://jena.apache.org/

RDF is mapped to Java-objects so that RDF-subject is an object instance if it has an identifier,
or RDF-subject is a class if it has no identifier, RDF-predicate is a property method,
and RDF-object is a literal value, other class or an object instance.

WEB SERVICES

Automatic web service engine using Apache Axis2 Web Services engine from
http://axis.apache.org/axis2/java/core/index.html
Axis2 is used partially, bypassing definition document generation and object-serialization.
Supports SOAP/REST-invocation styles, XML/JSON-formats and WSDL/WADL-documents.
SOAP=Simple Access Protocol
REST=Representational state transfer
XML=Extensible Markup Language
JSON=JavaScript Object Notation
WSDL=Web Service Definition Language
WADL=Web Application Definition Language

Web services are generated from methods with SOAP action when using SOAP or request path
when using REST as path to method in object tree, with request or response as one object.
Web clients to local web services are automatically available. If same application
is running remotely, local web client can be used to call it.

WEB CLIENTS

Automatic web client engine which generates a web client Java-interface from WSDL-document in
Internet or from local file. From the web client Java-interface is generated user interface
which can be used to call the web service interactively. Customized user interface can
also use the generated web client Java-interface.

Copyright- and license files are txt-files in lib-directory.

INSTRUCTIONS

Look javadoc of classes org.vaadin.objectview.ObjectView and
org.dummy.DummyClass and look at the source code.
Library inspects bean type classes having getter-,
setter- and other methods marked additionally with special keywords.
User interface-, persistence- and web service implementations are isolated
from application code with callback methods. Code required for application
contains mostly only application logic, except when using Vaadin custom
components or some other general purpose extensions.

If you wish, you can define in the same class all of the following:
1. Value objects for interaction between user and application
2. Actions for value objects
3. Data objects for storing in persistent store (maybe same as value objects)
4. Operations for data objects
5. Web service- and client methods

TEST APPLICATION

Put Vaadin library jar-file to lib-directory if this package is loaded from Vaadin Add-Ons Library
(otherwise one version should be present in the package) and give a command:

ant war

Result file deploy/objectview.war can be deployed to a servlet
container. It contains test application for this library.
The test application can be accessed from an address similar to:
http://<host name>:<port number>/objectview/?debug
Database files are written under user home in directory dummyObjectView.

Logging levels can be adjusted in the file:
webapp/objectview/WEB-INF/classes/log4j.properties

One servlet container can be obtained from the address:
http://keppi.weebly.com/

Smoke tested with Vaadin library version 8.0.0. Complete test suite
will be made later.

Smoke test can be performed as follows:
Go to the page:
http://<host name>:<port number>/objectview/?debug

In the bottom part of the page press button "Search by child".
This action tries to search data from persistent storage with
search criteria in the fields with title "Child"
above table "Read only list" where results will be displayed.
Action should not find any data if data has not been stored.
In the bottom part of the page press button "Store child".
This action tries to store fields with title "Child"
above table "Read only list" to persistent storage.
After this in the bottom part of the page press button "Search by child".
Same data as in the fields with title "Dummy child class" should
appear in the table "Read only list". Notice that if you change field
values in "Child", the values must be exactly same when searching.
Field "Date value" gets initially always current date. When application
is restarted, values must be same as during storing, so that they
are found when searched. At least "Date value" must be reset to the
same value it had when fiels were stored. Searching is performed as
conjunction of all values. Database engine is embedded to the
application and data is stored to directory "dummyObjectView"
under user's home directory ($HOME or %USERPROFILE%).

WEB SERVICES

One example web service operation with an echo effect can be accessed from an address similar to:
http://<host name>:<port number>/objectview/services/DummyObjectView/DummyObjectView.tabList.DummyClass.childRetrieve?booleanValue=true&charValue=a&byteValue=1&shortValue=1&intValue=1&longValue=1&floatValue=1&doubleValue=1&bigDecimalValue=1&bigIntegerValue=1&stringValue=&dateValue=1900-01-01T00:00:00.000&enumValue=ENUM1&groupEnumValue=ENUM2

WSDL-document can be accessed from an address similar to:
http://<host name>:<port number>/objectview/services/DummyObjectView?wsdl

WADL-document can be accessed from an address similar to:
http://<host name>:<port number>/objectview/services/DummyObjectView?wadl

Smoke test of web services can be performed as test described above.
Now instead of using buttons "Store child", "Search by child" and "Remove child",
one can use buttons "Web retrieve child", "Web add child" and
"Web delete child".

DEMO APPLICATIONS

Demo applications can be found in the directory demos.
demos/volunteer
demos/hospitality
demos/amazon
demos/bing
demos/bingmaps

Changing to these directories, war-file can be made giving a command:

ant war

Result will be in the deploy-directory.

Deploying to CaneProxy-servlet container can be accomplished with the following command:

ant deploy

All demos can be deployed once:

ant deploy-all

Set environment variable WEB_HOME to alternative webapps-directory for your own
second hand servlet container.

Volunteer

Volunteer-application runs standalone using embedded persistence engine.
This application is aiming to be start of registration system for volunteer activities.
Database files are written under user home in directory VolunteerObjectView.

Deploying this application to servlet container, it can be accessed with address similar to:
http://<host name>:<port number>/volunteer/[?debug]

Optional parameter ?debug opens Vaadin-debug window.
Replace <host name>:<port number> with your environment, like localhost:8000.

Main user mode is entered giving address similar to:
http://<host name>:<port number>/volunteer/?staffOnly=iAmStaff
and then giving credentials:
commander
NCKDO#5kceK
These keywords are given in file demos/volunteer/docroot/WEB-INF/web.xml.
Then it is possible to login with any username without password.

Logging levels can be adjusted in the file:
demos/volunteer/docroot/WEB-INF/classes/log4j.properties

Smoke test:
Go to tab Volunteer.
Press link Registration
Fill Registration form with dummy data, like a's to every field.
Press button Register.
Look that there is a message: Your credentials are...
Copy password to clip board.
Press button Logout.
Go to tab Volunteer.
Type to Login-form to field User name your character, like a.
Paste to Password-field your password. Press button Login.
Look that your data is in Registration form.
Change some field in Registration form.
Press button Update. Press button Logout.
Login again. Look that your updates are visible in Registration-form.
Press link Job registrations.
Choose work type Potato peeling.
Choose some day from calendar component. Choose some hour.
Press button Reserve. Check that your chosen day changes to green color.
Check that work reservation data is in table Work reservations.
Logout again. Browser can be also closed so that all data and session is lost.
Login with your credentials. Go to tab Reservation.
Choose work type Potato peeling. Check that day chosen earlier has green color
in calendar component. Check that reservation data is in table Work reservations.
Select the same day in calendar component.
Check that date is in fields Hour and Date below calendar component.
Press button Remove. Check that the day is not marked in calendar component,
and that the day is removed from table Work reservations.
Select some day and press button Reserve.
Move to next month in calendar component.
Select again some day and press button Reserve.
Move back to previous month in calendar component.
Check that the day you selected earlier is marked.

Hospitality

Hospitality-application is difficult to setup and needs running instance of OpenERP-system
with hotel management module installed, and application configuration in the directory hospitality_data
loaded to database. This application is aiming to be start for accommodation booking system.
This demo can be used mostly as source reference until setup instructions are available.
Looking at the script files for Mac/Linux in the directory demos/hospitality/openerp
(give in this directory command 'chmod +x *.sh') may help and reading instructions in the address:
http://www.openerp.com/

There are client-libraries for OpenERP and to net payment service in the address:
https://www.maksuturva.fi

Deploying this application to servlet container, it can be accessed with address similar to:
http://<host name>:<port number>/hospitality/[?debug]

Optional parameter ?debug opens Vaadin-debug window.
Replace <host name>:<port number> with your environment, like localhost:8000.

Logging levels can be adjusted in the file:
demos/hospitality/docroot/WEB-INF/classes/log4j.properties

Both demos have in their user interface add-on from Vaadin library, namely
Risto Yrjänä's StyleCalendar which is precompiled in demos/VAADIN-directory.

Amazon

Amazon-application is user interface to deprecated Amazon-SOAP-service. It is only example
of generating web client. So that service can be invoked, to file:

demos/amazon/docroot/WEB-INF/web.xml

must be added aws access key id, and secret access key id to following entries:

    <init-param>
      <param-name>org.amazon.AmazonObjectView.AWSAccessKeyId</param-name>
      <param-value></param-value>
    </init-param>
    <init-param>
      <param-name>org.amazon.AmazonObjectView.SecretAccessKey</param-name>
      <param-value></param-value>
    </init-param>

Access keys can be obtained from:
http://aws.amazon.com/iam/

Deploying this application to servlet container, it can be accessed with address similar to:
http://<host name>:<port number>/amazon/[?debug]

Optional parameter ?debug opens Vaadin-debug window.
Replace <host name>:<port number> with your environment, like localhost:8000.

Bing

Bing-application is an user interface to Microsoft Bing-web services. It is an example of
generating web client and associated user interface. So that services can be invoked, to file:

demos/bing/docroot/WEB-INF/web.xml

must be added application id to the following entry:

    <init-param>
      <param-name>org.bing.BingObjectView.applicationId</param-name>
      <param-value></param-value>
    </init-param>

Application id can be obtained from:
https://www.bingmapsportal.com/

Deploying this application to servlet container, it can be accessed with address similar to:

http://<host name>:<port number>/bing/[?debug]

Optional parameter ?debug opens Vaadin-debug window.
Replace <host name>:<port number> with your environment, like localhost:8000.

Smoke test with Bing can be performed as follows:

Go to the page:
http://<host name>:<port number>/bing/[?debug]
Go to the tab BingGeocode.
Type to the field optional_Query above form button your favourite street address.
Name and number of the street, comma, and the name of the city.
Press button _GeocodeClientCall.
Look in the section FORMOPTIONAL_GEOCODERESULT.FORMOPTIONAL_GEOCODERESULT
in subsection FORMOPTIONAL_GEOCODELOCATION.FORMOPTIONAL_GEOCODELOCATION
in entry GeocodeLocation
and check that there are relevant coordinates in the fields
optional_Latitude and optional_Longitude.

Go to the tab BingReverseGeocode and copy coordinate fields from the previous tab
to the section Location above form button to the fields
optional_Latitude and optional_Longitude.
Press button _ReverseGeocodeClientClass.
Look in the section FORMOPTIONAL_GEOCODERESULT.FORMOPTIONAL_GEOCODERESULT
in the entry GeocodeResult ant check that there is relevant address near specified coordinates.
Usually this address is not the same as given in the first tab,
but some address in neighbourhood.

Go to the tab BingGetMapUri.
Type in the entry Location, in the fields optional_Latitude and optional_Longitude
coordinates copied from the first tab.
Look in the section MapUriResponse in the field optional_Uri. Copy URL, and paste
it to the browser address field. Check that response is a map tile near specified location.

Go to the tab BingCalculateRoute.
Add in the section ArrayOfWaypoint, to the list field formOptional_Waypoint two entries.
Put in the both Location-entries, to the fields
optional_Latitude and optional_Longitude coordinates from the first tab.
Press button _CalculateRouteClientCall.
Look in the section RouteResult and check that there is some entry RouteLeg.

Go to the tab BingCalculateRouteFromMajorRoads.
Type in the section Waypoint, subsection Location, in the fields
optional_Latitude and optional_Longitude
coordinates 43.89 and -79.23.
Press button _CalculateRouteFromMajorRoadsClientCall.
Look in the section ArrayOfWaypoint, to the list FORMOPTIONAL_WAYPOINT.FORMOPTIONAL_WAYPOINT.
Check that there is first Waypoint with description
the south (on Hwy-48 / RR-68 / Markham Rd)
with coordinates near to 43.905 and -79.238.

Go to the tab BingSearch.
Type to the field optional_Query the same address as in tab BingGeocode.
Press button _SearchClientCall.
Check that there are same result coordinates as in tab BingGeocode.

BingMaps

BingMaps-application is an user interface to Microsoft Bing-search web
service. It is an example of generating web client and making a
customized user interface for it. So that services can be invoked, to
file:

demos/bingmaps/docroot/WEB-INF/web.xml

must be added application id to the following entry:

    <init-param>
      <param-name>org.bing.BingObjectView.applicationId</param-name>
      <param-value></param-value>
    </init-param>

Application id can be obtained from:
https://www.bingmapsportal.com/

In the following parameter, port number of running server must be changed.
Map tiles for BingMaps are read through servlet filter in local address of server machine.
Tile address can be also changed to other address, like openstreetmap.

    <init-param>
      <param-name>org.bingmaps.BingMapsObjectView.tileLayerURLTemplate</param-name>
      <param-value>http://localhost:8000/bingmaps/tile/{s}/{z}/{x}/{y}</param-value>
<!--
      <param-value>http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png</param-value>
-->
    </init-param>

Deploying this application to servlet container, it can be accessed with address similar to:

http://<host name>:<port number>/bingmaps/[?debug]

Optional parameter ?debug opens Vaadin-debug window.
Replace <host name>:<port number> with your environment, like localhost:8000.

Smoke test with BingMaps can be performed as follows:

Go to the page:
http://<host name>:<port number>/bingmaps/[?debug]
Type to text file your favourite street address. Select map type road.
Press button searchModelClientCall.
Watch that below title formattedAddress appears your address in canonized format,
with geographical coordinates.
Try to pan and zoom in map displayed below.

This demo has in it's user interface add-on from Vaadin library, namely
Matti Tahvonen's V-Leaflet which is precompiled in demos/VAADIN-directory.

TOMCAT

For using in Apache Tomcat server, disable session persistence from file
config/context.xml
Uncommenting section
<Manager pathname="" />

Development version control can be found in
https://bitbucket.org/jketuri/objectshrew
Support forum of the framework can be found in
http://keppi.weebly.com/objectshrew.html#/objectshrew/

Required tool for compiling core library:
http://ant.apache.org/index.html

Required tool for compiling libraries needed for demos:
https://maven.apache.org/
If using Windows, grab here a shell for running bash-scripts:
https://sourceforge.net/p/mingw-w64/wiki2/MSYS/

Used libraries:
http://commons.apache.org/proper/commons-bcel/
http://jena.apache.org/
http://axis.apache.org/axis2/java/core/
https://axis.apache.org/axis2/java/rampart/

Questions can be sent to email-address:
ywanderingj@yahoo.com
