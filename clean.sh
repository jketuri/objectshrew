set -e
pushd .
ant clean
pushd demos/amazon
ant clean
popd
pushd demos/bing
ant clean
popd
pushd demos/bingmaps
ant clean
popd
pushd demos/hospitality
ant clean
popd
pushd demos/volunteer
ant clean
popd
popd