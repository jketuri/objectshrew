#!/bin/sh

check()
{
    RV="$?"
    if test $RV != "0"
	then
        echo "RV=$RV"
	    exit 1
    fi
}

rm -f $HOME/objectview_pack.zip
check
zip -rv $HOME/objectview_pack * -x '*.class' -x 'build/*' -x '*.war' -x '*/.svn/*' -x 'demos/VAADIN/widgetsets/*' -x 'demos/volunteer/docroot/VAADIN/*' -x 'demos/hospitality/docroot/VAADIN/*'
check
ls -l $HOME/objectview_pack.zip
check
