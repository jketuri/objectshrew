#!/bin/bash
if test "${OPENERP_ENV+1}" != "1"
then
. env.sh
export OPENERP_ENV=1
fi
read -p 'Drop database? '
dropdb --username $USER openerp
dropuser --username $USER openerp
sudo rm -rvf /usr/local/pgsql/data
