#!/bin/bash
if test "${OPENERP_ENV+1}" != "1"
then
. env.sh
export OPENERP_ENV=1
fi
pushd /usr/local/pgsql
sudo mkdir data
sudo chown $USER data
initdb -D /usr/local/pgsql/data
postgres -D /usr/local/pgsql/data &
echo $! >$HOME/postgres.pid
createuser --createdb --username $USER --no-createrole --pwprompt openerp
kill `cat $HOME/postgres.pid`
popd
