#!/bin/bash
if test "${OPENERP_ENV+1}" != "1"
then
. env.sh
export OPENERP_ENV=1
fi
pushd yaml-0.1.3
./configure
make
sudo make install
popd
pushd Babel-0.9.5
python setup.py install
popd
pushd Beaker-1.5.4
python setup.py install
popd
pushd CherryPy-3.1.2
python setup.py install
popd
pushd FormEncode-1.2.2
python setup.py install
popd
pushd Mako-0.3.4
python setup.py install
popd
pushd MarkupSafe-0.9.3
python setup.py install
popd
pushd PyChart-1.39
python setup.py install
popd
pushd PyWebDAV-0.9.4.1
python setup.py install
popd
pushd PyYAML-3.09
python setup.py install
popd
pushd ReportLab_2_4
python setup.py install
popd
pushd zlib-1.2.5
./configure
make
sudo make install
popd
pushd libxml2-2.7.7
./configure
make
sudo make install
popd
pushd libxslt-1.1.26
./configure --with-libxml-prefix=/usr/local
make
sudo make install
popd
pushd lxml-2.2.8
export ARCHFLAGS='-arch i386'
python setup.py install --with-xslt-config=/usr/local/bin/xslt-config
popd
pushd egenix-mx-base-3.1.3.macosx-10.4-fat-py2.6_ucs2.prebuilt
#pushd egenix-mx-base-3.1.3.macosx-10.4-fat-py2.5_ucs2.prebuilt
python setup.py install
popd
pushd postgresql-8.4.4
./configure
make
sudo make install
popd
export PATH=/usr/local/pgsql/bin:$PATH
pushd psycopg2-2.2.2
python setup.py install --with-xslt-config=/usr/local/bin/xslt-config
popd
pushd pydot-1.0.2
python setup.py install
popd
if test "${OPENERP_VER}" = "6"
then
pushd openerp-server-6.0.1
python setup.py install
popd
pushd openerp-client-6.0.1
python setup.py install
popd
pushd openerp-web-6.0.1
python setup.py install
popd
cp -v openerp/openerp-web.cfg /Library/Python/2.6/site-packages/openerp_web-6.0.1-py2.6.egg/doc/openerp-web.cfg
else
pushd openerp-server-5.0.12
python setup.py install
popd
pushd openerp-client-5.0.12
python setup.py install
popd
pushd openerp-web-5.0.12
python setup.py install
popd
cp -v openerp/openerp-web.cfg /Library/Python/2.5/site-packages/openerp_web-5.0.12-py2.5.egg/config/openerp-web.cfg
fi
#openerp-server -s --stop-after-init
cp -v openerp/dot_openerp_serverrc $HOME/.openerp_serverrc
cp -v openerp/dot_openerprc $HOME/.openerprc
./hotel_install.sh
