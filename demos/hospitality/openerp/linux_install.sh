#!/bin/bash
if test "${OPENERP_ENV+1}" != "1"
then
. env.sh
export OPENERP_ENV=1
fi
if test "${OPENERP_VER}" = "6"
then
PYTHON=python
else
PYTHON=/usr/local/bin/python2.5
fi
tar -xjvf Python-2.5.5.tar.bz2
pushd Python-2.5.5
./configure
make
make altinstall
popd
yum install postgresql84.i386
yum install postgresql84-server.i386
yum install postgresql84-devel.i386
yum install python-devel.i386
if test "${OPENERP_VER}" = "6"
then
pushd egenix-mx-base-3.1.3.linux-i686-py2.6_ucs2.prebuilt.zip
else
pushd egenix-mx-base-3.1.3.linux-i686-py2.5_ucs2.prebuilt
fi
${PYTHON} setup.py install
popd
pushd PyChart-1.39
${PYTHON} setup.py install
popd
pushd PyWebDAV-0.9.4.1
${PYTHON} setup.py install
popd
pushd PyYAML-3.09
${PYTHON} setup.py install
popd
pushd lxml-2.2.8
${PYTHON} setup.py install
popd
pushd psycopg2-2.4
${PYTHON} setup.py install
popd
pushd pydot-1.0.2
${PYTHON} setup.py install
popd
pushd ReportLab_2_4
${PYTHON} setup.py install
popd
pushcd pytz-2010l
${PYTHON} setup.py install
popd
pushd setuptools-0.6c11
${PYTHON} setup.py install
popd
if test "${OPENERP_VER}" = "6"
then
pushd openerp-server-6.0.1
${PYTHON} setup.py install
popd
pushd openerp-web-6.0.1
${PYTHON} setup.py install
popd
cp -v openerp/openerp-web.cfg /opt/ActivePython-2.6/lib/python2.6/site-packages/openerp_web-6.0.1-py2.6.egg/doc/openerp-web.cfg
else
pushd openerp-server-5.0.14
${PYTHON} setup.py install
popd
pushd openerp-web-5.0.15
${PYTHON} setup.py install
popd
cp -v openerp/openerp-web.cfg /usr/local/lib/python2.5/site-packages/openerp_web-5.0.14-py2.5.egg/config/openerp-web.cfg
fi
cp -v openerp/dot_openerp_serverrc $HOME/.openerp_serverrc
cp -v openerp/dot_openerprc $HOME/.openerprc
cp -v openerp/openerp-web /etc/init.d/
cp -v openerp/openerp-server /etc/init.d/
cp -v openerp/openerp-web.cfg /etc/
cp -v openerp/dot_openerp_serverrc /etc/openerp-server.cfg
chmod ugo+rw /var/run
./linux_hotel_install.sh
