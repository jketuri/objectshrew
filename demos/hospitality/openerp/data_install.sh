#!/bin/bash
if test "${OPENERP_ENV+1}" != "1"
then
. env.sh
export OPENERP_ENV=1
fi
if test "${OPENERP_VER}" = "6"
then
ADDONS=/Library/Python/2.6/site-packages/openerp-server/addons
else
ADDONS=/Library/Python/2.5/site-packages/openerp-server/addons
fi
pushd ..
zip -rv hospitality_data hospitality_data
cp -v hospitality_data.zip $ADDONS
popd
