#!/bin/bash
if test "${OPENERP_ENV+1}" != "1"
then
. env.sh
export OPENERP_ENV=1
fi
if test "${OPENERP_VER}" = "6"
then
ADDONS=/opt/ActivePython-2.6/lib/python2.6/site-packages/openerp-server/addons
else
ADDONS=/usr/local/lib/python2.5/site-packages/openerp-server/addons
fi
cp -v hotel.zip $ADDONS
cp -v hotel_housekeeping.zip $ADDONS
cp -v hotel_reservation.zip $ADDONS
cp -v hotel_restaurant.zip $ADDONS
