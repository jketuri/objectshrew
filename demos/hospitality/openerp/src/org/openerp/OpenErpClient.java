/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.openerp;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcSun15HttpTransportFactory;

import org.vaadin.common.Support;

public class OpenErpClient {
    protected XmlRpcClient commonXmlRpcClient = null,
        dbXmlRpcClient = null,
        objectXmlRpcClient = null;

    public static class Triple {
        final String attribute;
        final String operator;
        final Object keys;

        public Triple(final String attribute,
                      final String operator,
                      final Object keys) {
            this.attribute = attribute;
            this.operator = operator;
            this.keys = keys;
        }

        public Triple(final String operator) {
            this.attribute = null;
            this.operator = operator;
            this.keys = null;
        }

        public String toString() {
            if (attribute != null) {
                return attribute + " " + operator + " " + Support.toString(keys);
            }
            return operator;
        }

    }

    public OpenErpClient(final String host,
                         final int port,
                         final String proxyHost,
                         final int proxyPort)
        throws MalformedURLException {
        commonXmlRpcClient = getXmlRpcClient(host, port, "/xmlrpc/common", proxyHost, proxyPort);
        dbXmlRpcClient = getXmlRpcClient(host, port, "/xmlrpc/db", proxyHost, proxyPort);
        objectXmlRpcClient = getXmlRpcClient(host, port, "/xmlrpc/object", proxyHost, proxyPort);
    }

    protected XmlRpcClient getXmlRpcClient(final String host,
                                           final int port,
                                           final String rpcString,
                                           final String proxyHost,
                                           final int proxyPort)
        throws MalformedURLException {
        final XmlRpcClient xmlRpcClient = new XmlRpcClient();
        final XmlRpcClientConfigImpl xmlrpcConfig = new XmlRpcClientConfigImpl();
        xmlrpcConfig.setEnabledForExtensions(true);
        xmlrpcConfig.setEnabledForExceptions(true);
        xmlrpcConfig.setServerURL(new URL("http", host, port, rpcString));
        xmlRpcClient.setConfig(xmlrpcConfig);
        final XmlRpcSun15HttpTransportFactory xmlRpcSun15HttpTransportFactory = new XmlRpcSun15HttpTransportFactory(xmlRpcClient);
        if (proxyHost != null) {
            xmlRpcSun15HttpTransportFactory.setProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort)));
        }
        xmlRpcClient.setTransportFactory(xmlRpcSun15HttpTransportFactory);
        return xmlRpcClient;
    }

    public Object[] getDatabases()
        throws MalformedURLException, XmlRpcException {
        // Retrieve databases
        return (Object[])dbXmlRpcClient.execute("list", new Object[0]);
    }

    public Integer connect(final String db,
                           final String login,
                           final String password)
        throws MalformedURLException, XmlRpcException {
        // Connect
        final Object[] parameters = new Object[] {db, login, password};
        final Object id = commonXmlRpcClient.execute("login", parameters);
        return id instanceof Integer ? (Integer)id : null;
    }

    public Object[] search(final String db,
                           final Integer clientId,
                           final String password,
                           final String relation,
                           final OpenErpClient.Triple[] triples)
        throws MalformedURLException, XmlRpcException {
        final Object[] tripleObjects = new Object[triples.length];
        int index = 0;
        for (final OpenErpClient.Triple triple : triples) {
            if (triple.attribute != null) {
                tripleObjects[index++] = new Object[] {triple.attribute, triple.operator, triple.keys};
            } else {
                tripleObjects[index++] = triple.operator;
            }
        }
        final Object[] parameters = new Object[] {db, clientId, password, relation, "search", tripleObjects};
        return (Object[])objectXmlRpcClient.execute("execute", parameters);
    }

    public Object create(final String db,
                         final Integer clientId,
                         final String password,
                         final String relation,
                         final Map values)
        throws MalformedURLException, XmlRpcException {
        final Object[] parameters = new Object[] {db, clientId, password, relation, "create", values};
        return objectXmlRpcClient.execute("execute", parameters);
    }

    public Object[] read(final String db,
                         final Integer clientId,
                         final String password,
                         final String relation,
                         final Object[] fields,
                         final Object[] ids)
        throws MalformedURLException, XmlRpcException {
        final Object[] parameters = new Object[] {db, clientId, password, relation, "read", ids, fields};
        return (Object[])objectXmlRpcClient.execute("execute", parameters);
    }

    public Boolean update(final String db,
                          final Integer clientId,
                          final String password,
                          final String relation,
                          final Object[] ids,
                          final Map values)
        throws MalformedURLException, XmlRpcException {
        final Object[] parameters = new Object[] {db, clientId, password, relation, "write", ids, values};
        return (Boolean)objectXmlRpcClient.execute("execute", parameters);
    }

    public Boolean delete(final String db,
                          final Integer clientId,
                          final String password,
                          final String relation,
                          final Object[] ids)
        throws MalformedURLException, XmlRpcException {
        final Object[] parameters = new Object[] {db, clientId, password, relation, "unlink", ids};
        return (Boolean)objectXmlRpcClient.execute("execute", parameters);
    }

    public Object[] getFields(final String db,
                              final Integer clientId,
                              final String password,
                              final String relation)
        throws MalformedURLException, XmlRpcException {
        final Object[] parameters = new Object[] {db, clientId, password, relation, "fields_get", new Object[0]};
        final Map fieldMap = (Map)objectXmlRpcClient.execute("execute", parameters);
        return fieldMap.keySet().toArray();
    }

    public DateFormat getDateTimeFormat() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    public Date getDateTime(final String value)
        throws ParseException {
        return getDateTimeFormat().parse(value);
    }

    public String getDateTime(final Date value) {
        return getDateTimeFormat().format(value);
    }

    public DateFormat getDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd");
    }

    public Date getDate(final String value)
        throws ParseException {
        return getDateFormat().parse(value);
    }

    public String getDate(final Date value) {
        return getDateFormat().format(value);
    }

    public <T> T getValue(final Object value) {
        return Boolean.FALSE.equals(value) ? null : (T)value;
    }

    public static void main(String argv[])
        throws Exception {
        final OpenErpClient openErpClient = new OpenErpClient("localhost", 8069, null, -1);
        final Object[] databases = openErpClient.getDatabases();
        System.out.println("databases=" + Support.toString(databases));
        final String db = (String)databases[0];
        final Integer clientId = openErpClient.connect(db, "admin", "openerp");
        System.out.println("connect=" + clientId);
        final Object[] ids = openErpClient.search(db, clientId, "openerp", "res.partner",
                                                  new OpenErpClient.Triple[] {});
        System.out.println("search=" + Support.toString(ids));
    }

}
