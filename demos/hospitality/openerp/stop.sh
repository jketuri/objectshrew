#!/bin/bash
if test "${OPENERP_ENV+1}" != "1"
then
. env.sh
fi
kill `cat openerp-web.pid`
kill -HUP `cat openerp-server.pid`
kill `cat postgres.pid`
