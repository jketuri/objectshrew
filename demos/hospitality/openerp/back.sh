#!/bin/sh

check()
{
    RV="$?"
    if test $RV != "0"
	then
        echo "RV=$RV"
	    exit 1
    fi
}

mkdir -p $HOME/back/openerp
check
cp -frv * $HOME/back/openerp/
check
rm -f $HOME/openerp.zip
check
zip -rv $HOME/openerp * -x 'build/*' -x '*.jar' -x '*.war' -x '*/.svn/*'
check
cp -v $HOME/openerp.zip $HOME/openerp_s
check
bzip2 $HOME/openerp_s
check
mv -v $HOME/openerp_s.bz2 $HOME/openerp_s
check
ls -l $HOME/openerp.zip $HOME/openerp_s
check
