#!/bin/bash
if test "${OPENERP_ENV+1}" != "1"
then
. env.sh
export OPENERP_ENV=1
fi
postgres -D /usr/local/pgsql/data &
echo $! >postgres.pid
openerp-server &
echo $! >openerp-server.pid
openerp-web &
echo $! >openerp-web.pid
