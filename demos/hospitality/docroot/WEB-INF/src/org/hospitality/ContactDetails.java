/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hospitality;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import org.apache.xmlrpc.XmlRpcException;

import org.openerp.OpenErpClient;

import org.vaadin.common.Support;

public class ContactDetails implements Serializable {
    private final static long serialVersionUID = 0L;

    final HospitalityObjectView hospitalityObjectView;

    protected String message = null;
    protected String name = null;
    protected String street = null;
    protected String postCode = null;
    protected String city = null;
    protected Country country = null;
    protected Language language = null;
    protected java.sql.Date birthDate = null;
    protected String email = null;
    protected String fax = null;
    protected String phone = null;
    protected String mobile = null;
    protected String password = null;
    protected boolean credentialsVisible = false;
    protected boolean changed = false;
    protected boolean languageChanged = false;
    protected NavigationLink accommodationLink = new NavigationLink("Look for accommodation", "Hospitality.tabList.Accommodations");

    public ContactDetails(final HospitalityObjectView hospitalityObjectView) {
        this.hospitalityObjectView = hospitalityObjectView;
        country = hospitalityObjectView.defaultCountry;
        language = hospitalityObjectView.defaultLanguage;
    }

    protected boolean checkChange(final Object newValue,
                                  final Object value) {
        if (newValue != null ? !newValue.equals(value) : value != null) {
            changed = true;
            return true;
        }
        return false;
    }

    public String getTitle() {
        return "PatronContactDetails";
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        checkChange(name, this.name);
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(final String street) {
        checkChange(street, this.street);
        this.street = street;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(final String postCode) {
        checkChange(postCode, this.postCode);
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        checkChange(city, this.city);
        this.city = city;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(final Country country) {
        checkChange(country, this.country);
        this.country = country;
    }

    public Country[] valuesCountry() {
        return hospitalityObjectView.hospitalityContext.countries;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(final Language language) {
        languageChanged = checkChange(language, this.language);
        this.language = language;
    }

    public String[] editLanguage(final Language language)
        throws IllegalAccessException, InvocationTargetException {
        if (languageChanged) {
            if (language != null) {
                this.language = language;
                hospitalityObjectView.changeLanguage(language.languageCode);
                return new String[] {"Hospitality"};
            }
        }
        return null;
    }

    public Language[] valuesLanguage() {
        return hospitalityObjectView.hospitalityContext.languages;
    }

    public java.sql.Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(final java.sql.Date birthDate) {
        checkChange(birthDate, this.birthDate);
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        checkChange(email, this.email);
        this.email = email;
    }

    public String getOptionalFax() {
        return fax;
    }

    public void setOptionalFax(final String fax) {
        checkChange(fax, this.fax);
        this.fax = fax;
    }

    public String getOptionalPhone() {
        return phone;
    }

    public void setOptionalPhone(final String phone) {
        checkChange(phone, this.phone);
        this.phone = phone;
    }

    public String getOptionalMobile() {
        return mobile;
    }

    public void setOptionalMobile(final String mobile) {
        checkChange(mobile, this.mobile);
        this.mobile = mobile;
    }

    public String[] register()
        throws MalformedURLException, NoSuchAlgorithmException, UnsupportedEncodingException, XmlRpcException {
        if (hospitalityObjectView.partnerId != null) {
            return new String[] {"Hospitality.tabList.Accommodation"};
        }
        final Random random = new Random(System.currentTimeMillis() ^ Long.MAX_VALUE);
        final StringBuilder builder = new StringBuilder();
        for (int index = 0; index < 10; index++) {
            builder.append((char)('0' + random.nextInt('z' - '0' + 1)));
        }
        password = builder.toString();
        final String passwordDigest = hospitalityObjectView.getPasswordDigest(password);
        final Map<String, Object> partner = new HashMap<String, Object>();
        partner.put("name", name);
        partner.put("comment", passwordDigest);
        partner.put("website", language.languageCode);
        final Object partnerId
            = hospitalityObjectView.openErpClient.create(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw,
                                                    "res.partner", partner);
        final Map<String, Object> address = new HashMap<String, Object>();
        address.put("partner_id", partnerId);
        address.put("type", "default");
        address.put("name", name);
        address.put("street", street);
        address.put("zip", postCode);
        address.put("city", city);
        address.put("country_id", country.countryId);
        address.put("birthdate", hospitalityObjectView.openErpClient.getDate(birthDate));
        if (email != null) {
            address.put("email", email);
        }
        if (fax != null) {
            address.put("fax", fax);
        }
        if (phone != null) {
            address.put("phone", phone);
        }
        if (mobile != null) {
            address.put("mobile", mobile);
        }
        final Object addressId
            = hospitalityObjectView.openErpClient.create(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw,
                                                    "res.partner.address", address);
        hospitalityObjectView.partnerId = (Integer)partnerId;
        hospitalityObjectView.addressId = (Integer)addressId;
        credentialsVisible = true;
        message = "Contact details registered";
        changed = false;
        return new String[] {null, "Hospitality.tabList.ContactDetails", "Hospitality.tabList.ContactDetails", "Hospitality.tabList.Payment"};
    }

    public boolean invisibleRegister() {
        return hospitalityObjectView.partnerId != null;
    }

    public String[] update()
        throws MalformedURLException, NoSuchAlgorithmException, UnsupportedEncodingException, XmlRpcException {
        if (hospitalityObjectView.partnerId == null) {
            return null;
        }
        if (!changed) {
            message = "No changes made";
            return new String[] {null, "Hospitality.tabList.ContactDetails.textMessage"};
        }
        final Map<String, Object> partner = new HashMap<String, Object>();
        partner.put("name", name);
        partner.put("website", language.languageCode);
        hospitalityObjectView.openErpClient.update(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "res.partner",
                                              new Object[] {hospitalityObjectView.partnerId}, partner);
        final Map<String, Object> address = new HashMap<String, Object>();
        address.put("name", name);
        address.put("street", street);
        address.put("zip", postCode);
        address.put("city", city);
        address.put("country_id", country.countryId);
        address.put("birthdate", hospitalityObjectView.openErpClient.getDate(birthDate));
        if (email != null) {
            address.put("email", email);
        }
        if (fax != null) {
            address.put("fax", fax);
        }
        if (phone != null) {
            address.put("phone", phone);
        }
        if (mobile != null) {
            address.put("mobile", mobile);
        }
        hospitalityObjectView.openErpClient.update(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "res.partner.address",
                                              new Object[] {hospitalityObjectView.addressId}, address);
        message = "Contact details updated";
        changed = false;
        return new String[] {null, "Hospitality.tabList.ContactDetails.textMessage", "Hospitality.tabList.Payment"};
    }

    public boolean invisibleUpdate() {
        return hospitalityObjectView.partnerId == null;
    }

    public NavigationLink getLinkAccommodation() {
        return accommodationLink;
    }

    public String getTextMessage() {
        return message;
    }

    public boolean invisibleTextCredentials() {
        return !credentialsVisible;
    }

    public String getTextCredentials() {
        return "yourUserIdentifier";
    }

    public Object[] argumentsTextCredentials() {
        return new Object[] {hospitalityObjectView.partnerId, password};
    }

}
