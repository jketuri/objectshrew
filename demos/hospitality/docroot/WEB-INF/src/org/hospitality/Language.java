/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hospitality;

import java.io.Serializable;

public class Language implements Serializable {
    private final static long serialVersionUID = 0L;

    public final String languageCode;

    protected final String name;

    public Language(final String languageCode,
                    final String name) {
        this.languageCode = languageCode;
        this.name = name;
    }

    @Override
    public int hashCode() {
        return languageCode.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return object != null && languageCode.equals(((Language)object).languageCode);
    }

    @Override
    public String toString() {
        return name;
    }

}
