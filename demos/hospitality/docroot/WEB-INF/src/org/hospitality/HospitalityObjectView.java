/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hospitality;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.xmlrpc.XmlRpcException;

import org.openerp.OpenErpClient;

import org.vaadin.common.Support;
import org.vaadin.objectview.ObjectView;
import org.vaadin.objectview.ObjectViewEnumerations;

public class HospitalityObjectView extends ObjectView {
    private final static long serialVersionUID = 0L;

    static Object contextObjectLock = new Object();

    Map<Object, Country> countryMap = null;
    Map<String, Country> countryCodeMap = null;
    Map<Object, Language> languageMap = null;
    Country[] countries = null;
    Language[] languages = null;
    OpenErpClient openErpClient = null;
    String db = "openerp", pw = "hospitalityhospitality";
    Integer clientId = null;
    Integer partnerId = null;
    Integer addressId = null;
    ContactDetails contactDetails = null;
    Reservation reservation = null;
    Payment payment = null;
    Country defaultCountry = null;
    Language defaultLanguage = null;
    HospitalityContext hospitalityContext = null;

    public final HospitalityObjectView.AccommodationList accommodationList = new HospitalityObjectView.AccommodationList();

    public static class AccommodationList extends ArrayList<Accommodation> {
        private final static long serialVersionUID = 0L;
    }

    protected void changeLanguage(final String languageCode)
        throws IllegalAccessException, InvocationTargetException {
        int dash = languageCode.indexOf('_');
        setContextLocale(new Locale(languageCode.substring(0, dash), languageCode.substring(dash + 1)));
    }

    protected String getPasswordDigest(final String password)
        throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        messageDigest.update(password.getBytes("UTF-8"));
        return Support.toHexString(messageDigest.digest());
    }

    public Object getObject() {
        setTheme("stylecalendartheme");
        try {
            synchronized (HospitalityObjectView.contextObjectLock) {
                hospitalityContext = (HospitalityContext)getContextObject();
                if (hospitalityContext != null) {
                    countries = hospitalityContext.countries;
                    countryMap = hospitalityContext.countryMap;
                    countryCodeMap = hospitalityContext.countryCodeMap;
                    languages = hospitalityContext.languages;
                    languageMap = hospitalityContext.languageMap;
                    openErpClient = hospitalityContext.openErpClient;
                } else {
                    languages = new Language[] {new Language("fi_FI", "Finnish"), new Language("en_US", "English")};
                    languageMap = new HashMap<Object, Language>();
                    for (final Language language : languages) {
                        languageMap.put(language.languageCode, language);
                    }
                    openErpClient = new OpenErpClient("localhost", 8069, null, -1);
                    clientId = openErpClient.connect(db, "admin", pw);
                    final Object[] countryIds
                        = openErpClient.search(db, clientId, pw, "res.country", new OpenErpClient.Triple[] {}),
                        countryObjects = openErpClient.read(db, clientId, pw, "res.country", null, countryIds);
                    int countryIndex = 0;
                    List<Country> countryList = new ArrayList<Country>();
                    countryMap = new HashMap<Object, Country>();
                    countryCodeMap = new HashMap<String, Country>();
                    for (final Object countryObject : countryObjects) {
                        final Map<String, Object> countryEntity = (Map<String, Object>)countryObject;
                        final Object countryId = countryIds[countryIndex++];
                        final String countryCode = (String)countryEntity.get("code");
                        final Country country = new Country(countryId, countryCode,
                                                            countryCode + " " + countryEntity.get("name"));
                        countryList.add(country);
                        countryMap.put(countryId, country);
                        countryCodeMap.put(countryCode, country);
                    }
                    countries = countryList.toArray(new Country[countryList.size()]);
                    hospitalityContext = new HospitalityContext();
                    hospitalityContext.countries = countries;
                    hospitalityContext.countryMap = countryMap;
                    hospitalityContext.countryCodeMap = countryCodeMap;
                    hospitalityContext.languages = languages;
                    hospitalityContext.languageMap = languageMap;
                    hospitalityContext.openErpClient = openErpClient;
                    setContextObject(hospitalityContext);
                }
            }
            if (clientId == null) {
                clientId = openErpClient.connect(db, "admin", pw);
            }
            final Locale locale = getLocale();
            if (locale != null) {
                defaultCountry = countryCodeMap.get(locale.getCountry());
                defaultLanguage = languageMap.get(locale.getLanguage() + "_" + locale.getCountry());
            }
            final Object[] roomIds
                = openErpClient.search(db, clientId, pw, "hotel.room", new OpenErpClient.Triple[] {});
            final ResourceBundle resourceBundle
                = ResourceBundle.getBundle("org.hospitality.resources.Hospitality");
            final Object[] roomObjects = openErpClient.read(db, clientId, pw, "hotel.room", null, roomIds);
            for (final Object roomObject : roomObjects) {
                final Map<String, Object> room = (Map<String, Object>)roomObject;
                final String name = (String)room.get("name"), descriptionName = "description_" + name;
                try {
                    resourceBundle.getString(descriptionName);
                } catch (MissingResourceException missingResourceException) {
                    continue;
                }
                final Object[] categoryId = (Object[])room.get("categ_id");
                final BigDecimal price = BigDecimal.valueOf((Double)room.get("list_price"));
                accommodationList.add(new Accommodation(this, (Integer)categoryId[0], name, descriptionName, price));
            }
            return new Hospitality(this);
        } catch (MalformedURLException malformedURLException) {
            throw new RuntimeException(malformedURLException);
        } catch (XmlRpcException xmlRpcException) {
            throw new RuntimeException(xmlRpcException);
        }
    }

}
