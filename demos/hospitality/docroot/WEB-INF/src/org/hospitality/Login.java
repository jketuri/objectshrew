/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hospitality;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import org.openerp.OpenErpClient;

public class Login implements Serializable {
    private final static long serialVersionUID = 0L;

    final HospitalityObjectView hospitalityObjectView;

    protected String userID = null, password = null;

    public Login(final HospitalityObjectView hospitalityObjectView) {
        this.hospitalityObjectView = hospitalityObjectView;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(final String userID) {
        this.userID = userID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String[] login()
        throws IllegalAccessException, InvocationTargetException, MalformedURLException, NoSuchAlgorithmException,
               ParseException, UnsupportedEncodingException, XmlRpcException {
        final List<OpenErpClient.Triple> tripleList = new ArrayList<OpenErpClient.Triple>();
        tripleList.add(new OpenErpClient.Triple("id", "=", userID));
        final Object[] partnerIds
            = hospitalityObjectView.openErpClient.search(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "res.partner",
                                                    tripleList.toArray(new OpenErpClient.Triple[tripleList.size()]));
        if (partnerIds == null || partnerIds.length == 0) {
            throw new RuntimeException("Login failed");
        }
        final Object[] partnerObjects
            = hospitalityObjectView.openErpClient.read(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "res.partner",
                                                  null, partnerIds);
        final Map<String, Object> partner = (Map<String, Object>)partnerObjects[0];
        final String passwordDigest = hospitalityObjectView.getPasswordDigest(password),
            dbPasswordDigest = (String)partner.get("comment");
        if (!passwordDigest.equals(dbPasswordDigest)) {
            throw new RuntimeException("Login failed");
        }
        final String lang = hospitalityObjectView.openErpClient.getValue(partner.get("website"));
        if (lang != null) {
            hospitalityObjectView.changeLanguage(lang);
        }
        tripleList.clear();
        tripleList.add(new OpenErpClient.Triple("partner_id", "=", partnerIds[0]));
        final Object[] addressIds
            = hospitalityObjectView.openErpClient.search(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "res.partner.address",
                                                    tripleList.toArray(new OpenErpClient.Triple[tripleList.size()]));
        final Object[] addressObjects
            = hospitalityObjectView.openErpClient.read(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "res.partner.address",
                                                  null, addressIds);
        final Map<String, Object> address = (Map<String, Object>)addressObjects[0];
        hospitalityObjectView.contactDetails.setName((String)address.get("name"));
        hospitalityObjectView.contactDetails.setStreet((String)address.get("street"));
        hospitalityObjectView.contactDetails.setPostCode((String)address.get("zip"));
        hospitalityObjectView.contactDetails.setCity((String)address.get("city"));
        final Object[] countryId = (Object[])address.get("country_id");
        final Country country = hospitalityObjectView.countryMap.get(countryId[0]);
        hospitalityObjectView.contactDetails.setCountry(country);
        final Language language = hospitalityObjectView.languageMap.get(lang);
        hospitalityObjectView.contactDetails.setLanguage(language);
        hospitalityObjectView.contactDetails.setBirthDate(new java.sql.Date(hospitalityObjectView.openErpClient.getDate((String)address.get("birthdate")).getTime()));
        hospitalityObjectView.contactDetails.setEmail((String)hospitalityObjectView.openErpClient.getValue(address.get("email")));
        hospitalityObjectView.contactDetails.setOptionalFax((String)hospitalityObjectView.openErpClient.getValue(address.get("fax")));
        hospitalityObjectView.contactDetails.setOptionalPhone((String)hospitalityObjectView.openErpClient.getValue(address.get("phone")));
        hospitalityObjectView.contactDetails.setOptionalMobile((String)hospitalityObjectView.openErpClient.getValue(address.get("mobile")));
        hospitalityObjectView.contactDetails.changed = false;
        hospitalityObjectView.partnerId = (Integer)partnerIds[0];
        hospitalityObjectView.addressId = (Integer)addressIds[0];
        return new String[] {"Hospitality.tabList.ContactDetails", "Hospitality"};
    }

}
