/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hospitality;

import java.io.Serializable;

public class Country implements Serializable {
    private final static long serialVersionUID = 0L;

    public final Object countryId;

    protected final String code;
    protected final String name;

    public Country(final Object countryId,
                   final String code,
                   final String name) {
        this.countryId = countryId;
        this.code = code;
        this.name = name;
    }

    @Override
    public int hashCode() {
        return countryId.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return object != null && countryId.equals(((Country)object).countryId);
    }

    @Override
    public String toString() {
        return name;
    }

}
