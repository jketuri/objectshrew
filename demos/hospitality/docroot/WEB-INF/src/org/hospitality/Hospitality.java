/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hospitality;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class Hospitality implements Serializable {
    private final static long serialVersionUID = 0L;

    final List<Object> tabList = new ArrayList<Object>();

    public Hospitality(final HospitalityObjectView hospitalityObjectView)
        throws MalformedURLException {
        hospitalityObjectView.contactDetails = new ContactDetails(hospitalityObjectView);
        hospitalityObjectView.reservation = new Reservation(hospitalityObjectView);
        hospitalityObjectView.payment = new Payment(hospitalityObjectView);
        tabList.add(new Welcome(hospitalityObjectView));
        tabList.add(hospitalityObjectView.contactDetails);
        tabList.add(new Accommodations(hospitalityObjectView));
        tabList.add(hospitalityObjectView.reservation);
        tabList.add(hospitalityObjectView.payment);
        tabList.add(new Links());
        tabList.add(new Feedback());
    }

    public List<Object> getTabList() {
        return tabList;
    }

}
