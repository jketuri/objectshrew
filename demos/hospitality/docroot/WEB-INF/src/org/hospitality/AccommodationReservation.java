/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hospitality;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccommodationReservation implements Serializable {
    private final static long serialVersionUID = 0L;

    protected final Accommodation accommodation;
    protected final long number;
    protected final BigDecimal totalPrice;

    public AccommodationReservation(final Accommodation accommodation,
                                    final long number,
                                    final BigDecimal totalPrice) {
        this.accommodation = accommodation;
        this.number = number;
        this.totalPrice = totalPrice;
    }

    public String getName() {
        return accommodation.getLink().getName();
    }

    public BigDecimal getPrice() {
        return accommodation.getTextPrice();
    }

    public long getNumber() {
        return number;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

}
