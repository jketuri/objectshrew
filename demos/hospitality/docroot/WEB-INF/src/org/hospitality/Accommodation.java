/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hospitality;

import java.io.Serializable;
import java.math.BigDecimal;

public class Accommodation implements Serializable {
    private final static long serialVersionUID = 0L;

    public final Integer categoryId;

    protected final String description;
    protected final BigDecimal price;
    protected final AccommodationLink link;

    public Accommodation(final HospitalityObjectView hospitalityObjectView,
                         final Integer categoryId,
                         final String name,
                         final String description,
                         final BigDecimal price) {
        this.categoryId = categoryId;
        this.description = description;
        this.price = price;
        link = new AccommodationLink(hospitalityObjectView, this, name);
    }

    public AccommodationLink getLink() {
        return link;
    }

    public String getTextDescription() {
        return description;
    }

    public BigDecimal getTextPrice() {
        return price;
    }

    public String toString() {
        return link.getName();
    }

}
