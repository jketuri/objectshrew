/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hospitality;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.vaadin.ui.Window;

import org.apache.xmlrpc.XmlRpcException;

import org.openerp.OpenErpClient;

import org.vaadin.common.Support;

import org.netpayment.NetPayment;
import org.netpayment.NetPaymentResponse;
import org.netpayment.smt.SmtNetPayment;

import org.vaadin.netpayment.NetPaymentResource;

public class Payment implements Serializable {
    private final static long serialVersionUID = 0L;

    final HospitalityObjectView hospitalityObjectView;
    final NetPaymentResource netPaymentResource;
    final Payment.AccommodationReservationList accommodationReservationList = new Payment.AccommodationReservationList();
    final PaymentResult paymentResult = new PaymentResult();
    final NetPayment netPayment = new SmtNetPayment();

    protected String message = null;
    protected boolean payDisabled = false;
    protected boolean messageVisible = false;
    protected Object[] reservationIds = null;

    public static class AccommodationReservationList extends ArrayList<AccommodationReservation> {
        private final static long serialVersionUID = 0L;
    }

    public Payment(final HospitalityObjectView hospitalityObjectView)
        throws MalformedURLException {
        this.hospitalityObjectView = hospitalityObjectView;
        netPaymentResource = new NetPaymentResource(netPayment, hospitalityObjectView) {
                private final static long serialVersionUID = 0L;

                @Override
                public void handle() {
                    final NetPaymentResponse netPaymentResponse = netPaymentResource.getNetPaymentResponse();
                    if (netPaymentResponse != null) {
                        switch (netPaymentResponse.getResult()) {
                        case OK:
                            try {
                                confirm();
                            } catch (MalformedURLException malformedURLException) {
                                throw new RuntimeException(malformedURLException);
                            } catch (XmlRpcException xmlRpcException) {
                                throw new RuntimeException(xmlRpcException);
                            }
                            message = "Payment made";
                            paymentResult.reference = netPaymentResponse.getReference();
                            paymentResult.amount = netPaymentResponse.getAmount();
                            paymentResult.currency = netPaymentResponse.getCurrency();
                            paymentResult.visible = true;
                            payDisabled = true;
                            break;
                        case CANCEL:
                            message = "Payment canceled";
                            break;
                        case DELAYED:
                            message = "Payment delayed";
                            payDisabled = true;
                            break;
                        default:
                            message = "Payment had error";
                        }
                        messageVisible = true;
                    }
                }

            };
    }

    private void confirm()
        throws MalformedURLException, XmlRpcException {
        final Map<String, Object> values = new HashMap<String, Object>();
        values.put("state", "confirm");
        hospitalityObjectView.openErpClient.update(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel.reservation", reservationIds, values);
    }

    public boolean disabled() {
        return hospitalityObjectView.partnerId == null;
    }

    public String[] start()
        throws MalformedURLException, ParseException, XmlRpcException {
        if (hospitalityObjectView.partnerId == null) {
            return null;
        }
        reservationIds
            = hospitalityObjectView.openErpClient.search(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel.reservation", new OpenErpClient.Triple[] {
                    new OpenErpClient.Triple("partner_id", "=", hospitalityObjectView.partnerId),
                    new OpenErpClient.Triple("state", "=", "draft")});
        final Object[] reservationObjects
            = hospitalityObjectView.openErpClient.read(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel.reservation", null, reservationIds);
        accommodationReservationList.clear();
        Object firstReservationId = null;
        BigDecimal totalSum = BigDecimal.ZERO;
        int orderRow = 0;
        Date firstDate = null, lastDate = null;
        for (final Accommodation accommodation : hospitalityObjectView.accommodationList) {
            int index = 0;
            int number = 0;
            for (final Object reservationId : reservationIds) {
                if (firstReservationId == null) firstReservationId = reservationId;
                final Object[] reservationLineIds
                    = hospitalityObjectView.openErpClient.search(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel_reservation.line", new OpenErpClient.Triple[] {
                            new OpenErpClient.Triple("categ_id", "=", accommodation.categoryId),
                            new OpenErpClient.Triple("line_id", "=", reservationId)});
                if (reservationLineIds.length > 0) {
                    final Map<String, Object> reservation = (Map<String, Object>)reservationObjects[index];
                    final String checkin = (String)reservation.get("checkin"),
                        checkout = (String)reservation.get("checkout");
                    final Date checkinDate = hospitalityObjectView.openErpClient.getDateTime(checkin),
                        checkoutDate = hospitalityObjectView.openErpClient.getDateTime(checkout);
                    if (firstDate == null || checkinDate.compareTo(firstDate) < 0) {
                        firstDate = checkinDate;
                    }
                    if (lastDate == null || checkoutDate.compareTo(lastDate) > 0) {
                        lastDate = checkoutDate;
                    }
                    number += (checkoutDate.getTime() - checkinDate.getTime()) / (24L * 60L * 60L * 1000L) + 1;
                }
                index++;
            }
            if (number > 0) {
                final BigDecimal totalPrice = accommodation.getTextPrice().multiply(BigDecimal.valueOf(number));
                accommodationReservationList.add(new AccommodationReservation(accommodation, number, totalPrice));
                totalSum = totalSum.add(totalPrice);
                netPayment.setOrderRow(++orderRow, accommodation.toString(), accommodation.toString() + " " + number,
                                       number, firstDate, accommodation.getTextPrice(), null,
                                       new BigDecimal(BigInteger.ZERO, 2), new BigDecimal(BigInteger.ZERO, 2));
            }
        }
        netPayment.setOrderRows(orderRow);
        netPayment.setPaymentId(String.valueOf(hospitalityObjectView.clientId));
        netPayment.setOrderId(String.valueOf(firstReservationId));
        netPayment.setReference("100");
        netPayment.setDueDate(lastDate);
        netPayment.setAmount(totalSum);
        netPayment.setBuyerName(hospitalityObjectView.contactDetails.getName());
        netPayment.setBuyerAddress(hospitalityObjectView.contactDetails.getStreet());
        netPayment.setBuyerPostalCode(hospitalityObjectView.contactDetails.getPostCode());
        netPayment.setBuyerCity(hospitalityObjectView.contactDetails.getCity());
        netPayment.setBuyerCountry(hospitalityObjectView.contactDetails.getCountry().code);
        if (hospitalityObjectView.contactDetails.getOptionalMobile() != null
            && !"".equals(hospitalityObjectView.contactDetails.getOptionalMobile().trim()))
            netPayment.setBuyerPhone(hospitalityObjectView.contactDetails.getOptionalMobile());
        else netPayment.setBuyerPhone(hospitalityObjectView.contactDetails.getOptionalPhone());
        netPayment.setBuyerEmail(hospitalityObjectView.contactDetails.getEmail());
        netPayment.setDeliveryName(hospitalityObjectView.contactDetails.getName());
        netPayment.setDeliveryAddress(hospitalityObjectView.contactDetails.getStreet());
        netPayment.setDeliveryPostalCode(hospitalityObjectView.contactDetails.getPostCode());
        netPayment.setDeliveryCity(hospitalityObjectView.contactDetails.getCity());
        netPayment.setDeliveryCountry((String)hospitalityObjectView.contactDetails.getCountry().code);
        payDisabled = accommodationReservationList.isEmpty();
        return new String[] {"Hospitality.tabList.Payment.accommodationReservationList",
                             "Hospitality.tabList.Payment.pay"};
    }

    public String getTitle() {
        return "Payment";
    }

    public Payment.AccommodationReservationList getAccommodationReservationList() {
        return accommodationReservationList;
    }

    public String[] pay()
        throws IOException, NoSuchAlgorithmException, URISyntaxException {
        netPaymentResource.proceed();
        return new String[] {null, "Hospitality.tabList.Payment.textMessage", "Hospitality.tabList.Payment.paymentResult"};
    }

    public boolean disabledPay() {
        return payDisabled;
    }

    public String iconPay() {
        return "https://www.maksuturva.fi/public_img/maksuturva_button_normal.gif";
    }

    public String getTextMessage() {
        return message;
    }

    public boolean invisibleTextMessage() {
        return !messageVisible;
    }

    public PaymentResult getPaymentResult() {
        return paymentResult;
    }

}
