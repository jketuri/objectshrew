/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hospitality;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.xmlrpc.XmlRpcException;

import org.openerp.OpenErpClient;

import org.vaadin.common.Support;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import org.vaadin.risto.stylecalendar.DateOptionsGenerator;
import org.vaadin.risto.stylecalendar.StyleCalendar;

public class Reservation implements Serializable {
    private final static long serialVersionUID = 0L;

    final HospitalityObjectView hospitalityObjectView;

    protected final TreeSet<Date> disabledSet = new TreeSet<Date>();
    protected final TreeSet<Date> yellowSet = new TreeSet<Date>();
    protected final TreeSet<Date> greenSet = new TreeSet<Date>();
    protected final TreeSet<Date> redSet = new TreeSet<Date>();
    protected final StyleCalendar styleCalendar = new StyleCalendar() {
            private final static long serialVersionUID = 0L;

            public void setShowingDate(Date monthToShow) {
                super.setShowingDate(monthToShow);
                fireValueChange(false);
            }

        };
    protected final Object[] accommodationValues;
    protected final Reservation.AccommodationSet accommodationSet = new Reservation.AccommodationSet();
    protected final Date currentDate = truncateDate(new Date());

    protected String message = null;

    Accommodation accommodation = null;

    public static class AccommodationSet extends HashSet<Accommodation> {
        private final static long serialVersionUID = 0L;
    }

    private Date truncateDate(final Date date) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public Reservation(final HospitalityObjectView hospitalityObjectView) {
        this.hospitalityObjectView = hospitalityObjectView;
        styleCalendar.setImmediate(true);
        styleCalendar.addListener(new Property.ValueChangeListener() {
                private final static long serialVersionUID = 0L;

                @Override
                public void valueChange(ValueChangeEvent event) {
                    final Date date = truncateDate((Date)event.getProperty().getValue());
                    if (date.compareTo(currentDate) <= 0 || disabledSet.contains(date)) {
                        return;
                    }
                    if (yellowSet.contains(date)) {
                        yellowSet.remove(date);
                    } else {
                        if (!redSet.contains(date) && !greenSet.contains(date)) {
                            yellowSet.add(date);
                        }
                    }
                    styleCalendar.requestRepaint();
                }

            });
        styleCalendar.setDateOptionsGenerator(new DateOptionsGenerator() {
                private final static long serialVersionUID = 0L;

                @Override
                public String getStyleName(Date date,
                                           final StyleCalendar context) {
                    date = truncateDate(date);
                    if (redSet.contains(date)) {
                        return "red";
                    }
                    if (yellowSet.contains(date)) {
                        return "yellow";
                    }
                    if (greenSet.contains(date)) {
                        return "green";
                    }
                    return null;
                }

                @Override
                public String getTooltip(final Date date,
                                         final StyleCalendar context) {
                    return null;
                }

                @Override
                public boolean isDateDisabled(Date date,
                                              final StyleCalendar context) {
                    date = truncateDate(date);
                    return date.compareTo(currentDate) <= 0 || disabledSet.contains(date);
                }

            });
        accommodationValues = hospitalityObjectView.accommodationList.toArray();
    }

    public String[] start()
        throws MalformedURLException, ParseException, XmlRpcException {
        redSet.clear();
        yellowSet.clear();
        greenSet.clear();
        if (!accommodationSet.isEmpty()) {
            final List<OpenErpClient.Triple> tripleList = new ArrayList<OpenErpClient.Triple>();
            final int accommodationNumber = accommodationSet.size() - 1;
            int accommodationIndex = 0;
            for (final Accommodation accommodation : accommodationSet) {
                if (accommodationIndex < accommodationNumber) {
                    tripleList.add(new OpenErpClient.Triple("|"));
                }
                accommodationIndex++;
                tripleList.add(new OpenErpClient.Triple("categ_id", "=", accommodation.categoryId));
            }
            final Object[] reservationLineIds
                = hospitalityObjectView.openErpClient.search(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel_reservation.line", tripleList.toArray(new OpenErpClient.Triple[tripleList.size()]));
            if (reservationLineIds.length > 0) {
                final String currentDateString = hospitalityObjectView.openErpClient.getDate(currentDate);
                tripleList.clear();
                tripleList.add(new OpenErpClient.Triple("checkout", ">", currentDateString));
                final int reservationLineIdNumber = reservationLineIds.length - 1;
                int reservationLineIdIndex = 0;
                for (final Object reservationLineId : reservationLineIds) {
                    if (reservationLineIdIndex < reservationLineIdNumber) {
                        tripleList.add(new OpenErpClient.Triple("|"));
                    }
                    reservationLineIdIndex++;
                    tripleList.add(new OpenErpClient.Triple("reservation_line", "=", reservationLineId));
                }
                final Object[] reservationIds
                    = hospitalityObjectView.openErpClient.search(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel.reservation", tripleList.toArray(new OpenErpClient.Triple[tripleList.size()])), reservationObjects
                    = hospitalityObjectView.openErpClient.read(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel.reservation", null, reservationIds);
                for (final Object reservationObject : reservationObjects) {
                    final Map<String, Object> reservation = (Map<String, Object>)reservationObject;
                    final Date checkin = hospitalityObjectView.openErpClient.getDateTime((String)reservation.get("checkin")),
                        checkout = hospitalityObjectView.openErpClient.getDateTime((String)reservation.get("checkout"));
                    final Calendar calendar = Calendar.getInstance();
                    if (currentDate.compareTo(checkin) > 0) {
                        calendar.setTime(currentDate);
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                    } else {
                        calendar.setTime(checkin);
                    }
                    final Integer partnerId = (Integer)((Object[])reservation.get("partner_id"))[0];
                    final String state = (String)reservation.get("state");
                    while (calendar.getTime().compareTo(checkout) <= 0) {
                        final Date date = truncateDate(calendar.getTime());
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        if (hospitalityObjectView.partnerId != null) {
                            if (hospitalityObjectView.partnerId.equals(partnerId)) {
                                if ("confirm".equals(state)) {
                                    greenSet.add(date);
                                    yellowSet.remove(date);
                                    redSet.remove(date);
                                } else {
                                    yellowSet.add(date);
                                    redSet.remove(date);
                                    greenSet.remove(date);
                                }
                                continue;
                            }
                        }
                        redSet.add(date);
                        yellowSet.remove(date);
                        greenSet.remove(date);
                    }
                }
            }
        }
        return new String[] {"Hospitality.tabList.Reservation.groupAccommodationSet", "Hospitality.tabList.Reservation.styleCalendar", "Hospitality.tabList.Reservation.book"};
    }

    public String getTitle() {
        return "BookAccommodation";
    }

    public String getTextInstructions() {
        return "BookingTool";
    }

    public Reservation.AccommodationSet getGroupAccommodationSet() {
        return accommodationSet;
    }

    public void setGroupAccommodationSet(final Set<Accommodation> accommodationSet) {
        this.accommodationSet.clear();
        if (accommodation != null) {
            this.accommodationSet.add(accommodation);
            accommodation = null;
        }
        this.accommodationSet.addAll(accommodationSet);
    }

    public String[] editGroupAccommodationSet(final Set<Accommodation> accommodationSet)
        throws MalformedURLException, ParseException, XmlRpcException {
        setGroupAccommodationSet(accommodationSet);
        return start();
    }

    public Object[] valuesGroupAccommodationSet() {
        return accommodationValues;
    }

    public StyleCalendar getStyleCalendar() {
        return styleCalendar;
    }

    public boolean disabledBook() {
        return hospitalityObjectView.partnerId == null;
    }

    public String[] book()
        throws MalformedURLException, ParseException, XmlRpcException {
        if (accommodationSet.isEmpty() || yellowSet.isEmpty() || hospitalityObjectView.partnerId == null) {
            message = "selectFirst";
            return new String[] {null, "Hospitality.tabList.Reservation.textMessage"};
        }
        clear();
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(yellowSet.first());
        for (;;) {
            final Date checkin = calendar.getTime();
            Date checkout = null;
            for (;;) {
                checkout = calendar.getTime();
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                if (!yellowSet.contains(calendar.getTime())) {
                    break;
                }
            }
            final Map<String, Object> reservation = new HashMap<String, Object>();
            reservation.put("partner_id", hospitalityObjectView.partnerId);
            reservation.put("shop_id", 1);
            reservation.put("pricelist_id", 1);
            reservation.put("partner_shipping_id", hospitalityObjectView.addressId);
            reservation.put("partner_invoice_id", hospitalityObjectView.addressId);
            reservation.put("partner_order_id", hospitalityObjectView.addressId);
            reservation.put("checkin", hospitalityObjectView.openErpClient.getDateTime(checkin));
            reservation.put("checkout", hospitalityObjectView.openErpClient.getDateTime(checkout));
            reservation.put("adults", 0);
            reservation.put("childs", 0);
            reservation.put("state", "draft");
            final Object reservationId
                = hospitalityObjectView.openErpClient.create(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel.reservation", reservation);
            final List<Integer> reservationLineList = new ArrayList<Integer>();
            for (final Accommodation accommodation : accommodationSet) {
                final Map<String, Object> reservationLine = new HashMap<String, Object>();
                reservationLine.put("categ_id", accommodation.categoryId);
                reservationLine.put("line_id", reservationId);
                hospitalityObjectView.openErpClient.create(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel_reservation.line", reservationLine);
            }
            final Date higher = yellowSet.higher(checkout);
            if (higher == null) {
                break;
            }
            calendar.setTime(higher);
        }
        message = null;
        return new String[] {"Hospitality.tabList.Payment", "Hospitality.tabList.Reservation.textMessage"};
    }

    public String[] cancel()
        throws MalformedURLException, ParseException, XmlRpcException {
        clear();
        start();
        message = null;
        return new String[] {null, "Hospitality.tabList.Reservation.textMessage", "Hospitality.tabList.Reservation.groupAccommodationSet", "Hospitality.tabList.Reservation.styleCalendar"};
    }

    public String getTextMessage() {
        return message;
    }

    protected void clear()
        throws MalformedURLException, ParseException, XmlRpcException {
        if (!accommodationSet.isEmpty()) {
            final Object[] reservationIds
                = hospitalityObjectView.openErpClient.search(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel.reservation", new OpenErpClient.Triple[] {
                        new OpenErpClient.Triple("partner_id", "=", hospitalityObjectView.partnerId),
                        new OpenErpClient.Triple("state", "=", "draft")});
            final List<Integer> reservationIdList = new ArrayList<Integer>(), reservationLineIdList = new ArrayList<Integer>();
            for (final Accommodation accommodation : accommodationSet) {
                for (final Object reservationId : reservationIds) {
                    final Object[] reservationLineIds
                        = hospitalityObjectView.openErpClient.search(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel_reservation.line", new OpenErpClient.Triple[] {
                                new OpenErpClient.Triple("categ_id", "=", accommodation.categoryId),
                                new OpenErpClient.Triple("line_id", "=", reservationId)});
                    if (reservationLineIds.length > 0) {
                        reservationIdList.add((Integer)reservationId);
                    }
                    for (Object reservationLineId : reservationLineIds) {
                        reservationLineIdList.add((Integer)reservationLineId);
                    }
                }
            }
            hospitalityObjectView.openErpClient.delete(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel.reservation", reservationIdList.toArray(new Integer[reservationIdList.size()]));
            hospitalityObjectView.openErpClient.delete(hospitalityObjectView.db, hospitalityObjectView.clientId, hospitalityObjectView.pw, "hotel_reservation.line", reservationLineIdList.toArray(new Integer[reservationLineIdList.size()]));
        }
    }

}
