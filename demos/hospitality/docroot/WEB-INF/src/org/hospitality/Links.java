/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hospitality;

import java.io.Serializable;
import java.util.ArrayList;

public class Links implements Serializable {
    private final static long serialVersionUID = 0L;

    protected Links.LinkList linkList = new Links.LinkList();
    {
        for (int index = 1; index <= 3; index++) {
            linkList.add(new Link("link" + index, "about:blank", "_blank"));
        }
    }

    static class LinkList extends ArrayList<Link> {
        private final static long serialVersionUID = 0L;
    }

    public String getTitle() {
        return "Links";
    }

    public Links.LinkList getLinkList() {
        return linkList;
    }

}
