/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hospitality;

import java.io.Serializable;

public class Welcome implements Serializable {
    private final static long serialVersionUID = 0L;

    final HospitalityObjectView hospitalityObjectView;

    protected final Login login;
    protected final NavigationLink registerLink = new NavigationLink("Register for reservations", "Hospitality.tabList.ContactDetails");
    protected final NavigationLink reservationsLink = new NavigationLink("View reservations", "Hospitality.tabList.Reservation");

    public Welcome(final HospitalityObjectView hospitalityObjectView) {
        this.hospitalityObjectView = hospitalityObjectView;
        login = new Login(hospitalityObjectView);
    }

    public String[] start() {
        return new String[] {"Hospitality.tabList.Payment"};
    }

    public String getTitle() {
        return "Hospitality";
    }

    public String getTextOverview() {
        return "overview";
    }

    public Login getLogin() {
        return login;
    }

    public NavigationLink getLinkRegister() {
        return registerLink;
    }

    public boolean disabledLinkRegister() {
        return hospitalityObjectView.partnerId != null;
    }

    public NavigationLink getLinkReservations() {
        return reservationsLink;
    }

}
