/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vaadin.netpayment;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.vaadin.server.ConnectorResourceHandler;
import com.vaadin.server.DownloadStream;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

import org.netpayment.NetPayment;
import org.netpayment.NetPaymentResponse;

public class NetPaymentResource extends ConnectorResourceHandler {
    private final static long serialVersionUID = 0L;

    private final NetPayment netPayment;
    private final UI ui;
    private final URL url;

    private boolean streamHandled = false;

    private NetPaymentResponse netPaymentResponse = null;

    public NetPaymentResource(final NetPayment netPayment,
                              final UI ui)
        throws MalformedURLException {
        this.netPayment = netPayment;
        this.ui = ui;
        this.url = new URL(new URL(this.ui.getPage().getUriFragment()), "netpayment/");
        netPayment.setOkReturn(new URL(this.url, "ok"));
        netPayment.setErrorReturn(new URL(this.url, "error"));
        netPayment.setCancelReturn(new URL(this.url, "cancel"));
        netPayment.setDelayedPayReturn(new URL(this.url, "delayedPay"));
        VaadinSession session = VaadinSession.getCurrent();
        session.addRequestHandler(this);
    }

    public void proceed()
        throws IOException, NoSuchAlgorithmException, URISyntaxException {
        this.streamHandled = false;
        this.netPaymentResponse = netPayment.proceed();
        this.ui.getPage().open(this.url.toString(), "_self");
    }

    public NetPaymentResponse getNetPaymentResponse() {
        return this.netPaymentResponse;
    }

    public boolean handleRequest(final VaadinSession session,
                                 final VaadinRequest request,
                                 final VaadinResponse response)
        throws IOException {
        if (this.netPaymentResponse != null) {
            final Map<String, String[]> parameterMap = request.getParameterMap();
            final Iterator<Map.Entry<String, String[]>> entries = parameterMap.entrySet().iterator();
            while (entries.hasNext()) {
                final Map.Entry<String, String[]> entry = entries.next();
                final String[] value = entry.getValue();
                this.netPaymentResponse.setVariable(entry.getKey(), value != null ? value[0] : null);
            }
        }
        final String relativeUri = request.getPathInfo();
        if (!relativeUri.startsWith("netpayment")) {
            return false;
        }
        netPaymentResponse.setResult(null);
        URL url = null;
        try {
            url = new URL(((HttpServletRequest)request).getRequestURI());
        } catch (MalformedURLException malformedURLException) {
            throw new RuntimeException(malformedURLException);
        }
        final String urlString = url.toString();
        if (urlString.endsWith("/ok")) {
            netPaymentResponse.setResult(NetPaymentResponse.Result.OK);
        } else if (urlString.endsWith("/error")) {
            netPaymentResponse.setResult(NetPaymentResponse.Result.ERROR);
        } else if (urlString.endsWith("/cancel")) {
            netPaymentResponse.setResult(NetPaymentResponse.Result.CANCEL);
        } else if (urlString.endsWith("/delayedPay")) {
            netPaymentResponse.setResult(NetPaymentResponse.Result.DELAYED);
        }
        if (streamHandled) {
            handle();
            this.ui.markAsDirtyRecursive();
            return true;
        }
        streamHandled = true;
        final DownloadStream stream
            = new DownloadStream(netPayment.getInputStream(), netPayment.getContentType(), null);
        stream.writeResponse(request, response);
        return true;
    }

    public void handle() {
    }

}
