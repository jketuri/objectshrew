/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netpayment;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;

public abstract class NetPaymentBase<RESPONSE_TYPE extends NetPaymentResponseBase> extends EntryList {
    private InputStream inputStream = null;
    private HttpClient httpClient = null;
    private String contentType = null;
    private boolean useProxy = false;

    protected URL url = null;

    public URL getURL() {
        return url;
    }

    protected abstract void prepare()
        throws NoSuchAlgorithmException, UnsupportedEncodingException;

    protected abstract RESPONSE_TYPE makeNetPaymentResponse();

    public RESPONSE_TYPE proceed()
        throws IOException, NoSuchAlgorithmException, URISyntaxException {
        prepare();
        List<NameValuePair> parameters = new ArrayList<NameValuePair>();
        for (final Map.Entry<String, String> entry : this) {
            if (entry.getValue() != null) {
                parameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
        }
        final HttpPost httpPost = new HttpPost(getURL().toExternalForm());
        httpPost.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
        final BasicHttpParams basicHttpParams = new BasicHttpParams();
        basicHttpParams.setParameter(CoreConnectionPNames.TCP_NODELAY, Boolean.FALSE);
        final HttpClient httpClient = new DefaultHttpClient(basicHttpParams);
        if (useProxy) {
            final HttpHost httpHost = new HttpHost("localhost", 8080);
            httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, httpHost);
        }
        final HttpResponse httpResponse = httpClient.execute(httpPost);
        final StatusLine statusLine = httpResponse.getStatusLine();
        final int statusCode = statusLine.getStatusCode();
        if (statusCode != HttpStatus.SC_OK) {
            throw new RuntimeException("Net payment failed");
        }
        final HttpEntity httpEntity = httpResponse.getEntity();
        inputStream = httpEntity.getContent();
        Header header = httpEntity.getContentType();
        if (header != null) {
            contentType = header.getValue();
        }
        final RESPONSE_TYPE netPaymentResponse = makeNetPaymentResponse();
        header = httpResponse.getFirstHeader("location");
        if (header != null) {
            final String location = header.getValue();
            final URL locationURL = new URL(getURL(), location);
            netPaymentResponse.url = locationURL;
            final URI uri = new URI(locationURL.toExternalForm());
            parameters = URLEncodedUtils.parse(uri, "UTF-8");
            for (final NameValuePair parameter : parameters) {
                netPaymentResponse.setVariable(parameter.getName(), parameter.getValue());
            }
        }
        return netPaymentResponse;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getContentType() {
        return contentType;
    }

    public void close() {
        httpClient.getConnectionManager().shutdown();
    }

}
