/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netpayment;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URL;
import java.security.NoSuchAlgorithmException;

public abstract class NetPaymentResponse extends NetPaymentResponseBase {
    public static enum Result {OK, ERROR, CANCEL, DELAYED};

    NetPaymentResponse.Result result = null;

    public abstract String getSellerId();

    public abstract String getReference();

    public abstract BigDecimal getAmount();

    public abstract String getCurrency();

    public NetPaymentResponse.Result getResult() {
        return result;
    }

    public void setResult(NetPaymentResponse.Result result) {
        this.result = result;
    }

}

