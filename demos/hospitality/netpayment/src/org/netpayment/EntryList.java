/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netpayment;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public abstract class EntryList extends ArrayList<Map.Entry<String, String>> {

    protected Map.Entry<String, String> searchEntry(final String name) {
        for (final Map.Entry<String, String> entry : this) {
            if (name.equals(entry.getKey())) {
                return entry;
            }
        }
        return null;
    }

    public void setVariable(final String name,
                            final String value) {
        final Map.Entry<String, String> entry = searchEntry(name);
        if (entry != null) {
            entry.setValue(value);
            return;
        }
        add(new AbstractMap.SimpleEntry<String, String>(name, value));
    }

    public String getVariable(final String name) {
        final Map.Entry<String, String> entry = searchEntry(name);
        if (entry != null) return entry.getValue();
        return null;
    }

    protected DateFormat getDateFormat() {
        return new SimpleDateFormat("dd.MM.yyyy");
    }

    protected void setDate(final String name,
                           final Date value) {
        setVariable(name, value != null ? getDateFormat().format(value) : null);
    }

    protected Date getDate(final String name) {
        final String value = getVariable(name);
        if (value == null) {
            return null;
        }
        try {
            return getDateFormat().parse(value);
        } catch (ParseException parseException) {
            throw new RuntimeException(parseException);
        }
    }

    protected void setInteger(final String name,
                              final Integer value) {
        setVariable(name, value != null ? value.toString() : null);
    }

    protected Integer getInteger(final String name) {
        final String value = getVariable(name);
        if (value == null) {
            return null;
        }
        return Integer.valueOf(value);
    }

    protected void setBigDecimal(final String name,
                                 final BigDecimal value) {
        setVariable(name, value != null ? value.setScale(2).toString().replace('.', ',') : null);
    }

    protected BigDecimal getBigDecimal(final String name) {
        final String value = getVariable(name);
        if (value == null) {
            return null;
        }
        return new BigDecimal(value.replace(',', '.'));
    }

    protected void setURL(final String name,
                          final URL url) {
        setVariable(name, url != null ? url.toExternalForm() : null);
    }

    protected URL getURL(final String name) {
        final String value = getVariable(name);
        if (value == null) {
            return null;
        }
        try {
            return new URL(value);
        } catch (MalformedURLException malformedURLException) {
            throw new RuntimeException(malformedURLException);
        }
    }

}