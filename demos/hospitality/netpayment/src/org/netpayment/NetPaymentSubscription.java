/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netpayment;

import java.net.URL;
import java.util.Date;

public abstract class NetPaymentSubscription extends NetPaymentBase<NetPaymentSubscriptionResponse> {

    public abstract void setWebStoreServiceId(final String webStoreServiceId);

    public abstract String getWebStoreServiceId();

    public abstract void setBusinessId(final String businessId);

    public abstract String getBusinessId();

    public abstract void setBusinessName(final String businessName);

    public abstract String getBusinessName();

    public abstract void setMarketingName(final String marketingName);

    public abstract String getMarketingName();

    public abstract void setBusinessWWW(final URL businessWWW);

    public abstract URL getBusinessWWW();

    public abstract void setBusinessEmail(final String businessEmail);

    public abstract String getBusinessEmail();

    public abstract void setBusinessPhone(final String businessPhone);

    public abstract String getBusinessPhone();

    public abstract void setBusinessFax(final String businessFax);

    public abstract String getBusinessFax();

    public abstract void setWebStoreIBAN(final String webStoreIBAN);

    public abstract String getWebStoreIBAN();

    public abstract void setWebStoreName(final String webStoreName);

    public abstract String getWebStoreName();

    public abstract void setWebStoreWWW(final URL webStoreWWW);

    public abstract URL getWebStoreWWW();

    public abstract void setWebStoreCustomerService(final String webStoreCustomerService);

    public abstract String getWebStoreCustomerService();

    public abstract void setBusinessAddress(final String businessAddress);

    public abstract String getBusinessAddress();

    public abstract void setBusinessPOBox(final String businessPOBox);

    public abstract String getBusinessPOBox();

    public abstract void setBusinessPostalCode(final String businessPostalCode);

    public abstract String getBusinessPostalCode();

    public abstract void setBusinessCity(final String businessCity);

    public abstract String getBusinessCity();

    public abstract void setBusinessCountryCode(final String businessCountryCode);

    public abstract String getBusinessCountryCode();

    public abstract void setBillingAddress(final String billingAddress);

    public abstract String getBillingAddress();

    public abstract void setBillingPOBox(final String billingPOBox);

    public abstract String getBillingPOBox();

    public abstract void setBillingPostalCode(final String billingPostalCode);

    public abstract String getBillingPostalCode();

    public abstract void setBillingCity(final String billingCity);

    public abstract String getBillingCity();

    public abstract void setBillingCountryCode(final String billingCountryCode);

    public abstract String getBillingCountryCode();

    public abstract void setWebStoreAddress(final String webStoreAddress);

    public abstract String getWebStoreAddress();

    public abstract void setWebStorePOBox(final String webStorePOBox);

    public abstract String getWebStorePOBox();

    public abstract void setWebStorePostalCode(final String webStorePostalCode);

    public abstract String getWebStorePostalCode();

    public abstract void setWebStoreCity(final String webStoreCity);

    public abstract String getWebStoreCity();

    public abstract void setWebStoreCountryCode(final String webStoreCountryCode);

    public abstract String getWebStoreCountryCode();

    public abstract void setContactPersonName(final String contactPersonName);

    public abstract String getContactPersonName();

    public abstract void setContactPersonEmail(final String contactPersonEmail);

    public abstract String getContactPersonEmail();

    public abstract void setContactPersonPhone(final String contactPersonPhone);

    public abstract String getContactPersonPhone();

    public abstract void setContactPersonMobile(final String contactPersonMobile);

    public abstract String getContactPersonMobile();

    public abstract void setContactPersonFax(final String contactPersonFax);

    public abstract String getContactPersonFax();

    public abstract void setBillingPersonName(final String billingPersonName);

    public abstract String getBillingPersonName();

    public abstract void setBillingPersonEmail(final String billingPersonEmail);

    public abstract String getBillingPersonEmail();

    public abstract void setBillingPersonPhone(final String billingPersonPhone);

    public abstract String getBillingPersonPhone();

    public abstract void setBillingPersonMobile(final String billingPersonMobile);

    public abstract String getBillingPersonMobile();

    public abstract void setBillingPersonFax(final String billingPersonFax);

    public abstract String getBillingPersonFax();

    public abstract void setWebStorePersonName(final String webStorePersonName);

    public abstract String getWebStorePersonName();

    public abstract void setWebStorePersonEmail(final String webStorePersonEmail);

    public abstract String getWebStorePersonEmail();

    public abstract void setWebStorePersonPhone(final String webStorePersonPhone);

    public abstract String getWebStorePersonPhone();

    public abstract void setWebStorePersonMobile(final String webStorePersonMobile);

    public abstract String getWebStorePersonMobile();

    public abstract void setWebStorePersonFax(final String webStorePersonFax);

    public abstract String getWebStorePersonFax();

    public abstract void setPaymentMethodInfo(final String paymentMethodInfo);

    public abstract String getPaymentMethodInfo();

    public abstract void setDeliveryMethodInfo(final String deliveryMethodInfo);

    public abstract String getDeliveryMethodInfo();

    public abstract void setServiceBeginDate(final Date serviceBeginDate);

    public abstract Date getServiceBeginDate();

    public abstract void setCampaignCode(final String campaignCode);

    public abstract String getCampaignCode();

    public abstract void setAdditionalInformation(final String additionalInformation);

    public abstract String getAdditionalInformation();

    public abstract void setResponseType(final String responseType);

    public abstract String getResponseType();

    public abstract void setOkReturn(final URL okReturn);

    public abstract URL getOkReturn();

    public abstract void setCancelReturn(final URL cancelReturn);

    public abstract URL getCancelReturn();

}