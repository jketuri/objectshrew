/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netpayment.smt;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.netpayment.NetPaymentSubscriptionResponse;
import org.vaadin.common.Support;

public class SmtNetPaymentSubscriptionResponse extends NetPaymentSubscriptionResponse {
    private final static long serialVersionUID = 0L;

    private final SmtNetPaymentSubscription smtNetPaymentSubscription;

    public SmtNetPaymentSubscriptionResponse(SmtNetPaymentSubscription smtNetPaymentSubscription) {
        this.smtNetPaymentSubscription = smtNetPaymentSubscription;
    }

    public String getBusinessId() {
        return getVariable("srv_businessid");
    }

    public URL getWebStoreWWW() {
        return getURL("srv_webstorewww");
    }

    public String getInterfaceUserId() {
        return getVariable("srv_interfaceuserid");
    }

    public String getInterfaceCredential() {
        return getVariable("srv_interfacecredential");
    }

    public String getExtranetUserId() {
        return getVariable("srv_extranetuserid");
    }

    public String getExtranetCredential() {
        return getVariable("srv_extranetcredential");
    }

    public String getState() {
        return getVariable("srv_state");
    }

    public String getErrorCode() {
        return getVariable("srv_errorcode");
    }

    public String getErrorDescription() {
        return getVariable("srv_errordescription");
    }

    public void check()
        throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final String value = getVariable("srv_businessid") + "&" + getVariable("srv_webstorewww")
            + "&" + getVariable("srv_interfaceuserid") + "&" + getVariable("srv_interfacecredential")
            + "&" + getVariable("srv_extranetuserid") + "&" + getVariable("srv_extranetcredential") + "&" + getVariable("srv_state")
            + "&" + smtNetPaymentSubscription.secretKey + "&";
        final MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        messageDigest.update(value.getBytes("UTF-8"));
        final byte[] digest = messageDigest.digest();
        final String hash = Support.toHexString(digest);
        if (!hash.equals(getVariable("srv_hash"))) {
            throw new RuntimeException("Invalid hash");
        }
    }

}
