/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netpayment.smt;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.netpayment.NetPaymentResponse;
import org.vaadin.common.Support;

public class SmtNetPaymentResponse extends NetPaymentResponse {
    private final static long serialVersionUID = 0L;

    private final SmtNetPayment smtNetPayment;

    public SmtNetPaymentResponse(SmtNetPayment smtNetPayment) {
        this.smtNetPayment = smtNetPayment;
    }

    public String getSellerId() {
        return getVariable("pmt_sellerid");
    }

    public String getReference() {
        return getVariable("pmt_reference");
    }

    public BigDecimal getAmount() {
        String value = getVariable("pmt_amount");
        return value != null ? new BigDecimal(value.replace(',', '.')) : null;
    }

    public String getCurrency() {
        return getVariable("pmt_currency");
    }

    public void check()
        throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final String value = getVariable("pmt_id") + "&" + getVariable("pmt_reference")
            + "&" + getVariable("pmt_amount") + "&" + getVariable("pmt_currency") + "&" + smtNetPayment.secretKey + "&";
        final MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        messageDigest.update(value.getBytes("UTF-8"));
        final byte[] digest = messageDigest.digest();
        final String hash = Support.toHexString(digest);
        if (!hash.equals(getVariable("pmt_hash"))) {
            throw new RuntimeException("Invalid hash");
        }
    }

}

