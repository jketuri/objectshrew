/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netpayment.smt;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import org.netpayment.NetPaymentSubscription;
import org.netpayment.NetPaymentSubscriptionResponse;
import org.vaadin.common.Support;

public class SmtNetPaymentSubscription extends NetPaymentSubscription {
    private final static long serialVersionUID = 0L;

    protected String secretKey = "11223344556677889900";

    public SmtNetPaymentSubscription()
        throws MalformedURLException {
        setVariable("srv_version", "0002");
        setVariable("srv_charset", "UTF-8");
        setVariable("srv_hashversion", "SHA-1");
        setVariable("srv_hash", null);
        setVariable("srv_keygeneration", "1");
        url = new URL("https://www.maksuturva.fi/MerchantSubscription.pmt");
    }

    public void setWebStoreServiceId(final String webStoreServiceId) {
        setVariable("srv_webstoreserviceid", webStoreServiceId);
    }

    public String getWebStoreServiceId() {
        return getVariable("srv_webstoreserviceid");
    }

    public void setBusinessId(final String businessId) {
        setVariable("srv_businessid", businessId);
    }

    public String getBusinessId() {
        return getVariable("srv_businessid");
    }

    public void setBusinessName(final String businessName) {
        setVariable("srv_businessname", businessName);
    }

    public String getBusinessName() {
        return getVariable("srv_businessname");
    }

    public void setMarketingName(final String marketingName) {
        setVariable("srv_marketingname", marketingName);
    }

    public String getMarketingName() {
        return getVariable("srv_marketingname");
    }

    public void setBusinessWWW(final URL businessWWW) {
        setURL("srv_businesswww", businessWWW);
    }

    public URL getBusinessWWW() {
        return getURL("srv_businesswww");
    }

    public void setBusinessEmail(final String businessEmail) {
        setVariable("srv_businessemail", businessEmail);
    }

    public String getBusinessEmail() {
        return getVariable("srv_businessemail");
    }

    public void setBusinessPhone(final String businessPhone) {
        setVariable("srv_businessphone", businessPhone);
    }

    public String getBusinessPhone() {
        return getVariable("srv_businessphone");
    }

    public void setBusinessFax(final String businessFax) {
        setVariable("srv_businessfax", businessFax);
    }

    public String getBusinessFax() {
        return getVariable("srv_businessfax");
    }

    public void setWebStoreIBAN(final String webStoreIBAN) {
        setVariable("srv_webstoreiban", webStoreIBAN);
    }

    public String getWebStoreIBAN() {
        return getVariable("srv_webstoreiban");
    }

    public void setWebStoreName(final String webStoreName) {
        setVariable("srv_webstorename", webStoreName);
    }

    public String getWebStoreName() {
        return getVariable("srv_webstorename");
    }

    public void setWebStoreWWW(final URL webStoreWWW) {
        setURL("srv_webstorewww", webStoreWWW);
    }

    public URL getWebStoreWWW() {
        return getURL("srv_webstorewww");
    }

    public void setWebStoreCustomerService(final String webStoreCustomerService) {
        setVariable("srv_webstorecustomerservice", webStoreCustomerService);
    }

    public String getWebStoreCustomerService() {
        return getVariable("srv_webstorecustomerservice");
    }

    public void setBusinessAddress(final String businessAddress) {
        setVariable("srv_businessaddress", businessAddress);
    }

    public String getBusinessAddress() {
        return getVariable("srv_businessaddress");
    }

    public void setBusinessPOBox(final String businessPOBox) {
        setVariable("srv_businesspobox", businessPOBox);
    }

    public String getBusinessPOBox() {
        return getVariable("srv_businesspobox");
    }

    public void setBusinessPostalCode(final String businessPostalCode) {
        setVariable("srv_businesspostalcode", businessPostalCode);
    }

    public String getBusinessPostalCode() {
        return getVariable("srv_businesspostalcode");
    }

    public void setBusinessCity(final String businessCity) {
        setVariable("srv_businesscity", businessCity);
    }

    public String getBusinessCity() {
        return getVariable("srv_businesscity");
    }

    public void setBusinessCountryCode(final String businessCountryCode) {
        setVariable("srv_businesscountrycode", businessCountryCode);
    }

    public String getBusinessCountryCode() {
        return getVariable("srv_businesscountrycode");
    }

    public void setBillingAddress(final String billingAddress) {
        setVariable("srv_billingaddress", billingAddress);
    }

    public String getBillingAddress() {
        return getVariable("srv_billingaddress");
    }

    public void setBillingPOBox(final String billingPOBox) {
        setVariable("srv_billingpobox", billingPOBox);
    }

    public String getBillingPOBox() {
        return getVariable("srv_billingpobox");
    }

    public void setBillingPostalCode(final String billingPostalCode) {
        setVariable("srv_billingpostalcode", billingPostalCode);
    }

    public String getBillingPostalCode() {
        return getVariable("srv_billingpostalcode");
    }

    public void setBillingCity(final String billingCity) {
        setVariable("srv_billingcity", billingCity);
    }

    public String getBillingCity() {
        return getVariable("srv_billingcity");
    }

    public void setBillingCountryCode(final String billingCountryCode) {
        setVariable("srv_billingcountrycode", billingCountryCode);
    }

    public String getBillingCountryCode() {
        return getVariable("srv_billingcountrycode");
    }

    public void setWebStoreAddress(final String webStoreAddress) {
        setVariable("srv_webstoreaddress", webStoreAddress);
    }

    public String getWebStoreAddress() {
        return getVariable("srv_webstoreaddress");
    }

    public void setWebStorePOBox(final String webStorePOBox) {
        setVariable("srv_webstorepobox", webStorePOBox);
    }

    public String getWebStorePOBox() {
        return getVariable("srv_webstorepobox");
    }

    public void setWebStorePostalCode(final String webStorePostalCode) {
        setVariable("srv_webstorepostalcode", webStorePostalCode);
    }

    public String getWebStorePostalCode() {
        return getVariable("srv_webstorepostalcode");
    }

    public void setWebStoreCity(final String webStoreCity) {
        setVariable("srv_webstorecity", webStoreCity);
    }

    public String getWebStoreCity() {
        return getVariable("srv_webstorecity");
    }

    public void setWebStoreCountryCode(final String webStoreCountryCode) {
        setVariable("srv_webstorecountrycode", webStoreCountryCode);
    }

    public String getWebStoreCountryCode() {
        return getVariable("srv_webstorecountrycode");
    }

    public void setContactPersonName(final String contactPersonName) {
        setVariable("srv_contactpersonname", contactPersonName);
    }

    public String getContactPersonName() {
        return getVariable("srv_contactpersonname");
    }

    public void setContactPersonEmail(final String contactPersonEmail) {
        setVariable("srv_contactpersonemail", contactPersonEmail);
    }

    public String getContactPersonEmail() {
        return getVariable("srv_contactpersonemail");
    }

    public void setContactPersonPhone(final String contactPersonPhone) {
        setVariable("srv_contactpersonphone", contactPersonPhone);
    }

    public String getContactPersonPhone() {
        return getVariable("srv_contactpersonphone");
    }

    public void setContactPersonMobile(final String contactPersonMobile) {
        setVariable("srv_contactpersonmobile", contactPersonMobile);
    }

    public String getContactPersonMobile() {
        return getVariable("srv_contactpersonmobile");
    }

    public void setContactPersonFax(final String contactPersonFax) {
        setVariable("srv_contactpersonfax", contactPersonFax);
    }

    public String getContactPersonFax() {
        return getVariable("srv_contactpersonfax");
    }

    public void setBillingPersonName(final String billingPersonName) {
        setVariable("srv_billingpersonname", billingPersonName);
    }

    public String getBillingPersonName() {
        return getVariable("srv_billingpersonname");
    }

    public void setBillingPersonEmail(final String billingPersonEmail) {
        setVariable("srv_billingpersonemail", billingPersonEmail);
    }

    public String getBillingPersonEmail() {
        return getVariable("srv_billingpersonemail");
    }

    public void setBillingPersonPhone(final String billingPersonPhone) {
        setVariable("srv_billingpersonphone", billingPersonPhone);
    }

    public String getBillingPersonPhone() {
        return getVariable("srv_billingpersonphone");
    }

    public void setBillingPersonMobile(final String billingPersonMobile) {
        setVariable("srv_billingpersonmobile", billingPersonMobile);
    }

    public String getBillingPersonMobile() {
        return getVariable("srv_billingpersonmobile");
    }

    public void setBillingPersonFax(final String billingPersonFax) {
        setVariable("srv_billingpersonfax", billingPersonFax);
    }

    public String getBillingPersonFax() {
        return getVariable("srv_billingpersonfax");
    }

    public void setWebStorePersonName(final String webStorePersonName) {
        setVariable("srv_webstorepersonname", webStorePersonName);
    }

    public String getWebStorePersonName() {
        return getVariable("srv_webstorepersonname");
    }

    public void setWebStorePersonEmail(final String webStorePersonEmail) {
        setVariable("srv_webstorepersonemail", webStorePersonEmail);
    }

    public String getWebStorePersonEmail() {
        return getVariable("srv_webstorepersonemail");
    }

    public void setWebStorePersonPhone(final String webStorePersonPhone) {
        setVariable("srv_webstorepersonphone", webStorePersonPhone);
    }

    public String getWebStorePersonPhone() {
        return getVariable("srv_webstorepersonphone");
    }

    public void setWebStorePersonMobile(final String webStorePersonMobile) {
        setVariable("srv_webstorepersonmobile", webStorePersonMobile);
    }

    public String getWebStorePersonMobile() {
        return getVariable("srv_webstorepersonmobile");
    }

    public void setWebStorePersonFax(final String webStorePersonFax) {
        setVariable("srv_webstorepersonfax", webStorePersonFax);
    }

    public String getWebStorePersonFax() {
        return getVariable("srv_webstorepersonfax");
    }

    public void setPaymentMethodInfo(final String paymentMethodInfo) {
        setVariable("srv_paymentmethodinfo", paymentMethodInfo);
    }

    public String getPaymentMethodInfo() {
        return getVariable("srv_paymentmethodinfo");
    }

    public void setDeliveryMethodInfo(final String deliveryMethodInfo) {
        setVariable("srv_deliverymethodinfo", deliveryMethodInfo);
    }

    public String getDeliveryMethodInfo() {
        return getVariable("srv_deliverymethodinfo");
    }

    public void setServiceBeginDate(final Date serviceBeginDate) {
        setDate("srv_servicebegindate", serviceBeginDate);
    }
        
    public Date getServiceBeginDate() {
        return getDate("srv_servicebegindate");
    }

    public void setCampaignCode(final String campaignCode) {
        setVariable("srv_campaigncode", campaignCode);
    }

    public String getCampaignCode() {
        return getVariable("srv_campaigncode");
    }

    public void setAdditionalInformation(final String additionalInformation) {
        setVariable("srv_additionalinformation", additionalInformation);
    }

    public String getAdditionalInformation() {
        return getVariable("srv_additionalinformation");
    }

    public void setResponseType(final String responseType) {
        setVariable("srv_resptype", responseType);
    }

    public String getResponseType() {
        return getVariable("srv_resptype");
    }

    public void setOkReturn(final URL okReturn) {
        setURL("srv_okreturn", okReturn);
    }

    public URL getOkReturn() {
        return getURL("srv_okreturn");
    }

    public void setErrorReturn(final URL errorReturn) {
        setURL("srv_errorreturn", errorReturn);
    }

    public URL getErrorReturn() {
        return getURL("srv_errorreturn");
    }

    public void setCancelReturn(final URL cancelReturn) {
        setURL("srv_cancelreturn", cancelReturn);
    }

    public URL getCancelReturn() {
        return getURL("srv_cancelreturn");
    }

    public void setSecretKey(final String secretKey) {
        this.secretKey = secretKey;
	}

    protected void prepare()
        throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final String value = getVariable("srv_webstorewww") + "&" + secretKey + "&";
        final MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        messageDigest.update(value.getBytes("UTF-8"));
        final byte[] digest = messageDigest.digest();
        setVariable("srv_hash", Support.toHexString(digest));
    }

    protected NetPaymentSubscriptionResponse makeNetPaymentResponse() {
        return new SmtNetPaymentSubscriptionResponse(this);
    }

}
