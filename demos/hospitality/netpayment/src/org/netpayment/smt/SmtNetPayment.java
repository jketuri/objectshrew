/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netpayment.smt;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Locale;

import org.netpayment.NetPayment;
import org.netpayment.NetPaymentResponse;
import org.vaadin.common.Support;

public class SmtNetPayment extends NetPayment {
    private final static long serialVersionUID = 0L;

    protected static String checkFields[]
        = new String[] {"pmt_action", "pmt_version", "pmt_selleriban", "pmt_id", "pmt_orderid", "pmt_reference",
                        "pmt_duedate", "pmt_amount", "pmt_currency", "pmt_okreturn", "pmt_errorreturn",
                        "pmt_cancelreturn", "pmt_delayedpayreturn", "pmt_escrow", "pmt_escrowchangeallowed",
                        "pmt_invoicefromseller", "pmt_paymentmethod", "pmt_buyeridentificationcode",
                        "pmt_buyername", "pmt_buyeraddress", "pmt_buyerpostalcode", "pmt_buyercity",
                        "pmt_buyercountry", "pmt_deliveryname", "pmt_deliveryaddress", "pmt_deliverypostalcode",
                        "pmt_deliverycity", "pmt_deliverycountry", "pmt_sellercosts"};
    protected static String checkOrderFields[]
        = new String[] {"pmt_row_name", "pmt_row_desc", "pmt_row_quantity", "pmt_row_articlenr", "pmt_row_unit",
                        "pmt_row_deliverydate", "pmt_row_price_gross", "pmt_row_price_net", "pmt_row_vat",
                        "pmt_row_discountpercentage", "pmt_row_type"};

    protected String secretKey = "11223344556677889900";

    public SmtNetPayment()
        throws MalformedURLException {
        setVariable("pmt_action", "NEW_PAYMENT_EXTENDED");
        setVariable("pmt_version", "0004");
        setSellerId("testikauppias");
        setSellerIban(null);
        setPaymentId("00000000000000000000");
        setOrderId("00000000000000000000000000000000000000000000000000");
        setReference("100");
        setDueDate(null);
        setLocale(null);
        setAmount(new BigDecimal(BigInteger.ZERO, 2));
        setCurrency("EUR");
        setOkReturn(null);
        setErrorReturn(null);
        setCancelReturn(null);
        setDelayedPayReturn(null);
        setVariable("pmt_escrow", "Y");
        setVariable("pmt_escrowchangeallowed", "N");
        setEstimatedDaysToDelivery(null);
        setBuyerName("testiostaja");
        setBuyerAddress("testiosoite");
        setBuyerPostalCode("00100");
        setBuyerCity("Helsinki");
        setBuyerCountry("00100");
        setBuyerPhone(null);
        setBuyerEmail("testi@testi.com");
        setDeliveryName(null);
        setDeliveryAddress(null);
        setDeliveryPostalCode(null);
        setDeliveryCity(null);
        setDeliveryCountry(null);
        setSellerCosts(new BigDecimal(BigInteger.ZERO, 2));
        setVariable("pmt_charset", "UTF-8");
        setVariable("pmt_charsethttp", "UTF-8");
        setVariable("pmt_hashversion", "SHA-1");
        setVariable("pmt_hash", null);
        setVariable("pmt_keygeneration", "1");
        url = new URL("https://www.maksuturva.fi/NewPaymentExtended.pmt");
    }

    @Override
    public void setSellerId(final String sellerId) {
        setVariable("pmt_sellerid", sellerId);
    }

    @Override
    public String getSellerId() {
        return getVariable("pmt_sellerid");
    }

    @Override
    public void setSellerIban(final String sellerIban) {
        setVariable("pmt_selleriban", sellerIban);
    }

    @Override
    public String getSellerIban() {
        return getVariable("pmt_selleriban");
    }

    @Override
    public void setPaymentId(final String paymentId) {
        setVariable("pmt_id", paymentId);
    }

    @Override
    public String getPaymentId() {
        return getVariable("pmt_id");
    }

    @Override
    public void setOrderId(final String orderId) {
        setVariable("pmt_orderid", orderId);
    }

    @Override
    public String getOrderId() {
        return getVariable("pmt_orderid");
    }

    @Override
    public void setReference(final String reference) {
        setVariable("pmt_reference", reference != null ? makeReference(reference) : null);
    }

    @Override
    public String getReference() {
        return getVariable("pmt_reference");
    }

    @Override
    public void setDueDate(final Date dueDate) {
        setDate("pmt_duedate", dueDate);
    }

    @Override
    public Date getDueDate() {
        return getDate("pmt_duedate");
    }

    @Override
    public void setLocale(final Locale locale) {
        setVariable("pmt_userlocale", locale != null ? locale.getLanguage() + "_FI" : null);
    }

    @Override
    public Locale getLocale() {
        final String userLocale = getVariable("pmt_userlocale");
        if (userLocale != null) {
            return new Locale(userLocale.substring(0, 2), "FI");
        }
        return null;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        setBigDecimal("pmt_amount", amount);
    }

    @Override
    public BigDecimal getAmount() {
        return getBigDecimal("pmt_amount");
    }

    @Override
    public void setCurrency(final String currency) {
        setVariable("pmt_currency", currency);
    }

    @Override
    public String getCurrency() {
        return getVariable("pmt_currency");
    }

    @Override
    public void setOkReturn(final URL okReturn) {
        setURL("pmt_okreturn", okReturn);
    }

    @Override
    public URL getOkReturn() {
        return getURL("pmt_okreturn");
    }

    @Override
    public void setErrorReturn(final URL errorReturn) {
        setURL("pmt_errorreturn", errorReturn);
    }

    @Override
    public URL getErrorReturn() {
        return getURL("pmt_errorreturn");
    }

    @Override
    public void setCancelReturn(final URL cancelReturn) {
        setURL("pmt_cancelreturn", cancelReturn);
    }

    @Override
    public URL getCancelReturn() {
        return getURL(getVariable("pmt_cancelreturn"));
	}

    @Override
    public void setDelayedPayReturn(final URL delayedPayReturn) {
        setURL("pmt_delayedpayreturn", delayedPayReturn);
	}

    @Override
    public URL getDelayedPayReturn() {
        return getURL("pmt_delayedpayreturn");
	}

    @Override
    public void setEstimatedDaysToDelivery(final Integer estimatedDaysToDelivery) {
        setInteger("pmt_estimateddaystodelivery", estimatedDaysToDelivery);
	}

    @Override
    public Integer getEstimatedDaysToDelivery() {
        return getInteger("pmt_estimateddaystodelivery");
	}

    @Override
    public void setBuyerName(final String buyerName) {
        setVariable("pmt_buyername", buyerName);
	}

    @Override
    public String getBuyerName() {
        return getVariable("pmt_buyername");
	}

    @Override
    public void setBuyerAddress(final String buyerAddress) {
        setVariable("pmt_buyeraddress", buyerAddress);
	}

    @Override
    public String getBuyerAddress() {
        return getVariable("pmt_buyeraddress");
	}

    @Override
    public void setBuyerCity(final String buyerCity) {
        setVariable("pmt_buyercity", buyerCity);
	}

    @Override
    public String getBuyerCity() {
        return getVariable("pmt_buyercity");
	}

    @Override
    public void setBuyerPostalCode(final String buyerPostalCode) {
        setVariable("pmt_buyerpostalcode", buyerPostalCode);
	}

    @Override
    public String getBuyerPostalCode() {
        return getVariable("pmt_buyerpostalcode");
	}

    @Override
    public void setBuyerCountry(final String buyerCountry) {
        setVariable("pmt_buyercountry", buyerCountry);
	}

    @Override
    public String getBuyerCountry() {
        return getVariable("pmt_buyercountry");
	}

    @Override
    public void setBuyerPhone(final String buyerPhone) {
        setVariable("pmt_buyerphone", buyerPhone);
	}

    @Override
    public String getBuyerPhone() {
        return getVariable("pmt_buyerphone");
	}

    @Override
    public void setBuyerEmail(final String buyerEmail) {
        setVariable("pmt_buyeremail", buyerEmail);
	}

    @Override
    public String getBuyerEmail() {
        return getVariable("pmt_buyeremail");
	}

    @Override
    public void setDeliveryName(final String deliveryName) {
        setVariable("pmt_deliveryname", deliveryName);
	}

    @Override
    public String getDeliveryName() {
        return getVariable("pmt_deliveryname");
	}

    @Override
    public void setDeliveryAddress(final String deliveryAddress) {
        setVariable("pmt_deliveryaddress", deliveryAddress);
	}

    @Override
    public String getDeliveryAddress() {
        return getVariable("pmt_deliveryaddress");
	}

    @Override
    public void setDeliveryPostalCode(final String deliveryPostalCode) {
        setVariable("pmt_deliverypostalcode", deliveryPostalCode);
	}

    @Override
    public String getDeliveryPostalCode() {
        return getVariable("pmt_deliverypostalcode");
	}

    @Override
    public void setDeliveryCity(final String deliveryCity) {
        setVariable("pmt_deliverycity", deliveryCity);
	}

    @Override
    public String getDeliveryCity() {
        return getVariable("pmt_deliverycity");
	}

    @Override
    public void setDeliveryCountry(final String deliveryCountry) {
        setVariable("pmt_deliverycountry", deliveryCountry);
	}

    @Override
    public String getDeliveryCountry() {
        return getVariable("pmt_deliverycountry");
	}

    @Override
    public void setSellerCosts(BigDecimal sellerCosts) {
        setBigDecimal("pmt_sellercosts", sellerCosts);
    }

    @Override
    public BigDecimal getSellerCosts() {
        return getBigDecimal("pmt_sellerCosts");
    }

    @Override
    public void setOrderRows(final Integer orderRows) {
        setInteger("pmt_rows", orderRows);
	}

    @Override
    public Integer getOrderRows() {
        return getInteger("pmt_rows");
	}

    @Override
    public void setOrderRow(final int orderRow,
                            final String productName,
                            final String productDescription,
                            final Integer productQuantity,
                            final Date deliveryDate,
                            final BigDecimal priceGross,
                            final BigDecimal priceNet,
                            final BigDecimal vat,
                            final BigDecimal discountPercentage) {
        setVariable("pmt_row_name" + orderRow, productName);
        setVariable("pmt_row_desc" + orderRow, productDescription);
        setInteger("pmt_row_quantity" + orderRow, productQuantity);
        setDate("pmt_row_deliverydate" + orderRow, deliveryDate);
        if (priceGross != null) {
            setBigDecimal("pmt_row_price_gross" + orderRow, priceGross);
        }
        if (priceNet != null) {
            setBigDecimal("pmt_row_price_net" + orderRow, priceNet);
        }
        setBigDecimal("pmt_row_vat" + orderRow, vat);
        setBigDecimal("pmt_row_discountpercentage" + orderRow, discountPercentage);
        setInteger("pmt_row_type" + orderRow, 1);
	}

    public void setSecretKey(final String secretKey) {
        this.secretKey = secretKey;
	}

    @Override
    protected void prepare()
        throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final StringBuffer buffer = new StringBuffer();
        for (final String checkField : checkFields) {
            final String variable = getVariable(checkField);
            if (variable != null) {
                buffer.append(variable);
                buffer.append("&");
            }
        }
        final int orderRows = getOrderRows();
        for (int orderRow = 1; orderRow <= orderRows; orderRow++) {
            for (final String checkOrderField : checkOrderFields) {
                final String variable = getVariable(checkOrderField + orderRow);
                if (variable != null) {
                    buffer.append(variable);
                    buffer.append("&");
                }
            }
        }
        buffer.append(secretKey + "&");
        final MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        messageDigest.update(buffer.toString().getBytes("UTF-8"));
        final byte[] digest = messageDigest.digest();
        setVariable("pmt_hash", Support.toHexString(digest));
    }

    @Override
    protected NetPaymentResponse makeNetPaymentResponse() {
        return new SmtNetPaymentResponse(this);
    }

}
