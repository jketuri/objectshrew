/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netpayment;

import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.Locale;

public abstract class NetPayment extends NetPaymentBase<NetPaymentResponse> {

    public abstract void setSellerId(final String sellerId);

    public abstract String getSellerId();

    public abstract void setSellerIban(final String sellerIban);

    public abstract String getSellerIban();

    public abstract void setPaymentId(final String paymentId);

    public abstract String getPaymentId();

    public abstract void setOrderId(final String orderId);

    public abstract String getOrderId();

    public abstract void setReference(final String reference);

    public abstract String getReference();

    public abstract void setDueDate(final Date dueDate);

    public abstract Date getDueDate();

    public abstract void setLocale(final Locale locale);

    public abstract Locale getLocale();

    public abstract void setAmount(final BigDecimal amount);

    public abstract BigDecimal getAmount();

    public abstract void setCurrency(final String currency);

    public abstract String getCurrency();

    public abstract void setOkReturn(final URL okReturn);

    public abstract URL getOkReturn();

    public abstract void setErrorReturn(final URL errorReturn);

    public abstract URL getErrorReturn();

    public abstract void setCancelReturn(final URL cancelReturn);

    public abstract URL getCancelReturn();

    public abstract void setDelayedPayReturn(final URL delayedPayReturn);

    public abstract URL getDelayedPayReturn();

    public abstract void setEstimatedDaysToDelivery(final Integer estimatedDaysToDelivery);

    public abstract Integer getEstimatedDaysToDelivery();

    public abstract void setBuyerName(final String buyerName);

    public abstract String getBuyerName();

    public abstract void setBuyerAddress(final String buyerAddress);

    public abstract String getBuyerAddress();

    public abstract void setBuyerCity(final String buyerCity);

    public abstract String getBuyerCity();

    public abstract void setBuyerPostalCode(final String buyerPostalCode);

    public abstract String getBuyerPostalCode();

    public abstract void setBuyerCountry(final String buyerCountry);

    public abstract String getBuyerCountry();

    public abstract void setBuyerPhone(final String buyerPhone);

    public abstract String getBuyerPhone();

    public abstract void setBuyerEmail(final String buyerEmail);

    public abstract String getBuyerEmail();

    public abstract void setDeliveryName(final String deliveryName);

    public abstract String getDeliveryName();

    public abstract void setDeliveryAddress(final String deliveryAddress);

    public abstract String getDeliveryAddress();

    public abstract void setDeliveryPostalCode(final String deliveryPostalCode);

    public abstract String getDeliveryPostalCode();

    public abstract void setDeliveryCity(final String deliveryCity);

    public abstract String getDeliveryCity();

    public abstract void setDeliveryCountry(final String deliveryCountry);

    public abstract String getDeliveryCountry();

    public abstract void setSellerCosts(BigDecimal sellerCosts);

    public abstract BigDecimal getSellerCosts();

    public abstract void setOrderRows(final Integer orderRows);

    public abstract Integer getOrderRows();

    public abstract void setOrderRow(final int orderRow,
                                     final String productName,
                                     final String productDescription,
                                     final Integer productQuantity,
                                     final Date deliveryDate,
                                     final BigDecimal priceGross,
                                     final BigDecimal priceNet,
                                     final BigDecimal vat,
                                     final BigDecimal discountPercentage);

    protected String makeReference(final String value) {
        long multiplier = 7L, subtrahend = 4L, sum = 0L;
        for (int index = value.length() - 1; index >= 0; index--) {
            int digit = Character.digit(value.charAt(index), 10);
            sum += digit * multiplier;
            if (multiplier == 1) {
                multiplier = 7;
                subtrahend = 4L;
            } else {
                multiplier -= subtrahend;
                subtrahend /= 2L;
            }
        }
        long checkSum = 10L - sum % 10L;
        if (checkSum == 10L) {
            checkSum = 0L;
        }
        return value + Character.forDigit((int)checkSum, 10);
    }

}
