#!/bin/sh

check()
{
    RV="$?"
    if test $RV != "0"
	then
        echo "RV=$RV"
	    exit 1
    fi
}

mkdir -p $HOME/back/netpayment
check
cp -frv * $HOME/back/netpayment/
check
rm -f $HOME/netpayment.zip
check
zip -rv $HOME/netpayment * -x 'build/*' -x '*.jar' -x '*.war' -x '*/.svn/*'
check
cp -v $HOME/netpayment.zip $HOME/netpayment_s
check
bzip2 $HOME/netpayment_s
check
mv -v $HOME/netpayment_s.bz2 $HOME/netpayment_s
check
ls -l $HOME/netpayment.zip $HOME/netpayment_s
check
