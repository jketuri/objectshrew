#!/bin/sh

check()
{
    RV="$?"
    if test $RV != "0"
	then
        echo "RV=$RV"
	    exit 1
    fi
}

mkdir -p $HOME/back/hospitality
check
cp -frv * $HOME/back/hospitality/
check
rm -f $HOME/hospitality.zip
check
zip -rv $HOME/hospitality * -x 'docroot/WEB-INF/classes/*' 'build/*' -x '*.jar' -x '*.war' -x '*/.svn/*'
check
cp -v $HOME/hospitality.zip $HOME/hospitality_s
check
bzip2 $HOME/hospitality_s
check
mv -v $HOME/hospitality_s.bz2 $HOME/hospitality_s
check
ls -l $HOME/hospitality.zip $HOME/hospitality_s
check
