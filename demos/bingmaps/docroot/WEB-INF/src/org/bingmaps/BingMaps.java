/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bingmaps;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.vaadin.objectview.ObjectView;

public class BingMaps implements Serializable {
    private final static long serialVersionUID = 0L;

    protected final List<Object> tabList = new ArrayList<Object>();

    public BingMaps(final BingMapsObjectView bingMapsObjectView) {
        tabList.add(new BingMapsQuery(bingMapsObjectView));
    }

    public ObjectView.LayoutType layoutType() {
        return ObjectView.LayoutType.VERTICAL_LAYOUT;
    }

    public String namespace() {
        return "";
    }

    public List<Object> getTabList() {
        return tabList;
    }

}
