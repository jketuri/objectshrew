/*
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bingmaps.rest;

import org.bing.models.RestServiceModels;
import org.bing.models.SimpleAddress;
import org.bing.requests.GeocodeRequest;
import org.bing.rest.BingGeocode;
import org.vaadin.addon.leaflet.LMap;
import org.vaadin.addon.leaflet.LTileLayer;
import org.vaadin.objectview.ObjectView;

import java.io.Serializable;
import java.lang.reflect.Field;

public class BingMapsQuery implements Serializable {
    private final static long serialVersionUID = 0L;

    protected static String[] typeValues = new String[]{"road", "aerial", "hybrid"};

    private final LMap lmap = new LMap();
    private final BingMapsObjectView bingMapsObjectView;
    private final GeocodeRequest geocodeRequest;
    private String addressLine;
    private String locality;
    private String countryRegion;
    private BingGeocode.LocationList locations = new BingGeocode.LocationList();

    public BingMapsQuery(final BingMapsObjectView bingMapsObjectView) {
        this.bingMapsObjectView = bingMapsObjectView;
        this.geocodeRequest = new GeocodeRequest();
        this.geocodeRequest.setOptionalBingMapsKey(bingMapsObjectView.getProperties().getProperty(BingMapsObjectView.class.getName() + ".applicationId"));;
        String tileLayerURLTemplate
            = bingMapsObjectView.getProperties().getProperty(BingMapsObjectView.class.getName() + ".tileLayerURLTemplate");
        if (tileLayerURLTemplate == null) {
            tileLayerURLTemplate = "http://localhost:8000/invoicemonitor/tile/{s}/{z}/{x}/{y}";
        }
        LTileLayer tileLayer = new LTileLayer(tileLayerURLTemplate);
        lmap.addBaseLayer(tileLayer, "local");
        lmap.zoomToContent();
    }

    public ObjectView.LayoutType layoutType() {
        return ObjectView.LayoutType.VERTICAL_LAYOUT;
    }

    /**
     * Response which is sent with geocodeRetrieve(), geocodeUpdate(), geocodeAdd(), geocodeDelete()
     * in server side. Response is converted to JSON or XML and sent to network as response
     * to request which was received with method setGeocodeRequest.
     **/
    public RestServiceModels.GeocodeResponse getGeocodeResponse() {
        return new RestServiceModels.GeocodeResponse();
    }

    /**
     * Response which is received with geocodeClientRetrieve(), geocodeClientUpdate(), geocodeClientAdd(),
     * geocodeClientDelete() in client side. Response is received from network and converted from JSON or XML
     * to object and set with this method for application by the framework.
     **/
    public void setGeocodeResponse(RestServiceModels.GeocodeResponse response) {
        this.locations.clear();
        for (RestServiceModels.Location location : response.getResourceSets()[0].getResources()) {
            this.locations.add(location);
        }
        if (!this.locations.isEmpty()) {
            /*
            org.vaadin.addon.leaflet.shared.Point center
                    = new org.vaadin.addon.leaflet.shared.Point(
                    this.locations.get(0).getPoint().getCoordinates()[0],
                    this.locations.get(0).getPoint().getCoordinates()[1]);
            lmap.flyTo(center, 18.0);
            */
            lmap.setCenter(this.locations.get(0).getPoint().getCoordinates()[0],
                    this.locations.get(0).getPoint().getCoordinates()[1]);
            lmap.setZoomLevel(18);
        }
    }

    public String clientLocationURI() {
        SimpleAddress address = new SimpleAddress();
        address.setOptionalAddressLine(getOptionalAddressLine());
        address.setOptionalLocality(getOptionalLocality());
        address.setOptionalAdminDistrict("");
        address.setOptionalPostalCode("");
        address.setOptionalCountryRegion(getOptionalCountryRegion());
        geocodeRequest.setOptionalAddress(address);
        return geocodeRequest.getRequestUrl();
    }

    public String clientMessageType() {
        return "application/json";
    }

    public String clientRequestType() {
        return "REST";
    }

    public boolean clientPlain() {
        return true;
    }

    /**
     * Action method in client side for REST GET-verb and SOAP-request with retrieve-action.
     */
    public String[] geocodeClientRetrieve() {
        return new String[]{null, "InvoiceMonitor.tabList.InvoiceMap.formLocations"};
    }

    public String getOptionalAddressLine() {
        return addressLine;
    }

    public void setOptionalAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getOptionalLocality() {
        return locality;
    }

    public void setOptionalLocality(String locality) {
        this.locality = locality;
    }

    public String getOptionalCountryRegion() {
        return countryRegion;
    }

    public void setOptionalCountryRegion(String countryRegion) {
        this.countryRegion = countryRegion;
    }

    public void setType(final String type) {
        BingMapsObjectView.getRequest().getSession().setAttribute(org.bingmaps.BingMapsObjectView.class.getName() + ".type", type);
    }

    public String getType() {
        return (String) BingMapsObjectView.getRequest().getSession().getAttribute(org.bingmaps.BingMapsObjectView.class.getName() + ".type");
    }

    public void editType(final String type) {
        if (!this.locations.isEmpty()) {
        }
    }

    public String[] valuesType() {
        return typeValues;
    }

    public BingGeocode.LocationList getFormLocations() {
        return this.locations;
    }

    public int mapWidth() {
        return 1000;
    }

    public int mapHeight() {
        return 1000;
    }

    public LMap getMap() {
        return lmap;
    }

}
