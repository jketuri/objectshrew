/*
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bingmaps;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.vaadin.addon.leaflet.LMap;
import org.vaadin.addon.leaflet.LTileLayer;
import org.vaadin.objectview.ObjectView;

public class BingMapsQuery implements Serializable {
    private final static long serialVersionUID = 0L;

    protected static String[] typeValues = new String[] {"road", "aerial", "hybrid"};

    private final BingMapsObjectView bingMapsObjectView;

    private Object searchModel = null;
    private String query = null;
    private String formattedAddress = null;
    private Double latitude = null;
    private Double longitude = null;

    protected final LMap map = new LMap();

    public BingMapsQuery(final BingMapsObjectView bingMapsObjectView) {
        this.bingMapsObjectView = bingMapsObjectView;
        final Properties properties = bingMapsObjectView.getProperties();
        String tileLayerURLTemplate
            = properties.getProperty(BingMapsObjectView.class.getName() + ".tileLayerURLTemplate");
        if (tileLayerURLTemplate == null) {
            tileLayerURLTemplate = "http://localhost:8000/bingmaps/tile/{s}/{z}/{x}/{y}";
        }
        LTileLayer tileLayer = new LTileLayer(tileLayerURLTemplate);
        map.addBaseLayer(tileLayer, "local");
        map.zoomToContent();
    }

    public ObjectView.LayoutType layoutType() {
        return ObjectView.LayoutType.VERTICAL_LAYOUT;
    }

    public String modelSearchWsdlURI() {
        return "/dev.virtualearth.net/webservices/v1/metadata/searchservice/dev.virtualearth.net.webservices.v1.search.wsdl";
    }

    public String modelSearchWebServiceName() {
        return "SearchService";
    }

    public String modelSearchWebServicePort() {
        return "BasicHttpBinding_ISearchService";
    }

    public String modelSearchWebOperationName() {
        return "Search";
    }

    public void setSearchModel(final Object searchModel)
    {
        this.searchModel = searchModel;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setType(final String type) {
        bingMapsObjectView.getRequest().getSession().setAttribute(BingMapsObjectView.class.getName() + ".type", type);
    }

    public String getType() {
        return (String)bingMapsObjectView.getRequest().getSession().getAttribute(BingMapsObjectView.class.getName() + ".type");
    }

    public String[] valuesType() {
        return typeValues;
    }

    public String[] editSearchModelClientCall()
        throws ClassNotFoundException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        final Properties properties = bingMapsObjectView.getProperties();
        final String applicationId = properties.getProperty(BingMapsObjectView.class.getName() + ".applicationId");
        final ClassLoader classLoader = searchModel.getClass().getClassLoader();
        final Class searchClass = classLoader.loadClass("_webservices._v1._search._contracts.Search"),
            searchRequestClass = classLoader.loadClass("_webservices._v1._search.SearchRequest"),
            credentialsClass = classLoader.loadClass("_webservices._v1._common.Credentials");
        final Method get_SearchRequest = searchModel.getClass().getMethod("get_SearchRequest"),
            getOptional_request = searchClass.getMethod("getOptional_request"),
            setOptional_Query = searchRequestClass.getMethod("setOptional_Query", String.class),
            getOptional_Credentials = searchRequestClass.getMethod("getOptional_Credentials"),
            setOptional_Credentials = searchRequestClass.getMethod("setOptional_Credentials", credentialsClass),
            setOptional_ApplicationId = credentialsClass.getMethod("setOptional_ApplicationId", String.class);
        final Object searchRequest = get_SearchRequest.invoke(searchModel);
        final Object request = getOptional_request.invoke(searchRequest);
        final Object credentials = getOptional_Credentials.invoke(request);
        setOptional_ApplicationId.invoke(credentials, applicationId);
        setOptional_Credentials.invoke(request, credentials);
        setOptional_Query.invoke(request, query);
        return null;
    }

    public String[] searchModelClientCall()
        throws ClassNotFoundException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        final ClassLoader classLoader = searchModel.getClass().getClassLoader();
        final Class searchContractsResponseClass = classLoader.loadClass("_webservices._v1._search._contracts.SearchResponse"),
            searchResponseClass = classLoader.loadClass("_webservices._v1._search.SearchResponse"),
            arrayOfSearchResultSetClass = classLoader.loadClass("_webservices._v1._search.ArrayOfSearchResultSet"),
            searchResultSetClass = classLoader.loadClass("_webservices._v1._search.SearchResultSet"),
            searchRegionClass = classLoader.loadClass("_webservices._v1._search.SearchRegion"),
            geocodeResultClass = classLoader.loadClass("_webservices._v1._common.GeocodeResult"),
            arrayOfGeocodeLocation = classLoader.loadClass("_webservices._v1._common.ArrayOfGeocodeLocation"),
            addressClass = classLoader.loadClass("_webservices._v1._common.Address"),
            geocodeLocationClass = classLoader.loadClass("_webservices._v1._common.GeocodeLocation");
        final Method get_SearchResponse = searchModel.getClass().getMethod("get_SearchResponse"),
            getOptional_SearchResult = searchContractsResponseClass.getMethod("getOptional_SearchResult"),
            getOptional_ResultSets = searchResponseClass.getMethod("getOptional_ResultSets"),
            getFormOptional_SearchResultSet = arrayOfSearchResultSetClass.getMethod("getFormOptional_SearchResultSet"),
            getOptional_SearchRegion = searchResultSetClass.getMethod("getOptional_SearchRegion"),
            getOptional_GeocodeLocation = searchRegionClass.getMethod("getOptional_GeocodeLocation"),
            getOptional_Address = geocodeResultClass.getMethod("getOptional_Address"),
            getOptional_FormattedAddress = addressClass.getMethod("getOptional_FormattedAddress"),
            getOptional_Locations = geocodeResultClass.getMethod("getOptional_Locations"),
            getFormOptional_GeocodeLocation = arrayOfGeocodeLocation.getMethod("getFormOptional_GeocodeLocation"),
            getOptional_Latitude = geocodeLocationClass.getMethod("getOptional_Latitude"),
            getOptional_Longitude = geocodeLocationClass.getMethod("getOptional_Longitude");
        final Object searchContractsResponse = get_SearchResponse.invoke(searchModel),
            searchResponse = getOptional_SearchResult.invoke(searchContractsResponse),
            resultSets = getOptional_ResultSets.invoke(searchResponse),
            searchResultSetList = getFormOptional_SearchResultSet.invoke(resultSets),
            searchResultSet = ((List)searchResultSetList).get(0);
        final Object searchRegion = getOptional_SearchRegion.invoke(searchResultSet),
            geocodeLocation = getOptional_GeocodeLocation.invoke(searchRegion);
        if (geocodeLocation != null) {
            final Object address = getOptional_Address.invoke(geocodeLocation),
                locations = getOptional_Locations.invoke(geocodeLocation),
                formOptionalGeocodeLocation = getFormOptional_GeocodeLocation.invoke(locations),
                location = ((List)formOptionalGeocodeLocation).get(0);
            formattedAddress = (String)getOptional_FormattedAddress.invoke(address);
            latitude = (Double)getOptional_Latitude.invoke(location);
            longitude = (Double)getOptional_Longitude.invoke(location);
            map.setCenter(latitude, longitude);
            map.setZoomLevel(18);
        } else {
            formattedAddress = "Not found";
            latitude = null;
            longitude = null;
        }
        return new String[] {null,
                             "BingMaps.tabList.BingMapsQuery.formattedAddress",
                             "BingMaps.tabList.BingMapsQuery.latitude",
                             "BingMaps.tabList.BingMapsQuery.longitude"};
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public int mapWidth() {
        return 1000;
    }

    public int mapHeight() {
        return 1000;
    }

    public LMap getMap() {
        return map;
    }

}
