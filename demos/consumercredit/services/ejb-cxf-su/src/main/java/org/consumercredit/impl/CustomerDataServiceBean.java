package org.consumercredit.impl;

import org.consumercredit.service.CustomerDataService;
import org.consumercredit.value.CustomerData;

import javax.jws.WebService;

@WebService(serviceName = "CustomerDataService", targetNamespace = "http://org.consumercredit", endpointInterface = "org.consumercredit.service.CustomerDataService")
public class CustomerDataServiceBean implements CustomerDataService {

    public CustomerData getCustomerData(String personId) {
        CustomerData customerData = new CustomerData();
        customerData.setFirstName("Mickey");
        customerData.setLastName("Mouse");
        customerData.setPersonId(personId);
        customerData.setCustomerId("1");
        customerData.setAge(30);
        customerData.setDefaultReport(false);
        customerData.setFormOfDwelling(CustomerData.FormOfDwelling.rentedFlat);
        customerData.setMaritalStatus(CustomerData.MaritalStatus.unmarried);
        return customerData;
    }

    public String test(String arg) {
        return "test";
    }

}
