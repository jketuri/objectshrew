package org.consumercredit.impl;

import org.consumercredit.service.CustomerDataService;
import org.consumercredit.value.CustomerData;

import javax.jws.WebService;

@WebService(serviceName = "CustomerDataService", targetNamespace = "http://org.consumercredit", endpointInterface = "org.consumercredit.service.CustomerDataService")
public class CustomerDataServiceImpl implements CustomerDataService {

    private org.consumercredit.ejb.CustomerDataService customerDataService;

    public void setCustomerDataService(
            org.consumercredit.ejb.CustomerDataService customerDataService) {
        this.customerDataService = customerDataService;
    }

    public org.consumercredit.ejb.CustomerDataService getCustomerDataService() {
        return customerDataService;
    }

    public CustomerData getCustomerData(String personId) {
        return customerDataService.getCustomerData(personId);
    }

}