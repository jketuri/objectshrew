package org.consumercredit.ejb;

import javax.ejb.Remote;

import org.consumercredit.value.CustomerData;

@Remote
public interface CustomerDataServiceRemote {
	
	public CustomerData getCustomerData(String personId);
	
}
