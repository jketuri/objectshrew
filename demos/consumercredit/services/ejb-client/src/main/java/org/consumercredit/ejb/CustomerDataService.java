package org.consumercredit.ejb;

import javax.ejb.Local;

import org.consumercredit.value.CustomerData;

@Local
public interface CustomerDataService {
	
	public CustomerData getCustomerData(String personId);
	
}
