#!/bin/bash
check()
{
    if test "$?" != "0"
    then
        exit 1
    fi
}
mvn install -Dmaven.test.skip=true -e
check
#mvn jbi:projectDeploy -e
cp -v ./war/target/war-0.0.1-SNAPSHOT.war $TOMCAT_HOME/webapps/
check
if test "$SMIX_TYPE" = "STANDALONE"
then
if test -d $HOME/apps/${SMIX}/deploy
then
cp -v ./sa/target/sa-0.0.1-SNAPSHOT.zip $HOME/apps/${SMIX}/deploy/
check
else
cp -v ./sa/target/sa-0.0.1-SNAPSHOT.zip $HOME/apps/${SMIX}/hotdeploy/
check
fi
elif test "$SMIX_TYPE" = "WAR"
then
curl -F "file=./sa/target/sa-0.0.1-SNAPSHOT.zip" -F "submit=Deploy" http://localhost:${TOMCAT_PORT}/${SMIX_WEB}/deployServiceAssembly.action
check
fi
