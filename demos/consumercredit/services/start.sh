#!/bin/bash
check()
{
    if test "$?" != "0"
    then
        exit 1
    fi
}
pushd $TOMCAT_HOME
check
"${DERBY_HOME}/bin/startNetworkServer" &
check
echo $! > ./derby_pid
check
"$HOME/apps/${SMIX}/bin/servicemix" &
check
echo $! > ./smix_pid
check
popd
