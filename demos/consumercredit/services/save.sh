#!/bin/bash
check()
{
    if test "$?" != "0"
    then
        exit 1
    fi
}
#mvn clean
#check
pushd ../..
check
rm -fv $HOME/lws.zip
check
zip -rv $HOME/lws laina_workspace -x 'laina_workspace/services/db/laina/*' -x "*.class" -x "*.bak" -x "*~" -x "*/target/*" -x "*/.metadata/*" -x "*/.svn/*"
check
ls -l $HOME/lws.zip
check
popd