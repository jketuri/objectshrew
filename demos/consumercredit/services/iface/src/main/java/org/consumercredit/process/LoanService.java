package org.consumercredit.process;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.consumercredit.value.CustomerData;
import org.consumercredit.value.LoanDecision;
import org.consumercredit.value.LoanData;

@WebService(name = "LoanServicePort", targetNamespace = "http://org.consumercredit")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle=SOAPBinding.ParameterStyle.WRAPPED)
public interface LoanService {

    @WebResult(name = "loanData", targetNamespace = "http://org.consumercredit")
    public LoanData start(@WebParam(name = "personId") String personId);

    @WebResult(name = "loanDecision", targetNamespace = "http://org.consumercredit")
    public LoanDecision complete(
            @WebParam(name = "customerId") String customerId,
            @WebParam(name = "loanId") String loanId);

    @WebResult(name = "customerData", targetNamespace = "http://org.consumercredit")
    public CustomerData retrieveCustomerData(
            @WebParam(name = "customerId") String customerId,
            @WebParam(name = "loanId") String loanId);

}
