package org.consumercredit.service;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.consumercredit.value.CustomerData;

@WebService(name = "CustomerDataServicePort", targetNamespace = "http://org.consumercredit")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle=SOAPBinding.ParameterStyle.WRAPPED)
public interface CustomerDataService {

    @WebResult(name = "customerData", targetNamespace = "http://org.consumercredit")
    public CustomerData getCustomerData(
            @WebParam(name = "personId") String personId);

}
