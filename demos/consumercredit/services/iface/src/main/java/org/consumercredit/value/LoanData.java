package org.consumercredit.value;

import java.util.ArrayList;
import java.util.List;

public class LoanData {

    private String customerId;
    private List<LoanItem> loanItem = new ArrayList<LoanItem>();

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<LoanItem> getLoanItem() {
        return loanItem;
    }

    public void setLoanItem(List<LoanItem> loanItem) {
        this.loanItem = loanItem;
    }

}
