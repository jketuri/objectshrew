package org.consumercredit.common;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Element;

import org.consumercredit.value.CustomerData;

public class Helper {

    public Element getChild(Element element, int number) {
        for (; number > 0; number--)
            element = (Element)element.getChildNodes().item(0);
        return element;
    }

    public Object getObject(Class<?> clazz, Element element)
            throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return (CustomerData) unmarshaller.unmarshal(element);
    }
    
    public long getTime() {
        return System.currentTimeMillis();
    }

}
