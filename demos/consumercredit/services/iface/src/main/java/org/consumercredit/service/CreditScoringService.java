package org.consumercredit.service;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.consumercredit.value.CreditScore;
import org.consumercredit.value.CustomerData;

@WebService(name = "CreditScoringServicePort", targetNamespace = "http://org.consumercredit")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle=SOAPBinding.ParameterStyle.WRAPPED)
public interface CreditScoringService {

    @WebResult(name = "creditScore", targetNamespace = "http://org.consumercredit")
    public CreditScore getCreditScore(
            @WebParam(name = "customerData") CustomerData customerData);

}
