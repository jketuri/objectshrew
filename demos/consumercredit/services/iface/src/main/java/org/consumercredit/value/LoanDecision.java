package org.consumercredit.value;

public class LoanDecision {

    public static enum Decision {
        offer, rejection
    };

    private Decision decision;
    private Integer creditScore;

    public Decision getDecision() {
        return decision;
    }

    public void setDecision(Decision decision) {
        this.decision = decision;
    }

    public Integer getCreditScore() {
        return creditScore;
    }

    public void setCreditScore(Integer creditScore) {
        this.creditScore = creditScore;
    }

}
