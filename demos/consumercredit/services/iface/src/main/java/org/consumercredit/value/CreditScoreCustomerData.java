package org.consumercredit.value;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "customerData", namespace = "http://org.consumercredit")
public class CreditScoreCustomerData extends CustomerData {

}
