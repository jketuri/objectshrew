package org.consumercredit.value;

public class CustomerData {

    public static enum FormOfDwelling {
        rentedFlat, ownedFlat
    };

    public static enum MaritalStatus {
        unmarried, married, registeredRelationship
    };

    private String firstName;
    private String lastName;
    private String address;
    private String postCode;
    private String city;
    private String country;
    private String personId;
    private String customerId;
    private Integer age;
    private Integer creditScore = null;
    private Boolean defaultReport;
    private FormOfDwelling formOfDwelling;
    private MaritalStatus maritalStatus;
    private Long time = Long.valueOf(System.currentTimeMillis());

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getCreditScore() {
        return creditScore;
    }

    public void setCreditScore(Integer creditScore) {
        this.creditScore = creditScore;
    }

    public Boolean getDefaultReport() {
        return defaultReport;
    }

    public void setDefaultReport(Boolean defaultReport) {
        this.defaultReport = defaultReport;
    }

    public FormOfDwelling getFormOfDwelling() {
        return formOfDwelling;
    }

    public void setFormOfDwelling(FormOfDwelling formOfDwelling) {
        this.formOfDwelling = formOfDwelling;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

}
