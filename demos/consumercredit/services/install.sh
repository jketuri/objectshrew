#!/bin/bash
check()
{
    if test "$?" != "0"
    then
        exit 1
    fi
}
if test "$1" = "smix"
then
mkdir -p $HOME/apps
check
if test "$SMIX_TYPE" = "STANDALONE"
then
pushd "$HOME/apps"
check
rm -rf "$HOME/apps/${DERBY}"
check
tar -xzvf "$HOME/pack/${DERBY}.tar.gz"
check
unzip -od $HOME/apps/ "$HOME/pack/apache-${ODE}.zip"
check
popd
mkdir -p $HOME/apps/${ODE}
check
unzip -od $HOME/apps/${ODE}/ "$HOME/apps/apache-${ODE}/${ODE}.zip"
check
if test "$ODE_DB_TYPE" = "EXTERNAL"
then
cp -v ode-jbi.properties $HOME/apps/${ODE}/
check
else
cp -v ode-jbi.properties.embedded $HOME/apps/${ODE}/ode-jbi.properties
check
fi
pushd $HOME/apps/${ODE}
check
rm -fv $HOME/apps/${ODE}.zip
check
zip -rv $HOME/apps/${ODE}.zip *
check
popd
check
rm -rf "$HOME/apps/${SMIX}"
pushd "$HOME/apps"
check
tar -xzvf "$HOME/pack/${SMIX}.tar.gz"
check
cp -v "$HOME/apps/${ODE}.zip" "$HOME/apps/${SMIX}/hotdeploy/"
check
# try
#cp -v "$HOME/apps/apache-${ODE}/${ODE}.zip" "$HOME/apps/${SMIX}/hotdeploy/"
#check
popd
cp -v ${DERBY_HOME}/lib/derbyclient.jar "$HOME/apps/${SMIX}/lib/"
check
cp -v $HOME/pack/jencks/jencks-2.1-all.jar "$HOME/apps/${SMIX}/lib/"
check
elif test "$SMIX_TYPE" = "WAR"
then
cp -v "$HOME/pack/${SMIX_WEB}.war" "$TOMCAT_HOME/webapps/"
check
#mkdir -pv $TOMCAT_HOME/conf/Catalina/localhost
#cp -v ${SMIX_WEB}.xml $TOMCAT_HOME/conf/Catalina/localhost/
fi
if test "$SMIX_DB_TYPE" = "EXTERNAL"
then
if test "$SMIX_TYPE" = "STANDALONE"
then
cp -v servicemix_standalone.xml "$HOME/apps/${SMIX}/conf/servicemix.xml"
check
cp -v jmx.xml log4j.xml "$HOME/apps/${SMIX}/conf/"
fi
fi
if test "$ODE_DB_TYPE" = "EXTERNAL"
then
if test "$SMIX_TYPE" = "STANDALONE"
then
cp -v jndi.xml "$HOME/apps/${SMIX}/conf/"
check
fi
cp -v derby.sql $TOMCAT_HOME/
check
pushd $TOMCAT_HOME
if test "$ODE" = "ode-jbi-1.2"
then
printf "connect 'jdbc:derby:ode;create=true';\nrun '$HOME/apps/apache-${ODE}/sql/derby.sql';\ndisconnect;exit;\n" | java -jar ${DERBY_HOME}/lib/derbyrun.jar ij
check
else
printf "connect 'jdbc:derby:ode;create=true';\nrun 'derby.sql';\ndisconnect;exit;\n" | java -jar ${DERBY_HOME}/lib/derbyrun.jar ij
fi
check
popd
fi
elif test "$1" = "smix_parts"
then
curl -F "file=@$HOME/apps/${SMIX}/hotdeploy/servicemix-shared-2008.01-installer.zip" -F "submit=Install" http://localhost:${TOMCAT_PORT}/${SMIX_WEB}/installSharedLibrary.action
check
curl -F "file=@$HOME/apps/${SMIX}/hotdeploy/servicemix-cxf-bc-2008.01-installer.zip" -F "submit=Install" http://localhost:${TOMCAT_PORT}/${SMIX_WEB}/installComponent.action
check
curl -F "file=@$HOME/apps/${SMIX}/hotdeploy/servicemix-cxf-se-2008.01-installer.zip" -F "submit=Install" http://localhost:${TOMCAT_PORT}/${SMIX_WEB}/installComponent.action
check
curl -F "file=@$HOME/apps/${SMIX}/hotdeploy/servicemix-drools-2008.01-installer.zip" -F "submit=Install" http://localhost:${TOMCAT_PORT}/${SMIX_WEB}/installComponent.action
check
printf "\n$HOME/apps/${ODE}.zip\n"
check
curl -F "file=@$HOME/apps/${ODE}.zip" -F "submit=Install" http://localhost:${TOMCAT_PORT}/${SMIX_WEB}/installComponent.action
check
curl -d "name=servicemix-cxf-bc" -d "submit=Start" http://localhost:${TOMCAT_PORT}/${SMIX_WEB}/startComponent.action
check
curl -d "name=servicemix-cxf-se" -d "submit=Start" http://localhost:${TOMCAT_PORT}/${SMIX_WEB}/startComponent.action
check
curl -d "name=servicemix-drools" -d "submit=Start" http://localhost:${TOMCAT_PORT}/${SMIX_WEB}/startComponent.action
check
#curl -d "name=OdeBpelEngine" -d "submit=Start" http://localhost:${TOMCAT_PORT}/${SMIX_WEB}/startComponent.action
elif test "$1" = "ode_src"
then
sudo gem install rubygems-update
check
sudo update_rubygems
check
sudo gem install buildr -v 1.2.10
check
sudo gem install rake -v 0.7.3
check
elif test "$1" = "smix_war"
then
pushd $HOME/apps
check
if test ! -d smix_war
then
mkdir -p smix_war
check
cd smix_war
check
jar -xvf $HOME/pack/${SMIX_WEB}.war
check
else
cd smix_war
check
fi
cp -v ${DERBY_HOME}/lib/derbyclient.jar WEB-INF/lib/
check
cp -v $HOME/pack/jencks/jencks-2.1-all.jar WEB-INF/lib/
check
cp -v $HOME/apps/${SMIX}/lib/jencks-2.1.jar WEB-INF/lib/
check
rm -fv WEB-INF/lib/geronimo-jta_1.0.1B_spec-1.1.jar
check
cp -v $HOME/apps/${SMIX}/lib/geronimo-connector-2.0.1.jar WEB-INF/lib/
check
cp -v $HOME/apps/${SMIX}/lib/geronimo-jta_1.1_spec-1.1.jar WEB-INF/lib/
check
cp -v $HOME/apps/${SMIX}/lib/geronimo-transaction-2.0.1.jar WEB-INF/lib/
check
cp -v $HOME/apps/${SMIX}/lib/backport-util-concurrent-2.2.jar WEB-INF/lib/
check
cp -v $HOME/apps/${SMIX}/lib/howl-1.0.1-1.jar WEB-INF/lib/
check
cp -v $HOME/apps/${SMIX}/lib/spring-tx-2.5.5.jar WEB-INF/lib/
check
rm -fv ../${SMIX_WEB}.war
check
jar -cvf ../${SMIX_WEB}.war *
check
popd
cp -v servicemix.xml $HOME/apps/smix_war/WEB-INF/
check
fi
