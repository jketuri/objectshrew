package org.consumercredit.client;

import java.lang.reflect.Method;
import java.net.URL;
import java.util.List;

import org.junit.Test;

public class LoanClientTest {
    
    @Test
    public void testLoanClient()
        throws Exception {
        LoanClient loanClient = LoanClient.getLoanClient(new URL("http://localhost:8080/loanService/?wsdl"));
        Object loanServicePort = loanClient.getLoanServicePort();
        Method start = loanServicePort.getClass().getMethod("start", new Class[] {java.lang.String.class});
        Object loanData = start.invoke(loanServicePort, new Object[] {"1"});
        Method getCustomerId = loanData.getClass().getMethod("getCustomerId");
        String customerId = (String)getCustomerId.invoke(loanData);
        Method getLoanItem = loanData.getClass().getMethod("getLoanItem");
        List loanItemList = (List)getLoanItem.invoke(loanData);
        Object loanItem = loanItemList.get(0);
        Method getLoanId = loanItem.getClass().getMethod("getLoanId");
        String loanId = (String)getLoanId.invoke(loanItem);
        System.out.println("customerId=" + customerId);
        System.out.println("loanId=" + loanId);
        Method retrieveCustomerData = loanServicePort.getClass().getMethod("retrieveCustomerData", new Class[] {String.class, String.class});
        Object customerData = retrieveCustomerData.invoke(loanServicePort, new Object[] {customerId, loanId});
        Method getFirstName = customerData.getClass().getMethod("getFirstName");
        String firstName = (String)getFirstName.invoke(customerData);
        Method getLastName = customerData.getClass().getMethod("getLastName");
        String lastName = (String)getLastName.invoke(customerData);
        Method getPersonId = customerData.getClass().getMethod("getPersonId");
        String personId = (String)getPersonId.invoke(customerData);
        System.out.println("personId=" + personId);
        System.out.println("firstName=" + firstName);
        System.out.println("lastName=" + lastName);
        Method complete = loanServicePort.getClass().getMethod("complete", new Class[] {String.class, String.class});
        Object loanDecision = complete.invoke(loanServicePort, new Object[] {customerId, loanId});
        Method getCreditScore = loanDecision.getClass().getMethod("getCreditScore");
        Object creditScore = getCreditScore.invoke(loanDecision);
        Method getDecision = loanDecision.getClass().getMethod("getDecision");
        Object decision = getDecision.invoke(loanDecision);
        Method value = decision.getClass().getMethod("value");
        Object decisionValue = value.invoke(decision);
        System.out.println("creditScore=" + creditScore);
        System.out.println("decision=" + decisionValue);
    }
    
}
