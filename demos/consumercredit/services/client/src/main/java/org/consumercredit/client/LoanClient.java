package org.consumercredit.client;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class LoanClient {

    Object servicePort = null;
    URL address = null;

    public static LoanClient getLoanClient(URL address) {
        LoanClient loanClient = new LoanClient();
        loanClient.address = address;
        return loanClient;
    }

    public Object getLoanServicePort()
        throws ClassNotFoundException {
        if (servicePort == null) {
            QName serviceName = new QName("http://org.consumercredit", "LoanService");
            Service service = Service.create(address, serviceName);
            servicePort = service.getPort(Class.forName("org.consumercredit.LoanServicePort"));
        }
        return servicePort;
    }
    
}
