
package org.consumercredit;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for startResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="startResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="loanData" type="{http://org.consumercredit}loanData" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "startResponse", propOrder = {
    "loanData"
})
public class StartResponse {

    protected LoanData loanData;

    /**
     * Gets the value of the loanData property.
     * 
     * @return
     *     possible object is
     *     {@link LoanData }
     *     
     */
    public LoanData getLoanData() {
        return loanData;
    }

    /**
     * Sets the value of the loanData property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanData }
     *     
     */
    public void setLoanData(LoanData value) {
        this.loanData = value;
    }

}
