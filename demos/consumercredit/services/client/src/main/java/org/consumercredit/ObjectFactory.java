
package org.consumercredit;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.consumercredit package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CompleteResponse_QNAME = new QName("http://org.consumercredit", "completeResponse");
    private final static QName _Complete_QNAME = new QName("http://org.consumercredit", "complete");
    private final static QName _Start_QNAME = new QName("http://org.consumercredit", "start");
    private final static QName _RetrieveCustomerData_QNAME = new QName("http://org.consumercredit", "retrieveCustomerData");
    private final static QName _RetrieveCustomerDataResponse_QNAME = new QName("http://org.consumercredit", "retrieveCustomerDataResponse");
    private final static QName _StartResponse_QNAME = new QName("http://org.consumercredit", "startResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.consumercredit
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link StartResponse }
     * 
     */
    public StartResponse createStartResponse() {
        return new StartResponse();
    }

    /**
     * Create an instance of {@link CustomerData }
     * 
     */
    public CustomerData createCustomerData() {
        return new CustomerData();
    }

    /**
     * Create an instance of {@link LoanData }
     * 
     */
    public LoanData createLoanData() {
        return new LoanData();
    }

    /**
     * Create an instance of {@link LoanDecision }
     * 
     */
    public LoanDecision createLoanDecision() {
        return new LoanDecision();
    }

    /**
     * Create an instance of {@link Complete }
     * 
     */
    public Complete createComplete() {
        return new Complete();
    }

    /**
     * Create an instance of {@link LoanItem }
     * 
     */
    public LoanItem createLoanItem() {
        return new LoanItem();
    }

    /**
     * Create an instance of {@link Start }
     * 
     */
    public Start createStart() {
        return new Start();
    }

    /**
     * Create an instance of {@link RetrieveCustomerDataResponse }
     * 
     */
    public RetrieveCustomerDataResponse createRetrieveCustomerDataResponse() {
        return new RetrieveCustomerDataResponse();
    }

    /**
     * Create an instance of {@link CompleteResponse }
     * 
     */
    public CompleteResponse createCompleteResponse() {
        return new CompleteResponse();
    }

    /**
     * Create an instance of {@link RetrieveCustomerData }
     * 
     */
    public RetrieveCustomerData createRetrieveCustomerData() {
        return new RetrieveCustomerData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompleteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://org.consumercredit", name = "completeResponse")
    public JAXBElement<CompleteResponse> createCompleteResponse(CompleteResponse value) {
        return new JAXBElement<CompleteResponse>(_CompleteResponse_QNAME, CompleteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Complete }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://org.consumercredit", name = "complete")
    public JAXBElement<Complete> createComplete(Complete value) {
        return new JAXBElement<Complete>(_Complete_QNAME, Complete.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Start }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://org.consumercredit", name = "start")
    public JAXBElement<Start> createStart(Start value) {
        return new JAXBElement<Start>(_Start_QNAME, Start.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveCustomerData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://org.consumercredit", name = "retrieveCustomerData")
    public JAXBElement<RetrieveCustomerData> createRetrieveCustomerData(RetrieveCustomerData value) {
        return new JAXBElement<RetrieveCustomerData>(_RetrieveCustomerData_QNAME, RetrieveCustomerData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveCustomerDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://org.consumercredit", name = "retrieveCustomerDataResponse")
    public JAXBElement<RetrieveCustomerDataResponse> createRetrieveCustomerDataResponse(RetrieveCustomerDataResponse value) {
        return new JAXBElement<RetrieveCustomerDataResponse>(_RetrieveCustomerDataResponse_QNAME, RetrieveCustomerDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://org.consumercredit", name = "startResponse")
    public JAXBElement<StartResponse> createStartResponse(StartResponse value) {
        return new JAXBElement<StartResponse>(_StartResponse_QNAME, StartResponse.class, null, value);
    }

}
