
package org.consumercredit;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for completeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="completeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="loanDecision" type="{http://org.consumercredit}loanDecision" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "completeResponse", propOrder = {
    "loanDecision"
})
public class CompleteResponse {

    protected LoanDecision loanDecision;

    /**
     * Gets the value of the loanDecision property.
     * 
     * @return
     *     possible object is
     *     {@link LoanDecision }
     *     
     */
    public LoanDecision getLoanDecision() {
        return loanDecision;
    }

    /**
     * Sets the value of the loanDecision property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanDecision }
     *     
     */
    public void setLoanDecision(LoanDecision value) {
        this.loanDecision = value;
    }

}
