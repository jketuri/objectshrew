
package org.consumercredit;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for formOfDwelling.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="formOfDwelling">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="rentedFlat"/>
 *     &lt;enumeration value="ownedFlat"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "formOfDwelling")
@XmlEnum
public enum FormOfDwelling {

    @XmlEnumValue("rentedFlat")
    RENTED_FLAT("rentedFlat"),
    @XmlEnumValue("ownedFlat")
    OWNED_FLAT("ownedFlat");
    private final String value;

    FormOfDwelling(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FormOfDwelling fromValue(String v) {
        for (FormOfDwelling c: FormOfDwelling.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
