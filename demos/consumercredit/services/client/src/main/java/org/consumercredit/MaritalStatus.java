
package org.consumercredit;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for maritalStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="maritalStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="unmarried"/>
 *     &lt;enumeration value="married"/>
 *     &lt;enumeration value="registeredRelationship"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "maritalStatus")
@XmlEnum
public enum MaritalStatus {

    @XmlEnumValue("unmarried")
    UNMARRIED("unmarried"),
    @XmlEnumValue("married")
    MARRIED("married"),
    @XmlEnumValue("registeredRelationship")
    REGISTERED_RELATIONSHIP("registeredRelationship");
    private final String value;

    MaritalStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MaritalStatus fromValue(String v) {
        for (MaritalStatus c: MaritalStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
