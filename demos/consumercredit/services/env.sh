if test "$1" = "tomcat"
then
export TOMCAT_HOME=$HOME/apache-tomcat-6.0.26
export TOMCAT_PORT=8080
else
export TOMCAT_HOME=$HOME/KeppiProxy
export TOMCAT_PORT=8000
fi
if test "$2" = "war"
then
export SMIX_TYPE=WAR
else
export SMIX_TYPE=STANDALONE
fi
export SMIX_DB_TYPE=EXTERNAL
export ODE_DB_TYPE=EMBEDDED
#export SMIX=apache-servicemix-4.1.0-SNAPSHOT
#export SMIX=apache-servicemix-3.3.1
export SMIX=apache-servicemix-3.3.2-SNAPSHOT
export SMIX_WEB=apache-servicemix-web-3.3.1
#export ODE=ode-jbi-2.0-beta3-SNAPSHOT
export ODE=ode-jbi-1.3.4-SNAPSHOT
#export ODE=ode-jbi-1.2
export PATH=/System/Library/Frameworks/JavaVM.framework/Versions/1.5/Home/bin:$PATH
export JAVA_OPTS="-XX:MaxPermSize=128M -Xmx512M -Dorg.apache.cxf.jaxws.checkPublishEndpointPermission=false"
#export JAVA_OPTS="-Dcom.sun.management.jmxremote.local.only=false -Djava.rmi.server.hostname=localhost"
export DERBY=db-derby-10.2.2.0-bin
export DERBY_HOME="$HOME/apps/${DERBY}"

pushd $HOME/laina/svn/laina_servicemix_poc/alpha/laina_workspace/services
