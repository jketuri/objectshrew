#!/bin/bash
check()
{
    if test "$?" != "0"
    then
        exit 1
    fi
}
pushd $TOMCAT_HOME
check
kill `cat ./derby_pid`
check
kill `cat ./smix_pid`
check
popd