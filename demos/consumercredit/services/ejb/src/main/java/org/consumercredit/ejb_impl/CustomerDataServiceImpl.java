package org.consumercredit.ejb_impl;

import javax.ejb.Stateless;

import org.consumercredit.ejb.CustomerDataService;
import org.consumercredit.ejb.CustomerDataServiceRemote;
import org.consumercredit.value.CustomerData;

@Stateless
public class CustomerDataServiceImpl
	implements CustomerDataService, CustomerDataServiceRemote {
	
	public CustomerData getCustomerData(String personId) {
		CustomerData customerData = new CustomerData();
		customerData.setFirstName("Mikki");
		return customerData;
	}
	
}