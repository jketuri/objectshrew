#!/bin/bash
set -e
DIR=`pwd`
BASE=$DIR/..
VAADIN_VERSION=8.0.0
GWT_VERSION=2.4.0

if test "$1" = "StyleCalendar" -o "$1" = ""; then
pushd $DIR
if test -d StyleCalendar
then
cd StyleCalendar
git pull
else
git clone -b Vaadin7 https://github.com/ripla/StyleCalendar.git
cd StyleCalendar
fi
mkdir -p WebContent/WEB-INF/lib
rm -fv build-widgetset.xml
sed -e "s%@version@%${VAADIN_VERSION}%" -e "s%^\(.*\)value=\"\${base}gwt\".*/>%\\1value=\"\${base}gwt-${GWT_VERSION}\"/>%" -e "s%^\(.*\)<property name=\"widgetset\" value=\".*\"/>%\\1<property name=\"widgetset\" value=\"org.vaadin.risto.stylecalendar.widgetset.StyleCalendarWidgetset\"/>%" -e "s%^\(.*\)<property name=\"jar-destination\" value=\".*\"/>%\\1<property name=\"jar-destination\" value=\"${dir}stylecalendar.jar\"/>%" -e "s%^\(.*\)<property name=\"widgetset-title\" value=\".*\"/>%\\1<property name=\"widgetset-title\" value=\"StyleCalendar\"/>%" "$DIR/build-widgetset.xml" >build-widgetset.xml
sed -e "s%^\(.*\)\.convertToModel(value, getLocale()));%\\1.convertToModel(value, Date.class, getLocale()));%" -e "s%^\(.*\)public Date convertToModel(String value, Locale locale)%\\1public Date convertToModel(String value, Class<? extends Date> targetType, Locale locale)%" -e "s%^\(.*\)public String convertToPresentation(Date value, Locale locale)%\\1public String convertToPresentation(Date value, Class<? extends String> targetType, Locale locale)%" addon/src/main/java/org/vaadin/risto/stylecalendar/StyleCalendarField.java >sed.out
mv -fv sed.out addon/src/main/java/org/vaadin/risto/stylecalendar/StyleCalendarField.java
#sed -e "s%^\(.*\)calendar\.removeListener(this);%\\1calendar.removeListener((com.vaadin.data.Property.ValueChangeListener)this);%" -i "" src/org/vaadin/risto/stylecalendar/StyleCalendarField.java
#cp -frv src/* ../../WEB-INF/src/
ant -f build-widgetset.xml compile-widgetset
ant -f build-widgetset.xml package-jar
popd
mkdir -p lib
cp -v "$DIR/StyleCalendar/stylecalendar.jar" lib/
mkdir -p "VAADIN/widgetsets/"
cp -rv "$DIR/StyleCalendar/WebContent/VAADIN/widgetsets/org.vaadin.risto.stylecalendar.widgetset.StyleCalendarWidgetset" "VAADIN/widgetsets/"
cp -rv "$DIR/StyleCalendar/demo/src/main/webapp/VAADIN/themes" "VAADIN/"
fi

if test "$1" = "g-leaflet" -o "$1" = ""; then
pushd $DIR
if test -d g-leaflet
then
cd g-leaflet
git pull
else
git clone https://github.com/mstahv/g-leaflet
cd g-leaflet
fi
mvn clean install -DskipTests -U
popd
fi

if test "$1" = "g-leaflet-draw" -o "$1" = ""; then
pushd $DIR
if test -d g-leaflet-draw
then
cd g-leaflet-draw
git pull
else
git clone https://github.com/mstahv/g-leaflet-draw
cd g-leaflet-draw
fi
VERSION=`grep "<version>" ../g-leaflet/pom.xml | head -n1 | sed -e "s%.*<version>\(.*\)</version>%\1%"`
sed -e '1h;2,$H;$!d;g' -e "s%\( *\)<artifactId>g-leaflet</artifactId>\([^<]*\)<version>[^<]*</version>%\1<artifactId>g-leaflet</artifactId>\2<version>$VERSION</version>%" pom.xml >sed.out
mv -fv sed.out pom.xml
mvn clean install -U
popd
fi

if test "$1" = "v-leaflet" -o "$1" = ""; then
pushd $DIR
if test -d v-leaflet
then
cd v-leaflet
git pull
else
git clone -b vaadin-8 https://github.com/mstahv/v-leaflet
cd v-leaflet
fi
mkdir -p src/main/java/org/bingmaps
cp -v $DIR/AppWidgetSet.gwt.xml src/main/java/org/bingmaps
cp -v "pom.xml" "$DIR"
#sed -e "s%<vaadin.version>.*</vaadin.version>%<vaadin.version>${VAADIN_VERSION}</vaadin.version>%" -e "s%<extraJvmArgs>.*</extraJvmArgs>%<extraJvmArgs>-Xmx512M -Xss1m</extraJvmArgs>%" -e "s%<module>org.vaadin.addon.leaflet.Widgetset</module>%<module>org.bingmaps.AppWidgetset</module>%" <"$DIR/pom.xml" >"pom.xml"
sed -e "s%<vaadin.version>.*</vaadin.version>%<vaadin.version>${VAADIN_VERSION}</vaadin.version>%" -e "s%<Vaadin-Widgetsets>.*</Vaadin-Widgetsets>%<Vaadin-Widgetsets>org.vaadin.addon.leaflet.Widgetset,org.bingmaps.AppWidgetset</Vaadin-Widgetsets>%" -e "s%<module>org.vaadin.addon.leaflet.Widgetset</module>%<module>org.bingmaps.AppWidgetset</module>%" -e "s%<extraJvmArgs>.*</extraJvmArgs>%<extraJvmArgs>-Xmx1024M -Xss1m</extraJvmArgs>%" -e "s%.*</dependencies>%<dependency>\
            <groupId>com.vaadin</groupId>\
            <artifactId>vaadin-compatibility-client</artifactId>\
            <version>\${vaadin.version}</version>\
        </dependency>\
</dependencies>%" <"$DIR/pom.xml" >"pom.xml"
VERSION=`grep "<version>" ../g-leaflet/pom.xml | head -n1 | sed -e "s%.*<version>\(.*\)</version>%\1%"`
sed -e '1h;2,$H;$!d;g' -e "s%\( *\)<artifactId>g-leaflet</artifactId>\([^<]*\)<version>[^<]*</version>%\1<artifactId>g-leaflet</artifactId>\2<version>$VERSION</version>%" pom.xml >sed.out
mv -fv sed.out pom.xml
export MAVEN_OPTS="-Xmx512m -Xss1m"
mvn clean install vaadin:compile -DskipTests -U
popd
mkdir -p lib
cp -rv "$DIR/v-leaflet/target/testwebapp/VAADIN/widgetsets/org.bingmaps.AppWidgetset" "VAADIN/widgetsets/"
#cp -rv "$DIR/v-leaflet/target/testwebapp/VAADIN/widgetsets/org.vaadin.addon.leaflet.Widgetset" "VAADIN/widgetsets/"
cp -v "$DIR/g-leaflet-draw/target/"*.jar "lib/"
cp -v "$DIR/g-leaflet/target/"*.jar "lib/"
cp -v "$DIR/v-leaflet/target/"*.jar "lib/"
cp -v "$DIR/v-leaflet/target/libs/"*.jar "lib/"
fi
