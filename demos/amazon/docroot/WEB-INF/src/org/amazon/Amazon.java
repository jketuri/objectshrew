/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amazon;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import org.vaadin.objectview.ObjectViewWebClient;

public class Amazon implements Serializable {
    private final static long serialVersionUID = 0L;
    private final static String HMAC_SHA1_ALGORITHM = "HmacSHA1";

    private Object serviceStatusModel = null;
    private String awsAccessKeyId = null;
    private String secretAccessKey = null;

    public Amazon(final AmazonObjectView amazonObjectView) {
        Properties properties = amazonObjectView.getProperties();
        awsAccessKeyId = properties.getProperty(AmazonObjectView.class.getName() + ".AWSAccessKeyId");
        secretAccessKey = properties.getProperty(AmazonObjectView.class.getName() + ".SecretAccessKey");
    }

    public String namespace() {
        return "";
    }

    public String modelServiceStatusWsdlURI() {
        return "https://fba-inventory.amazonaws.com/AmazonFBAInventory.wsdl";
    }

    public String modelServiceStatusWebServiceName() {
        return "AmazonFBAInventory";
    }

    public String modelServiceStatusWebServicePort() {
        return "AmazonFBAInventoryPort";
    }

    public String modelServiceStatusWebOperationName() {
        return "GetServiceStatus";
    }

    public Object getServiceStatusModel()
    {
        return serviceStatusModel;
    }

    public void setServiceStatusModel(final Object serviceStatusModel)
    {
        this.serviceStatusModel = serviceStatusModel;
    }

    public Header get_GetServiceStatusHeader()
        throws IllegalAccessException, InvalidKeyException, InvocationTargetException, NoSuchAlgorithmException,
               UnsupportedEncodingException
    {
        final Header header = new Header();
        header.set_AWSAccessKeyId(awsAccessKeyId);
        final String timestamp = getTimestamp();
        header.set_Timestamp(timestamp);
        final String signature = calculateSignature("GetServiceStatus" + timestamp, secretAccessKey);
        header.set_Signature(signature);
        return header;
    }

    private static String calculateSignature(final String stringToSign,
                                             final String secretKey)
        throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {
        // get an hmac_sha256 key from the raw key bytes
        final SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes("UTF-8"), HMAC_SHA1_ALGORITHM);
        // get an hmac_sha256 Mac instance and initialize with the signing key
        final Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
        mac.init(signingKey);
        // compute the hmac on input data bytes
        final byte[] rawHmac = mac.doFinal(stringToSign.getBytes());
        // base64-encode the hmac
        final String result = new String(Base64.encodeBase64(rawHmac));
        return result;
    }

    /**
     * Calculates a time stamp from "now" in UTC and returns it in ISO8601 string
     * format. The soap message expires 15 minutes after this time stamp.
     * AWS only supports UTC and it's canonical representation as 'Z' in an
     * ISO8601 time string. E.g.  2008-02-10T23:59:59Z
     * 
     * See http://www.w3.org/TR/xmlschema-2/#dateTime for more details.
     * 
     * @return ISO8601 time stamp string for "now" in UTC.
     */
    private String getTimestamp() {
        final SimpleDateFormat iso8601 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        iso8601.setTimeZone(TimeZone.getTimeZone("UTC"));
        return iso8601.format(new Date());
    }

}
