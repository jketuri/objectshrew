/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.amazon;

import java.io.Serializable;

public class Header implements Serializable {
    private final static long serialVersionUID = 0L;

    private String awsAccessKeyId = null;
    private String signature = null;
    private String timestamp = null;

    public boolean invisible() {
        return true;
    }

    public String get_AWSAccessKeyId() {
        return awsAccessKeyId;
    }

    public void set_AWSAccessKeyId(String awsAccessKeyId) {
        this.awsAccessKeyId = awsAccessKeyId;
    }

    public String namespace_AWSAccessKeyId() {
        return "http://security.amazonaws.com/doc/2007-01-01/";
    }

    public String get_Timestamp() {
        return timestamp;
    }

    public void set_Timestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String namespace_Timestamp() {
        return "http://security.amazonaws.com/doc/2007-01-01/";
    }

    public String get_Signature() {
        return signature;
    }

    public void set_Signature(String signature) {
        this.signature = signature;
    }

    public String namespace_Signature() {
        return "http://security.amazonaws.com/doc/2007-01-01/";
    }

}

