package org.bing.requests;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bing.enums.EntityType;
import org.bing.enums.ImageFormatType;
import org.bing.enums.ImageResolutionType;
import org.bing.enums.ImageryType;
import org.bing.enums.TravelModeType;
import org.bing.models.BoundingBox;
import org.bing.models.Coordinate;
import org.bing.models.ImageryPushpin;
import org.bing.models.RouteOptions;
import org.bing.models.SimpleWaypoint;

//https://msdn.microsoft.com/en-us/library/ff701724.aspx

/// <summary>
/// Requests an image from the REST imagery service.
/// </summary>
public class ImageryRequest extends BaseImageryRestRequest
{
    private ImageryType imagerySet;
    private Coordinate centerPoint;
    private boolean declutterPins;
    private ImageFormatType format;
    private BoundingBox mapArea;
    private boolean showTraffic;
    private int mapWidth = 350;
    private int mapHeight = 350;
    private boolean getMetadata;
    private String query;
    private int zoomLevel = 0;
    private boolean highlightEntity;
    private EntityType entityType;
    private List<ImageryPushpin> pushpins;
    private List<SimpleWaypoint> waypoints;
    private RouteOptions routeOptions;
    private ImageResolutionType resolution;

    private final int minMapWidth = 80;
    private final int minMapHeight = 80;
    private final int maxMapWidth = 2000;
    private final int maxMapHeight = 1500;

    /// <summary>
    /// The type of imagery for which you are requesting metadata.
    /// </summary>
    public ImageryType getImagerySet() { return this.imagerySet; }

    public void setImagerySet(ImageryType imagerySet) { this.imagerySet = imagerySet; }

    /// <summary>
    /// Required when imagerySet is Birdseye or BirdseyeWithLabels. Optional for other imagery sets. The center point to use for the imagery metadata.
    /// </summary>
    public Coordinate getCenterPoint() { return this.centerPoint; }

    public void setCenterPoint(Coordinate centerPoint) { this.centerPoint = centerPoint; }

    /// <summary>
    /// Specifies whether to change the display of overlapping pushpins so that they display separately on a map. 
    /// </summary>
    public boolean getDeclutterPins() { return this.declutterPins; }

    public void setDeclutterPins(boolean declutterPins) { this.declutterPins = declutterPins; }

    /// <summary>
    /// The image format to use for the static map.
    /// </summary>
    public ImageFormatType getOptionalFormat() { return this.format; }

    public void setOptionalFormat(ImageFormatType format) { this.format = format; }

    /// <summary>
    /// Required when a center point or set of route points are not specified. The geographic area to display on the map.
    /// </summary>
    public BoundingBox getMapArea() { return this.mapArea; }

    public void setMapArea(BoundingBox mapArea) { this.mapArea = mapArea; }

    /// <summary>
    /// Specifies if the traffic flow layer should be displayed on the map or not. Default is false.
    /// </summary>
    public boolean getShowTraffic() { return this.showTraffic; }

    public void setShowTraffic(boolean showTraffic) { this.showTraffic = showTraffic; }

    /// <summary>
    /// The width of the map. Default is 350px.
    /// </summary>
    public int getMapWidth() { return this.mapWidth; }

    public void setMapWidth(int value) {
        if (value < this.minMapWidth){
            this.mapWidth = this.minMapWidth;
        } else if (value > this.maxMapWidth) {
            this.mapWidth = this.maxMapWidth;
        } else {
            this.mapWidth = value;
        }
    }

    /// <summary>
    /// The height of the map. Default is 350px.
    /// </summary>
    public int getMapHeight() { return this.mapHeight; }

    public void setMapHeight(int value) {
        if (value < this.minMapHeight) {
            this.mapHeight = this.minMapHeight;
        } else if (value > this.maxMapHeight) {
            this.mapHeight = this.maxMapHeight;
        } else {
            this.mapHeight = value;
        }
    }

    /// <summary>
    /// Optional. Specifies whether to return metadata for the static map instead of the image. 
    /// The static map metadata includes the size of the static map and the placement and size 
    /// of the pushpins on the static map.
    /// </summary>
    public boolean getGetMetadata() { return this.getMetadata; }

    public void setGetMetadata(boolean GetMetadata) { this.getMetadata = getMetadata; }

    /// <summary>
    /// A query string that is used to determine the map location to display.
    /// </summary>
    public String getQuery() { return this.query; }

    public void setQuery(String query) { this.query = query; }

    /// <summary>
    /// The level of zoom to display.
    /// </summary>
    public int getZoomLevel() { return this.zoomLevel; }

    public void setZoomLevel(int value) {
        if (value < 1) {
            this.zoomLevel = 1;
        } else if (value > 21) {
            this.zoomLevel = 21;
        } else {
            this.zoomLevel = value;
        }
    }

    /// <summary>
    /// Highlights a polygon for an entity.
    /// </summary>
    public boolean getHighlightEntity() { return this.highlightEntity; }

    public void setHighlightEntity(boolean highlightEntity) { this.highlightEntity = highlightEntity; }

    /// <summary>
    /// Indicates the type of entity that should be highlighted. The entity of this type that 
    /// contains the centerPoint will be highlighted. Supported EntityTypes: CountryRegion, AdminDivision1, or PopulatedPlace.
    /// </summary>
    public EntityType getOptionalEntityType() { return this.entityType; }

    public void setOptionalEntityType(EntityType entityType) { this.entityType = entityType; }

    /// <summary>
    /// List of pushpins to display on the map. 
    /// </summary>
    public List<ImageryPushpin> getPushpins() { return this.pushpins; }

    public void setPushpins(List<ImageryPushpin> pushpins) { this.pushpins = pushpins; }

    /// <summary>
    /// Specifies two or more locations that define the route and that are in sequential order. 
    /// A route is defined by a set of waypoints and viaWaypoints (intermediate locations that the route must pass through). 
    /// You can have a maximum of 25 waypoints, and a maximum of 10 viaWaypoints between each set of waypoints. 
    /// The start and end points of the route cannot be viaWaypoints.
    /// </summary>
    public List<SimpleWaypoint> getWaypoints() { return this.waypoints; }

    public void setWaypoints(List<SimpleWaypoint> waypoints) { this.waypoints = waypoints; }

    /// <summary>
    /// Options for calculating route.
    /// </summary>
    public RouteOptions getRouteOptions() { return this.routeOptions; }

    public void setRouteOptions(RouteOptions routeOptions) { this.routeOptions = routeOptions; }

    /// <summary>
    /// Specifies the resolution of the labels on the image to retrieve.
    /// </summary>
    public ImageResolutionType getResolution() { return this.resolution; }

    public void setResolution(ImageResolutionType resolution) { this.resolution = resolution; }

    /// <summary>
    /// Gets the request URL. If both a Query and Address are specified, the Query value will be used. 
    /// Throws an exception if a Query or Address value is not specified.
    /// </summary>
    /// <returns>Geocode request URL for GET request.</returns>
    @Override
        public String getRequestUrl()
    {
        return getPostRequestUrl() + "&" + getPushpinsAsString();
    }

    /// <summary>
    /// Gets a URL for requesting imagery data for a POST request.
    /// </summary>
    /// <returns>Imagery request URL for POST request.</returns>
    public String getPostRequestUrl()
    {
        boolean isQuery = this.query != null && !this.query.trim().equals("");
        boolean isRoute = (this.waypoints != null && this.waypoints.size() >= 2);

        StringBuilder sb = new StringBuilder();
        sb.append("https://dev.virtualearth.net/REST/v1/Imagery/Map/");

        sb.append(this.imagerySet.toString());
            
        if (this.centerPoint != null) {
            sb.append(String.format("/{0:0.#####},{1:0.#####}/", this.centerPoint.getLat(), this.centerPoint.getLon()));
            if (this.zoomLevel != 0) {
                sb.append(this.zoomLevel);
            }
            else if (this.highlightEntity) {
                sb.append(this.entityType.toString());
            }
            else if (this.imagerySet == ImageryType.Road || this.imagerySet == ImageryType.Aerial || this.imagerySet == ImageryType.AerialWithLabels) {
                throw new RuntimeException("A zoom level or Entity type must be specified when when a center point is specified.");
            }
        } else if (isQuery) {
            try {
                sb.append(String.format("/{0}", URLEncoder.encode(this.query, "UTF-8")));
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException(ex);
            }
        }
            
        if (isRoute) {
            sb.append(String.format("/Routes/{0}?", (this.routeOptions != null ? this.routeOptions.getTravelMode() : TravelModeType.Driving).toString()));
        }
        else {
            sb.append("?");
        }

        sb.append(String.format("ms={0},{1}", mapWidth, mapHeight));

        if (this.declutterPins) {
            sb.append("&dcl=1");
        }

        if (this.format != null) {
            sb.append(String.format("&fmt={0}", this.format.toString()));
        }

        if (this.mapArea != null && (this.centerPoint == null || !isRoute)) {
            sb.append(String.format("&ma={0}", this.mapArea.toString()));
        }

        if (this.showTraffic) {
            sb.append("&ml=TrafficFlow");
        }

        if (this.getMetadata) {
            sb.append("&mmd=1");
        }

        if (this.highlightEntity) {
            sb.append("&he=1");
        }

        if (this.resolution == ImageResolutionType.High) {
            sb.append("&dpi=Large");
        }

        // Routing Parameters

        if (isRoute)
            {                
                if (this.waypoints.size() > 25) {
                    throw new RuntimeException("More than 25 waypoints in route request.");
                }

                for (int i = 0; i < this.waypoints.size(); i++) {

                    sb.append(String.format("&wp.{0}=", i));

                    if (this.waypoints.get(i).getCoordinate() != null) {
                        sb.append(String.format("{0:0.#####},{1:0.#####}", this.waypoints.get(i).getCoordinate().getLat(), this.waypoints.get(i).getCoordinate().getLon()));
                    } else {
                        try {
                            sb.append(String.format("{0}", URLEncoder.encode(this.waypoints.get(i).getAddress(), "UTF-8")));
                        } catch (UnsupportedEncodingException ex) {
                            throw new RuntimeException(ex);
                        }
                    }
                }

                if (this.routeOptions != null) {
                    sb.append(this.routeOptions.getUrlParam());
                }                
            }
            
        sb.append(getBaseRequestUrl());

        return sb.toString();
    }

    /// <summary>
    /// Returns the Pushpin information as a formatted string. 
    /// </summary>
    /// <returns>A formatted string of pushpin data.</returns>
    public String getPushpinsAsString()
    {
        //pp=38.889586530732335,-77.05010175704956;23;LMT&pp=38.88772364638439,-77.0472639799118;7;KMT

        if (this.pushpins != null && this.pushpins.size() > 0) {
            List<String> pushpins = new ArrayList<>();
            for (ImageryPushpin pushpin : this.pushpins) {
                pushpins.add(pushpin.toString());
            }
            String s = String.join("&", pushpins.toArray(new String[pushpins.size()]));
            return s;
        }

        return "";
    }

}
