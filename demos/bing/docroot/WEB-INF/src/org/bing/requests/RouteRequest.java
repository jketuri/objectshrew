package org.bing.requests;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.bing.enums.TravelModeType;
import org.bing.models.SimpleWaypoint;
import org.bing.models.RouteOptions;

/// <summary>
/// A request that calculates routes between waypoints.
/// </summary>
public class RouteRequest extends BaseRestRequest
{
    private SimpleWaypoints waypoints;
    private RouteOptions routeOptions;

    public static class SimpleWaypoints extends ArrayList<SimpleWaypoint> {}

    /// <summary>
    /// Specifies two or more locations that define the route and that are in sequential order. 
    /// A route is defined by a set of waypoints and viaWaypoints (intermediate locations that the route must pass through). 
    /// You can have a maximum of 25 waypoints, and a maximum of 10 viaWaypoints between each set of waypoints. 
    /// The start and end points of the route cannot be viaWaypoints.
    /// </summary>
    public SimpleWaypoints getWaypoints() { return this.waypoints; }

    public void setWaypoints(SimpleWaypoints waypoints) { this.waypoints = waypoints; }

    /// <summary>
    /// Options to use when calculate route.
    /// </summary>
    public RouteOptions getRouteOptions() { return this.routeOptions; }

    public void setRouteOptions(RouteOptions routeOptions) { this.routeOptions = this.routeOptions; }

    /// <summary>
    /// Gets the request URL to perform a query for route directions.
    /// </summary>
    /// <returns>A request URL to perform a query for route directions.</returns>
    @Override
    public String getRequestUrl()
    {
        //https://dev.virtualearth.net/REST/v1/Routes?wayPoint.1=wayPoint1&viaWaypoint.2=viaWaypoint2&waypoint.3=wayPoint3&wayPoint.n=wayPointn&heading=heading&optimize=optimize&avoid=avoidOptions&distanceBeforeFirstTurn=distanceBeforeFirstTurn&routeAttributes=routeAttributes&maxSolutions=maxSolutions&tolerances=tolerance1,tolerance2,tolerancen&distanceUnit=distanceUnit&mfa=mfa&key=BingMapsKey

        if (this.waypoints == null) {
            throw new RuntimeException("Waypoints not specified.");
        } else if (this.waypoints.size() < 2) {
            throw new RuntimeException("Not enough Waypoints specified.");
        } else if (this.waypoints.get(0).getIsViaPoint() || this.waypoints.get(this.waypoints.size() - 1).getIsViaPoint()) {
            throw new RuntimeException("Start and end waypoints must not be ViaWaypoints.");
        }

        StringBuilder sb = new StringBuilder();

        TravelModeType travelMode = (this.routeOptions != null) ? this.routeOptions.getTravelMode() : TravelModeType.Driving;

        sb.append(String.format("https://dev.virtualearth.net/REST/v1/Routes/%s?", travelMode.toString()));

        int wayCnt = 0, viaCnt = 0;

        for (int i = 0; i < this.waypoints.size(); i++) {
            if (this.waypoints.get(i).getIsViaPoint()) {
                sb.append(String.format("&vwp.%d=", i));
                viaCnt++;

                if (travelMode == TravelModeType.Transit) {
                    throw new RuntimeException("ViaWaypoints not supported for Transit directions.");
                }
            } else {
                sb.append(String.format("&wp.%d=", i));

                if (viaCnt > 10) {
                    throw new RuntimeException("More than 10 viaWaypoints between waypoints.");
                }

                wayCnt++;
                viaCnt = 0;
            }

            if (this.waypoints.get(i).getCoordinate() != null) {
                sb.append(String.format("%.5f,%.5f", this.waypoints.get(i).getCoordinate().getLat(), this.waypoints.get(i).getCoordinate().getLon()));
            } else {
                try {
                    sb.append(String.format("%s", URLEncoder.encode(this.waypoints.get(i).getAddress(), "UTF-8")));
                } catch (UnsupportedEncodingException ex) {
                    throw new RuntimeException(ex);
                }
            }
        }

        if (wayCnt > 25) {
            throw new RuntimeException("More than 25 waypoints in route request.");
        }

        if (this.routeOptions != null) {
            sb.append(this.routeOptions.getUrlParam());
        }    

        sb.append(getBaseRequestUrl());

        return sb.toString();
    }

}
