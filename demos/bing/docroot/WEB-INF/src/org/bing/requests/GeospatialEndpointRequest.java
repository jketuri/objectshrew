package org.bing.requests;

/// <summary>
/// The Geospatial Endpoint Service is a REST service that provides information about Geospatial Platform services 
/// for the language and geographical region you specify. The service information includes available service endpoints 
/// and language support for these endpoints. Disputed geographical areas and embargoed countries or regions that 
/// do not have any service support are also identified. 
/// https://msdn.microsoft.com/en-us/library/dn948525.aspx
/// </summary>
public class GeospatialEndpointRequest extends BaseRestRequest
{
    private boolean useHTTPS;

    /// <summary>
    /// When set to true the returned service URL's will use HTTPS.
    /// </summary>
    public boolean getUseHTTPS() { return this.useHTTPS; }

    public void setUseHTTPS(boolean useHTTPS) { this.useHTTPS = useHTTPS; }


    /// <summary>
    /// Gets the request URL. If both a Query and Address are specified, the Query value will be used. Throws an exception if a Query or Address value is not specified.
    /// </summary>
    /// <returns>Geocode request URL for GET request.</returns>
    @Override
        public String getRequestUrl()
    {
        String url = "https://dev.virtualearth.net/REST/v1/GeospatialEndpoint";

        if (this.culture != null && !this.culture.trim().equals("")) {
            url += String.format("/{0}", this.culture);
        } else {
            throw new RuntimeException("Invalid Language value specified.");
        }

        if (this.userRegion != null && !this.userRegion.trim().equals("")) {
            url += String.format("/{0}", this.userRegion);
        } else {
            throw new RuntimeException("Invalid UserRegion value specified.");
        }

        if (this.userLocation != null) {
            url += String.format("/{0}", this.userLocation);
        }

        if (this.useHTTPS) {
            url += "?uriScheme=http";
        } else {
            url += "?";
        }

        return url + getBaseRequestUrl();
    }

}
