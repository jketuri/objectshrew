package org.bing.requests;

/// <summary>
/// Abstract class that all Imagery rest requests will derive from.
/// </summary>
public abstract class BaseImageryRestRequest extends BaseRestRequest
{
}
