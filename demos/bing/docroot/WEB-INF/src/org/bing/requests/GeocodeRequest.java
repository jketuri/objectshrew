package org.bing.requests;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.bing.models.SimpleAddress;

/// <summary>
/// Geocodes a query to its coordinates.
/// </summary>
public class GeocodeRequest extends BaseRestRequest
{
    private String query;
    private SimpleAddress address;
    private int maxResults = 5;
    private boolean includeIso2;
    private boolean includeNeighborhood;

    /// <summary>
    /// A free form string address or Landmark. Overrides the Address values if both are specified.
    /// </summary>
    public String getOptionalQuery() { return this.query; }

    public void setOptionalQuery(String query) { this.query = query; }

    /// <summary>
    /// The Address to geocode.
    /// </summary>
    public SimpleAddress getOptionalAddress() { return address; }

    public void setOptionalAddress(SimpleAddress address) { this.address = address; }

    /// <summary>
    /// Specifies the maximum number of locations to return in the response.
    /// </summary>
    public int getMaxResults() { return this.maxResults; }

    public void setMaxResults(int value)
    {
        if (value > 0 && value <= 20) {
            this.maxResults = value;
        }
    }

    /// <summary>
    /// When you specified the two-letter ISO country code is included for addresses in the response. 
    /// </summary>
    public boolean getIncludeIso2() { return this.includeIso2; }

    public void setIncludeIso2(boolean IncludeIso2) { this.includeIso2 = includeIso2; }

    /// <summary>
    /// Specifies to include the neighborhood in the response when it is available. 
    /// </summary>
    public boolean getIncludeNeighborhood() { return this.includeNeighborhood; }

    public void setIncludeNeighborhood(boolean includeNeighborhood) { this.includeNeighborhood = includeNeighborhood; }

    /// <summary>
    /// Gets the request URL. If both a Query and Address are specified, the Query value will be used. Throws an exception if a Query or Address value is not specified.
    /// </summary>
    /// <returns>Geocode request URL for GET request.</returns>
    @Override
    public String getRequestUrl()
    {
        String url = "https://dev.virtualearth.net/REST/v1/Locations";
            
        try {
            if (this.query != null && !this.query.trim().equals("")) {
                url += String.format("?q=%s", URLEncoder.encode(this.query, "UTF-8"));
            }
            else if (this.address != null) {
                    String separator = "?";
                    if (this.address.getOptionalAddressLine() != null && !this.address.getOptionalAddressLine().trim().equals("")) {
                        url += String.format("%saddressLine=%s", separator, URLEncoder.encode(this.address.getOptionalAddressLine(), "UTF-8"));
                        separator = "&";
                    }

                    if (this.address.getOptionalLocality() != null && !this.address.getOptionalLocality().trim().equals("")) {
                        url += String.format("%slocality=%s", separator, URLEncoder.encode(this.address.getOptionalLocality(), "UTF-8"));
                        separator = "&";
                    }

                    if (this.address.getOptionalAdminDistrict() != null && !this.address.getOptionalAdminDistrict().trim().equals("")) {
                        url += String.format("%sadminDistrict=%s", separator, URLEncoder.encode(this.address.getOptionalAdminDistrict(), "UTF-8"));
                        separator = "&";
                    }

                    if (this.address.getOptionalPostalCode() != null && !this.address.getOptionalPostalCode().trim().equals("")) {
                        url += String.format("%spostalCode=%s", separator, URLEncoder.encode(this.address.getOptionalPostalCode(), "UTF-8"));
                        separator = "&";
                    }

                    if (this.address.getOptionalCountryRegion() != null && !this.address.getOptionalCountryRegion().trim().equals("")) {
                        url += String.format("%scountryRegion=%s", separator, URLEncoder.encode(this.address.getOptionalCountryRegion(), "UTF-8"));
                    }
                } else {
                throw new RuntimeException("No Query or Address value specified.");
            }
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
        if (this.includeIso2) {
            url += "&incl=ciso2";
        }
        if (this.includeNeighborhood) {
            url += "&inclnb=1";
        }
        if (this.maxResults != 5) {
            url += "&maxResults=" + this.maxResults;
        }
        return url + getBaseRequestUrl();
    }

}
