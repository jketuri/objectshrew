package org.bing.requests;

import org.bing.models.BoundingBox;
import org.bing.models.Coordinate;

/// <summary>
/// An abstract class in which all REST service requests derive from.
/// </summary>
public abstract class BaseRestRequest
{ 
    protected String bingMapsKey;
    protected String culture;
    protected BoundingBox userMapView;
    protected Coordinate userLocation;
    protected String userRegion;
    protected String userIp;

    public BaseRestRequest() {
        this.userMapView = new BoundingBox();
        this.userMapView.invisible = true;
    }

    /// <summary>
    /// The Bing Maps key for making the request.
    /// </summary>
    public String getOptionalBingMapsKey() { return this.bingMapsKey; }

    public void setOptionalBingMapsKey(String bingMapsKey) { this.bingMapsKey = bingMapsKey; }

    /// <summary>
    /// The culture to use for the request. An IETF language code, that includes the language and region code subtags, such as en-us or zh-hans. 
    /// </summary>
    public String getOptionalCulture() { return this.culture; }

    public void setOptionalCulture(String culture) { this.culture = culture; }

    /// <summary>
    /// The geographic region that corresponds to the current viewport.
    /// </summary>
    public BoundingBox getOptionalUserMapView() { return this.userMapView; }

    public void setOptionalUserMapView(BoundingBox userMapView) { this.userMapView = userMapView; }

    /// <summary>
    /// The user’s current position.
    /// </summary>
    public Coordinate getOptionalUserLocation() { return this.userLocation; }

    public void setOptionalUserLocation(Coordinate userLocation) { this.userLocation = userLocation; }

    /// <summary>
    /// An ISO 3166-1 alpha-2 region code, such as US, IN, and CN.
    /// </summary>
    public String getOptionalUserRegion() { return this.userRegion; }

    public void setOptionalUserRegion(String userRegion) { this.userRegion = userRegion; }

    /// <summary>
    /// An Internet Protocol version 4 (IPv4) address.
    /// </summary>
    public String getOptionalUserIp() { return this.userIp; }

    public void setOptionalUserIp(String userIp) { this.userIp = userIp; }

    /// <summary>
    /// Abstract method which generates the Bing Maps REST request URL.
    /// </summary>
    /// <returns>A Bing Maps REST request URL.</returns>
    public abstract String getRequestUrl();

    public String getBaseRequestUrl()
    {
        String url = "";

        if (this.culture != null && !this.culture.trim().equals("")) {
            url += "&c=" + this.culture;
        }

        if (this.userMapView != null && !this.userMapView.invisible()) {
            //South Latitude, West Longitude, North Latitude, East Longitude
            url += String.format("&umv=%s", this.userMapView.toString());
        } 

        if (this.userLocation != null && this.userLocation.getLat() != 0.0 && this.userLocation.getLon() != 0.0) {
            url += String.format("&ul=%s", this.userLocation);
        }

        if (this.userIp != null && !this.userIp.trim().equals("")) {
            url += "&uip=" + this.userIp;
        }

        return url + "&key=" + this.bingMapsKey + "&clientApi=CSToolkit";
    }
}
