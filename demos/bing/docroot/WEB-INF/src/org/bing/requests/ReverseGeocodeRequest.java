package org.bing.requests;

import java.util.ArrayList;
import java.util.List;

import  org.bing.enums.EntityType;
import  org.bing.models.Coordinate;

/// <summary>
/// Requests a that converts a coordinate into a location such as an address.
/// </summary>
public class ReverseGeocodeRequest extends BaseRestRequest
{
    private Coordinate point;
    private EntityTypes includeEntityTypes;
    private boolean includeIso2;
    private boolean includeNeighborhood;

    public static class EntityTypes extends ArrayList<EntityType> {}

    /// <summary>
    /// A central coordinate to perform the nearby search.
    /// </summary>
    public Coordinate getPoint() { return this.point; }
        
    public void setPoint(Coordinate point) { this.point = point; }

    /// <summary>
    /// Specifies the entity types that you want to return in the response. Only the types you specify will be returned. If the point cannot be mapped to the entity types you specify, no location information is returned in the response.
    /// </summary>
    public EntityTypes getIncludeEntityTypes() { return this.includeEntityTypes; }

    public void setIncludeEntityTypes(EntityTypes includeEntityTypes) { this.includeEntityTypes = includeEntityTypes; }

    /// <summary>
    /// When you specified the two-letter ISO country code is included for addresses in the response. 
    /// </summary>
    public boolean getIncludeIso2() { return this.includeIso2; }

    public void setIncludeIso2(boolean includeIso2) { this.includeIso2 = includeIso2; }

    /// <summary>
    /// Specifies to include the neighborhood in the response when it is available. 
    /// </summary>
    public boolean getIncludeNeighborhood() { return this.includeNeighborhood; }

    public void setIncludeNeighborhood(boolean includeNeighborhood) { this.includeNeighborhood = this.includeNeighborhood; }

    /// <summary>
    /// Gets the request URL to perform a reverse geocode query.
    /// </summary>
    /// <returns>A request URL to perform a reverse geocode query.</returns>
    @Override
    public String getRequestUrl()
    {
        String url = String.format("https://dev.virtualearth.net/REST/v1/Locations/%.5f,%.5f?", 
                                   this.point.getLat(),
                                   this.point.getLon());

        if (this.includeEntityTypes != null && this.includeEntityTypes.size() > 0) {
            url += "&includeEntityTypes=";
            for (int i = 0; i < this.includeEntityTypes.size(); i++) {
                url += this.includeEntityTypes.get(i).toString();

                if (i < this.includeEntityTypes.size() - 1) {
                    url += ",";
                }
            }
        }

        if (this.includeIso2) {
            url += "&incl=ciso2";
        }

        if (this.includeNeighborhood) {
            url += "&inclnb=1";
        }

        return url + getBaseRequestUrl();       
    }

}
