package org.bing.requests;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.bing.enums.DistanceUnitType;
import org.bing.enums.RouteAttributeType;
import org.bing.models.SimpleWaypoint;

/// <summary>
/// Requests routes from a location to major nearby roads.
/// </summary>
public class RouteMajorRoadsRequest extends BaseRestRequest
{
    private SimpleWaypoint destination;
    private boolean excludeInstructions;
    private DistanceUnitType distanceUnits;
    private RouteAttributeTypes routeAttributes;

    public static class RouteAttributeTypes extends ArrayList<RouteAttributeType> {}

    /// <summary>
    /// Specifies the final location for all the routes. 
    /// A destination can be specified as a Point, a landmark, or an address.
    /// </summary>
    public SimpleWaypoint getDestination() { return this.destination; }

    public void setDestination(SimpleWaypoint destination) { this.destination = destination; }

    /// <summary>
    /// Specifies to return only starting points for each major route in the response. 
    /// When this option is not specified, detailed directions for each route are returned. 
    /// </summary>
    public boolean getExcludeInstructions() { return this.excludeInstructions; }

    public void setExcludeInstructions(boolean excludeInstructions) { this.excludeInstructions = excludeInstructions; }

    /// <summary>
    /// The units to use for distance.
    /// </summary>
    public DistanceUnitType getDistanceUnits() { return this.distanceUnits; }

    public void setDistanceUnits(DistanceUnitType distanceUnits) { this.distanceUnits = distanceUnits; }

    /// <summary>
    /// Specifies to include or exclude parts of the routes response.
    /// </summary>
    public RouteAttributeTypes getRouteAttributes() { return this.routeAttributes; }

    public void setRouteAttributes(RouteAttributeTypes routeAttributes) { this.routeAttributes = routeAttributes; }

    /// <summary>
    /// Gets the request URL to perform a query for routes using major roads.
    /// </summary>
    /// <returns>A request URL to perform a query for routes using major roads.</returns>
    @Override
    public String getRequestUrl()
    {
        //https://dev.virtualearth.net/REST/v1/Routes/FromMajorRoads?destination=destination&exclude=routes&rpo=routePathOutput&du=distanceUnit&key=BingMapsKey

        if (this.destination == null || (this.destination.getCoordinate() == null && this.destination.getAddress() != null && !this.destination.getAddress().trim().equals(""))) {
            throw new RuntimeException("Destination value is invalid.");
        }

        String url = String.format("https://dev.virtualearth.net/REST/v1/Routes/FromMajorRoads?destination=");

        if (this.destination.getAddress() != null && !this.destination.getAddress().trim().equals("")) {
            try {
                url += URLEncoder.encode(this.destination.getAddress(), "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException(ex);
            }
        } else {
            url += String.format("%.5f,%.5f", this.destination.getCoordinate().getLat(), this.destination.getCoordinate().getLon());
        }

        if (this.excludeInstructions) {
            url += "&exclude=routes";
        }

        if (this.routeAttributes != null && this.routeAttributes.size() > 0) {
            url += "&ra=";

            for (int i = 0; i < this.routeAttributes.size(); i++) {
                url += this.routeAttributes.get(i).toString();

                if (i < this.routeAttributes.size() - 1) {
                    url += ",";
                }
            }
        }

        if (this.distanceUnits != DistanceUnitType.KM) {
            url += "&du=mi";
        }

        return url + getBaseRequestUrl();
    }

}
