package org.bing.requests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bing.enums.DistanceUnitType;
import org.bing.enums.ElevationType;
import org.bing.internal.SpatialTools;
import org.bing.models.BoundingBox;
import org.bing.models.Coordinate;

//http://msdn.microsoft.com/en-us/library/jj158961.aspx

/// <summary>
/// A request for elevation data.
/// </summary>
public class ElevationRequest extends BaseRestRequest
{
    private int row = 2, col = 2, samples = 0;
    private List<Coordinate> points;
    private BoundingBox bounds;
    private ElevationType height;
    private boolean getGeoidOffset;

    /// <summary>
    /// A set of coordinates on the Earth to use in elevation calculations. 
    /// The exact use of these points depends on the type of elevation request. 
    /// Overrides the Bounds value if both are specified.
    /// The maximum number of points is 1024.
    /// </summary>
    public List<Coordinate> getPoints() { return this.points; }

    public void setPoints(List<Coordinate> points) { this.points = points; }

    /// <summary>
    /// Specifies the rectangular area over which to provide elevation values. 
    /// </summary>
    public BoundingBox getBounds() { return this.bounds; }

    public void setBounds(BoundingBox bounds) { this.bounds = bounds; }

    /// <summary>
    /// Specifies the number of rows to use to divide the bounding box area into a grid. The rows and columns that define the bounding box each count as two (2) of the rows and columns. Elevation values are returned for all vertices of the grid. 
    /// </summary>
    public int getRow() { return this.row; }

    public void setRow(int value) {
        if (value < 2) {
            row = 2;
        }
        else if (value > 512) {
            row = 512;
        }
        else {
            row = value;
        }
    }

    /// <summary>
    /// Specifies the number of columns to use to divide the bounding box area into a grid. The rows and columns that define the bounding box each count as two (2) of the rows and columns. Elevation values are returned for all vertices of the grid. 
    /// </summary>
    public int getCol() { return this.col; }

    public void setCol(int value) {
        if (value < 2) {
            this.col = 2;
        } else if (value > 512) {
            this.col = 512;
        } else {
            this.col = value;
        }
    }

    /// <summary>
    /// Specifies the number of equally-spaced elevation values to provide along a polyline path. Used when Points value is set. Make = 1024
    /// </summary>
    public int getSamples() { return this.samples; }

    public void setSamples(int value) {
        if (value < 0) {
            this.samples = 0;
        } else if (value > 1024) {
            this.samples = 1024;
        } else {
            this.samples = value;
        }
    }

    /// <summary>
    /// Specifies which sea level model to use to calculate elevation.
    /// </summary>
    public ElevationType getHeight() { return this.height; }

    public void setHeight(ElevationType height) { this.height = height; }

    /// <summary>
    /// A boolean indicating if the offset from the geoid should be returned. Requires a list of points to be specified.
    /// </summary>
    public boolean getGetGeoidOffset() { return this.getGeoidOffset; }

    public void setGetGeoidOffset(boolean getGeoidOffset) { this.getGeoidOffset = getGeoidOffset; }

    /// <summary>
    /// Gets a URL for requesting elevation data for a GET request.
    /// </summary>
    /// <returns>Elevation request URL for GET request.</returns>
    @Override
        public String getRequestUrl()
    {
        return getPostRequestUrl() + "&" + getPointsAsString();
    }

    /// <summary>
    /// Gets a URL for requesting elevation data for a POST request.
    /// </summary>
    /// <returns>Elevation request URL for POST request.</returns>
    public String getPostRequestUrl()
    {
        //https://dev.virtualearth.net/REST/v1/Elevation/Bounds?bounds=boundingBox&rows=rows&cols=cols&heights=heights&key=BingMapsKey
        //https://dev.virtualearth.net/REST/v1/Elevation/SeaLevel?points=lat1,long1,lat2,long2,latn,longn&key=BingMapsKey
        //https://dev.virtualearth.net/REST/v1/Elevation/List?points=lat1,long1,lat2,long2,latn,longn&heights=heights&key=BingMapsKey
        //https://dev.virtualearth.net/REST/v1/Elevation/Polyline?points=lat1,long1,lat2,long2,latn,longn&heights=heights&samples=samples&key=BingMapsKey
            
        StringBuilder sb = new StringBuilder();
        sb.append("https://dev.virtualearth.net/REST/v1/Elevation/");

        String separator = "?";

        if (this.points != null && this.points.size() > 0)
            {
                if (this.getGeoidOffset)
                    {
                        //Get elevation geoid offsets
                        sb.append("SeaLevel");
                    }
                else if (samples == 0)
                    {
                        //Get elevations for a list of points
                        sb.append("List");
                    }
                else
                    {
                        //Get elevations along a Polyline with samples
                        sb.append(String.format("Polyline?samples={0}", samples));
                        separator = "&";
                    }
            }
        else if (this.bounds != null)
            {
                if (this.row * this.col > 1024) {
                    throw new RuntimeException("Row * Col value is greater than 1024.");
                }
                
                sb.append(String.format("Bounds?bounds={0}&rows={1}&cols={2}", this.bounds.toString(), row, col));
                separator = "&";
            }
        else {
            throw new RuntimeException("No Points or Bounds value specified.");
        }

        if (!this.getGeoidOffset && this.height == ElevationType.Ellipsoid)
            {
                sb.append(String.format("{0}height=ellipsoid", separator));
            }

        sb.append(String.format("{0}key={1}", separator, this.bingMapsKey));

        return sb.toString();
    }

    /// <summary>
    /// Returns the Point information as a formatted string. Only the first 1024 points will be used.
    /// </summary>
    /// <returns>A formatted string of point data.</returns>
    public String getPointsAsString()
    {
        //points=38.8895,77.0501,38.8877,-77.0472,38.8904,-77.0474,38.8896,77.0351

        StringBuilder sb = new StringBuilder();
        sb.append("points=");

        if (this.points != null && this.points.size() > 0)
            {
                int max = Math.min(this.points.size(), 1024);

                for (int i = 0; i < max; i++)
                    {
                        //Only need 5 decimal places. Any more are insignificant.
                        sb.append(String.format("{0:0.#####},{1:0.#####}", this.points.get(i).getLat(), this.points.get(i).getLon()));

                        if (i < max - 1) {
                            sb.append(",");
                        }
                    }
            }

        return sb.toString();
    }

    /// <summary>
    /// Gets a list of coordinates that are related to the returned index of the elevation data.
    /// </summary>
    /// <returns>A list of coordinates that are related to the index of the returned elevation data.</returns>
    public List<Coordinate> getElevationCoordinates()
    {            
        if (this.points != null && this.points.size() > 0) {
            if (this.getGeoidOffset || samples == 0) {
                return this.points;                    
            } else {
                //Calculate distance of polyline
                double totalDistance = 0;
                for (int i = 0; i < this.points.size() - 1; i++) {
                    totalDistance += SpatialTools.haversineDistance(this.points.get(i), this.points.get(i + 1), DistanceUnitType.KM);
                }

                double segmentLength = totalDistance / samples;
                    
                List<Coordinate> coords = new ArrayList<>(samples);
                coords.add(this.points.get(0));

                int idx = 0;

                //Calculate equally spaced coordinates along polyline
                for (int s = 0; s < samples; s++)
                    {
                        double dist = 0;
                        double travel = segmentLength * s;
                        double dx = travel;

                        for (int i = 0; i < this.points.size() - 1; i++) {
                            dist += SpatialTools.haversineDistance(this.points.get(i), this.points.get(i + 1), DistanceUnitType.KM);

                            if (dist >= travel) {
                                idx = i;
                                break;
                            }

                            dx = travel - dist;
                        }
                        
                        if (dx != 0 && idx < this.points.size() - 1) {
                            double bearing = SpatialTools.calculateBearing(this.points.get(idx), this.points.get(idx + 1));
                            coords.add(SpatialTools.calculateCoord(this.points.get(idx), bearing, dx, DistanceUnitType.KM));
                        }
                    }

                return coords;
            }
        }
        else if (this.bounds != null) {
            double dLat = Math.abs(this.bounds.getNorthLatitude() - this.bounds.getSouthLatitude()) / row;
            double dLon = Math.abs(this.bounds.getWestLongitude() - this.bounds.getEastLongitude()) / col;

            double x, y;

            Coordinate[] coords = new Coordinate[row * col];
            // The elevation values are ordered starting with the southwest corner, and then proceed west to east and south to north.
            for (int r = 0; r < row; r++) {
                y = this.bounds.getSouthLatitude() + (dLat * r);

                for (int c = 0; c < col; c++) {
                    x = this.bounds.getWestLongitude() + (dLon * c);

                    int idx = r * row + c;

                    coords[idx] = new Coordinate(y, x);
                }
            }

            return Arrays.asList(coords);
        }

        return null;
    }

}
