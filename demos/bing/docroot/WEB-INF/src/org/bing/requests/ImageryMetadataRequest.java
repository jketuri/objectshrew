package org.bing.requests;

import org.bing.enums.ImageryType;
import org.bing.models.Coordinate;

//https://msdn.microsoft.com/en-us/library/ff701716.aspx
    
/// <summary>
/// Requests imagery metadata information from Bing Maps.
/// </summary>
public class ImageryMetadataRequest extends BaseRestRequest
{
    private ImageryType imagerySet;
    private Coordinate centerPoint;
    private boolean getBasicInfo;
    private boolean includeImageryProviders;
    private double orientation = 0;
    private int zoomLevel = 0;
    private boolean useHTTPS;

    /// <summary>
    /// The type of imagery for which you are requesting metadata.
    /// </summary>
    public ImageryType getImagerySet() { return this.imagerySet; }

    public void setImagerySet(ImageryType imagerySet) { this.imagerySet = imagerySet; }

    /// <summary>
    /// Required when imagerySet is Birdseye or BirdseyeWithLabels. Optional for other imagery sets. The center point to use for the imagery metadata.
    /// </summary>
    public Coordinate getCenterPoint() { return centerPoint; }

    public void setCenterPoint(Coordinate centerPoint) { this.centerPoint = centerPoint; }

    /// <summary>
    /// Get only the basic metadata for an imagery set at a specific location. This URL does not return a map tile URL. 
    /// </summary>
    public boolean getGetBasicInfo() { return this.getBasicInfo; }

    public void setGetBasicInfo(boolean getBasicInfo) { this.getBasicInfo = getBasicInfo; }

    /// <summary>
    /// When you specified the two-letter ISO country code is included for addresses in the response. 
    /// </summary>
    public boolean getIncludeImageryProviders() { return this.includeImageryProviders; }

    public void setIncludeImageryProviders(boolean IncludeImageryProviders) { this.includeImageryProviders = includeImageryProviders; }

    /// <summary>
    /// The orientation of the viewport to use for the imagery metadata. This option only applies to Birdseye imagery.
    /// </summary>
    public double getOrientation() { return this.orientation; }

    public void setOrientation(double value) {
        if (value < 0) {
            orientation = value % 360 + 360;
        }
        else if (value > 360) {
            orientation = value % 360;
        }
        else {
            orientation = value;
        }
    }

    /// <summary>
    /// Required if a centerPoint is specified and imagerySet is set to Road, Aerial or AerialWithLabels The level of zoom to use for the imagery metadata.
    /// </summary>
    public int getZoomLevel() { return this.zoomLevel; }

    public void setZoomLevel(int value) {
        if (value < 1) {
            this.zoomLevel = 1;
        } else if (value > 21) {
            this.zoomLevel = 21;
        } else {
            this.zoomLevel = value;
        }
    }

    /// <summary>
    /// When set to true tile URL's will use HTTPS.
    /// </summary>
    public boolean getUseHTTPS() { return this.useHTTPS; }

    public void setUseHTTPS(boolean useHTTPS) { this.useHTTPS = useHTTPS; }

    /// <summary>
    /// Gets the request URL. Throws an exception if a zoom level is not specified when a centerPoint is specified when ImagerySet is Road, Aerial and AerialWithLabels.
    /// </summary>
    /// <returns>Imagery Metadata request URL for GET request.</returns>
    @Override
        public String getRequestUrl()
    {
        String url = "https://dev.virtualearth.net/REST/v1/Imagery/";

        if (this.getBasicInfo) {
            url += "BasicMetadata/";
        }
        else {
            url += "Metadata/";
        }

        url += this.imagerySet.toString();

        if (this.centerPoint != null)
            {
                url += String.format("/{0:0.#####},{1:0.#####}?", this.centerPoint.getLat(), this.centerPoint.getLon());

                if (this.imagerySet == ImageryType.Road || this.imagerySet == ImageryType.Aerial || this.imagerySet == ImageryType.AerialWithLabels) {
                    if (this.zoomLevel == 0) {
                        throw new RuntimeException("Zoom Level must be specified when a centerPoint is specified and ImagerySet is Road, Aerial and AerialWithLabels.");
                    } else {
                        url += "zl=" + zoomLevel;
                    }
                }
            } else {
            url += "?";
        }

        if (this.orientation != 0) {
            url += "&dir=" + this.orientation;
        }

        if (this.includeImageryProviders) {
            url += "&incl=ImageryProviders";
        }

        if (this.useHTTPS) {
            url += "&uriScheme=https";
        }

        return url + getBaseRequestUrl();
    }

}
