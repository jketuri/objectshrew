package org.bing.requests;

import org.bing.enums.SeverityType;
import org.bing.enums.TrafficType;
import org.bing.models.BoundingBox;

import java.util.ArrayList;
import java.util.List;

/// <summary>
/// Requests traffic information.
/// </summary>
public class TrafficRequest extends BaseRestRequest
{
    private BoundingBox mapArea;
    private boolean includeLocationCodes;
    private List<SeverityType> severity;
    private List<TrafficType> trafficType;

    /// <summary>
    /// Specifies the area to search for traffic incident information. 
    /// A rectangular area specified as a bounding box. 
    /// The size of the area can be a maximum of 500 km x 500 km. 
    /// </summary>
    public BoundingBox getMapArea() { return this.mapArea; }

    public void setMapArea(BoundingBox mapArea) { this.mapArea = mapArea; }

    /// <summary>
    /// Specifies whether to include traffic location codes in the response. 
    /// Traffic location codes provide traffic incident information for pre-defined road segments. 
    /// A subscription is typically required to be able to interpret these codes for a geographical area or country.
    /// Default is false.
    /// </summary>
    public boolean getIncludeLocationCodes() { return this.includeLocationCodes; }

    public void setIncludeLocationCodes(boolean includeLocationCodes) { this.includeLocationCodes = includeLocationCodes; }

    /// <summary>
    /// Specifies severity level of traffic incidents to return. 
    /// The default is to return traffic incidents for all severity levels.
    /// </summary>
    public List<SeverityType> getSeverity() { return this.severity; }

    public void setSeverity(List<SeverityType> severity) { this.severity = severity; }

    /// <summary>
    /// Specifies the type of traffic incidents to return.
    /// </summary>
    public List<TrafficType> getTrafficType() { return this.trafficType; }

    public void setTrafficType(List<TrafficType> trafficType) { this.trafficType = trafficType; }

    /// <summary>
    /// Gets a URL for requesting traffic data for a GET request.
    /// </summary>
    /// <returns>Traffic request URL for GET request.</returns>
    @Override
        public String getRequestUrl()
    {
        //URL Schema:
        //https://dev.virtualearth.net/REST/v1/Traffic/Incidents/mapArea/includeLocationCodes?severity=severity1,severity2,severityn&type=type1,type2,typen&key=BingMapsKey

        //Examples:
        //https://dev.virtualearth.net/REST/v1/Traffic/Incidents/37,-105,45,-94?key=YourBingMapsKey
        //https://dev.virtualearth.net/REST/V1/Traffic/Incidents/37,-105,45,-94/true?t=9,2&s=2,3&o=xml&key=BingMapsKey

        if (this.mapArea == null) {
            throw new RuntimeException("MapArea not specified.");
        }

        String url = String.format("https://dev.virtualearth.net/REST/V1/Traffic/Incidents/{0:0.#####},{1:0.#####},{2:0.#####},{3:0.#####}{4}",
                                   this.mapArea.getSouthLatitude(),
                                   this.mapArea.getWestLongitude(),
                                   this.mapArea.getNorthLatitude(),
                                   this.mapArea.getEastLongitude(),
                                   (this.includeLocationCodes)? "/true?" : "?");

        if (this.severity != null && this.severity.size() > 0) {
            List<String> severities = new ArrayList<>();
            for (SeverityType severity : this.severity) {
                severities.add(severity.toString());
            }
            url += "&severity=" + String.join(",", severities.toArray(new String[severities.size()]));
        }

        if (this.trafficType != null && this.trafficType.size() > 0) {
            List<String> trafficTypes = new ArrayList<>();
            for (TrafficType trafficType : this.trafficType) {
                trafficTypes.add(trafficType.toString());
            }
            url += "&type=" + String.join(",", trafficTypes.toArray(new String[trafficTypes.size()]));
        }

        return url + getBaseRequestUrl();
    }

}
