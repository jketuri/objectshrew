package org.bing.models;

/// <summary>
/// A simple address class that can be passed in to requests.
/// </summary>
public class SimpleAddress
{
    private String addressLine;
    private String locality;
    private String adminDistrict;
    private String postalCode;
    private String countryRegion;

    /// <summary>
    /// The official street line of an address relative to the area, as specified by the Locality, or PostalCode, properties. Typical use of this element would be to provide a street address or any official address.
    /// </summary>
    public String getOptionalAddressLine() { return this.addressLine; }

    public void setOptionalAddressLine(String addressLine) { this.addressLine = addressLine; }

    /// <summary>
    /// The locality, such as the city or neighborhood, that corresponds to an address.
    /// </summary>
    public String getOptionalLocality() { return this.locality; }

    public void setOptionalLocality(String locality) { this.locality = locality; }

    /// <summary>
    /// The subdivision name in the country or region for an address. This element is typically treated as the first order administrative subdivision, but in some cases it is the second, third, or fourth order subdivision in a country, dependency, or region.
    /// </summary>
    public String getOptionalAdminDistrict() { return this.adminDistrict;  }

    public void setOptionalAdminDistrict(String adminDistrict) { this.adminDistrict = adminDistrict; }

    /// <summary>
    /// The post code, postal code, or ZIP Code of an address.
    /// </summary>
    public String getOptionalPostalCode() { return this.postalCode; }

    public void setOptionalPostalCode(String postalCode) { this.postalCode = postalCode; }

    /// <summary>
    /// The ISO country code for the country. 
    /// </summary>
    public String getOptionalCountryRegion() { return this.countryRegion; }

    public void setOptionalCountryRegion(String countryRegion) { this.countryRegion = countryRegion; }
}
