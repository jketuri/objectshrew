package org.bing.models;

/// <summary>
/// Pushpin defination for Bing Maps REST imagery service: https://msdn.microsoft.com/en-us/library/ff701719.aspx
/// </summary>
public class ImageryPushpin
{
    private Coordinate location;
    private int iconStyle = 1;
    private String label = "";

    /// <summary>
    /// Coordinate to display pushpin.
    /// </summary>
    public Coordinate getLocation() { return this.location; }

    public void setLocation(Coordinate location) { this.location = location; }

    /// <summary>
    /// The icon style to use for the pushpin.
    /// </summary>
    public int getIconStyle() { return this.iconStyle; }

    public void setIconStyle(int value) {
        if (value < 0) {
            this.iconStyle = 0;
        } else {
            this.iconStyle = value;
        }
    }

    /// <summary>
    /// Label to display on top of pushpin.
    /// </summary>
    public String getLabel() { return this.label; }

    public void setLabel(String value) {
        if (value == null) {
            this.label = "";
        } else {
            this.label = value;
        }
    }

    @Override
        public String toString()
    {
        if (this.location != null) {
            return String.format("pp={0:0.#####},{1:0.#####};{2};{3}", this.location.getLat(), this.location.getLon(), this.iconStyle, this.label);
        } else {
            return "";
        }
    }

}

