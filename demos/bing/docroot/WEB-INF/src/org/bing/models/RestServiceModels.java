package org.bing.models;

import java.util.ArrayList;
import java.util.Date;

import org.bing.internal.DateTimeHelper;

public class RestServiceModels
{

    public static class DoubleList extends ArrayList<Double> {}

    public static class IntList extends ArrayList<Integer> {}

    public static class DoubleDoubleList extends ArrayList<ArrayList<Double>> {}

    public static class Address
    {
        private String addressLine;
        private String adminDistrict;
        private String adminDistrict2;
        private String countryRegion;
        private String locality;
        private String postalCode;
        private String countryRegionIso2;
        private String formattedAddress;
        private String neighborhood;
        private String landmark;

        public String getAddressLine() { return this.addressLine; }
        
        public void setAddressLine(String addressLine) { this.addressLine = addressLine; }

        public String getAdminDistrict() { return this.adminDistrict; }

        public void setAdminDistrict(String adminDistrict) { this.adminDistrict = adminDistrict; }

        public String getAdminDistrict2() { return this.adminDistrict2; }

        public void setAdminDistrict2(String adminDistrict2) { this.adminDistrict2 = adminDistrict2; }
        
        public String getCountryRegion() { return this.countryRegion; }

        public void setCountryRegion(String getCountryRegion) { this.countryRegion = countryRegion; }

        public String getLocality() { return this.locality; }

        public void setLocality(String locality) { this.locality = locality; }

        public String getPostalCode() { return this.postalCode; }

        public void setPostalCode(String postalCode) { this.postalCode = postalCode; }

        public String getCountryRegionIso2() { return this.countryRegionIso2; }

        public void setCountryRegionIso2(String countryRegionIso2) { this.countryRegionIso2 = countryRegionIso2; }

        public String getFormattedAddress() { return this.formattedAddress; }

        public void setFormattedAddress(String formattedAddress) { this.formattedAddress = formattedAddress; }

        public String getNeighborhood() { return this.neighborhood; }

        public void setNeighborhood(String getNeighborhood) { this.neighborhood = neighborhood; }

        public String getLandmark() { return this.landmark; }

        public void setLandmark(String landmark) { this.landmark = landmark; }
    }

    public static class CoverageArea {
        private DoubleList boundingBox;
        private int zoomMax;
        private int zoomMin;

        /// <summary>
        /// Bounding box of the coverage area. Structure [South Latitude, West Longitude, North Latitude, East Longitude]
        /// </summary>
        public DoubleList getBbox() { return this.boundingBox; }

        public void setBbox(DoubleList boundingBox) { this.boundingBox = boundingBox; }

        public int getZoomMax() { return this.zoomMax; }

        public void setZoomMax(int zoomMax) { this.zoomMax = zoomMax; }

        public int getZoomMin() { return this.zoomMin; }

        public void setZoomMin(int zoomMin) { this.zoomMin = zoomMin; }
    }

    // !!    [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public static class BirdseyeMetadata extends ImageryMetadata {
        private double orientation;
        private int tilesX;
        private int tilesY;

        public double getOrientation() { return this.orientation; }

        public void setOrientation(double orientation) { this.orientation = orientation; }

        public int getTilesX() { return this.tilesX; }

        public void setTilesX(int tilesX) { this.tilesX = tilesX; }

        public int getTilesY() { return this.tilesY; }

        public void setTilesY(int tilesY) { this.tilesY = tilesY; }
    }

    public static class Detail {
        private int compassDegrees;
        private String maneuverType;
        private int[] startPathIndices;
        private int[] endPathIndices;
        private String roadType;
        private String[] locationCodes;
        private String[] names;
        private String mode;
        private RoadShield roadShieldRequestParameters;

        public int getCompassDegrees() { return this.compassDegrees; }

        public void setCompassDegrees(int compassDegrees) { this.compassDegrees = compassDegrees; }

        public String getManeuverType() { return this.maneuverType; }

        public void setManeuverType(String maneuverType) { this.maneuverType = maneuverType; }

        public int[] getStartPathIndices() { return this.startPathIndices; }

        public void setStartPathIndices(int[] startPathIndices) { this.startPathIndices = startPathIndices; }

        public int[] getEndPathIndices() { return this.endPathIndices; }

        public void setEndPathIndices(int[] endPathIndices) { this.endPathIndices = endPathIndices; }

        public String getRoadType() { return this.roadType; }

        public void setRoadType(String roadType) { this.roadType = roadType; }

        public String[] getLocationCodes() { return this.locationCodes; }

        public void setLocationCodes(String[] locationCodes) { this.locationCodes = locationCodes; }

        public String[] getNames() { return this.names; }

        public void setNames(String[] names) { this.names = names; }

        public String getMode() { return this.mode; }

        public void setMode(String mode) { this.mode = mode; }

        public RoadShield getRoadShieldRequestParameters() { return this.roadShieldRequestParameters; }

        public void setRoadShieldRequestParameters(RoadShield roadShieldRequestParameters) { this.roadShieldRequestParameters = roadShieldRequestParameters; }
    }

    public static class Generalization {
        private IntList pathIndices;
        private double latLongTolerance;

        public IntList getPathIndices() { return this.pathIndices = pathIndices; }

        public void setPathIndices(IntList pathIndices) { this.pathIndices = pathIndices; }

        public double getLatLongTolerance() { return this.latLongTolerance; }
    
        public void setLatLongTolerance(double latLongTolerance) { this.latLongTolerance = latLongTolerance; }
    }

    public static class Hint {
        private String hintType;
        private String text;

        public String getHintType() { return this.hintType; }

        public void setHintType(String hintType) { this.hintType = hintType; }

        public String getText() { return this.text; }

        public void setText(String text) { this.text = text; }
    }

    // !!   [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public static class ImageryMetadata extends Resource {
        private int imageHeight;
        private int imageWidth;
        private ImageryProvider[] imageryProviders;
        private String imageUrl;
        private String[] imageUrlSubdomains;
        private String vintageEnd;
        private String vintageStart;
        private int zoomMax;
        private int zoomMin;

        public int getImageHeight() { return this.imageHeight; }

        public void setImageHeight(int getImageHeight) { this.imageHeight = imageHeight; }

        public int getImageWidth() { return this.imageWidth; }

        public void setImageWidth(int getImageWidth) { this.imageWidth = imageWidth; }

        public ImageryProvider[] ImageryProviders() { return this.imageryProviders; }

        public void setImageryProviders(ImageryProvider[] ImageryProviders) { this.imageryProviders = imageryProviders; }

        public String getImageUrl() { return this.imageUrl; }

        public void setImageUrl(String imageUrl) { this.imageUrl = imageUrl; }

        public String[] ImageUrlSubdomains() { return this.imageUrlSubdomains; }

        public void setImageUrlSubdomains(String[] ImageUrlSubdomains) { this.imageUrlSubdomains = imageUrlSubdomains; }

        public String getVintageEnd() { return this.vintageEnd; }

        public void setVintageEnd(String vintageEnd) { this.vintageEnd = vintageEnd; }

        public String getVintageStart() { return this.vintageStart; }

        public void setVintageStart(String vintageStart) { this.vintageStart = vintageStart; }

        public int getZoomMax() { return this.zoomMax; }

        public void setZoomMax(int zoomMax) { this.zoomMax = zoomMax; }

        public int getZoomMin() { return this.zoomMin; }

        public void setZoomMin(int ZoomMin) { this.zoomMin = zoomMin; }

    }

    public static class ImageryProvider {
        private String attribution;
        private CoverageArea[] coverageAreas;

        public String getAttribution() { return this.attribution; }

        public void setAttribution(String attribution) { this.attribution = attribution; }

        public CoverageArea[] CoverageAreas() { return this.coverageAreas; }

        public void setCoverageAreas(CoverageArea[] CoverageAreas) { this.coverageAreas = coverageAreas; }

    }

    public static class Instruction {
        private String maneuverType;
        private String text;
        private String formattedText;

        public String getManeuverType() { return this.maneuverType; }

        public void setManeuverType(String getManeuverType) { this.maneuverType = maneuverType; }

        public String getText() { return this.text; }

        public void setText(String text) { this.text = text; }

        public String getFormattedText() { return this.formattedText; }

        public void setFormattedText(String formattedText) { this.formattedText = formattedText; }

    }

    public static class ItineraryItem extends BasicItineraryItem {
        private BasicItineraryItem[] childItineraryItems;

        public BasicItineraryItem[] getChildItineraryItems() { return this.childItineraryItems; }

        public void setChildItineraryItems(BasicItineraryItem[] childItineraryItems) { this.childItineraryItems = childItineraryItems; }

    }

    public static class BasicItineraryItem {
        private String compassDirection;
        private Detail[] details;
        private String exit;
        private Hint[] hints;
        private String iconType;
        private Instruction instruction;
        private Point maneuverPoint;
        private String sideOfStreet;
        private String[] signs;
        private String time;
        private Date timeUtc;
        private String tollZone;
        private String towardsRoadName;
        private TransitLine transitLine;
        private int transitStopId;
        private String transitTerminus;
        private double travelDistance;
        private double travelDuration;
        private String travelMode;
        private Warning[] warnings;

        public String getCompassDirection() { return this.compassDirection; }

        public void setCompassDirection(String getCompassDirection) { this.compassDirection = compassDirection; }

        public Detail[] getDetails() { return this.details; }

        public void setDetails(Detail[] Details) { this.details = details; }

        public String getExit() { return this.exit; }

        public void setExit(String exit) { this.exit = exit; }

        public Hint[] getHints() { return this.hints; }

        public void setHints(Hint[] hints) { this.hints = hints; }

        public String getIconType() { return this.iconType; }

        public void setIconType(String iconType) { this.iconType = iconType; }

        public Instruction getInstruction() { return this.instruction; }

        public void setInstruction(Instruction instruction) { this.instruction = instruction; }

        public Point getManeuverPoint() { return this.maneuverPoint; }

        public void setManeuverPoint(Point maneuverPoint) { this.maneuverPoint = maneuverPoint; }

        public String getSideOfStreet() { return this.sideOfStreet; }

        public void setSideOfStreet(String sideOfStreet) { this.sideOfStreet = sideOfStreet; }

        public String[] getSigns() { return this.signs; }

        public void setSigns(String[] signs) { this.signs = signs; }

        public String getTime() { return this.time; }

        public void setTime(String time) { this.time = time; }

        public Date getTimeUtc() {
            if (this.time == null || "".equals(this.time)) {
                return new Date();
            } else {
                return DateTimeHelper.fromOdataJson(this.time);
            }
        }

        public void setTimeUtc(Date value) {
            if (value == null) {
                this.time = "";
            } else {
                String v = DateTimeHelper.toOdataJson(value);
                if (v != null) {
                    this.time = v;
                } else {
                    this.time = "";
                }
            }
        }

        public String getTollZone() { return this.tollZone; }

        public void setTollZone(String tollZone) { this.tollZone = tollZone; }

        public String getTowardsRoadName() { return this.towardsRoadName; }
    
        public void setTowardsRoadName(String towardsRoadName) { this.towardsRoadName = towardsRoadName; }

        public TransitLine getTransitLine() { return this.transitLine; }

        public void setTransitLine(TransitLine transitLine) { this.transitLine = transitLine; }

        public int getTransitStopId() { return this.transitStopId; }

        public void setTransitStopId(int transitStopId) { this.transitStopId = transitStopId; }

        public String getTransitTerminus() { return this.transitTerminus; }

        public void setTransitTerminus(String transitTerminus) { this.transitTerminus = transitTerminus; }

        public double getTravelDistance() { return this.travelDistance; }

        public void setTravelDistance(double travelDistance) { this.travelDistance = travelDistance; }

        public double getTravelDuration() { return this.travelDuration; }

        public void setTravelDuration(double travelDuration) { this.travelDuration = travelDuration; }

        public String getTravelMode() { return this.travelMode; }

        public void setTravelMode(String travelMode) { this.travelMode = travelMode; }

        public Warning[] getWarnings() { return this.warnings; }

        public void setWarnings(Warning[] warnings) { this.warnings = warnings; }

    }

    public static class Line {
        private String type;
        private DoubleDoubleList coordinates;

        public String getType() { return this.type; }

        public void setType(String type) { this.type = type; }

        public DoubleDoubleList Coordinates() { return this.coordinates; }

        public void setCoordinates(DoubleDoubleList coordinates) { this.coordinates = coordinates; }

    }

    // !!    [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public static class Location extends Resource {
        private String name;
        private Point point;
        private String entityType;
        private Address address;
        private String confidence;
        private String[] matchCodes;
        private Point[] geocodePoints;
        private QueryParseValue[] queryParseValues;

        public String getName() { return this.name; }

        public void setName(String name) { this.name = name; }

        public Point getPoint() { return this.point; }

        public void setPoint(Point point) { this.point = point; }

        public String getEntityType() { return this.entityType; }

        public void setEntityType(String entityType) { this.entityType = entityType; }

        public Address getAddress() { return this.address; }

        public void setAddress(Address address) { this.address = address; }

        public String getConfidence() { return this.confidence; }

        public void setConfidence(String confidence) { this.confidence = confidence; }

        public String[] getMatchCodes() { return this.matchCodes; }

        public void setMatchCodes(String[] matchCodes) { this.matchCodes = matchCodes; }

        public Point[] getGeocodePoints() { return this.geocodePoints; }

        public void setGeocodePoints(Point[] geocodePoints) { this.geocodePoints = geocodePoints; }

        public QueryParseValue[] getQueryParseValues() { return this.queryParseValues; }

        public void setQueryParseValues(QueryParseValue[] queryParseValues) { this.queryParseValues = queryParseValues; }
    }

    public static class QueryParseValue {
        private String property;
        private String value;

        public String getProperty() { return this.property; }

        public void setProperty(String property) { this.property = property; }

        public String getValue() { return this.value; }

        public void setValue(String value) { this.value = value; }
    }

    // !!    [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public static class PushpinMetdata {
        private Pixel anchor;
        private Pixel bottomRightOffset;
        private Pixel topLeftOffset;
        private Point point;

        public Pixel getAnchor() { return this.anchor; }

        public void setAnchor(Pixel anchor) { this.anchor = anchor; }

        public Pixel getBottomRightOffset() { return this.bottomRightOffset; }

        public void setBottomRightOffset(Pixel bottomRightOffset) { this.bottomRightOffset = bottomRightOffset; }

        public Pixel getTopLeftOffset() { return this.topLeftOffset; }

        public void setTopLeftOffset(Pixel topLeftOffset) { this.topLeftOffset = topLeftOffset; }

        public Point getPoint() { return this.point; }

        public void setPoint(Point point) { this.point = point; }

    }

    // !!    [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public static class Pixel {
        private String x;
        private String y;

        public String getX() { return this.x; }

        public void setX(String x) { this.x = x; }

        public String getY() { return this.y; }

        public void setY(String y) { this.y = y; }
    }

    public static class Point extends Shape {
        private String type;
        private double[] coordinates;
        private String calculationMethod;
        private String[] usageTypes;

        public String getType() { return this.type; }

        public void setType(String type) { this.type = type; }

        /// <summary>
        /// Latitude,Longitude
        /// </summary>
        public double[] getCoordinates() { return this.coordinates; }

        public void setCoordinates(double[] coordinates) { this.coordinates = coordinates; }

        public String getCalculationMethod() { return this.calculationMethod; }

        public void setCalculationMethod(String calculationMethod) { this.calculationMethod = calculationMethod; }

        public String[] getUsageTypes() { return this.usageTypes; }

        public void setUsageTypes(String[] usageTypes) { this.usageTypes = usageTypes; }

        public Coordinate getCoordinate() {
            if (this.coordinates.length >= 2) {
                return new Coordinate(this.coordinates[0], this.coordinates[1]);
            }

            return null;
        }

    }

    public static class Resource {
        private DoubleList boundingBox;
        private String type;

        /// <summary>
        /// Bounding box of the response. Structure [South Latitude, West Longitude, North Latitude, East Longitude]
        /// </summary>
        public DoubleList getBbox() { return this.boundingBox; }

        public void setBbox(DoubleList boundingBox) { this.boundingBox = boundingBox; }

        public String get__type() { return this.type; }

        public void set__type(String type) { this.type = type; }
    }

    public static class ResourceSet {
        private long estimatedTotal;

        public long getEstimatedTotal() { return this.estimatedTotal; }

        public void setEstimatedTotal(long estimatedTotal) { this.estimatedTotal = estimatedTotal; }
    }

    public static class GeocodeResourceSet extends ResourceSet {
        private Location[] resources;

        public Location[] getResources() { return this.resources; }

        public void setResources(Location[] resources) { this.resources = resources; }
    }

    public static class RouteResourceSet extends ResourceSet {
        private Route route;

        public Route getResources() { return this.route; }

        public void setResources(Route route) { this.route = route; }
    }

    public static class Response {
        private String copyright;
        private String brandLogoUri;
        private int statusCode;
        private String statusDescription;
        private String authenticationResultCode;
        private String[] errorDetails;
        private String traceId;

        public String getCopyright() { return this.copyright; }

        public void setCopyright(String copyright) { this.copyright = copyright; }

        public String getBrandLogoUri() { return this.brandLogoUri; }

        public void setBrandLogoUri(String brandLogoUri) { this.brandLogoUri = brandLogoUri; }

        public int getStatusCode() { return this.statusCode; }

        public void setStatusCode(int statusCode) { this.statusCode = statusCode; }

        public String getStatusDescription() { return this.statusDescription; }

        public void setStatusDescription(String statusDescription) { this.statusDescription = statusDescription; }

        public String getAuthenticationResultCode() { return this.authenticationResultCode; }

        public void setAuthenticationResultCode(String authenticationResultCode) { this.authenticationResultCode = authenticationResultCode; }

        public String[] getErrorDetails() { return this.errorDetails; }

        public void setErrorDetails(String[] errorDetails) { this.errorDetails = errorDetails; }

        public String getTraceId() { return this.traceId; }

        public void setTraceId(String traceId) { this.traceId = traceId; }
    }

    public static class GeocodeResponse extends Response {
        private GeocodeResourceSet[] resourceSets;

        public GeocodeResourceSet[] getResourceSets() { return this.resourceSets; }

        public void setResourceSets(GeocodeResourceSet[] resourceSets) { this.resourceSets = resourceSets; }

    }

    public static class RouteResponse extends Response {
        private RouteResourceSet resourceSets;

        public RouteResourceSet getResourceSets() { return this.resourceSets; }

        public void setResourceSets(RouteResourceSet resourceSets) { this.resourceSets = resourceSets; }

    }

    public static class RoadShield {
        private int bucket;
        private Shield[] shields;

        public int getBucket() { return this.bucket; }

        public void setBucket(int bucket) { this.bucket = bucket; }

        public Shield[] getShields() { return this.shields; }

        public void setShields(Shield[] shields) { this.shields = shields; }
    }

    // !!    [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public static class Route extends Resource {
        private String id;
        private String distanceUnit;
        private String durationUnit;
        private double travelDistance;
        private double travelDuration;
        private double travelDurationTraffic;
        private String trafficCongestion;
        private String trafficDataUsed;
        private RouteLegs routeLegs;
        private RoutePath routePath;

        public static class RouteLegs extends ArrayList<RouteLeg> {}

        public String getId() { return this.id; }

        public void setId(String id) { this.id = id; }

        public String getDistanceUnit() { return this.distanceUnit; }

        public void setDistanceUnit(String distanceUnit) { this.distanceUnit = distanceUnit; }

        public String getDurationUnit() { return this.durationUnit; }

        public void setDurationUnit(String durationUnit) { this.durationUnit = durationUnit; }

        public double getTravelDistance() { return this.travelDistance; }

        public void setTravelDistance(double travelDistance) { this.travelDistance = travelDistance; }

        public double getTravelDuration() { return this.travelDuration; }

        public void setTravelDuration(double travelDuration) { this.travelDuration = travelDuration; }

        public double getTravelDurationTraffic() { return this.travelDurationTraffic; }

        public void setTravelDurationTraffic(double travelDurationTraffic) { this.travelDurationTraffic = travelDurationTraffic; }

        public String getTrafficCongestion() { return this.trafficCongestion; }

        public void setTrafficCongestion(String trafficCongestion) { this.trafficCongestion = trafficCongestion; }

        public String getTrafficDataUsed() { return this.trafficDataUsed; }

        public void setTrafficDataUsed(String trafficDataUsed) { this.trafficDataUsed = trafficDataUsed; }

        public RouteLegs getFormRouteLegs() { return this.routeLegs; }

        public void setFormRouteLegs(RouteLegs routeLegs) { this.routeLegs = routeLegs; }

        public RoutePath getRoutePath() { return this.routePath; }

        public void setRoutePath(RoutePath routePath) { this.routePath = routePath; }
    }

    public static class RouteLeg {
        private double travelDistance;
        private double travelDuration;
        private double cost;
        private String description;
        private Point actualStart;
        private Point actualEnd;
        private Location startLocation;
        private Location endLocation;
        private ItineraryItems itineraryItems;
        private String routeRegion;
        private RouteSubLegs routeSubLegs;
        private String startTime;
        private Date startTimeUtc;
        private String endTime;
        private Date endTimeUtc;
        private Object[] alternateVias;

        public static class ItineraryItems extends ArrayList<ItineraryItem> {}

        public static class RouteSubLegs extends ArrayList<RouteSubLeg> {}

        public double getTravelDistance() { return this.travelDistance; }

        public void setTravelDistance(double travelDistance) { this.travelDistance = travelDistance; }

        public double getTravelDuration() { return this.travelDuration; }

        public void setTravelDuration(double travelDuration) { this.travelDuration = travelDuration; }

        public double getCost() { return this.cost; }

        public void setCost(double cost) { this.cost = cost; }

        public String getDescription() { return this.description; }

        public void setDescription(String description) { this.description = description; }

        public Point getActualStart() { return this.actualStart; }

        public void setActualStart(Point actualStart) { this.actualStart = actualStart; }

        public Point getActualEnd() { return this.actualEnd; }

        public void setActualEnd(Point actualEnd) { this.actualEnd = actualEnd; }

        public Location getStartLocation() { return this.startLocation; }

        public void setStartLocation(Location startLocation) { this.startLocation = startLocation; }

        public Location getEndLocation() { return this.endLocation; }

        public void setEndLocation(Location EndLocation) { this.endLocation = endLocation; }

        public ItineraryItems getFormItineraryItems() { return this.itineraryItems; }

        public void setFormItineraryItems(ItineraryItems itineraryItems) { this.itineraryItems = itineraryItems; }

        public String getRouteRegion() { return this.routeRegion; }

        public void setRouteRegion(String routeRegion) { this.routeRegion = routeRegion; }

        public RouteSubLegs getFormRouteSubLegs() { return this.routeSubLegs; }
        
        public void setFormRouteSubLegs(RouteSubLegs routeSubLegs) { this.routeSubLegs = routeSubLegs; }

        public String getStartTime() { return this.startTime; }

        public void setStartTime(String startTime) { this.startTime = startTime; }

        public Date getStartTimeUtc() {
            if (this.startTime == null || "".equals(this.startTime)) {
                return new Date();
            } else {
                return DateTimeHelper.fromOdataJson(this.startTime);
            }
        }
    
        public void setStartTimeUtc(Date value) {
            if (value == null) {
                this.startTime = "";
            } else {
                String v = DateTimeHelper.toOdataJson(value);
                if (v != null) {
                    this.startTime = v;
                } else {
                    this.startTime = "";
                }
            }
        }

        public String getEndTime() { return this.endTime; }

        public void setEndTime(String endTime) { this.endTime = endTime; }

        public Date getEndTimeUtc() {
            if (this.endTime == null || "".equals(this.endTime)) {
                return new Date();
            } else {
                return DateTimeHelper.fromOdataJson(this.endTime);
            }
        }

        public void setEndTimeUtc(Date value) {
            if (value == null) {
                this.endTime = "";
            } else {
                String v = DateTimeHelper.toOdataJson(value);
                if (v != null) {
                    this.endTime = v;
                } else {
                    this.endTime = "";
                }
            }
        }

        // !! TODO: What is the base class?
        public Object[] getAlternateVias() { return this.alternateVias; }

        public void setAlternateVias(Object[] alternateVias) { this.alternateVias = alternateVias; }
    }

    public static class RouteSubLeg {
        private Waypoint endWaypoint;
        private Waypoint startWaypoint;
        private double travelDistance;
        private double travelDuration;
        private double travelDurationTraffic;

        public Waypoint getEndWaypoint() { return this.endWaypoint; }

        public void setEndWaypoint(Waypoint endWaypoint) { this.endWaypoint = endWaypoint; }

        public Waypoint getStartWaypoint() { return this.startWaypoint; }

        public void setStartWaypoint(Waypoint startWaypoint) { this.startWaypoint = startWaypoint; }

        public double getTravelDistance() { return this.travelDistance; }

        public void setTravelDistance(double travelDistance) { this.travelDistance = travelDistance; }

        public double getTravelDuration() { return this.travelDuration; }

        public void setTravelDuration(double travelDuration) { this.travelDuration = travelDuration; }

        public double getTravelDurationTraffic() { return this.travelDurationTraffic; }

        public void setTravelDurationTraffic(double travelDurationTraffic) { this.travelDurationTraffic = travelDurationTraffic; }
    }

    public static class Waypoint extends Point {
        private String description;
        private boolean isVia;
        private String locationIdentifier;
        private int routePathIndex;

        public String getDescription() { return this.description; }

        public void setDescription(String description) { this.description = description; }

        public boolean getIsVia() { return this.isVia; }

        public void setIsVia(boolean isVia) { this.isVia = isVia; }

        public String getLocationIdentifier() { return this.locationIdentifier; }

        public void setLocationIdentifier(String locationIdentifier) { this.locationIdentifier = locationIdentifier; }

        public int getRoutePathIndex() { return this.routePathIndex; }

        public void setRoutePathIndex(int routePathIndex) { this.routePathIndex = routePathIndex; }
    }

    public static class RoutePath {
        private Line line;
        private Generalizations generalizations;

        public static class Generalizations extends ArrayList<Generalization> {}

        public Line getLine() { return this.line; }

        public void setLine(Line line) { this.line = line; }

        public Generalizations getFormGeneralizations() { return this.generalizations; }

        public void setFormGeneralizations(Generalizations generalizations) { this.generalizations = generalizations; }
    }

    public static class Shape {
        private double[] boundingBox;

        public double[] BoundingBox() { return this.boundingBox; }

        public void setBoundingBox(double[] boundingBox) { this.boundingBox = boundingBox; }
    }

    public static class Shield {
        private String[] labels;
        private int roadShieldType;

        public String[] getLabels() { return this.labels; }

        public void setLabels(String[] labels) { this.labels = labels; }

        public int getRoadShieldType() { return this.roadShieldType; }

        public void setRoadShieldType(int roadShieldType) { this.roadShieldType = roadShieldType; }
    }

    // !!    [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public static class StaticMapMetadata extends ImageryMetadata {
        private Point mapCenter;
        private PushpinMetdata[] pushpins;
        private String zoom;

        public Point getMapCenter() { return this.mapCenter; }

        public void setMapCenter(Point mapCenter) { this.mapCenter = mapCenter; }

        public PushpinMetdata[] getPushpins() { return this.pushpins; }

        public void setPushpins(PushpinMetdata[] pushpins) { this.pushpins = pushpins; }

        public String getZoom() { return this.zoom; }

        public void setZoom(String zoom) { this.zoom = zoom; }
    }

    // !!    [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public static class TrafficIncident extends Resource {
        private Point point;
        private String congestion;
        private String description;
        private String detour;
        private String start;
        private Date startDateTimeUtc;
        private String end;
        private Date endDateTimeUtc;
        private long incidentId;
        private String lane;
        private String lastModified;
        private Date lastModifiedDateTimeUtc;
        private boolean roadClosed;
        private int severity;
        private Point toPoint;
        private String[] locationCodes;
        private int type;
        private boolean verified;

        public Point getPoint() { return this.point; }

        public void setPoint(Point point) { this.point = point; }

        public String getCongestion() { return this.congestion; }

        public void setCongestion(String congestion) { this.congestion = congestion; }

        public String getDescription() { return this.description; }

        public void setDescription(String description) { this.description = description; }

        public String getDetour() { return this.detour; }

        public void setDetour() { this.detour = detour; }

        public String getStart() { return this.start; }

        public void setStart() { this.start = start; }

        public Date getStartDateTimeUtc() {
            if (this.start == null || "".equals(this.start)) {
                return new Date();
            } else {
                return DateTimeHelper.fromOdataJson(this.start);
            }
        }

        public void setStartDateTimeUtc(Date value) {
            if (value == null) {
                this.start = "";
            } else {
                String v = DateTimeHelper.toOdataJson(value);
                if (v != null) {
                    this.start = v;
                } else {
                    this.start = "";
                }
            }
        }

        public String getEnd() { return this.end; }

        public void setEnd(String end) { this.end = end; }

        public Date getEndDateTimeUtc() {
            if (end == null || "".equals(end)) {
                return new Date();
            } else {
                return DateTimeHelper.fromOdataJson(this.end);
            }
        }

        public void setEndDateTimeUtc(Date value) {
            if (value == null) {
                this.end = "";
            } else {
                String v = DateTimeHelper.toOdataJson(value);
                if (v != null) {
                    this.end = v;
                } else {
                    this.end = "";
                }
            }
        }

        public long getIncidentId() { return this.incidentId; }

        public void setIncidentId(long incidentId) { this.incidentId = incidentId; }

        public String getLane() { return this.lane; }

        public void setLane(String ane) { this.lane = lane; }

        public String getLastModified() { return this.lastModified; }

        public void setLastModified(String lastModified) { this.lastModified = lastModified; }

        public Date getLastModifiedDateTimeUtc() {
            if (this.lastModified == null || "".equals(this.lastModified)) {
                return new Date();
            } else {
                return DateTimeHelper.fromOdataJson(this.lastModified);
            }
        }

        public void setLastModifiedDateTimeUtc(Date value) {
            if (value == null) {
                this.lastModified = "";
            } else {
                String v = DateTimeHelper.toOdataJson(value);
                if (v != null) {
                    this.lastModified = v;
                } else {
                    this.lastModified = "";
                }
            }
        }

        public boolean getRoadClosed() { return this.roadClosed; }

        public void setRoadClosed() { this.roadClosed = roadClosed; }

        public int getSeverity() { return this.severity; }

        public void setSeverity(int severity) { this.severity = severity; }

        public Point getToPoint() { return this.toPoint; }

        public void setToPoint(Point toPoint) { this.toPoint = toPoint; }

        public String[] getLocationCodes() { return this.locationCodes; }

        public void setLocationCodes(String[] locationCodes) { this.locationCodes = locationCodes; }

        public int getType() { return this.type; }

        public void setType(int type) { this.type = type; }

        public boolean getVerified() { return this.verified; }

        public void setVerified(boolean verified) { this.verified = verified; }
    }

    public static class TransitLine {
        private String verboseName;
        private String abbreviatedName;
        private long agencyId;
        private String agencyName;
        private long lineColor;
        private long lineTextColor;
        private String uri;
        private String phoneNumber;
        private String providerInfo;

        public String getVerboseName() { return this.verboseName; }

        public void setVerboseName(String verboseName) { this.verboseName = verboseName; }

        public String getAbbreviatedName() { return this.abbreviatedName; }

        public void setAbbreviatedName(String abbreviatedName) { this.abbreviatedName = abbreviatedName; }

        public long getAgencyId() { return this.agencyId; }

        public void setAgencyId(long agencyId) { this.agencyId = agencyId; }

        public String getAgencyName() { return this.agencyName; }

        public void setAgencyName(String agencyName) { this.agencyName = agencyName; }

        public long getLineColor() { return this.lineColor; }

        public void setLineColor(long lineColor) { this.lineColor = lineColor; }

        public long getLineTextColor() { return this.lineTextColor; }

        public void setLineTextColor(long lineTextColor) { this.lineTextColor = lineTextColor; }

        public String getUri() { return this.uri; }

        public void setUri() { this.uri = uri; }

        public String getPhoneNumber() { return this.phoneNumber; }

        public void setPhoneNumber() { this.phoneNumber = phoneNumber; }

        public String getProviderInfo() { return this.providerInfo; }

        public void setProviderInfo() { this.providerInfo = providerInfo; }
    }

    public static class Warning {
        private String origin;
        private Coordinate originCoordinate;
        private String severity;
        private String text;
        private String to;
        private Coordinate toCoordinate;
        private String warningType;

        public String getOrigin() { return this.origin; }

        public void setOrigin(String origin) { this.origin = origin; }

        public Coordinate getOriginCoordinate() {
            if (this.origin == null || "".equals(this.origin)) {
                return null;
            } else {
                String[] latLng = this.origin.split(",");
                double lat, lon;

                try {
                    if (latLng.length >= 2) {
                        lat = Double.parseDouble(latLng[0]);
                        lon = Double.parseDouble(latLng[1]);
                        return new Coordinate(lat, lon);
                    }
                } catch (NumberFormatException ex) {}

                return null;
            }
        }

        public void setOriginCoordinate(Coordinate value) {
            if (value == null) {
                this.origin = "";
            } else {
                this.origin = String.format("{0},{1}", value.getLat(), value.getLon());
            }
        }

        public String getSeverity() { return this.severity; }

        public void setSeverity(String severity) { this.severity = severity; }

        public String getText() { return this.text; }

        public void setText(String text) { this.text = text; }

        public String getTo() { return this.to; }

        public void setTo(String to) { this.to = to; }

        public Coordinate getToCoordinate() {
            if (this.to == null || "".equals(this.to)) {
                return null;
            } else {
                String[] latLng = this.to.split(",");
                double lat, lon;

                try {
                    if (latLng.length >= 2) {
                        lat = Double.parseDouble(latLng[0]);
                        lon = Double.parseDouble(latLng[1]);
                        return new Coordinate(lat, lon);
                    }
                } catch (NumberFormatException ex) {}

                return null;
            }
        }

        public void setToCoordinate(Coordinate value) {
            if (value == null) {
                this.to = "";
            } else {
                this.to = String.format("{0},{1}", value.getLat(), value.getLon());
            }
        }

        public String getWarningType() { return this.warningType; }

        public void setWarningType(String warningType) { this.warningType = warningType; }
    }

    // !!    [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public static class CompressedPointList extends Resource {
        private String value;
    
        public String getValue() { return this.value; }

        public void setValue(String value) { this.value = value; }
    }

    // !!    [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public static class ElevationData extends Resource {
        private int[] elevations;
        private int zoomLevel;

        public int[] getElevations() { return this.elevations; }

        public void setElevations(int[] elevations) { this.elevations = elevations; }

        public int getZoomLevel() { return this.zoomLevel; }

        public void setZoomLevel(int zoomLevel) { this.zoomLevel = zoomLevel; }
    }

    // !!    [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public static class SeaLevelData extends Resource {
        private int[] offsets;
        private int zoomLevel;

        public int[] getOffsets() { return this.offsets; }

        public void setOffsets(int[] offsets) { this.offsets = offsets; }

        public int getZoomLevel() { return this.zoomLevel; }

        public void setZoomLevel(int getZoomLevel) { this.zoomLevel = zoomLevel; }
    }

    /// <summary>
    /// This response specifies:
    ///  - Whether this is a politically disputed area, such as an area claimed by more than one country.
    ///  - Whether services are available in the user’s region.
    ///  - A list of available geospatial services including endpoints and language support for each service.
    /// </summary>
    // !!    [DataContract(Namespace = "http://schemas.microsoft.com/search/local/ws/rest/v1")]
    public static class GeospatialEndpointResponse extends Resource {
        private boolean isDisputedArea;
        private boolean isSupported;
        private String userRegion;
        private GeospatialService[] services;

        /// <summary>
        /// Specifies if this area in the request is claimed by more than one country. 
        /// </summary>
        public boolean getIsDisputedArea() { return this.isDisputedArea; }

        public void setIsDisputedArea(boolean IsDisputedArea) { this.isDisputedArea = isDisputedArea; }

        /// <summary>
        /// Specifies if Geospatial Platform services are available in the country or region. Microsoft does not support services in embargoed areas.
        /// </summary>
        public boolean getIsSupported() { return this.isSupported; }

        public void IsSupported(boolean IsSupported) { this.isSupported = isSupported; }

        /// <summary>
        /// The country or region that was used to determine service support. If you specified a User Location in 
        /// the request that is in a non-disputed country or region, this country or region is returned in the response.
        /// </summary>
        public String getUr() { return this.userRegion; }

        public void setUr(String userRegion) { this.userRegion = userRegion; }

        /// <summary>
        /// Information for each geospatial service that is available in the country or region and language specified in the request.
        /// </summary>
        public GeospatialService[] getServices() { return this.services; }

        public void setServices(GeospatialService[] Services) { this.services = services; }
    }

    /// <summary>
    /// Information for a geospatial service that is available in the country or region and language specified in the request.
    /// </summary>
    public static class GeospatialService {
        private String endpoint;
        private String fallbackLanguage;
        private boolean languageSupported;
        private String serviceName;

        /// <summary>
        /// The URL service endpoint to use in this region. Note that to use the service, you must typically add parameters specific to 
        /// the service. These parameters are not described in this documentation.
        /// </summary>
        public String getEndpoint() { return this.endpoint; }

        public void setEndpoint(String endpoint) { this.endpoint = endpoint; }

        /// <summary>
        /// Set to true if the service supports the language in the request for the region. If the language is supported, then the 
        /// service endpoint will return responses using this language. If it is not supported, then the service will use the fallback language.
        /// </summary>
        public String getFallbackLanguage() { return this.fallbackLanguage; }

        public void setFallbackLanguage(String fallbackLanguage) { this.fallbackLanguage = fallbackLanguage; }

        /// <summary>
        /// Specifies the secondary or fallback language in this region or country. If the requested language is not supported 
        /// and a fallback language is not available, United States English (en-us) is used.
        /// </summary>
        public boolean getLanguageSupported() { return this.languageSupported; }

        public void setLanguageSupported(boolean languageSupported) { this.languageSupported = languageSupported; }

        /// <summary>
        /// An abbreviated name for the service.
        /// </summary>
        public String getServiceName() { return this.serviceName; }

        public void setServiceName(String serviceName) { this.serviceName = serviceName; }
    }

}