package org.bing.models;

import java.math.BigDecimal;
import java.math.RoundingMode;

/// <summary>
/// A class that defines location coordinate value.
/// </summary>
public class Coordinate
{
    private double _latitude, _longitude;

    /// <summary>
    /// A location coordinate.
    /// </summary>
    public Coordinate()
    {
    }

    /// <summary>
    /// A location coordinate.
    /// </summary>
    /// <param name="latitude">Latitude coordinate vlaue.</param>
    /// <param name="longitude">Longitude coordinate value.</param>
    public Coordinate(double latitude, double longitude)
    {
        this._latitude = latitude;
        this._longitude = longitude;
    }

    /// <summary>
    /// Latitude coordinate.
    /// </summary>
    public double getLat() {
        return this._latitude;
    }

    public void setLat(double value) {
        if (!Double.isNaN(value) && value <= 90 && value >= -90) {
            // Only need to keep the first 5 decimal places. Any more just adds more data being passed around.
            this._latitude = BigDecimal.valueOf(value).setScale(5, RoundingMode.HALF_UP).doubleValue();
        }
    }

    /// <summary>
    /// Longitude coordinate.
    /// </summary>
    public double getLon() {
        return this._longitude;
    }

    public void setLon(double value) {
        if (!Double.isNaN(value) && value <= 180 && value >= -180) {
            // Only need to keep the first 5 decimal places. Any more just adds more data being passed around.
            this._longitude = BigDecimal.valueOf(value).setScale(5, RoundingMode.HALF_UP).doubleValue();
        }
    }

    @Override
    public String toString() {
        return String.format("%0$7.5f,%1$7.5f", this._latitude, this._longitude);
    }

}

