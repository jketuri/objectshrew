package org.bing.models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.bing.enums.AvoidType;
import org.bing.enums.DistanceUnitType;
import org.bing.enums.RouteAttributeType;
import org.bing.enums.RouteOptimizationType;
import org.bing.enums.RouteTimeType;
import org.bing.enums.TravelModeType;

/// <summary>
/// A class that defines the options that can to use when calculating a route.
/// </summary>
public class RouteOptions
{
    private AvoidTypes avoid;
    private int distanceBeforeFirstTurn = 0, heading = 0, maxSolutions = 1;
    private RouteOptimizationType optimize;
    private TravelModeType travelMode;
    private Date dateTime;
    private RouteTimeType timeType;
    private RouteAttributeTypes routeAttributes;
    private DistanceUnitType distanceUnits;
    private Doubles tolerances;

    public static class AvoidTypes extends ArrayList<AvoidType> {}

    public static class RouteAttributeTypes extends ArrayList<RouteAttributeType> {}

    public static class Doubles extends ArrayList<Double> {}

    public RouteOptions()
    {
        this.optimize = RouteOptimizationType.Time;
        this.travelMode = TravelModeType.Driving;
        this.timeType = RouteTimeType.Departure;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 15);
        this.dateTime = calendar.getTime();
    }

    /// <summary>
    /// Specifies the road types to minimize or avoid when a route is created for the driving travel mode. 
    /// </summary>
    public AvoidTypes getAvoid() { return this.avoid; }

    public void setAvoid(AvoidTypes avoid) { this.avoid = avoid; }

    /// <summary>
    /// Specifies the distance before the first turn is allowed in the route. 
    /// This option only applies to the driving travel mode.
    /// An integer distance specified in meters. 
    /// Use this parameter to make sure that the moving vehicle has enough distance to make the first turn. 
    /// </summary>
    public int getDistanceBeforeFirstTurn() { return distanceBeforeFirstTurn; }

    public void setDistanceBeforeFirstTurn(int value) {
        if (value >= 0) {
            distanceBeforeFirstTurn = value;
        }
    }

    /// <summary>
    /// Specifies the initial heading for the route. An integer value between 0 and 359 that represents degrees from 
    /// north where north is 0 degrees and the heading is specified clockwise from north.
    /// </summary>
    public int getHeading() { return heading; }

    public void setHeading(int value) {
        if (value < 0) {
            heading = 0;
        } else if (value > 359) {
            heading = 359;
        } else {
            heading = value;
        }
    }

    /// <summary>
    /// Specifies what parameters to use to optimize the route.
    /// </summary>
    public RouteOptimizationType getOptimize() { return this.optimize; }

    public void setOptimize(RouteOptimizationType optimize) { this.optimize = optimize; }

    /// <summary>
    /// The mode of travel for the route. Default: Driving. 
    /// </summary>
    public TravelModeType getTravelMode() { return this.travelMode; }

    public void setTravelMode(TravelModeType travelMode) { this.travelMode = travelMode; }

    /// <summary>
    /// The dateTime parameter identifies the desired time to be used when calculating a route. This is supported by driving and transit routes. 
    /// When calculating driving routes the route optimization type should be TimeWithTraffic. The route time will be used as the departure time.
    /// When calculating transit routes timeType can be specified. 
    /// </summary>
    public Date getDateTime() { return this.dateTime; }

    public void setDateTime(Date dateTime) { this.dateTime = dateTime; }

    /// <summary>
    /// Specifies how to interpret the date and transit time value that is specified by the dateTime parameter.
    /// </summary>
    public RouteTimeType getTimeType() { return this.timeType; }

    public void setTimeType(RouteTimeType timeType) { this.timeType = timeType; }

    /// <summary>
    /// Specifies to include or exclude parts of the routes response.
    /// </summary>
    public RouteAttributeTypes getRouteAttributes() { return this.routeAttributes; }

    public void setRouteAttributes(RouteAttributeTypes routeAttributes) { this.routeAttributes = routeAttributes; }

    /// <summary>
    /// The units to use for distance.
    /// </summary>
    public DistanceUnitType getDistanceUnits() { return this.distanceUnits; }

    public void setDistanceUnits(DistanceUnitType distanceUnits) { this.distanceUnits = distanceUnits; }

    /// <summary>
    ///  Specifies a series of tolerance values. Each value produces a subset of points that approximates the route 
    ///  that is described by the full set of points. This parameter is only valid when the routePathOutput parameter 
    ///  is set to Points. You can specify a maximum of seven (7) tolerance values.
    /// </summary>
    public Doubles getTolerances() { return this.tolerances; }

    public void setTolerances(Doubles tolerances) { this.tolerances = tolerances; }

    /// <summary>
    /// Specifies the maximum number of transit or driving routes to return. An interger between 1 and 3.
    /// This parameter is available for the Driving and Transit travel modes for routes between two waypoints. 
    /// This parameter does not support routes with more than two waypoints. For driving routes, you must not set 
    /// the avoid and distanceBeforeFirstTurn parameters.  The maxSolutions parameter is supported for routes in 
    /// the United States, Canada, Mexico, United Kingdom, Australia, and India.
    /// </summary>
    public int getMaxSolutions() { return maxSolutions; }

    public void setMaxSolutions(int value) {
        if (value < 0) {
            maxSolutions = 1;
        } else if (value > 3) {
            maxSolutions = 3;
        } else {
            maxSolutions = value;
        }
    }

    public String getUrlParam()
    {
        StringBuilder sb = new StringBuilder();
        if (this.optimize != RouteOptimizationType.Time) {
            sb.append(String.format("&optmz={0}", optimize.toString()));
        }
        if (this.travelMode == TravelModeType.Driving && this.avoid != null && this.avoid.size() > 0) {
            sb.append("&avoid=");
            for (int i = 0; i < this.avoid.size(); i++) {
                sb.append(avoid.get(i).toString());
                if (i < this.avoid.size() - 1) {
                    sb.append(",");
                }
            }
        }
        if (this.travelMode == TravelModeType.Driving && this.distanceBeforeFirstTurn > 0) {
            sb.append(String.format("&dbft={0}", distanceBeforeFirstTurn));
        }
        if (this.heading > 0) {
            sb.append(String.format("&hd={0}", heading));
        }
        if (this.distanceUnits != DistanceUnitType.KM) {
            sb.append("&du=mi");
        }
        if (this.tolerances != null && this.tolerances.size() > 0) {
            sb.append("&tl=");
            int cnt = Math.min(this.tolerances.size(), 7);
            for (int i = 0; i < cnt; i++) {
                sb.append(String.format("{0:0.######}", this.tolerances.get(i)));
                if (i < this.tolerances.size() - 1) {
                    sb.append(",");
                }
            }
        }
        if (this.routeAttributes != null && this.routeAttributes.size() > 0) {
            sb.append("&ra=");
            for (int i = 0; i < this.routeAttributes.size(); i++) {
                sb.append(this.routeAttributes.get(i).toString());
                if (i < this.routeAttributes.size() - 1) {
                    sb.append(",");
                }
            }
        }
        if (this.dateTime != null) {
            if (this.travelMode == TravelModeType.Transit) {
                sb.append(String.format("&dt={0:G}", this.dateTime));
                sb.append(String.format("&tt={0}", this.timeType.toString()));
            }
            else if (this.travelMode == TravelModeType.Driving) {
                sb.append(String.format("&dt={0:G}", this.dateTime));
            }
        }
        if (this.travelMode != TravelModeType.Walking) {
            sb.append(String.format("&maxSolns={0}", maxSolutions));
        }
        return sb.toString();
    }

}
