package org.bing.models;

/// <summary>
/// A simple waypoint class that can be used to calculate a route.
/// </summary>
public class SimpleWaypoint
{
    private Coordinate coordinate;
    private String address;
    private boolean isViaPoint;

    /// <summary>
    /// A waypoint to calculate a route through.
    /// </summary>
    public SimpleWaypoint()
    {
    }

    /// <summary>
    /// A waypoint to calculate a route through.
    /// </summary>
    /// <param name="coordinate">Coordinate of the waypoint.</param>
    public SimpleWaypoint(Coordinate coordinate)
    {
        this.coordinate = coordinate;
    }

    /// <summary>
    /// A waypoint to calculate a route through.
    /// </summary>
    /// <param name="address">Address or location description of waypoint.</param>
    public SimpleWaypoint(String address)
    {
        this.address = address;
    }

    /// <summary>
    /// A waypoint to calculate a route through.
    /// </summary>
    /// <param name="coordinate">Coordinate of the waypoint.</param>
    /// <param name="address">Address or location description of waypoint.</param>
    public SimpleWaypoint(Coordinate coordinate, String address)
    {
        this.coordinate = coordinate;
        this.address = address;
    }

    /// <summary>
    /// The coordinate of the waypoint. When specified this will be used instead of the Address value in requests.
    /// </summary>
    public Coordinate getCoordinate() { return this.coordinate; }

    public void setCoordinate(Coordinate coordinate) { this.coordinate = coordinate; }

    /// <summary>
    /// The address query for the waypoint. 
    /// </summary>
    public String getAddress() { return this.address; }

    public void setAddress(String address) { this.address = address; }

    /// <summary>
    /// A bool indicating whether the waypoint is a via point.
    /// </summary>
    public boolean getIsViaPoint() { return this.isViaPoint; }

    public void setIsViaPoint(boolean isViaPoint) { this.isViaPoint = isViaPoint; }

}
