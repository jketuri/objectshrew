package org.bing.models;

public class BoundingBox
{
    private double southLatitude;
    private double westLongitude;
    private double northLatitude;
    private double eastLongitude;

    public boolean invisible;

    public BoundingBox()
    {
    }

    /// <summary>
    /// Bounding box generated from edge coordinates.  
    /// </summary>
    /// <param name="edges">The edges of the bounding box. Structure [South Latitude, West Longitude, North Latitude, East Longitude]</param>
    public BoundingBox(double[] edges)
    {
        if (edges != null && edges.length >= 4) {
            this.southLatitude = edges[0];
            this.westLongitude = edges[1];
            this.northLatitude = edges[2];
            this.eastLongitude = edges[3];
        }
    }

    public boolean invisible() {
        return this.invisible;
    }

    public double getSouthLatitude() { return this.southLatitude; }

    public void setSouthLatitude(double southLatitude) { this.southLatitude = southLatitude; }

    public double getWestLongitude() { return this.westLongitude; }

    public void setWestLongitude(double westLongitude) { this.westLongitude = westLongitude;  }

    public double getNorthLatitude() { return this.northLatitude; }

    public void setNorthLatitude(double northLatitude) { this.northLatitude = northLatitude; }

    public double getEastLongitude() { return this.eastLongitude; }

    public void setEastLongitude(double eastLongitude) { this.eastLongitude = eastLongitude; }

    @Override
    public String toString() {
        return String.format("%0$7.5f,%1$7.5f,%2$7.5f,%3$7.5f",
                             southLatitude,
                             westLongitude,
                             northLatitude,
                             eastLongitude);
    }

}
