package org.bing.internal;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/// <summary>
/// A helper class for working iwth OData Dates.
/// </summary>
public class DateTimeHelper
{
    /// <summary>
    /// Converts a DateTime object into an OData date.
    /// </summary>
    /// <param name="dateTime">The DateTime to convert.</param>
    /// <returns>An OData version of the DateTime.</returns>
    public static String toOdataJson(Date dateTime)
    {
        Calendar d1 = Calendar.getInstance();
        d1.set(Calendar.YEAR, 1970);
        d1.set(Calendar.MONTH, 1);
        d1.set(Calendar.DAY_OF_MONTH, 1);
        d1.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar d2 = Calendar.getInstance();
        d2.setTime(dateTime);
        d2.setTimeZone(TimeZone.getTimeZone("UTC"));
        return "\\/Date(" + String.valueOf(d2.getTimeInMillis() - d1.getTimeInMillis()) + ")\\/";
    }

    /// <summary>
    /// Converts an OData Date into a DateTime object.
    /// </summary>
    /// <param name="jsonDate">OData Date to convert.</param>
    /// <returns>The converted DateTime object.</returns>
    public static Date fromOdataJson(String jsonDate)
    {
        //  /Date(1235764800000)/
        //  /Date(1467298867000-0700)/
            
        jsonDate = jsonDate.replace("/Date(", "").replace(")/", "");

        long ms = 0;    // number of milliseconds since midnight Jan 1, 1970
        long hours = 0;

        int pIdx = jsonDate.indexOf("+");
        int mIdx = jsonDate.indexOf("-");

        if (pIdx > 0) {
            ms = Long.parseLong(jsonDate.substring(0, pIdx));

            //Hack: The offset is meant to be in minutes, but for some reason the response from the REST services uses 700 which is meant to be 7 hours.
            hours = Long.parseLong(jsonDate.substring(mIdx)) / 100;
        }
        else if (mIdx > 0) {
            ms = Long.parseLong(jsonDate.substring(0, mIdx));

            //Hack: The offset is meant to be in minutes, but for some reason the response from the REST services uses 700 which is meant to be 7 hours.
            hours = Long.parseLong(jsonDate.substring(mIdx)) / 100;
        }
        else {
            ms = Long.parseLong(jsonDate);
        }
            
        Calendar dateTime = Calendar.getInstance();
        dateTime.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateTime.set(Calendar.YEAR, 1970);
        dateTime.set(Calendar.MONTH, 1);
        dateTime.set(Calendar.DAY_OF_MONTH, 1);
        dateTime.set(Calendar.HOUR_OF_DAY, 1);
        dateTime.set(Calendar.MINUTE, 1);
        dateTime.set(Calendar.SECOND, 1);
        dateTime.set(Calendar.MILLISECOND, 0);
        while (ms > 0) {
            dateTime.add(Calendar.MILLISECOND, (int)(ms % Integer.MAX_VALUE));
            ms -= ms % Integer.MAX_VALUE;
        }
        while (hours > 0) {
            dateTime.add(Calendar.HOUR_OF_DAY, (int)(hours % Integer.MAX_VALUE));
            hours -= hours % Integer.MAX_VALUE;
        }
        return dateTime.getTime();
    }
}
