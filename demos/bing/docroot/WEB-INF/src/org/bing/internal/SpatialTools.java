package org.bing.internal;

import org.bing.enums.DistanceUnitType;
import org.bing.models.Coordinate;

/// <summary>
/// A set of useful spatial math tools.
/// </summary>
public class SpatialTools
{

    /// <summary>
    /// The approximate spherical radius of the Earth
    /// </summary>
    public static class EarthRadius
    {
        /// <summary>
        /// Earth Radius in Kilometers
        /// </summary>
        public static final double KM = 6378.135;

        /// <summary>
        /// Earth Radius in Meters
        /// </summary>
        public static final double Meters = 6378135;

        /// <summary>
        /// Earth Radius in Miles
        /// </summary>
        public static final double Miles = 3963.189;

        /// <summary>
        /// Earth Radius in Feet
        /// </summary>
        public static final double Feet = 20925640;
    }

    /// <summary>
    /// Retrieves the radius of the earth in a specific distance unit for WGS84.
    /// </summary>
    /// <param name="units">Unit of distance measurement.</param>
    /// <returns>The radius of the earth in a specified distance units.</returns>
    public static double getEarthRadius(DistanceUnitType units)
    {
        switch (units) {
        case Miles:
            return EarthRadius.Miles;
        case KM:
        default:
            return EarthRadius.KM;
        }
    }

    /// <summary>
    /// Converts distances from miles to km or km to miles.
    /// </summary>
    /// <param name="distance">Distance to convert.</param>
    /// <param name="fromUnits">The units that the distance is in.</param>
    /// <param name="toUnits">The units to convert the distance to.</param>
    /// <returns>A distance in the specified unit of measurement.</returns>
    public static double convertDistance(double distance, DistanceUnitType fromUnits, DistanceUnitType toUnits)
    {
        if (fromUnits == toUnits || Double.isNaN(distance)) {
            return distance;
        }

        //Convert the distance to kilometers
        if (fromUnits == DistanceUnitType.Miles) {
            distance *= 1.609344;
        }

        if (toUnits == DistanceUnitType.Miles) {
            distance /= 1.609344;
        }

        return distance;
    }

    /// <summary>
    /// Converts an angle that is in degrees to radians. Angle * (PI / 180)
    /// </summary>
    /// <param name="angle">An angle in degrees</param>
    /// <returns>An angle in radians</returns>
    public static double toRadians(double angle)
    {
        return angle * (Math.PI / 180);
    }

    /// <summary>
    /// Converts an angle that is in radians to degress. Angle * (180 / PI)
    /// </summary>
    /// <param name="angle">An angle in radians</param>
    /// <returns>An angle in degrees</returns>
    public static double toDegrees(double angle)
    {
        return angle * (180 / Math.PI);
    }

    /// <summary>
    /// Calculate the distance between two coordinates on the surface of a sphere (Earth).
    /// </summary>
    /// <param name="origin">First coordinate to calculate distance between.</param>
    /// <param name="destination">Second coordinate to calculate distance between.</param>
    /// <param name="units">Unit of distance measurement.</param>
    /// <returns>The shortest distance in the specifed units.</returns>
    public static double haversineDistance(Coordinate origin, Coordinate destination, DistanceUnitType units)
    {
        return haversineDistance(origin.getLat(), origin.getLon(),
                                 destination.getLat(), destination.getLon(), units);
    }

    /// <summary>
    /// Calculate the distance between two coordinates on the surface of a sphere (Earth).
    /// </summary>
    /// <param name="origLat">Origin Latitude.</param>
    /// <param name="origLon">Origin Longitude.</param>
    /// <param name="destLat">Destination Latitude.</param>
    /// <param name="destLon">Destination Longitude.</param>
    /// <param name="units">Unit of distance measurement.</param>
    /// <returns>The shortest distance in the specifed units.</returns>
    public static double haversineDistance(double origLat, double origLon, double destLat, double destLon, DistanceUnitType units)
    {
        double radius = getEarthRadius(units);

        double dLat = toRadians(destLat - origLat);
        double dLon = toRadians(destLon - origLon);

        double a = Math.pow(Math.sin(dLat / 2), 2) + Math.pow(Math.cos(toRadians(origLat)), 2) * Math.pow(Math.sin(dLon / 2), 2);
        double centralAngle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return radius * centralAngle;
    }

    /// <summary>Calculates the bearing between two coordinates.</summary>
    /// <param name="origin" type="Coordinate">Initial location.</param>
    /// <param name="dest" type="Coordinate">Second location.</param>
    /// <returns>The bearing angle between two coordinates.</returns>
    public static double calculateBearing(Coordinate origin, Coordinate dest)
    {
        double lat1 = toRadians(origin.getLat());
        double lon1 = origin.getLon();
        double lat2 = toRadians(dest.getLat());
        double lon2 = dest.getLon();
        double dLon = toRadians(lon2 - lon1);
        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
        return (toDegrees(Math.atan2(y, x)) + 360.0) % 360.0;
    }

    /// <summary>Calculates a destination coordinate given a starting coordinate, bearing, and distance in KM's.</summary>
    /// <param name="origin" type="Coordinate">Initial location.</param>
    /// <param name="brng" type="Number">Bearing pointing towards new location.</param>
    /// <param name="arcLength" type="Number">A distance in KM's.</param>
    /// <param name="units" type="DistanceUnitType">Unit of measurement of the arcLength</param>
    /// <returns>A destination coordinate based on an origin point, bearing and distance.</returns>
    public static Coordinate calculateCoord(Coordinate origin, double brng, double arcLength, DistanceUnitType units)
    {
        double earthRadius = getEarthRadius(units);
        double lat1 = toRadians(origin.getLat()),
            lon1 = toRadians(origin.getLon()),
            centralAngle = arcLength / earthRadius;

        double lat2 = Math.asin(Math.sin(lat1) * Math.cos(centralAngle) + Math.cos(lat1) * Math.sin(centralAngle) * Math.cos(toRadians(brng)));
        double lon2 = lon1 + Math.atan2(Math.sin(toRadians(brng)) * Math.sin(centralAngle) * Math.cos(lat1), Math.cos(centralAngle) - Math.sin(lat1) * Math.sin(lat2));

        return new Coordinate(toDegrees(lat2), toDegrees(lon2));
    }
}
