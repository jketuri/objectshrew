/*
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bing.rest;

import java.io.Serializable;

import org.bing.models.SimpleWaypoint;
import org.bing.models.RestServiceModels.Route;
import org.bing.models.RestServiceModels.RouteResourceSet;
import org.bing.models.RestServiceModels.RouteResponse;
import org.bing.requests.RouteMajorRoadsRequest;
import org.bing.requests.RouteRequest;

public class BingCalculateRoutesFromMajorRoads implements Serializable {
    private final static long serialVersionUID = 0L;

    private RouteMajorRoadsRequest routeMajorRoadsRequest;
    private RouteResponse routeResponse = new RouteResponse();
    private Route route = new Route();

    public BingCalculateRoutesFromMajorRoads(final BingObjectView bingObjectView) {
        this.routeMajorRoadsRequest = new RouteMajorRoadsRequest();
        this.routeMajorRoadsRequest.setOptionalBingMapsKey(bingObjectView.getApplicationId());
        RouteResourceSet resourceSet = new RouteResourceSet();
        resourceSet.setResources(route);
        this.routeResponse.setResourceSets(resourceSet);
        this.routeMajorRoadsRequest.setDestination(new SimpleWaypoint(""));
    }

    public RouteMajorRoadsRequest getRouteMajorRoadsQuery() {
        return this.routeMajorRoadsRequest;
    }

    public void setRouteMajorRoadsQuery(RouteMajorRoadsRequest routeMajorRoadsRequest) {
    }

    public Route getRoute() {
        return route;
    }

    /**
     * Response which is sent with geocodeRetrieve(), geocodeUpdate(), geocodeAdd(), geocodeDelete()
     * in server side. Response is converted to JSON or XML and sent to network as response
     * to request which was received with method setGeocodeRequest.
     **/
    public RouteResponse getRouteResponse() {
        return routeResponse;
    }

    /**
     * Response which is received with routeClientRetrieve(), routeClientUpdate(), routeClientAdd(),
     * routeClientDelete() in client side. Response is received from network and converted from JSON or XML
     * to object and set with this method for application by the framework.
     **/
    public void setRouteResponse(RouteResponse response) {
    }

    public String clientLocationURI() {
        return routeMajorRoadsRequest.getRequestUrl();
    }

    public String clientMessageType() {
        return "application/json";
    }

    public String clientRequestType() {
        return "REST";
    }

    public boolean clientPlain() {
        return true;
    }

    /**
     * Action method in client side for REST GET-verb and SOAP-request with retrieve-action.
     */
    public String[] routeClientRetrieve() {
        return new String[] {null, "Bing.tabList.BingCalculateRoutesFromMajorRoads.route"};
    }

    /**
     * Action method in client side for REST PUT-verb and SOAP-request with update-action.
     */
    public void routeClientUpdate() {
    }

    /**
     * Action method in client side for REST POST-verb and SOAP-request with add-action.
     */
    public void routeClientAdd() {
    }

    /**
     * Action method in client side for REST DELETE-verb and SOAP-request with delete-action.
     */
    public void routeClientDelete() {
    }

}
