/*
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bing.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.bing.models.RestServiceModels.GeocodeResponse;
import org.bing.models.RestServiceModels.Location;
import org.bing.requests.GeocodeRequest;

public class BingGeocode implements Serializable {
    private final static long serialVersionUID = 0L;

    public static class LocationList extends ArrayList<Location> {}

    private GeocodeRequest geocodeRequest;
    private BingGeocode.LocationList locations = new BingGeocode.LocationList();

    public BingGeocode(final BingObjectView bingObjectView) {
        this.geocodeRequest = new GeocodeRequest();
        this.geocodeRequest.setOptionalBingMapsKey(bingObjectView.getApplicationId());
        this.geocodeRequest.setOptionalQuery("Rekola, Vantaa, Finland");
    }

    public GeocodeRequest getGeocode() {
        return this.geocodeRequest;
    }

    public void setGeocode(GeocodeRequest geocodeRequest) {
    }

    public BingGeocode.LocationList getFormLocations() {
        return this.locations;
    }

    /**
     * Response which is sent with geocodeRetrieve(), geocodeUpdate(), geocodeAdd(), geocodeDelete()
     * in server side. Response is converted to JSON or XML and sent to network as response
     * to request which was received with method setGeocodeRequest.
     **/
    public GeocodeResponse getGeocodeResponse() {
        return new GeocodeResponse();
    }

    /**
     * Response which is received with geocodeClientRetrieve(), geocodeClientUpdate(), geocodeClientAdd(),
     * geocodeClientDelete() in client side. Response is received from network and converted from JSON or XML
     * to object and set with this method for application by the framework.
     **/
    public void setGeocodeResponse(GeocodeResponse response) {
        this.locations.clear();
        for (Location location : response.getResourceSets()[0].getResources()) {
            this.locations.add(location);
        }
    }

    public String clientLocationURI() {
        return geocodeRequest.getRequestUrl();
    }

    public String clientMessageType() {
        return "application/json";
    }

    public String clientRequestType() {
        return "REST";
    }

    public boolean clientPlain() {
        return true;
    }

    /**
     * Action method in client side for REST GET-verb and SOAP-request with retrieve-action.
     */
    public String[] geocodeClientRetrieve() {
        return new String[] {null, "Bing.tabList.BingGeocode.formLocations"};
    }

    /**
     * Action method in client side for REST PUT-verb and SOAP-request with update-action.
     */
    public void geocodeClientUpdate() {
    }

    /**
     * Action method in client side for REST POST-verb and SOAP-request with add-action.
     */
    public void geocodeClientAdd() {
    }

    /**
     * Action method in client side for REST DELETE-verb and SOAP-request with delete-action.
     */
    public void geocodeClientDelete() {
    }

}
