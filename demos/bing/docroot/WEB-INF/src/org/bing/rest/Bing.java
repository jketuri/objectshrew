/*
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bing.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class Bing implements Serializable {
    private final static long serialVersionUID = 0L;

    private Set<String> invisibleSet = new HashSet<String>();

    protected final List<Object> tabList = new ArrayList<Object>();

    public Bing(final BingObjectView bingObjectView) {
        invisibleSet.add("Bing.tabList.BingGeocode.geocode.requestUrl");
        invisibleSet.add("Bing.tabList.BingGeocode.geocode.optionalBingMapsKey");
        invisibleSet.add("Bing.tabList.BingReverseGeocode.geocode.requestUrl");
        invisibleSet.add("Bing.tabList.BingReverseGeocode.geocode.optionalBingMapsKey");
        invisibleSet.add("Bing.tabList.BingGetMapUri.geocode.requestUrl");
        invisibleSet.add("Bing.tabList.BingGetMapUri.geocode.optionalBingMapsKey");
        invisibleSet.add("Bing.tabList.BingCalculateRoute.geocode.requestUrl");
        invisibleSet.add("Bing.tabList.BingCalculateRoute.geocode.optionalBingMapsKey");
        invisibleSet.add("Bing.tabList.BingCalculateRoutesFromMajorRoads.geocode.requestUrl");
        invisibleSet.add("Bing.tabList.BingCalculateRoutesFromMajorRoads.geocode.optionalBingMapsKey");
        invisibleSet.add("Bing.tabList.BingSearch.geocode.requestUrl");
        invisibleSet.add("Bing.tabList.BingSearch.geocode.optionalBingMapsKey");
        tabList.add(new BingGeocode(bingObjectView));
        tabList.add(new BingReverseGeocode(bingObjectView));
        tabList.add(new BingGetMapUri(bingObjectView));
        tabList.add(new BingCalculateRoute(bingObjectView));
        tabList.add(new BingCalculateRoutesFromMajorRoads(bingObjectView));
        tabList.add(new BingSearch(bingObjectView));
    }

    public String namespace() {
        return "";
    }

    public Set<String> invisibleSet() {
        return this.invisibleSet;
    }

    public List<Object> getTabList() {
        return this.tabList;
    }

}
