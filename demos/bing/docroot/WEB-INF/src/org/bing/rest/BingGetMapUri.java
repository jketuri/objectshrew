/*
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bing.rest;

import java.io.Serializable;

public class BingGetMapUri implements Serializable {
    private final static long serialVersionUID = 0L;

    private Object getMapUriModel = null;

    public BingGetMapUri(final BingObjectView bingObjectView) {
    }

    public String modelGetMapUriWsdlURI() {
        return "/dev.virtualearth.net/webservices/v1/metadata/imageryservice/imageryservice.wsdl";
    }

    public String modelGetMapUriWebServiceName() {
        return "ImageryService";
    }

    public String modelGetMapUriWebServicePort() {
        return "BasicHttpBinding_IImageryService";
    }

    public String modelGetMapUriWebOperationName() {
        return "GetMapUri";
    }

    public Object getGetMapUriModel()
    {
        return getMapUriModel;
    }

    public void setGetMapUriModel(final Object getMapUriModel)
    {
        this.getMapUriModel = getMapUriModel;
    }

}
