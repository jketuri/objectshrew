package org.bing.enums;

/// <summary>
/// Types of location based entities.
/// </summary>
public enum EntityType
{
    /// <summary>
    /// A street address or RoadBlock.
    /// </summary>
    Address,

        /// <summary>
        /// A section of a populated place that is typically well-known, but often with indistinct boundaries. 
        /// </summary>
        Neighborhood,

        /// <summary>
        /// A concentrated area of human settlement, such as a city, town or village.
        /// </summary>
        PopulatedPlace,

        /// <summary>
        /// The smallest post code category, such as a zip code. 
        /// </summary>
        Postcode1,

        /// <summary>
        /// First administrative level within the country/region level, such as a state or a province.
        /// </summary>
        AdminDivision1,

        /// <summary>
        /// Second administrative level within the country/region level, such as a county.
        /// </summary>
        AdminDivision2,

        /// <summary>
        /// Country or region
        /// </summary>
        CountryRegion,

        }
