package org.bing.enums;

/// <summary>
/// Specifies the type of a traffic incident.
/// </summary>
public enum TrafficType
{
    /// <summary>
    /// Accident incident type.
    /// </summary>
    Accident(1),

        /// <summary>
        /// Congestion incident type.
        /// </summary>
        Congestion(2),

        /// <summary>
        /// Disabled vehicle incident type.
        /// </summary>
        DisabledVehicle(3),

        /// <summary>
        /// Mass transit incident type.
        /// </summary>
        MassTransit(4),

        /// <summary>
        /// Miscellaneous incident type.
        /// </summary>
        Miscellaneous(5),

        /// <summary>
        /// Other news incident type.
        /// </summary>
        OtherNews(6),

        /// <summary>
        /// Planned event incident type.
        /// </summary>
        PlannedEvent(7),

        /// <summary>
        /// Road hazard incident type.
        /// </summary>
        RoadHazard(8),

        /// <summary>
        /// Construction incident type.
        /// </summary>
        Construction(9),

        /// <summary>
        /// Alert incident type.
        /// </summary>
        Alert(10),

        /// <summary>
        /// Weather incident type.
        /// </summary>
        Weather(11);

    private final int type;

    TrafficType(int type) {
        this.type = type;
    }
}
