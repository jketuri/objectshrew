package org.bing.enums;

/// <summary>
/// The level of confidence that the geocoded location result is a match.
/// </summary>
public enum ConfidenceLevel
{
    /// <summary>
    /// No confidence level set.
    /// </summary>
    None,

        /// <summary>
        /// High confidence match.
        /// </summary>
        High,

        /// <summary>
        /// Medium confidence match.
        /// </summary>
        Medium,

        /// <summary>
        /// Low confidence match.
        /// </summary>
        Low

        }

