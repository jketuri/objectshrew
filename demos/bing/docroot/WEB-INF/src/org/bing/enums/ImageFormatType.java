package org.bing.enums;

/// <summary>
/// Imagery format types.
/// </summary>
public enum ImageFormatType
{
    /// <summary>
    /// GIF image format.
    /// </summary>
    GIF,

        /// <summary>
        /// JPEG image format. JPEG format is the default for Road, Aerial and AerialWithLabels imagery.
        /// </summary>
        JPEG,

        /// <summary>
        /// PNG image format. PNG is the default format for CollinsBart and OrdnanceSurvey imagery.
        /// </summary>
        PNG
        }

