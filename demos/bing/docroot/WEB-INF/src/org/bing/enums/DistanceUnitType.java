package org.bing.enums;


/// <summary>
/// Units of measurements for distances.
/// </summary>
public enum DistanceUnitType
{
    /// <summary>
    /// Distances in Kilometers.
    /// </summary>
    KM,

        /// <summary>
        /// Distances in Miles
        /// </summary>
        Miles
        }
