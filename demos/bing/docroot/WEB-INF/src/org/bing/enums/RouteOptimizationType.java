package org.bing.enums;

/// <summary>
/// Specifies what parameters to use to optimize the route on the map.
/// </summary>
public enum RouteOptimizationType
{
    /// <summary>
    /// Optimizes route for shortest distance.
    /// </summary>
    Distance,

        /// <summary>
        /// Optimizes route for shortest travel time.
        /// </summary>
        Time,

        /// <summary>
        /// Optimizes route for shortest travel time with respect to current traffic conditions.
        /// </summary>
        TimeWithTraffic,

        /// <summary>
        /// The route is calculated to minimize the time and avoid road closures. Traffic information is not used in the calculation. 
        /// </summary>
        TimeAvoidClosure
        }
