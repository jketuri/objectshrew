package org.bing.enums;

/// <summary>
/// Relative elevation type.
/// </summary>
public enum ElevationType
{
    /// <summary>
    /// Geoid Earth model (EGM2008 2.5’).
    /// </summary>
    Sealevel,

        /// <summary>
        /// Ellipsoid Earth model (WGS84).
        /// </summary>
        Ellipsoid
        }
