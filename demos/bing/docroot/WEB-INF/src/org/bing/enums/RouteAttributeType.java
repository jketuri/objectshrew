package org.bing.enums;

/// <summary>
/// The type of route attributes to include in a route response.
/// </summary>
public enum RouteAttributeType
{
    /// <summary>
    /// Used to specify the following attributes as a group: excluteItinerary, routePath, and transitStops.
    /// </summary>
    All,

        /// <summary>
        /// Do not include detailed directions in the response. Detailed directions are provided as itinerary items and contain details such as written instructions and traffic location codes.
        /// </summary>
        ExcludeItinerary,
        
        /// <summary>
        /// Include a set of point (latitude and longitude) values that describe the route’s path in the response.
        /// </summary>
        RoutePath,

        /// <summary>
        /// Include only travel time and distance for the route, and does not provide other information. 
        /// </summary>
        RouteSummariesOnly,

        /// <summary>
        /// Include information about transit stops for transit routes.
        /// </summary>
        TransitStops
        }
