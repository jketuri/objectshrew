package org.bing.enums;

/// <summary>
/// Specifies the severity level of a traffic incident.
/// </summary>
public enum SeverityType {
    /// <summary>
    /// Low impact severity.
    /// </summary>
    LowImpact(1),

        /// <summary>
        /// Minor severity.
        /// </summary>
        Minor(2),

        /// <summary>
        /// Moderate severity.
        /// </summary>
        Moderate(3),

        /// <summary>
        /// Serious severity.
        /// </summary>
        Serious(4);

    private final int severity;

    SeverityType(int severity) {
        this.severity = severity;
    }
}
