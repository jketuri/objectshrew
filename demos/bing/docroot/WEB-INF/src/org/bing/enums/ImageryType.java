package org.bing.enums;

/// <summary>
/// Types of map imagery.
/// </summary>
public enum ImageryType
{
    /// <summary>
    /// Aerial imagery.
    /// </summary>
    Aerial,
        
        /// <summary>
        /// Aerial imagery with a road overlay.
        /// </summary>
        AerialWithLabels,

        /// <summary>
        /// Bird’s eye (oblique-angle) imagery
        /// </summary>
        Birdseye,

        /// <summary>
        /// Bird’s eye imagery with a road overlay.
        /// </summary>
        BirdseyeWithLabels,

        /// <summary>
        /// A dark version of the road maps.
        /// </summary>
        CanvasDark,

        /// <summary>
        /// A lighter version of the road maps which also has some of the details such as hill shading disabled.
        /// </summary>
		CanvasLight,

        /// <summary>
        /// A grayscale version of the road maps.
        /// </summary>
		CanvasGray,

        /// <summary>
        /// Roads without additional imagery. Uses legacy static tile service.
        /// </summary>
        Road,

        /// <summary>
        /// Roads without additional imagery. Uses dynamic tile service.
        /// </summary>
        RoadOnDemand,

        /// <summary>
        /// Ordnance Survey imagery.
        /// </summary>
        OrdnanceSurvey,

        /// <summary>
        /// Collins Bart imagery
        /// </summary>
        CollinsBart
        }
