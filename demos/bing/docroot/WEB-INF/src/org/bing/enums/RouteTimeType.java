package org.bing.enums;

/// <summary>
/// Specifies how to interpret the date and transit time value that is specified by the dateTime parameter.
/// </summary>
public enum RouteTimeType
{
    /// <summary>
    /// The dateTime parameter contains the desired arrival time for a transit request.
    /// </summary>
    Arrival,

        /// <summary>
        /// The dateTime parameter contains the desired departure time for a transit request.
        /// </summary>
        Departure,

        /// <summary>
        /// The dateTime parameter contains the latest departure time available for a transit request.
        /// </summary>
        LastAvailable
        }
