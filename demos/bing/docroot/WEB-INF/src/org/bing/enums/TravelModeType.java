
package org.bing.enums;

/// <summary>
/// The mode of travel for the route.
/// </summary>
public enum TravelModeType
{
    /// <summary>
    /// Driving mode.
    /// </summary>
    Driving,

        /// <summary>
        /// Walking mode.
        /// </summary>
        Walking,

        /// <summary>
        /// Transit mode.
        /// </summary>
        Transit
        }
