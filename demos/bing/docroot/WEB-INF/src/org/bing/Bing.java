/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class Bing implements Serializable {
    private final static long serialVersionUID = 0L;

    private Map<String, Object> defaultValues = new HashMap<String, Object>();
    private Set<String> invisibleSet = new HashSet<String>();

    protected final List<Object> tabList = new ArrayList<Object>();

    public Bing(final BingObjectView bingObjectView) {
        final Properties properties = bingObjectView.getProperties();
        final String applicationId = properties.getProperty(BingObjectView.class.getName() + ".applicationId"),
            token = properties.getProperty(BingObjectView.class.getName() + ".token");
        defaultValues.put("Bing.tabList.BingGeocode.geocodeModel._GeocodeInput.optional_request.optional_Credentials.optional_ApplicationId", applicationId);
        defaultValues.put("Bing.tabList.BingGeocode.geocodeModel._GeocodeInput.optional_request.optional_Credentials.optional_Token", token);
        invisibleSet.add("Bing.tabList.BingGeocode.geocodeModel._GeocodeInput.optional_request.optional_Credentials");
        defaultValues.put("Bing.tabList.BingReverseGeocode.reverseGeocodeModel._ReverseGeocodeInput.optional_request.optional_Credentials.optional_ApplicationId", applicationId);
        defaultValues.put("Bing.tabList.BingReverseGeocode.reverseGeocodeModel._ReverseGeocodeInput.optional_request.optional_Credentials.optional_Token", token);
        invisibleSet.add("Bing.tabList.BingReverseGeocode.reverseGeocodeModel._ReverseGeocodeInput.optional_request.optional_Credentials");
        defaultValues.put("Bing.tabList.BingGetMapUri.getMapUriModel._GetMapUriInput.optional_request.optional_Credentials.optional_ApplicationId", applicationId);
        defaultValues.put("Bing.tabList.BingGetMapUri.getMapUriModel._GetMapUriInput.optional_request.optional_Credentials.optional_Token", token);
        invisibleSet.add("Bing.tabList.BingGetMapUri.getMapUriModel._GetMapUriInput.optional_request.optional_Credentials");
        defaultValues.put("Bing.tabList.BingCalculateRoute.calculateRouteModel._CalculateRouteInput.optional_request.optional_Credentials.optional_ApplicationId", applicationId);
        defaultValues.put("Bing.tabList.BingCalculateRoute.calculateRouteModel._CalculateRouteInput.optional_request.optional_Credentials.optional_Token", token);
        invisibleSet.add("Bing.tabList.BingCalculateRoute.calculateRouteModel._CalculateRouteInput.optional_request.optional_Credentials");
        defaultValues.put("Bing.tabList.BingCalculateRoutesFromMajorRoads.calculateRoutesFromMajorRoadsModel._CalculateRoutesFromMajorRoadsInput.optional_request.optional_Credentials.optional_ApplicationId", applicationId);
        defaultValues.put("Bing.tabList.BingCalculateRoutesFromMajorRoads.calculateRoutesFromMajorRoadsModel._CalculateRoutesFromMajorRoadsInput.optional_request.optional_Credentials.optional_Token", token);
        invisibleSet.add("Bing.tabList.BingCalculateRoutesFromMajorRoads.calculateRoutesFromMajorRoadsModel._CalculateRoutesFromMajorRoadsInput.optional_request.optional_Credentials");
        defaultValues.put("Bing.tabList.BingSearch.searchModel._SearchInput.optional_request.optional_Credentials.optional_ApplicationId", applicationId);
        defaultValues.put("Bing.tabList.BingSearch.searchModel._SearchInput.optional_request.optional_Credentials.optional_Token", token);
        invisibleSet.add("Bing.tabList.BingSearch.searchModel._SearchInput.optional_request.optional_Credentials");
        tabList.add(new BingGeocode());
        tabList.add(new BingReverseGeocode());
        tabList.add(new BingGetMapUri());
        tabList.add(new BingCalculateRoute());
        tabList.add(new BingCalculateRoutesFromMajorRoads());
        tabList.add(new BingSearch());
    }

    public String namespace() {
        return "";
    }

    public Map<String, Object> defaultValues() {
        return defaultValues;
    }

    public Set<String> invisibleSet() {
        return invisibleSet;
    }

    public List<Object> getTabList() {
        return tabList;
    }

}
