/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.volunteer;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.NoSuchAlgorithmException;

public class Logout implements Serializable {
    private final static long serialVersionUID = 0L;

    protected VolunteerObjectView volunteerObjectView = null;
    protected boolean invisible = false;

    public Logout(final VolunteerObjectView volunteerObjectView) {
        this.volunteerObjectView = volunteerObjectView;
    }

    public boolean invisible() {
        return !volunteerObjectView.volunteer.login.loggedIn;
    }

    public void setInvisible(final boolean invisible) {
        this.invisible = invisible;
    }

    public String[] logout() {
        volunteerObjectView.volunteer.login.setInvisible(false);
        volunteerObjectView.volunteer.registration.setVisible(false);
        volunteerObjectView.volunteer.login.loggedIn = false;
        volunteerObjectView.volunteer.message = "Logged out";
        return new String[] {null, "VolunteerMain.tabList.Volunteer"};
    }

}
