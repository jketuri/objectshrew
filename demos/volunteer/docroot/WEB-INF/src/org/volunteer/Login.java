/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.volunteer;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.NoSuchAlgorithmException;

public class Login implements Serializable {
    private final static long serialVersionUID = 0L;

    protected VolunteerObjectView volunteerObjectView = null;
    protected Volunteer.VolunteerList searchVolunteerList = null;
    protected Volunteer searchVolunteer = null;
    protected String userName = null;
    protected String password = null;
    protected String passwordDigest = null;
    protected boolean invisible = false;
    protected boolean loggedIn = false;

    public Login() {
    }

    public Login(final VolunteerObjectView volunteerObjectView) {
        this.volunteerObjectView = volunteerObjectView;
        searchVolunteerList = new Volunteer.VolunteerList();
        searchVolunteer = new Volunteer();
        searchVolunteer.setLogin(new Login());
        searchVolunteerList.add(searchVolunteer);
    }

    public String id() {
        return userName;
    }

    public boolean invisible() {
        return invisible;
    }

    public void setInvisible(final boolean invisible) {
        this.invisible = invisible;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public String getVolatilePassword() {
        return password;
    }

    public void setVolatilePassword(final String password) {
        this.password = password;
    }

    public String getStoragePasswordDigest() {
        return passwordDigest;
    }

    public void setStoragePasswordDigest(final String passwordDigest) {
        this.passwordDigest = passwordDigest;
    }

    public Volunteer.VolunteerList getVolunteerSearch() {
        return searchVolunteerList;
    }

    public void setVolunteerSearch(final Volunteer.VolunteerList volunteerList) {
        if (volunteerList.isEmpty()) {
            if (volunteerObjectView.isMainUserType) {
                if (volunteerObjectView.mainUser.equals(getUserName())
                    && volunteerObjectView.mainPassword.equals(getVolatilePassword())) {
                    volunteerObjectView.isMainUser = true;
                    volunteerObjectView.volunteer.registration.setVisible(true);
                    volunteerObjectView.volunteer.message = "Main user logged in";
                    setInvisible(true);
                    return;
                }
            }
            volunteerObjectView.volunteer.message = "Credentials not found";
            return;
        }
        final Volunteer volunteer = volunteerList.get(0);
        volunteerObjectView.volunteer.registration.setName(volunteer.registration.getName());
        volunteerObjectView.volunteer.registration.setEmail(volunteer.registration.getEmail());
        volunteerObjectView.volunteer.registration.setMobile(volunteer.registration.getMobile());
        volunteerObjectView.volunteer.registration.setReserveName(volunteer.registration.getReserveName());
        volunteerObjectView.volunteer.registration.setReserveEmail(volunteer.registration.getReserveEmail());
        volunteerObjectView.volunteer.registration.setReserveMobile(volunteer.registration.getReserveMobile());
        volunteerObjectView.volunteer.registration.setVisible(true);
        volunteerObjectView.volunteer.message = "Logged in";
        loggedIn = true;
        setInvisible(true);
    }

    public String[] volunteerSearch()
        throws MalformedURLException, NoSuchAlgorithmException, UnsupportedEncodingException {
        searchVolunteer.login.setUserName(userName != null ? userName : "");
        if (volunteerObjectView.isMainUser) {
            searchVolunteer.login.setStoragePasswordDigest(null);
        } else {
            searchVolunteer.login.setStoragePasswordDigest(password != null ? volunteerObjectView.getPasswordDigest(password) : "");
        }
        return new String[] {null, "VolunteerMain.tabList.Volunteer"};
    }

    public boolean invisibleVolunteerSearch() {
        return volunteerObjectView.volunteer.registration.visible || loggedIn;
    }

}
