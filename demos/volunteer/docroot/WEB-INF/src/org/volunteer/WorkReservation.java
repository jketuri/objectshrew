/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.volunteer;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import org.vaadin.common.Support;

public class WorkReservation implements Serializable {
    private final static long serialVersionUID = 0L;

    protected String workTypeName = null;
    protected String userName = null;
    protected Calendar calendar = null;
    protected Date date = null;
    protected Integer year = null;
    protected Integer month = null;
    protected Integer hour = null;

    public WorkReservation() {
    }

    public WorkReservation(final String workTypeName,
                           final Calendar calendar,
                           final Integer hour,
                           final String userName) {
        this.workTypeName = workTypeName;
        this.calendar = calendar;
        this.hour = hour;
        this.userName = userName;
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
    }

    public boolean equals(Object other) {
        WorkReservation otherWorkReservation = (WorkReservation)other;
        return userName.equals(otherWorkReservation.userName)
            && workTypeName.equals(otherWorkReservation.workTypeName)
            && calendar.getTime().getTime() == otherWorkReservation.calendar.getTime().getTime()
            && hour.equals(otherWorkReservation.hour);
    }

    public String id() {
        return userName + "\t" + workTypeName + "\t" + calendar.getTime().getTime();
    }

    public void setWorkTypeName(String workTypeName) {
        this.workTypeName = workTypeName;
    }

    public String getWorkTypeName() {
        return workTypeName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setStorageCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public Calendar getStorageCalendar() {
        return calendar;
    }

    public Date getVolatileDate() {
        if (date == null) {
            final Calendar displayCalendar = Calendar.getInstance();
            displayCalendar.setTime(calendar.getTime());
            if (hour != null) {
                displayCalendar.set(Calendar.HOUR_OF_DAY, hour);
            }
            date = displayCalendar.getTime();
        }
        return date;
    }

    public void setStorageYear(Integer year) {
        this.year = year;
    }

    public Integer getStorageYear() {
        return year;
    }

    public void setStorageMonth(Integer month) {
        this.month = month;
    }

    public Integer getStorageMonth() {
        return month;
    }

    public void setStorageHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getStorageHour() {
        return hour;
    }

    public String toString() {
        return Support.toString(this);
    }

}

