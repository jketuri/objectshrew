/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.volunteer;

import java.io.File;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.vaadin.objectview.ObjectView.StorageType;

public class VolunteerMain implements Serializable {
    private final static long serialVersionUID = 0L;

    protected final List<Object> tabList = new ArrayList<Object>();

    protected VolunteerObjectView volunteerObjectView = null;

    public VolunteerMain(final VolunteerObjectView volunteerObjectView)
        throws MalformedURLException {
        this.volunteerObjectView = volunteerObjectView;
        volunteerObjectView.volunteer = new Volunteer(volunteerObjectView);
        volunteerObjectView.workTypes = new WorkTypes(volunteerObjectView);
        volunteerObjectView.reservation = new Reservation(volunteerObjectView);
        tabList.add(volunteerObjectView.volunteer);
        tabList.add(volunteerObjectView.workTypes);
        tabList.add(volunteerObjectView.reservation);
    }

    public StorageType storageType() {
        return StorageType.TDB;
    }

    public String storageDirectory() {
        String environment = System.getenv("ENVIRONMENT");
        String dataDirectory = null;
        if (!"development".equals(environment)) {
            Properties properties = volunteerObjectView.getProperties();
            dataDirectory = properties.getProperty(VolunteerObjectView.class.getName() + ".dataDirectory");
        }
        if (dataDirectory == null) {
            dataDirectory = System.getProperty("user.home");
        }
        return new File(dataDirectory, "VolunteerObjectView").getPath();
    }

    public List<Object> getTabList() {
        return tabList;
    }

}
