/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.volunteer;

import java.io.UnsupportedEncodingException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import com.vaadin.server.ConnectorResourceHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.server.VaadinSession;

import org.vaadin.common.Support;
import org.vaadin.objectview.ObjectView;
import org.vaadin.objectview.ObjectViewEnumerations;

public class VolunteerObjectView extends ObjectView {
    private final static long serialVersionUID = 0L;

    WorkTypes workTypes = null;
    Volunteer volunteer = null;
    Reservation reservation = null;
    String mainUser = null;
    String mainPassword = null;
    String mainUserParameter = null;
    String mainUserParameterValue = null;
    boolean isMainUserType = false;
    boolean isMainUser = false;

    public final VolunteerObjectView.WorkTypeList workTypeList = new VolunteerObjectView.WorkTypeList();

    public static class WorkTypeList extends ArrayList<WorkType> {
        private final static long serialVersionUID = 0L;
    }

    protected String getPasswordDigest(final String password)
        throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        messageDigest.update(password.getBytes("UTF-8"));
        return Support.toHexString(messageDigest.digest());
    }

    public Object getObject() {
        setTheme("stylecalendartheme");
        VaadinSession session = VaadinSession.getCurrent();
        session.addRequestHandler(new ConnectorResourceHandler() {

                private final static long serialVersionUID = 0L;

                public boolean handleRequest(final VaadinSession session,
                                             final VaadinRequest request,
                                             final VaadinResponse response) {
                    final Map<String, String[]> parameterMap = request.getParameterMap();
                    String userType[] = parameterMap.get(mainUserParameter);
                    if (userType != null && userType.length > 0) {
                        isMainUserType = mainUserParameterValue.equals(userType[0]);
                    }
                    return false;
                }

            });
        Properties properties = getProperties();
        mainUser = properties.getProperty(VolunteerObjectView.class.getName() + ".mainUserName");
        mainPassword = properties.getProperty(VolunteerObjectView.class.getName() + ".mainUserPassword");
        mainUserParameter = properties.getProperty(VolunteerObjectView.class.getName() + ".mainUserParameter");
        mainUserParameterValue = properties.getProperty(VolunteerObjectView.class.getName() + ".mainUserParameterValue");
        try {
            final String workTypes = properties.getProperty(VolunteerObjectView.class.getName() + ".workTypes");
            final XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
            final XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new StringReader(workTypes));
            XMLEvent xmlEvent = null;
            Map<String, String> workTypeMap = null;
            while (xmlEventReader.hasNext()) {
                xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent instanceof StartElement
                    && "workType".equals(((StartElement)xmlEvent).getName().getLocalPart())) {
                    workTypeMap = new HashMap<String, String>();
                    while (xmlEventReader.hasNext()) {
                        xmlEvent = xmlEventReader.nextTag();
                        if (xmlEvent instanceof EndElement
                            && "workType".equals(((EndElement)xmlEvent).getName().getLocalPart())) {
                            break;
                        }
                        if (xmlEvent instanceof StartElement) {
                            workTypeMap.put(((StartElement)xmlEvent).getName().getLocalPart(),
                                            xmlEventReader.nextEvent().asCharacters().getData());
                        }
                    }
                    workTypeList.add(new WorkType(this, workTypeMap.get("name"), workTypeMap.get("description"), 1));
                }
            }
            return new VolunteerMain(this);
        } catch (XMLStreamException xmlStreamException) {
            throw new RuntimeException(xmlStreamException);
        } catch (MalformedURLException malformedURLException) {
            throw new RuntimeException(malformedURLException);
        }
    }

}
