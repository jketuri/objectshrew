/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.volunteer;

import java.io.Serializable;

public class LoginLink extends NavigationLink {
    private final static long serialVersionUID = 0L;

    protected VolunteerObjectView volunteerObjectView = null;

    public LoginLink(final VolunteerObjectView volunteerObjectView) {
        super("Logging in", "VolunteerMain.tabList.Volunteer");
        this.volunteerObjectView = volunteerObjectView;
    }

    public String[] navigate() {
        volunteerObjectView.volunteer.login.setInvisible(false);
        volunteerObjectView.volunteer.registration.setVisible(false);
        return new String[] {link, link};
    }

}
