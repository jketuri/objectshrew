/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.volunteer;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import org.vaadin.common.Support;

public class Registration implements Serializable {
    private final static long serialVersionUID = 0L;

    protected VolunteerObjectView volunteerObjectView = null;
    protected Volunteer.VolunteerList registerSearchVolunteerList = null;
    protected Volunteer registerSearchVolunteer = null;
    protected Volunteer.VolunteerList registerVolunteerList = null;
    protected Volunteer.VolunteerList registerVolunteerStoreList = null;
    protected String name = null;
    protected String email = null;
    protected String mobile = null;
    protected String reserveName = null;
    protected String reserveEmail = null;
    protected String reserveMobile = null;
    protected String userName = null;
    protected String password = null;
    protected boolean credentialsVisible = false;
    protected boolean changed = false;
    protected boolean visible = false;

    public Registration() {
    }

    public Registration(final VolunteerObjectView volunteerObjectView) {
        this.volunteerObjectView = volunteerObjectView;
        registerSearchVolunteerList = new Volunteer.VolunteerList();
        registerSearchVolunteer = new Volunteer();
        registerSearchVolunteer.setLogin(new Login());
        registerSearchVolunteerList.add(registerSearchVolunteer);
        registerVolunteerStoreList = new Volunteer.VolunteerList();
        registerVolunteerStoreList.add(volunteerObjectView.volunteer);
    }

    public String id() {
        return userName;
    }

    protected void checkChange(final Object newValue,
                               final Object value) {
        if (newValue != null ? !newValue.equals(value) : value != null) {
            changed = true;
        }
    }

    public boolean invisible() {
        return !visible;
    }

    public void setVisible(final boolean visible) {
        this.visible = visible;
    }

    public String getTitle() {
        return "Registration";
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        checkChange(name, this.name);
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        checkChange(email, this.email);
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(final String mobile) {
        checkChange(mobile, this.mobile);
        this.mobile = mobile;
    }

    public String getReserveName() {
        return reserveName;
    }

    public void setReserveName(final String reserveName) {
        checkChange(reserveName, this.reserveName);
        this.reserveName = reserveName;
    }

    public String getReserveEmail() {
        return reserveEmail;
    }

    public void setReserveEmail(final String reserveEmail) {
        checkChange(reserveEmail, this.reserveEmail);
        this.reserveEmail = reserveEmail;
    }

    public String getReserveMobile() {
        return reserveMobile;
    }

    public void setReserveMobile(final String reserveMobile) {
        checkChange(reserveMobile, this.reserveMobile);
        this.reserveMobile = reserveMobile;
    }

    public String getTextCredentials() {
        return "Your credentials are";
    }

    public boolean invisibleTextCredentials() {
        return !credentialsVisible;
    }

    public String getFormOptionalVolatileUserName() {
        return userName;
    }

    public boolean invisibleFormOptionalVolatileUserName() {
        return !credentialsVisible;
    }

    public String getFormOptionalVolatilePasswordtext() {
        return password;
    }

    public boolean invisibleFormOptionalVolatilePasswordtext() {
        return !credentialsVisible;
    }

    public Volunteer.VolunteerList getRegisterStoreSearch() {
        registerSearchVolunteer.login.setUserName(getEmail());
        return registerSearchVolunteerList;
    }

    public void setRegisterStoreSearch(final Volunteer.VolunteerList volunteerList) {
        registerVolunteerList = volunteerList;
    }

    public Volunteer.VolunteerList getRegisterStore() {
        return registerVolunteerStoreList;
    }

    public String[] registerStore()
        throws MalformedURLException, NoSuchAlgorithmException, UnsupportedEncodingException {
        if (volunteerObjectView.volunteer.login.loggedIn) {
            return new String[] {"VolunteerMain.tabList.Reservation"};
        }
        if (registerVolunteerList.isEmpty()) {
            userName = email;
            final Random random = new Random(System.currentTimeMillis() ^ Long.MAX_VALUE);
            final StringBuilder builder = new StringBuilder();
            for (int index = 0; index < 10; index++) {
                builder.append((char)('0' + random.nextInt('z' - '0' + 1)));
            }
            password = builder.toString();
            volunteerObjectView.volunteer.login.setUserName(email);
            volunteerObjectView.volunteer.login.setStoragePasswordDigest(volunteerObjectView.getPasswordDigest(password));
            credentialsVisible = true;
            changed = false;
            volunteerObjectView.volunteer.message = "Registered";
            volunteerObjectView.volunteer.login.loggedIn = true;
        } else {
            volunteerObjectView.volunteer.message = "User already registered";
        }
        return new String[] {null, "VolunteerMain.tabList.Volunteer"};
    }

    public boolean invisibleRegisterStore() {
        return !visible || volunteerObjectView.volunteer.login.loggedIn;
    }

    public Volunteer.VolunteerList getUpdateStore() {
        return registerVolunteerStoreList;
    }

    public String[] updateStore()
        throws MalformedURLException, NoSuchAlgorithmException, UnsupportedEncodingException {
        if (!volunteerObjectView.volunteer.login.loggedIn) {
            return null;
        }
        if (changed) {
            changed = false;
            volunteerObjectView.volunteer.message = "Updated";
        } else {
            volunteerObjectView.volunteer.message = "No changes";
        }
        return new String[] {null, "VolunteerMain.tabList.Volunteer.textMessage"};
    }

    public boolean invisibleUpdateStore() {
        return !volunteerObjectView.volunteer.login.loggedIn;
    }

}
