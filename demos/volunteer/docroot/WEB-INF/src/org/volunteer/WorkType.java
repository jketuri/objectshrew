/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.volunteer;

import java.io.Serializable;
import java.math.BigDecimal;

public class WorkType implements Serializable {
    private final static long serialVersionUID = 0L;

    protected String description = null;
    protected String name = null;
    protected int numberVolunteers = 1;
    protected WorkTypeLink link = null;

    public WorkType() {
        this.name = "No selection";
        this.description = name;
        link = new WorkTypeLink(null, this, name);
    }

    public WorkType(final VolunteerObjectView volunteerObjectView,
                    final String name,
                    final String description,
                    final int numberVolunteers) {
        this.name = name;
        this.description = description;
        this.numberVolunteers = numberVolunteers;
        link = new WorkTypeLink(volunteerObjectView, this, name);
    }

    public WorkTypeLink getLinkVolatileWorkType() {
        return link;
    }

    public String getName() {
        return name;
    }

    public String getTextDescription() {
        return description;
    }

    public String toString() {
        return name;
    }

}
