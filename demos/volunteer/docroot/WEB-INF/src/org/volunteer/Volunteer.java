/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.volunteer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Volunteer implements Serializable {
    private final static long serialVersionUID = 0L;

    protected VolunteerObjectView volunteerObjectView = null;
    protected String message = null;
    protected NavigationLink loginLink = null;
    protected NavigationLink registrationLink = null;
    protected NavigationLink reservationsLink = null;

    Login login = null;
    Logout logout = null;
    Registration registration = null;

    public static class VolunteerList extends ArrayList<Volunteer> {
        private final static long serialVersionUID = 0L;
    }

    public Volunteer() {
    }

    public Volunteer(final VolunteerObjectView volunteerObjectView) {
        this.volunteerObjectView = volunteerObjectView;
        volunteerObjectView.volunteer = this;
        login = new Login(volunteerObjectView);
        logout = new Logout(volunteerObjectView);
        registration = new Registration(volunteerObjectView);
        loginLink = new LoginLink(volunteerObjectView);
        registrationLink = new RegistrationLink(volunteerObjectView);
        reservationsLink = new NavigationLink("Job reservations", "VolunteerMain.tabList.Reservation");
    }

    public String id() {
        return login.id();
    }

    public String getTitle() {
        return "Volunteer for Pioneering";
    }

    public String getTextOverview() {
        return "Overview of Volunteer for Pioneering";
    }

    public void setRegistration(final Registration registration) {
        this.registration = registration;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setLogin(final Login login) {
        this.login = login;
    }

    public Login getLogin() {
        return login;
    }

    public void setVolatileLogout(final Logout logout) {
        this.logout = logout;
    }

    public Logout getVolatileLogout() {
        return logout;
    }

    public NavigationLink getLinkVolatileLogin() {
        return loginLink;
    }

    public boolean invisibleLinkVolatileLogin() {
        return !login.invisible || login.loggedIn;
    }

    public NavigationLink getLinkVolatileRegistration() {
        return registrationLink;
    }

    public boolean invisibleLinkVolatileRegistration() {
        return registration.visible || login.loggedIn;
    }

    public NavigationLink getLinkVolatileReservations() {
        return reservationsLink;
    }

    public String getTextMessage() {
        return message;
    }

}
