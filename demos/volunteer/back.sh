#!/bin/sh

check()
{
    RV="$?"
    if test $RV != "0"
	then
        echo "RV=$RV"
	    exit 1
    fi
}

mkdir -p $HOME/back/volunteer
check
cp -frv * $HOME/back/volunteer/
check
rm -f $HOME/volunteer.zip
check
zip -rv $HOME/volunteer * -x 'build/*' -x '*.jar' -x '*.war' -x '*/.svn/*'
check
cp -v $HOME/volunteer.zip $HOME/volunteer_s
check
bzip2 $HOME/volunteer_s
check
mv -v $HOME/volunteer_s.bz2 $HOME/volunteer_s
check
ls -l $HOME/volunteer.zip $HOME/volunteer_s
check
