#!/bin/sh

check()
{
    RV="$?"
    if test $RV != "0"
	then
        echo "RV=$RV"
	    exit 1
    fi
}

mkdir -p $HOME/back/objectview
check
cp -frv * $HOME/back/objectview/
check
rm -f $HOME/objectview.zip
check
zip -rv $HOME/objectview * -x '*.class' -x 'build/*' -x '*.jar' -x '*.war' -x '*/.svn/*' -x 'demos/StyleCalendar/*' -x 'demos/g-leaflet/*' -x 'demos/g-leaflet-draw/*' -x 'demos/v-leaflet/*' -x 'demos/VAADIN/widgetsets/*' -x 'demos/volunteer/docroot/VAADIN/*' -x 'demos/hospitality/docroot/VAADIN/*' -x 'demos/bing/docroot/VAADIN/*' -x 'demos/amazon/docroot/VAADIN/*' -x 'demos/bingmaps/docroot/VAADIN/*' -x '*.mar'
check
cp -v $HOME/objectview.zip $HOME/objectview_s
check
bzip2 $HOME/objectview_s
check
mv -v $HOME/objectview_s.bz2 $HOME/objectview_s
check
ls -l $HOME/objectview.zip $HOME/objectview_s
check
