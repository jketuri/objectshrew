/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vaadin.objectview;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ObjectViewEnumerations {

    public static <T extends Enum<T>> T addEnum(final Class<T> enumType,
                                                final String name,
                                                final int ordinal)
        throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        final Constructor constructor = enumType.getDeclaredConstructors()[0];
        final Method[] methods = constructor.getClass().getDeclaredMethods();
        for (final Method method : methods) {
            if (method.getName().equals("acquireConstructorAccessor")) {
                method.setAccessible(true);
                method.invoke(constructor, new Object[0]);
            }
        }
        final Field[] fields = constructor.getClass().getDeclaredFields();
        Object constructorAccessor = null;
        for (Field field : fields) {
            if (field.getName().equals("constructorAccessor")) {
                field.setAccessible(true);
                constructorAccessor = field.get(constructor);
            }
        }
        final Method newInstance
            = constructorAccessor.getClass().getMethod("newInstance", new Class[] {Object[].class});
        newInstance.setAccessible(true);
        return (T)newInstance.invoke(constructorAccessor, new Object[] {new Object[] {name, ordinal}});
    }

}
