/**
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vaadin.objectview;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.wsdl.WSDLException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public abstract class ObjectViewWebFilter implements Filter
{
    protected FilterConfig config = null;

    public abstract ObjectView getObjectView();

    public void init(final FilterConfig config) {
        this.config = config;
    }

    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain chain)
        throws ServletException, IOException {
        Exception exception = null;
        final String relativeUri = ((HttpServletRequest)request).getPathInfo();
        if (relativeUri == null) {
            chain.doFilter(request, response);
            return;
        }
        final StringTokenizer tokens = new StringTokenizer(relativeUri, "/");
        if (!tokens.hasMoreTokens() || !tokens.nextToken().equals("services")
            || request.getAttribute(ObjectViewWebFilter.class.getName() + ".ObjectView") != null) {
            chain.doFilter(request, response);
            return;
        }
        try {
            final ObjectView objectView = getObjectView();
            final Object object = objectView.getObject();
            final Class objectClass = object.getClass();
            final Method namespaceMethod = ObjectView.getMethod(objectClass, "namespace");
            final String namespace = (String)namespaceMethod.invoke(object);
            objectView.setup(object, request.getLocale(), config.getServletContext());
            final String serviceName = objectClass.getSimpleName();
            final URL context = new URL(((HttpServletRequest)request).getRequestURL().toString());
            final Map parameterMap = request.getParameterMap();
            if (parameterMap != null) {
                String filename = null;
                Document document = null;
                final String values[] = (String[])parameterMap.get(""),
                    value = values != null && values.length > 0 ? values[0] : null;
                if (parameterMap.containsKey("wsdl") || "wsdl".equals(value)) {
                    document = objectView.webService.getWSDL(context);
                    filename = objectView.webService.getServiceName() + ".wsdl";
                } else if (parameterMap.containsKey("wadl") || "wadl".equals(value)) {
                    document = objectView.webService.getWADL(context);
                    filename = objectView.webService.getServiceName() + ".wadl";
                }
                if (document != null) {
                    final TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    final Transformer transformer = transformerFactory.newTransformer();
                    final DOMSource xmlSource = new DOMSource(document);
                    transformer.transform(xmlSource, new StreamResult(response.getOutputStream()));
                }
            }
            final RequestDispatcher requestDispatcher
                = config.getServletContext().getNamedDispatcher("AxisServlet");
            objectView.webService.getWSDL(context);
            request.setAttribute(ObjectViewWebFilter.class.getName() + ".ObjectView", objectView);
            requestDispatcher.forward(request, response);
        } catch (final ClassNotFoundException classNotFoundException) {
            exception = classNotFoundException;
        } catch (final IllegalAccessException illegalAccessException) {
            exception = illegalAccessException;
        } catch (final InstantiationException instantiationException) {
            exception = instantiationException;
        } catch (final InvocationTargetException invocationTargetException) {
            exception = invocationTargetException;
        } catch (final NoSuchMethodException noSuchMethodException) {
            exception = noSuchMethodException;
        } catch (final ParserConfigurationException parserConfigurationException) {
            exception = parserConfigurationException;
        } catch (final SAXException saxException) {
            exception = saxException;
        } catch (final TransformerException transformerException) {
            exception = transformerException;
        } catch (final URISyntaxException uriSyntaxException) {
            exception = uriSyntaxException;
        } catch (final WSDLException wsdlException) {
            exception = wsdlException;
        }
        if (exception != null) {
            throw new ServletException(exception);
        }
    }

    public void destroy() {
    }

}
