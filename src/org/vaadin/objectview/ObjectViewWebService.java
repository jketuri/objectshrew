/**
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vaadin.objectview;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeSet;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.wsdl.Binding;
import javax.wsdl.BindingFault;
import javax.wsdl.BindingInput;
import javax.wsdl.BindingOperation;
import javax.wsdl.BindingOutput;
import javax.wsdl.Definition;
import javax.wsdl.Fault;
import javax.wsdl.Input;
import javax.wsdl.Message;
import javax.wsdl.Operation;
import javax.wsdl.OperationType;
import javax.wsdl.Output;
import javax.wsdl.Part;
import javax.wsdl.Port;
import javax.wsdl.PortType;
import javax.wsdl.Service;
import javax.wsdl.Types;
import javax.wsdl.WSDLException;
import javax.wsdl.extensions.soap.SOAPAddress;
import javax.wsdl.extensions.soap.SOAPBinding;
import javax.wsdl.extensions.soap.SOAPFault;
import javax.wsdl.extensions.soap.SOAPHeader;
import javax.wsdl.extensions.soap.SOAPOperation;
import javax.wsdl.factory.WSDLFactory;
import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.axiom.soap.SOAPBody;
import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.axiom.soap.SOAPFactory;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.AddressingConstants;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.description.AxisEndpoint;
import org.apache.axis2.description.AxisOperation;
import org.apache.axis2.description.AxisOperationFactory;
import org.apache.axis2.description.AxisService;
import org.apache.axis2.description.WSDL11ToAxisServiceBuilder;
import org.apache.axis2.description.WSDL2Constants;
import org.apache.axis2.receivers.AbstractMessageReceiver;
import org.apache.axis2.receivers.AbstractInOutMessageReceiver;
import org.apache.axis2.transport.http.AxisServlet;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.neethi.Policy;
import org.apache.neethi.PolicyEngine;
import org.apache.ws.commons.schema.resolver.URIResolver;
import org.apache.xerces.dom.DOMImplementationImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

import com.vaadin.server.DeploymentConfiguration;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServlet;

import com.ibm.wsdl.extensions.schema.SchemaImpl;
import com.ibm.wsdl.extensions.soap.SOAPAddressImpl;
import com.ibm.wsdl.extensions.soap.SOAPBindingImpl;
import com.ibm.wsdl.extensions.soap.SOAPBodyImpl;
import com.ibm.wsdl.extensions.soap.SOAPFaultImpl;
import com.ibm.wsdl.extensions.soap.SOAPHeaderImpl;
import com.ibm.wsdl.extensions.soap.SOAPOperationImpl;

public class ObjectViewWebService implements LSResourceResolver, URIResolver
{
    protected static final Logger logger = LoggerFactory.getLogger(ObjectViewWebService.class);

    protected static final String WADL = "http://wadl.dev.java.net/2009/02";
    protected static final String WADL_URI = "http://www.w3.org/Submission/wadl/wadl.xsd";
    protected static final String WSSE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
    protected static final String WSSE_UTIL = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
    protected static final String XMLDSIG = "http://www.w3.org/TR/xmldsig-core/xmldsig-core-schema.xsd";
    protected static final String XML = "http://www.w3.org/2001/xml.xsd";
    protected static final String XML_DTD = "http://www.w3.org/2001/XMLSchema.dtd";

    protected static final String XML_TYPE = "text/xml";
    protected static final String JSON_TYPE = "application/json";

    protected static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    protected static final String DATE_FORMAT = "yyyy-MM-dd";
    protected static final String TIME_FORMAT = "HH:mm:ss.SSS";
    protected static final String DATE_TIME_TZ_FORMAT = ObjectViewWebService.DATE_TIME_FORMAT + "Z";
    protected static final String DATE_TZ_FORMAT = ObjectViewWebService.DATE_FORMAT + "Z";
    protected static final String TIME_TZ_FORMAT = ObjectViewWebService.TIME_FORMAT + "Z";

    protected static final DOMImplementationLS domImplementationLS
        = (DOMImplementationLS)DOMImplementationImpl.getDOMImplementation();
    protected final static Map<String, String> resourcePaths = new HashMap<String, String>();
    protected final static Map<Class, String> schemaTypes = new HashMap<Class, String>();
    protected final static Map<Class, ObjectViewWebService.StringToTypedValueConversion> stringToTypedValueConversions
        = new HashMap<Class, ObjectViewWebService.StringToTypedValueConversion>();
    protected final static Map<Class, ObjectViewWebService.TypedValueToStringConversion> typedValueToStringConversions
        = new HashMap<Class, ObjectViewWebService.TypedValueToStringConversion>();
    static {
        ObjectViewWebService.resourcePaths.put(org.apache.axis2.namespace.Constants.URI_WSDL11_SOAP,
                                               "soap.xsd");
        ObjectViewWebService.resourcePaths.put(org.apache.axis2.namespace.Constants.NS_URI_WSDL11,
                                               "wsdl.xsd");
        ObjectViewWebService.resourcePaths.put(ObjectViewWebService.WSSE,
                                               "oasis-200401-wss-wssecurity-secext-1.0.xsd");
        ObjectViewWebService.resourcePaths.put(ObjectViewWebService.WSSE_UTIL,
                                               "oasis-200401-wss-wssecurity-utility-1.0.xsd");
        ObjectViewWebService.resourcePaths.put(ObjectViewWebService.XMLDSIG,
                                               "xmldsig-core-schema.xsd");
        ObjectViewWebService.resourcePaths.put(ObjectViewWebService.XML,
                                               "xml.xsd");
        ObjectViewWebService.resourcePaths.put(ObjectViewWebService.XML_DTD,
                                               "XMLSchema.dtd");
        ObjectViewWebService.resourcePaths.put(ObjectViewWebService.WADL_URI,
                                               "wadl.xsd");
        ObjectViewWebService.schemaTypes.put(java.lang.Character.TYPE, "character");
        ObjectViewWebService.schemaTypes.put(java.lang.Character.class, "character");
        ObjectViewWebService.schemaTypes.put(java.lang.String.class, "string");
        ObjectViewWebService.schemaTypes.put(java.lang.Byte.TYPE, "byte");
        ObjectViewWebService.schemaTypes.put(java.lang.Byte.class, "byte");
        ObjectViewWebService.schemaTypes.put(java.lang.Boolean.TYPE, "boolean");
        ObjectViewWebService.schemaTypes.put(java.lang.Boolean.class, "boolean");
        ObjectViewWebService.schemaTypes.put(java.lang.Integer.TYPE, "int");
        ObjectViewWebService.schemaTypes.put(java.lang.Integer.class, "int");
        ObjectViewWebService.schemaTypes.put(java.lang.Short.TYPE, "short");
        ObjectViewWebService.schemaTypes.put(java.lang.Short.class, "short");
        ObjectViewWebService.schemaTypes.put(java.lang.Long.TYPE, "long");
        ObjectViewWebService.schemaTypes.put(java.lang.Long.class, "long");
        ObjectViewWebService.schemaTypes.put(java.lang.Double.TYPE, "double");
        ObjectViewWebService.schemaTypes.put(java.lang.Double.class, "double");
        ObjectViewWebService.schemaTypes.put(java.lang.Float.TYPE, "float");
        ObjectViewWebService.schemaTypes.put(java.lang.Float.class, "float");
        ObjectViewWebService.schemaTypes.put(java.math.BigDecimal.class, "decimal");
        ObjectViewWebService.schemaTypes.put(java.math.BigInteger.class, "integer");
        ObjectViewWebService.schemaTypes.put(java.sql.Date.class, "date");
        ObjectViewWebService.schemaTypes.put(java.sql.Time.class, "time");
        ObjectViewWebService.schemaTypes.put(java.sql.Timestamp.class, "dateTime");
        ObjectViewWebService.schemaTypes.put(java.util.Date.class, "dateTime");
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Character.TYPE,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

                                                                   public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                                                                       return value != null && value.length() > 0 ? Character.valueOf(value.charAt(0)) : null;
                                                                   }

                                                               });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Character.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                return Character.valueOf(value.charAt(0));
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.String.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                return value;
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Byte.TYPE,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

                                                                   public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                                                                       return Byte.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Byte.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                return Byte.valueOf(value);
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Boolean.TYPE,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

                                                                   public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                                                                       if ("true".equalsIgnoreCase(value)) {
                                                                           return Boolean.TRUE;
                                                                       }
                                                                       if ("false".equalsIgnoreCase(value)) {
                                                                           return Boolean.FALSE;
                                                                       }
                                                                       return null;
                                                                   }

                                                               });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Boolean.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                if ("true".equalsIgnoreCase(value)) {
                    return Boolean.TRUE;
                }
                if ("false".equalsIgnoreCase(value)) {
                    return Boolean.FALSE;
                }
                return null;
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Integer.TYPE,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

                                                                   public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                                                                       return Integer.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Integer.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                return Integer.valueOf(value);
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Short.TYPE,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

                                                                   public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                                                                       return Short.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Short.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                return Short.valueOf(value);
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Long.TYPE,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

                                                                   public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                                                                       return Long.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Long.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                return Long.valueOf(value);
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Double.TYPE,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

                                                                   public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                                                                       return Double.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Double.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                return Double.valueOf(value);
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Float.TYPE,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

                                                                   public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                                                                       return Float.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.stringToTypedValueConversions.put(java.lang.Float.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                return Float.valueOf(value);
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.math.BigDecimal.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                return new BigDecimal(value);
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.math.BigInteger.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value) {
                return new BigInteger(value);
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.sql.Date.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value)
                throws ParseException {
                final Calendar calendar = ObjectViewWebService.parseXmlDate(value);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                return new java.sql.Date(calendar.getTime().getTime());
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.sql.Time.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value)
                throws ParseException {
                final Calendar calendar = ObjectViewWebService.parseXmlTime(value);
                calendar.set(Calendar.YEAR, 0);
                calendar.set(Calendar.MONTH, 0);
                calendar.set(Calendar.DAY_OF_MONTH, 0);
                return new java.sql.Time(calendar.getTime().getTime());
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.sql.Timestamp.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value)
                throws ParseException {
                return new java.sql.Timestamp(ObjectViewWebService.parseXmlDateTime(value).getTime().getTime());
            }

        });
        ObjectViewWebService.stringToTypedValueConversions.put(java.util.Date.class,
                                                               new ObjectViewWebService.StringToTypedValueConversion() {

            public Object convert(final ObjectView.FieldInfo fieldInfo, final String value)
                throws ParseException {
                return new java.sql.Timestamp(ObjectViewWebService.parseXmlDateTime(value).getTime().getTime());
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Character.TYPE,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

                                                                   public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                                                                       return String.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Character.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return String.valueOf(value);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.String.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return (String)value;
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Byte.TYPE,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

                                                                   public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                                                                       return String.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Byte.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return String.valueOf(value);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Boolean.TYPE,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

                                                                   public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                                                                       return String.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Boolean.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return String.valueOf(value);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Integer.TYPE,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

                                                                   public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                                                                       return String.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Integer.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return String.valueOf(value);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Short.TYPE,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

                                                                   public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                                                                       return String.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Short.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return String.valueOf(value);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Long.TYPE,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

                                                                   public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                                                                       return String.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Long.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return String.valueOf(value);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Double.TYPE,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

                                                                   public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                                                                       return String.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Double.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return String.valueOf(value);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Float.TYPE,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

                                                                   public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                                                                       return String.valueOf(value);
                                                                   }

                                                               });
        ObjectViewWebService.typedValueToStringConversions.put(java.lang.Float.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return String.valueOf(value);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.math.BigDecimal.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return String.valueOf(value);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.math.BigInteger.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return String.valueOf(value);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.sql.Date.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return ObjectViewWebService.formatXmlDate((Date)value, true);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.sql.Time.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return ObjectViewWebService.formatXmlTime((Date)value, true);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.sql.Timestamp.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return ObjectViewWebService.formatXmlDateTime((Date)value, true);
            }

        });
        ObjectViewWebService.typedValueToStringConversions.put(java.util.Date.class,
                                                               new ObjectViewWebService.TypedValueToStringConversion() {

            public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
                return ObjectViewWebService.formatXmlDateTime((Date)value, true);
            }

        });
    }

    public String proxyName = null;
    public int proxyPort = 0;

    protected final Map<String, SchemaData> schemaDataMap = new HashMap<String, SchemaData>();
    protected final Map<String, byte[]> resources = new HashMap<String, byte[]>();

    protected Object object = null;
    protected String serviceName = null;
    protected String namespace = null;
    protected List<ObjectView.FieldInfo> fieldInfoList = null;
    protected Map<String, ObjectView.FieldInfo> typeFieldInfoMap = null;
    protected ConfigurationContext configurationContext = null;
    protected Policy policy = null;
    protected Document wadl = null;
    protected Document wsdl = null;
    protected Schema wadlSchema = null;
    protected Schema wsdlSchema = null;
    protected ServletContext servletContext = null;
    protected MultiThreadedHttpConnectionManager multiThreadedHttpConnectionManager = null;

    protected static class ConversionException extends Exception {
        private final static long serialVersionUID = 0L;

        ConversionException(final String message,
                            final Exception exception) {
            super(message, exception);
        }

    }

    protected static class StringToTypedValueConversion {

        public Object convert(final ObjectView.FieldInfo fieldInfo, final String value)
            throws ParseException {
            return value;
        }

    }

    protected static enum MessageType {REQUEST, RESPONSE, ERROR, ANY};

    protected static class TypedValueToStringConversion {

        public String convert(final ObjectView.FieldInfo fieldInfo, final Object value) {
            return value != null ? value.toString() : null;
        }

    }

    protected static class NamespaceData {
        final Map<String, OMNamespace> namespaces = new HashMap<String, OMNamespace>();
        final Map<String, String> namespacePrefixes = new HashMap<String, String>();
        int namespacePrefixIndex = 0;
    }

    protected class SchemaData {
        SchemaImpl schemaImpl = null;
        Set<String> typeNames = new HashSet<String>();
    }

    protected class ObjectViewInMessageReceiver extends AbstractInOutMessageReceiver {
        private final String namePath;

        ObjectViewInMessageReceiver(final String namePath) {
            this.namePath = namePath;
        }

        public void invokeBusinessLogic(final MessageContext inMessageContext,
                                        final MessageContext outMessageContext)
            throws AxisFault {
            try {
                executeRequest(inMessageContext, namePath);
            } catch (Exception exception) {
                ObjectViewWebService.logger.error(exception.toString(), exception);
                throw new AxisFault(exception.toString(), exception);
            }
        }

    }

    protected class ObjectViewInOutMessageReceiver extends AbstractInOutMessageReceiver {
        private final String namePath;

        ObjectViewInOutMessageReceiver(final String namePath) {
            this.namePath = namePath;
        }

        public void invokeBusinessLogic(final MessageContext inMessageContext,
                                        final MessageContext outMessageContext)
            throws AxisFault {
            try {
                final ObjectView.FieldInfo fieldInfo = executeRequest(inMessageContext, namePath);
                final Object outObject = fieldInfo.responseFieldInfo.accessor.invoke(fieldInfo.object);
                final OMFactory factory = OMAbstractFactory.getOMFactory();
                final HttpServletRequest request
                    = (HttpServletRequest)inMessageContext.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
                NamespaceData namespaceData = null;
                String contentType = request.getContentType();
                if (contentType != null) {
                    final int semicolon = contentType.indexOf(';');
                    if (semicolon != -1) {
                        contentType = contentType.substring(0, semicolon);
                    }
                    if (!contentType.trim().toLowerCase().endsWith("/json")) {
                        contentType = null;
                    }
                }
                if (contentType == null) {
                    namespaceData = new NamespaceData();
                }
                final OMElement outElement
                    = convertObjectToElements(factory, outObject, fieldInfo.responseFieldInfo, namespaceData,
                                              false, false);
                final SOAPFactory soapFactory = OMAbstractFactory.getSOAP11Factory();
                final SOAPEnvelope outEnvelope = soapFactory.createSOAPEnvelope();
                final SOAPBody body = soapFactory.createSOAPBody();
                body.addChild(outElement);
                outEnvelope.addChild(body);
                outMessageContext.setEnvelope(outEnvelope);
            } catch (Exception exception) {
                ObjectViewWebService.logger.error(exception.toString(), exception);
                throw new AxisFault(exception.toString(), exception);
            }
        }

    }

    protected static String convertFromXmlDateValue(String value,
                                                    final boolean hasFractionalSeconds,
                                                    final boolean[] useTimeZone) {
        int index,
            length = value.length();
        if (value.endsWith("Z")) {
            index = length - 1;
            useTimeZone[0] = true;
        } else {
            index = value.lastIndexOf('+');
            if (index == -1) {
                index = value.lastIndexOf('-');
                if (index != -1 && length - index != 5) {
                    index = -1;
                }
            }
            if (index != -1) {
                if (Character.isDigit(value.charAt(index + 1))) {
                    value = value.substring(0, index + 3)
                        + value.substring(index + 4);
                }
                useTimeZone[0] = true;
            } else {
                useTimeZone[0] = false;
            }
            if (index == -1) {
                index = length;
            }
        }
        int dot = value.indexOf('.');
        if (dot != -1) {
            final String fractionalSeconds = value.substring(dot, index);
            final long milliSeconds
                = (long)(Double.parseDouble(fractionalSeconds) * 1000.0 + .5);
            value = value.substring(0, dot + 1)
                + String.valueOf(milliSeconds) + value.substring(index);
        } else if (hasFractionalSeconds) {
            value = value.substring(0, index) + ".0";
            if (index < value.length()) {
                value += value.substring(index + 1);
            }
        }
        return value;
    }

    protected static String convertToXmlDateValue(String value,
                                                  final boolean useTimeZone) {
        if (useTimeZone) {
            value = value.substring(0, value.length() - 2) + ":"
                + value.substring(value.length() - 2);
        }
        return value;
    }

    protected static Calendar parseXmlDateValue(String value,
                                                String format,
                                                final String formatTZ,
                                                final boolean hasFractionalSeconds)
        throws ParseException {
        final boolean[] useTimeZone = new boolean[1];
        try {
            value = convertFromXmlDateValue(value, hasFractionalSeconds,
                                            useTimeZone);
        } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            ObjectViewWebService.logger.error(indexOutOfBoundsException.toString(), indexOutOfBoundsException);
            throw new ParseException(indexOutOfBoundsException.toString() + ", value=" + value, 0);
        }
        if (useTimeZone[0]) {
            format = formatTZ;
        }
        final DateFormat dateFormat
            = new SimpleDateFormat(format);
        dateFormat.parse(value);
        return dateFormat.getCalendar();
    }

    protected static Calendar parseXmlDateTime(final String value)
        throws ParseException {
        return parseXmlDateValue(value, ObjectViewWebService.DATE_TIME_FORMAT,
                                 ObjectViewWebService.DATE_TIME_TZ_FORMAT, true);
    }

    protected static Calendar parseXmlDate(final String value)
        throws ParseException {
        return parseXmlDateValue(value, ObjectViewWebService.DATE_FORMAT,
                                 ObjectViewWebService.DATE_TZ_FORMAT, false);
    }

    protected static Calendar parseXmlTime(final String value)
        throws ParseException {
        return parseXmlDateValue(value, ObjectViewWebService.TIME_FORMAT,
                                 ObjectViewWebService.TIME_TZ_FORMAT, true);
    }

    protected static String formatXmlDateValue(final Date date,
                                               String format,
                                               final String formatTZ,
                                               final boolean useTimeZone) {
        if (useTimeZone) {
            format = formatTZ;
        }
        final DateFormat dateFormat
            = new SimpleDateFormat(format);
        return convertToXmlDateValue(dateFormat.format(date), useTimeZone);
    }

    protected static String formatXmlDateTime(final Date date,
                                              final boolean useTimeZone) {
        return formatXmlDateValue(date,
                                  ObjectViewWebService.DATE_TIME_FORMAT,
                                  ObjectViewWebService.DATE_TIME_TZ_FORMAT,
                                  useTimeZone);
    }

    protected static String formatXmlDate(final Date date,
                                          final boolean useTimeZone) {
        return formatXmlDateValue(date, 
                                  ObjectViewWebService.DATE_FORMAT,
                                  ObjectViewWebService.DATE_TZ_FORMAT,
                                  useTimeZone);
    }

    protected static String formatXmlTime(final Date date,
                                          final boolean useTimeZone) {
        return formatXmlDateValue(date,
                                  ObjectViewWebService.TIME_FORMAT,
                                  ObjectViewWebService.TIME_TZ_FORMAT,
                                  useTimeZone);
    }

    public static ObjectViewWebService getObjectViewWebService(final ServletContext servletContext,
                                                               final Object object,
                                                               final String namespace,
                                                               final String serviceName,
                                                               final List<ObjectView.FieldInfo> fieldInfoList,
                                                               final Map<String, ObjectView.FieldInfo> typeFieldInfoMap) {
        synchronized (servletContext) {
            ObjectViewWebService objectViewWebService
                = (ObjectViewWebService)servletContext.getAttribute(ObjectViewWebService.class.getName());
            if (objectViewWebService == null) {
                objectViewWebService = new ObjectViewWebService(servletContext, object, namespace,
                                                                serviceName, fieldInfoList, typeFieldInfoMap);
                servletContext.setAttribute(ObjectViewWebService.class.getName(), objectViewWebService);
            }
            return objectViewWebService;
        }
    }

    private ObjectViewWebService(final ServletContext servletContext,
                                 final Object object,
                                 final String namespace,
                                 final String serviceName,
                                 final List<ObjectView.FieldInfo> fieldInfoList,
                                 final Map<String, ObjectView.FieldInfo> typeFieldInfoMap) {
        this.object = object;
        this.namespace = namespace;
        this.serviceName = serviceName;
        this.fieldInfoList = fieldInfoList;
        this.typeFieldInfoMap = typeFieldInfoMap;
        this.servletContext = servletContext;
        this.configurationContext
            = (ConfigurationContext)servletContext.getAttribute(AxisServlet.CONFIGURATION_CONTEXT);
        this.multiThreadedHttpConnectionManager = new MultiThreadedHttpConnectionManager();
        final HttpClient httpClient = new HttpClient(multiThreadedHttpConnectionManager);
        this.configurationContext.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, httpClient);
        this.configurationContext.setProperty(HTTPConstants.REUSE_HTTP_CLIENT, Constants.VALUE_TRUE);
        proxyName
            = servletContext.getInitParameter(ObjectViewWebService.class.getName() + ".proxyName");
        if (proxyName != null) {
            proxyPort
                = Integer.parseInt(servletContext.getInitParameter(ObjectViewWebService.class.getName() + ".proxyPort"));
        }
        try {
            final StAXOMBuilder builder = new StAXOMBuilder(servletContext.getResourceAsStream("/WEB-INF/policy.xml"));
            this.policy = PolicyEngine.getPolicy(builder.getDocumentElement());
        } catch (XMLStreamException xmlStreamException) {
            throw new RuntimeException(xmlStreamException);
        }
    }

    protected ObjectViewWebService() {
    }

    protected String getClassName(final String name) {
        return name.trim().replace('-', '_');
    }

    protected String getClassName(final String namespace,
                                  final String name)
        throws URISyntaxException {
        final StringBuilder builder = new StringBuilder();
        final StringTokenizer tokens = new StringTokenizer(new URI(namespace).getPath(), "/");
        while (tokens.hasMoreTokens()) {
            builder.append("_" + getClassName(tokens.nextToken()) + ".");
        }
        return builder.toString() + name;
    }

    public Properties getProperties() {
        final VaadinService service = VaadinService.getCurrent();
        final DeploymentConfiguration configuration = service.getDeploymentConfiguration();
        return configuration.getInitParameters();
    }

    public String getServiceName() {
        return serviceName;
    }

    public InputStream resolveEntity(final String systemId,
                                     final String baseURI)
        throws IOException {
        URL url = null;
        if (baseURI != null) {
            URL baseURL = null;
            if (baseURI.indexOf(':') != -1) {
                baseURL = new URL(baseURI);
            } else {
                baseURL = servletContext.getResource("/WEB-INF/" + baseURI);
            }
            url = new URL(baseURL, systemId);
        } else {
            if (systemId == null) {
                return null;
            }
            url = new URL(systemId);
        }
        final String name = url.toExternalForm();
        InputStream in = null;
        final byte[] resource = resources.get(name);
        if (resource != null) {
            if (ObjectViewWebService.logger.isTraceEnabled()) {
                ObjectViewWebService.logger.trace("Returning " + name + " from byte array");
            }
            return new ByteArrayInputStream(resource);
        }
        String resourcePath = ObjectViewWebService.resourcePaths.get(name);
        if (resourcePath != null) {
            in = servletContext.getResourceAsStream("/WEB-INF/" + resourcePath);
        }
        if (in == null) {
            resourcePath = url.getHost().toLowerCase() + url.getPath();
            in = servletContext.getResourceAsStream("/WEB-INF/" + resourcePath);
        }
        if (in != null) {
            if (ObjectViewWebService.logger.isTraceEnabled()) {
                ObjectViewWebService.logger.trace("Returning " + name + " from path " + resourcePath);
            }
        } else {
            in = url.openConnection().getInputStream();
            if (ObjectViewWebService.logger.isTraceEnabled()) {
                ObjectViewWebService.logger.trace("Reading from network " + url);
            }
        }
        return in;
    }

    public LSInput resolveResource(final String type,
                                   final String namespaceURI,
                                   final String publicId,
                                   final String systemId,
                                   final String baseURI) {
        try {
            final InputStream in = resolveEntity(systemId, baseURI);
            final LSInput lsInput
                = ObjectViewWebService.domImplementationLS.createLSInput();
            lsInput.setByteStream(in);
            return lsInput;
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    public InputSource resolveEntity(final String targetNamespace,
                                     final String schemaLocation,
                                     final String baseUri) {
        try {
            final InputStream in = resolveEntity(schemaLocation, baseUri);
            return new InputSource(in);
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    protected ObjectView.FieldInfo executeRequest(final MessageContext inMessageContext,
                                                  final String namePath)
        throws AxisFault, IllegalAccessException, InstantiationException, InvocationTargetException,
               MalformedURLException, NoSuchMethodException, URISyntaxException, XMLStreamException,
               ObjectViewWebService.ConversionException {
        final HttpServletRequest request
            = (HttpServletRequest)inMessageContext.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
        final ObjectView objectView
            = (ObjectView)request.getAttribute(ObjectViewWebFilter.class.getName() + ".ObjectView");
        final ObjectView.FieldInfo fieldInfo = objectView.executorFieldInfoMap.get(namePath);
        final SOAPEnvelope inEnvelope = inMessageContext.getEnvelope();
        final SOAPBody body = inEnvelope.getBody();
        final Iterator iterator = body.getChildElements();
        Object inObject = null;
        if (iterator != null && iterator.hasNext()) {
            inObject = convertElementsToObject((OMElement)iterator.next(), fieldInfo.requestFieldInfo,
                                               false, false, false);
        }
        fieldInfo.requestFieldInfo.mutator.invoke(fieldInfo.object, inObject);
        objectView.callExecutor(namePath);
        return fieldInfo;
    }

    protected String typeToSchemaType(final String name,
                                      final Class type) {
        final String schemaType = ObjectViewWebService.schemaTypes.get(type);
        if (schemaType != null) {
            return schemaType;
        }
        if (type.isEnum()) {
            return "string";
        }
        throw new RuntimeException("Service cannot be described with schema (name=" + name + ", type=" + type + ")");
    }

    protected Object stringToTypedValue(final ObjectView.FieldInfo fieldInfo,
                                        final String value)
        throws ObjectViewWebService.ConversionException {
        final Class type = fieldInfo.isList || fieldInfo.isArray ? fieldInfo.elementType : fieldInfo.type;
        final ObjectViewWebService.StringToTypedValueConversion stringToTypedValueConversion
            = ObjectViewWebService.stringToTypedValueConversions.get(type);
        Exception exception = null;
        try {
            if (stringToTypedValueConversion != null) {
                return stringToTypedValueConversion.convert(fieldInfo, value);
            }
        } catch (final NumberFormatException numberFormatException) {
            exception = numberFormatException;
        } catch (final ParseException parseException) {
            exception = parseException;
        }
        if (exception == null && type.isEnum()) {
            try {
                return Enum.valueOf(type, value);
            } catch (IllegalArgumentException illegalArgumentException) {
                exception = illegalArgumentException;
            }
        }
        throw new ObjectViewWebService.ConversionException("String value cannot be converted to typed value (name=" + fieldInfo.name + ", type=" + type + ", value=" + value + ")", exception);
    }

    protected String typedValueToString(final ObjectView.FieldInfo fieldInfo,
                                        final Object value)
        throws ObjectViewWebService.ConversionException {
        final ObjectViewWebService.TypedValueToStringConversion typedValueToStringConversion
            = ObjectViewWebService.typedValueToStringConversions.get(fieldInfo.type);
        if (typedValueToStringConversion != null) {
            return typedValueToStringConversion.convert(fieldInfo, value);
        }
        if (fieldInfo.type.isEnum()) {
            return value.toString();
        }
        throw new ObjectViewWebService.ConversionException("Typed value cannot be converted to string value (name=" + fieldInfo.name + ", type=" + fieldInfo.type + ", value=" + value + ")", null);
    }

    protected synchronized Document toWSDL(final String locationURI)
        throws AxisFault, ParserConfigurationException, SAXException, TransformerException, WSDLException {
        if (wsdl != null) {
            return wsdl;
        }
        final WSDLFactory factory = WSDLFactory.newInstance();
        final Definition definition = factory.newDefinition();
        definition.setTargetNamespace(namespace);
        definition.addNamespace("tns", namespace);
        definition.addNamespace("xsd", org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD);
        definition.addNamespace("soap", org.apache.axis2.namespace.Constants.URI_WSDL11_SOAP);
        definition.addNamespace("wsse", ObjectViewWebService.WSSE);
        definition.addNamespace("wsa", AddressingConstants.Submission.WSA_NAMESPACE);
        final AxisService axisService = makeWSDL(definition, locationURI);
        configurationContext.deployService(axisService);
        wsdl = factory.newWSDLWriter().getDocument(definition);
        return wsdl;
    }

    protected void validateWSDL(final String locationURI,
                                final Node node)
        throws IOException, ParserConfigurationException, SAXException, TransformerException, WSDLException {
        if (wsdlSchema == null) {
            toWSDL(locationURI);
        }
        validate(wsdlSchema, node);
    }
    
    protected void validateWADL(final String locationURI,
                                final Node node)
        throws IOException, ParserConfigurationException, SAXException, WSDLException {
        if (wadlSchema == null) {
            toWADL(locationURI);
        }
        validate(wadlSchema, node);
    }

    protected void validate(final Schema schema,
                            final Node node)
        throws IOException, SAXException {
        final Validator validator = schema.newValidator();
        validator.validate(new DOMSource(node));
    }

    protected Document newDocument()
        throws ParserConfigurationException {
        final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        return documentBuilder.newDocument();
    }

    protected AxisService makeWSDL(final Definition definition,
                                   final String locationURI)
        throws AxisFault, ParserConfigurationException, SAXException, TransformerException, WSDLException {
        final Document document = newDocument();
        definition.setTypes(getDocumentTypes(document, definition, locationURI));
        final PortType portType = definition.createPortType();
        portType.setQName(new QName(namespace, upperCaseFirst(serviceName) + "PortType"));
        portType.setUndefined(false);
        final Binding binding = definition.createBinding();
        binding.setQName(new QName(namespace, upperCaseFirst(serviceName) + "SoapBinding"));
        binding.setPortType(portType);
        binding.setUndefined(false);
        final SOAPBinding soapBinding = new SOAPBindingImpl();
        soapBinding.setStyle("document");
        soapBinding.setTransportURI("http://schemas.xmlsoap.org/soap/http");
        binding.addExtensibilityElement(soapBinding);
        final List<ObjectView.FieldInfo> messageFieldInfos = new ArrayList<ObjectView.FieldInfo>();
        addMessages(fieldInfoList, messageFieldInfos, definition, portType, binding, locationURI);
        definition.addPortType(portType);
        final Port port = definition.createPort();
        port.setBinding(binding);
        port.setName(serviceName + "Port");
        if (locationURI != null) {
            final SOAPAddress soapAddress = new SOAPAddressImpl();
            soapAddress.setLocationURI(locationURI);
            port.addExtensibilityElement(soapAddress);
        }
        final Service service = definition.createService();
        service.setQName(new QName(namespace, serviceName));
        service.addPort(port);
        definition.addService(service);
        final WSDL11ToAxisServiceBuilder serviceBuilder
            = new WSDL11ToAxisServiceBuilder(definition, service.getQName(),
                                             port.getName());
        serviceBuilder.setCustomResolver(this);
        serviceBuilder.setServerSide(true);
        final AxisService axisService = serviceBuilder.populateService();
        final Iterator<ObjectView.FieldInfo> messageFieldInfoItems
            = messageFieldInfos.iterator();
        while (messageFieldInfoItems.hasNext()) {
            final ObjectView.FieldInfo fieldInfo = messageFieldInfoItems.next();
            final AxisOperation axisOperation = axisService.getOperationByAction(fieldInfo.namePath);
            AbstractMessageReceiver messageReceiver = null;
            if (fieldInfo.responseFieldInfo != null) {
                messageReceiver = new ObjectViewInOutMessageReceiver(fieldInfo.namePath);
            } else {
                messageReceiver = new ObjectViewInMessageReceiver(fieldInfo.namePath);
            }
            axisOperation.setMessageReceiver(messageReceiver);
        }
        return axisService;
    }

    protected void addMessages(final List<ObjectView.FieldInfo> fieldInfoList,
                               final List<ObjectView.FieldInfo> messageFieldInfos,
                               final Definition definition,
                               final PortType portType,
                               final Binding binding,
                               final String locationURI)
        throws AxisFault, ParserConfigurationException, SAXException, TransformerException, WSDLException {
        final QName submissionWSAWNS =
            new QName(AddressingConstants.Submission.WSA_NAMESPACE, AddressingConstants.WSA_ACTION);
        final Map<String, Message> messages = new HashMap<String, Message>();
        final Iterator<ObjectView.FieldInfo> fieldInfos = fieldInfoList.iterator();
        while (fieldInfos.hasNext()) {
            final ObjectView.FieldInfo fieldInfo = fieldInfos.next();
            if (fieldInfo.isClient) {
                continue;
            }
            if (fieldInfo.requestFieldInfo == null) {
                if (fieldInfo.fieldInfoList != null) {
                    addMessages(fieldInfo.fieldInfoList, messageFieldInfos, definition, portType, binding,
                                locationURI);
                }
                continue;
            }
            final Operation operation = definition.createOperation();
            operation.setName(fieldInfo.namePath);
            operation.setUndefined(false);
            final BindingOperation bindingOperation = definition.createBindingOperation();
            bindingOperation.setName(fieldInfo.namePath);
            bindingOperation.setOperation(operation);
            final Input input = definition.createInput();
            input.setName(fieldInfo.namePath + "Input");
            input.setExtensionAttribute(submissionWSAWNS, namespace + "/" + fieldInfo.namePath);
            operation.setInput(input);
            Message inputMessage = messages.get(fieldInfo.requestFieldInfo.namePath);
            if (inputMessage == null) {
                inputMessage = addMessage(definition, fieldInfo.requestFieldInfo.namePath);
                addPart(definition, inputMessage, fieldInfo.requestFieldInfo.namePath);
                messages.put(fieldInfo.requestFieldInfo.namePath, inputMessage);
            }
            final BindingInput bindingInput = definition.createBindingInput();
            final javax.wsdl.extensions.soap.SOAPBody inputBody = new SOAPBodyImpl();
            inputBody.setUse("literal");
            final List inputParts = new ArrayList();
            inputParts.add(fieldInfo.requestFieldInfo.namePath + "Part");
            inputBody.setParts(inputParts);
            bindingInput.setName(fieldInfo.namePath + "Input");
            bindingInput.addExtensibilityElement(inputBody);
            bindingOperation.setBindingInput(bindingInput);
            if (fieldInfo.isProtected) {
                final Part part = definition.createPart();
                part.setName("Security");
                part.setElementName(new QName(ObjectViewWebService.WSSE, "Security"));
                inputMessage.addPart(part);
                final SOAPHeader soapHeader = new SOAPHeaderImpl();
                soapHeader.setMessage(new QName(namespace, fieldInfo.namePath));
                soapHeader.setPart("Security");
                soapHeader.setUse("literal");
                bindingInput.addExtensibilityElement(soapHeader);
            }
            input.setMessage(inputMessage);
            if (fieldInfo.responseFieldInfo != null) {
                operation.setStyle(OperationType.REQUEST_RESPONSE);
                final Output output = definition.createOutput();
                output.setName(fieldInfo.namePath + "Output");
                Message outputMessage = messages.get(fieldInfo.responseFieldInfo.namePath);
                if (outputMessage == null) {
                    outputMessage = addMessage(definition, fieldInfo.responseFieldInfo.namePath);
                    addPart(definition, outputMessage, fieldInfo.responseFieldInfo.namePath);
                    messages.put(fieldInfo.responseFieldInfo.namePath, outputMessage);
                }
                output.setMessage(outputMessage);
                operation.setOutput(output);
                final javax.wsdl.extensions.soap.SOAPBody outputBody = new SOAPBodyImpl();
                outputBody.setUse("literal");
                final List outputParts = new ArrayList();
                outputParts.add(fieldInfo.responseFieldInfo.namePath + "Part");
                outputBody.setParts(outputParts);
                final BindingOutput bindingOutput = definition.createBindingOutput();
                bindingOutput.setName(fieldInfo.namePath + "Output");
                bindingOutput.addExtensibilityElement(outputBody);
                bindingOperation.setBindingOutput(bindingOutput);
            } else {
                operation.setStyle(OperationType.ONE_WAY);
            }
            if (fieldInfo.fieldInfoList != null) {
                Message faultMessage = null;
                for (final ObjectView.FieldInfo elementFieldInfo: fieldInfo.fieldInfoList) {
                    if (elementFieldInfo.isError) {
                        if (faultMessage == null) {
                            final Fault fault = definition.createFault();
                            fault.setName("fault");
                            faultMessage = addMessage(definition, elementFieldInfo.namePath);
                            fault.setMessage(faultMessage);
                            operation.addFault(fault);
                            final SOAPFault soapFault = new SOAPFaultImpl();
                            soapFault.setName("fault");
                            soapFault.setUse("literal");
                            final BindingFault bindingFault = definition.createBindingFault();
                            bindingFault.setName("fault");
                            bindingFault.addExtensibilityElement(soapFault);
                            bindingOperation.addBindingFault(bindingFault);
                        }
                        addPart(definition, faultMessage, elementFieldInfo.namePath);
                    }
                }
            }
            portType.addOperation(operation);
            String verb = null;
            if (fieldInfo.namePath.endsWith("Retrieve")) {
                verb = "Retrieve";
            } else if (fieldInfo.namePath.endsWith("Update")) {
                verb = "Update";
            } else if (fieldInfo.namePath.endsWith("Add")) {
                verb = "Add";
            } else if (fieldInfo.namePath.endsWith("Delete")) {
                verb = "Delete";
            }
            final SOAPOperation soapOperation = new SOAPOperationImpl();
            soapOperation.setSoapActionURI(fieldInfo.namePath);
            soapOperation.setStyle("document");
            bindingOperation.addExtensibilityElement(soapOperation);
            binding.addBindingOperation(bindingOperation);
            definition.addBinding(binding);
            messageFieldInfos.add(fieldInfo);
        }
    }

    protected synchronized Document toWADL(final String locationURI)
        throws ParserConfigurationException, SAXException, WSDLException {
        if (wadlSchema != null) {
            return wadl;
        }
        final NamespaceData namespaceData = new NamespaceData();
        final Document wadl = newDocument();
        final Element application = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:application");
        application.setAttributeNS(org.apache.axis2.namespace.Constants.NS_URI_XMLNS, "xmlns:tns", namespace);
        application.setAttributeNS(org.apache.axis2.namespace.Constants.NS_URI_XMLNS,
                                   "xmlns:xsi", org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSI);
        application.setAttributeNS(org.apache.axis2.namespace.Constants.NS_URI_XMLNS, "xmlns:xsd",
                                   org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD);
        application.setAttributeNS(org.apache.axis2.namespace.Constants.NS_URI_XMLNS,
                                   "xmlns:wadl", ObjectViewWebService.WADL);
        application.setAttribute("xsi:schemaLocation", "http://wadl.dev.java.net/2009/02 wadl.xsd");
        final Element grammars = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:grammars"),
            schema = wadl.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:schema");
        schema.setAttribute("targetNamespace", namespace);
        schema.setAttributeNS(org.apache.axis2.namespace.Constants.NS_URI_XMLNS,
                              "xmlns:xsd", org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD);
        addImport(wadl, schema, ObjectViewWebService.WADL, "http://www.w3.org/Submission/wadl/wadl.xsd");
        final Element fault = wadl.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:element");
        fault.setAttribute("name", "Fault");
        final Element faultComplexType = wadl.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:complexType");
        final Element faultSequence = wadl.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:sequence");
        final Element faultCode = wadl.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:element");
        faultCode.setAttribute("name", "faultcode");
        faultCode.setAttribute("type", "xsd:string");
        faultSequence.appendChild(faultCode);
        final Element faultString = wadl.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:element");
        faultString.setAttribute("name", "faultstring");
        faultString.setAttribute("type", "xsd:string");
        faultSequence.appendChild(faultString);
        final Element faultActor = wadl.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:element");
        faultActor.setAttribute("name", "faultactor");
        faultActor.setAttribute("type", "xsd:anyURI");
        faultActor.setAttribute("minOccurs", "0");
        faultSequence.appendChild(faultActor);
        final Element detail = wadl.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:element");
        detail.setAttribute("name", "detail");
        detail.setAttribute("minOccurs", "0");
        final Element detailComplexType
            = makeComplexType(wadl, null, fieldInfoList, MessageType.ERROR, namespace, false, namespaceData);
        detail.appendChild(detailComplexType);
        faultSequence.appendChild(detail);
        faultComplexType.appendChild(faultSequence);
        fault.appendChild(faultComplexType);
        schema.appendChild(fault);
        makeComplexType(wadl, schema, fieldInfoList, MessageType.REQUEST, namespace, true, namespaceData);
        makeComplexType(wadl, schema, fieldInfoList, MessageType.RESPONSE, namespace, true, namespaceData);
        grammars.appendChild(schema);
        application.appendChild(grammars);
        final Element resources = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:resources");
        resources.setAttribute("base", locationURI);
        addResources(wadl, resources, schema, fieldInfoList);
        application.appendChild(resources);
        wadl.appendChild(application);
        this.wadl = wadl;
        wadlSchema = makeValidationSchema(schema);
        return wadl;
    }

    protected void addResources(final Document wadl,
                                final Element resources,
                                final Element schema,
                                final List<ObjectView.FieldInfo> fieldInfoList)
        throws WSDLException {
        Element resource = null;
        String namePath = null;
        final Iterator<ObjectView.FieldInfo> fieldInfos = fieldInfoList.iterator();
        while (fieldInfos.hasNext()) {
            final ObjectView.FieldInfo fieldInfo = fieldInfos.next();
            if (fieldInfo.isClient) {
                continue;
            }
            if (fieldInfo.requestFieldInfo == null) {
                if (fieldInfo.fieldInfoList != null) {
                    addResources(wadl, resources, schema, fieldInfo.fieldInfoList);
                }
                continue;
            }
            final boolean isRetrieve = fieldInfo.namePath.endsWith("Retrieve"),
                isDelete = fieldInfo.namePath.endsWith("Delete"),
                isUpdate = fieldInfo.namePath.endsWith("Update");
            final int length = isRetrieve ? "Retrieve".length() : isDelete ? "Delete".length()
                : isUpdate ? "Update".length() : "Add".length();
            final String fieldInfoNamePath = fieldInfo.namePath.substring(0, fieldInfo.namePath.length() - length);
            if (namePath == null || !fieldInfoNamePath.equals(namePath)) {
                if (resource != null) {
                    resources.appendChild(resource);
                }
                resource = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:resource");
                resource.setAttribute("path", fieldInfoNamePath);
                namePath = fieldInfoNamePath;
            }
            final Element method = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:method"),
                request = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:request");
            if (isRetrieve || isDelete) {
                if (isRetrieve) {
                    method.setAttribute("name", "GET");
                } else {
                    method.setAttribute("name", "DELETE");
                }
                final Element formatParam
                    = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:param");
                formatParam.setAttribute("name", "format");
                formatParam.setAttribute("style", "query");
                final Element xmlOption
                    = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:option");
                xmlOption.setAttribute("value", "xml");
                xmlOption.setAttribute("mediaType", ObjectViewWebService.XML_TYPE);
                formatParam.appendChild(xmlOption);
                final Element jsonOption
                    = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:option");
                jsonOption.setAttribute("value", "json");
                jsonOption.setAttribute("mediaType", ObjectViewWebService.JSON_TYPE);
                formatParam.appendChild(jsonOption);
                request.appendChild(formatParam);
                for (final ObjectView.FieldInfo parameterFieldInfo : fieldInfo.requestFieldInfo.fieldInfoList) {
                    if (parameterFieldInfo.mutator == null) {
                        continue;
                    }
                    final Element param
                        = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:param");
                    param.setAttribute("name", parameterFieldInfo.name);
                    param.setAttribute("style", "query");
                    request.appendChild(param);
                }
            } else {
                if (isUpdate) {
                    method.setAttribute("name", "PUT");
                } else {
                    method.setAttribute("name", "POST");
                }
                final Element representation
                    = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:representation");
                representation.setAttribute("mediaType", ObjectViewWebService.XML_TYPE);
                representation.setAttribute("element", "tns:" + fieldInfo.requestFieldInfo.namePath);
                request.appendChild(representation);
                final Element jsonRepresentation
                    = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:representation");
                jsonRepresentation.setAttribute("mediaType", ObjectViewWebService.JSON_TYPE);
                request.appendChild(jsonRepresentation);
            }
            method.appendChild(request);
            final Element response
                = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:response"),
                representation
                = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:representation");
            response.setAttribute("status", "200");
            representation.setAttribute("mediaType", ObjectViewWebService.XML_TYPE);
            representation.setAttribute("element", "tns:" + fieldInfo.responseFieldInfo.namePath);
            response.appendChild(representation);
            final Element jsonRepresentation
                = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:representation");
            jsonRepresentation.setAttribute("mediaType", ObjectViewWebService.JSON_TYPE);
            response.appendChild(jsonRepresentation);
            method.appendChild(response);
            final Element faultResponse
                = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:response"),
                faultRepresentation
                = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:representation");
            faultRepresentation.setAttribute("mediaType", ObjectViewWebService.XML_TYPE);
            faultRepresentation.setAttribute("element", "tns:Fault");
            faultResponse.setAttribute("status", "500");
            faultResponse.appendChild(faultRepresentation);
            final Element faultJsonRepresentation
                = wadl.createElementNS(ObjectViewWebService.WADL, "wadl:representation");
            faultJsonRepresentation.setAttribute("mediaType", ObjectViewWebService.JSON_TYPE);
            faultResponse.appendChild(faultJsonRepresentation);
            method.appendChild(faultResponse);
            resource.appendChild(method);
        }
        if (resource != null) {
            resources.appendChild(resource);
        }
    }

    protected Types getDocumentTypes(final Document document,
                                     final Definition definition,
                                     final String locationURI)
        throws SAXException, TransformerException, WSDLException {
        final NamespaceData namespaceData = new NamespaceData();
        final SchemaData schemaData = getSchemaData(document, namespace);
        final Element schema = schemaData.schemaImpl.getElement();
        addImport(document, schema, org.apache.axis2.namespace.Constants.NS_URI_WSDL11, null);
        addImport(document, schema, org.apache.axis2.namespace.Constants.URI_WSDL11_SOAP, null);
        addImport(document, schema, ObjectViewWebService.WSSE, null);
        makeComplexType(document, schema, fieldInfoList, MessageType.ERROR, namespace, true, namespaceData);
        makeComplexType(document, schema, fieldInfoList, MessageType.REQUEST, namespace, true, namespaceData);
        makeComplexType(document, schema, fieldInfoList, MessageType.RESPONSE, namespace, true, namespaceData);
        final Types types = definition.createTypes();
        for (final SchemaData aSchemaData : schemaDataMap.values()) {
            types.addExtensibilityElement(aSchemaData.schemaImpl);
            addImports(document, aSchemaData.schemaImpl.getElement(), namespaceData);
        }
        wsdlSchema = makeValidationSchema(schema);
        return types;
    }

    protected void addImports(final Document document,
                              final Element schema,
                              final NamespaceData namespaceData)
        throws TransformerException {
        final String targetNamespace = schema.getAttribute("targetNamespace");
        final Node referenceNode = schema.getFirstChild();
        for (final Map.Entry<String, String> namespacePrefixEntry
                 : namespaceData.namespacePrefixes.entrySet()) {
            final String namespace = namespacePrefixEntry.getKey(),
                namespacePrefix = namespacePrefixEntry.getValue();
            schema.setAttributeNS(org.apache.axis2.namespace.Constants.NS_URI_XMLNS,
                                  "xmlns:" + namespacePrefix, namespace);
            if (!namespace.equals(targetNamespace)) {
                final Element importElement
                    = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "import");
                importElement.setAttribute("namespace", namespace);
                schema.insertBefore(importElement, referenceNode);
            }
        }
        saveResource(schema, targetNamespace);
    }

    protected byte[] toBytes(final Element element,
                             final boolean indent)
        throws TransformerException {
        final DOMSource domSource = new DOMSource(element);
        final ByteArrayOutputStream bout = new ByteArrayOutputStream();
        final StreamResult streamResult = new StreamResult(bout);
        final Transformer transformer
            = TransformerFactory.newInstance().newTransformer();
        if (indent) {
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        }
        transformer.transform(domSource, streamResult);
        return bout.toByteArray();
    }

    protected byte[] saveResource(final Element element,
                                  final String name)
        throws TransformerException {
        final byte[] resource = toBytes(element, false);
        resources.put(name, resource);
        return resource;
    }

    protected SchemaData getSchemaData(final Document document,
                                       final String namespace) {
        SchemaData schemaData = schemaDataMap.get(namespace);
        if (schemaData != null) {
            return schemaData;
        }
        schemaData = new SchemaData();
        schemaData.schemaImpl = new SchemaImpl();
        schemaDataMap.put(namespace, schemaData);
        final Element schema = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:schema");
        schema.setAttribute("targetNamespace", namespace);
        schema.setAttributeNS(org.apache.axis2.namespace.Constants.NS_URI_XMLNS,
                              "xmlns:tns", namespace);
        schema.setAttributeNS(org.apache.axis2.namespace.Constants.NS_URI_XMLNS,
                              "xmlns:xsd", org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD);
        schemaData.schemaImpl.setElement(schema);
        schemaData.schemaImpl.setElementType(new QName(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "schema"));
        return schemaData;
    }

    protected void addComplexType(final Document document,
                                  final Element element,
                                  final Element complexType,
                                  final String name,
                                  String namespace,
                                  final NamespaceData namespaceData) {
        if (namespace == null) {
            namespace = ObjectViewWebService.this.namespace;
        }
        final String namespacePrefix = getNamespacePrefix(namespace, namespaceData),
            typeName = upperCaseFirst(name);
        element.setAttribute("type", namespacePrefix + ":" + typeName);
        final SchemaData schemaData = getSchemaData(document, namespace);
        if (!schemaData.typeNames.contains(typeName)) {
            final Element schema = schemaData.schemaImpl.getElement();
            complexType.setAttribute("name", typeName);
            schema.appendChild(complexType);
            schemaData.typeNames.add(typeName);
        }
    }

    protected void addSchemaType(final Document document,
                                 final Element element,
                                 final ObjectView.FieldInfo fieldInfo,
                                 String typeName)
        throws WSDLException {
        if (typeName == null) {
            typeName = typeToSchemaType(fieldInfo.name, fieldInfo.type);
        }
        if ("character".equals(typeName)) {
            final Element simpleType
                = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD,
                                           "xsd:simpleType");
            final Element restriction
                = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD,
                                           "xsd:restriction");
            restriction.setAttribute("base", "xsd:string");
            final Element length
                = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD,
                                           "xsd:length");
            length.setAttribute("value", "1");
            restriction.appendChild(length);
            simpleType.appendChild(restriction);
            element.appendChild(simpleType);
            return;
        }
        element.setAttribute("type", "xsd:" + typeName);
    }

    protected void addImport(final Document document,
                             final Element schema,
                             final String namespace,
                             String schemaLocation) {
        if (schemaLocation == null) {
            schemaLocation = namespace;
        }
        final Element include = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:import");
        include.setAttribute("namespace", namespace);
        include.setAttribute("schemaLocation", schemaLocation);
        schema.appendChild(include);
    }

    protected Schema makeValidationSchema(final Element schemaElement)
        throws SAXException {
        final SchemaFactory schemaFactory
            = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        schemaFactory.setResourceResolver(this);
        final Schema schema
            = schemaFactory.newSchema(new DOMSource(schemaElement));
        return schema;
    }

    private final String upperCaseFirst(final String name) {
        return Character.toUpperCase(name.charAt(0)) + name.substring(1);
    }

    protected Element makeComplexType(final Document document,
                                      Element parent,
                                      final List<ObjectView.FieldInfo> fieldInfoList,
                                      final MessageType messageType,
                                      final String namespace,
                                      final boolean isTopLevelElement,
                                      final NamespaceData namespaceData)
        throws WSDLException {
        Element complexType = null, sequence = null;
        if (parent == null) {
            complexType = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:complexType");
            parent = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:sequence");
        }
        boolean added = false;
        for (final ObjectView.FieldInfo fieldInfo : fieldInfoList) {
            if (fieldInfo.isClient || fieldInfo.isMenu
                || fieldInfo.accessor == null && fieldInfo.mutator == null
                || messageType == MessageType.REQUEST && !fieldInfo.isRequest
                || messageType == MessageType.RESPONSE && !fieldInfo.isResponse
                || messageType == MessageType.ERROR && !fieldInfo.isError
                || messageType == MessageType.ANY && fieldInfo.fieldInfoList != null) {
                if (!fieldInfo.isClient && !fieldInfo.isMenu && fieldInfo.fieldInfoList != null) {
                    makeComplexType(document, parent, fieldInfo.fieldInfoList, messageType,
                                    fieldInfo.namespace, isTopLevelElement, namespaceData);
                    added = true;
                }
                continue;
            }
            if ("".equals(fieldInfo.name)) {
                final Element any = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:any");
                parent.appendChild(any);
                added = true;
                continue;
            }
            final Element element = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:element");
            element.setAttribute("name", isTopLevelElement ? fieldInfo.namePath : fieldInfo.name);
            if (fieldInfo.isList) {
                if (fieldInfo.isSimple) {
                    final Element elementSimpleType = makeSimpleType(document, fieldInfo, fieldInfo.elementType);
                    if (elementSimpleType != null) {
                        element.appendChild(elementSimpleType);
                    } else {
                        final String elementTypeName = typeToSchemaType(fieldInfo.name, fieldInfo.elementType);
                        addSchemaType(document, element, fieldInfo, elementTypeName);
                    }
                } else if (fieldInfo.fieldInfoList != null) {
                    final Element elementComplexType
                        = makeComplexType(document, null, fieldInfo.fieldInfoList, MessageType.ANY,
                                          fieldInfo.namespace, false, namespaceData);
                    if (fieldInfo.namespace != null && namespace != null
                        && !fieldInfo.namespace.equals(namespace)) {
                        addComplexType(document, element, elementComplexType, fieldInfo.name, fieldInfo.namespace,
                                       namespaceData);
                    } else {
                        element.appendChild(elementComplexType);
                    }
                }
                if (!isTopLevelElement) {
                    element.setAttribute("minOccurs", "0");
                    element.setAttribute("maxOccurs", "unbounded");
                }
            } else if (fieldInfo.fieldInfoList != null) {
                final Element elementComplexType
                    = makeComplexType(document, null, fieldInfo.fieldInfoList, MessageType.ANY,
                                      fieldInfo.namespace, false, namespaceData);
                if (fieldInfo.namespace != null && namespace != null
                    && !fieldInfo.namespace.equals(namespace)) {
                    addComplexType(document, element, elementComplexType, fieldInfo.name, fieldInfo.namespace,
                                   namespaceData);
                } else {
                    element.appendChild(elementComplexType);
                }
            } else if (fieldInfo.isSimple) {
                final Element simpleType = makeSimpleType(document, fieldInfo, null);
                if (simpleType != null) {
                    element.appendChild(simpleType);
                } else {
                    addSchemaType(document, element, fieldInfo, null);
                }
            }
            if (!fieldInfo.isList && complexType != null) {
                element.setAttribute("minOccurs", fieldInfo.isOptional ? "0" : "1");
                element.setAttribute("maxOccurs", "1");
            }
            parent.appendChild(element);
            added = true;
        }
        if (complexType != null) {
            if (added) {
                complexType.appendChild(parent);
            }
            return complexType;
        }
        return parent;
    }

    protected Element makeSimpleType(final Document document,
                                     final ObjectView.FieldInfo fieldInfo,
                                     Class type) {
        if (fieldInfo.minLength == 0
            && fieldInfo.maxLength == 0
            && fieldInfo.pattern == null
            && fieldInfo.values == null) {
            return null;
        }
        if (type == null) {
            type = fieldInfo.type;
        }
        final Element simpleType = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:simpleType");
        final Element restriction = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:restriction");
        restriction.setAttribute("base", "xsd:" + typeToSchemaType(fieldInfo.name, fieldInfo.type));
        if (fieldInfo.minLength > 0) {
            if (fieldInfo.minLength == fieldInfo.maxLength) {
                final Element length
                    = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:length");
                length.setAttribute("value", String.valueOf(fieldInfo.minLength));
                restriction.appendChild(length);
            } else {
                final Element minLength
                    = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:minLength");
                minLength.setAttribute("value", String.valueOf(fieldInfo.minLength));
                restriction.appendChild(minLength);
            }
        }
        if (fieldInfo.maxLength > 0
            && fieldInfo.maxLength != fieldInfo.minLength) {
            final Element maxLength
                = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:maxLength");
            maxLength.setAttribute("value", String.valueOf(fieldInfo.maxLength));
            restriction.appendChild(maxLength);
        }
        if (fieldInfo.pattern != null) {
            final Element pattern
                = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:pattern");
            pattern.setAttribute("value", fieldInfo.pattern);
            restriction.appendChild(pattern);
        }
        if (fieldInfo.values != null) {
            for (final String value : fieldInfo.values) {
                final Element enumeration
                    = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD, "xsd:enumeration");
                enumeration.setAttribute("value", value);
                restriction.appendChild(enumeration);
            }
        }
        simpleType.appendChild(restriction);
        return simpleType;
    }

    protected Part addPart(final Definition definition,
                           final Message message,
                           final String namePath) {
        final Part part = definition.createPart();
        part.setName(namePath + "Part");
        part.setElementName(new QName(namespace, namePath));
        message.addPart(part);
        return part;
    }

    /**
     * Adds SOAP message definition
     * @param definition definition where WSDL-elements are added
     * @param namePath name of the SOAP-message
     * @throws WSDLException if gettings WSDL-part of service parameter fails
     */
    protected Message addMessage(final Definition definition,
                                 final String namePath)
        throws WSDLException {
        final Message message = definition.createMessage();
        message.setQName(new QName(namespace, namePath + "Message"));
        message.setUndefined(false);
        definition.addMessage(message);
        return message;
    }

    protected List<Object> convertElementsToObjectList(List<Object> list,
                                                       final OMElement elementValue[],
                                                       final Iterator listElements,
                                                       final ObjectView.FieldInfo fieldInfo)
        throws IllegalAccessException, InstantiationException, InvocationTargetException, URISyntaxException,
               ObjectViewWebService.ConversionException {
        String name = fieldInfo.name;
        if (fieldInfo.isForm) {
            name = name.substring("form".length());
        }
        if (fieldInfo.isOptional) {
            name = name.substring("optional".length());
        }
        if (fieldInfo.isReadOnly) {
            name = name.substring("readOnly".length());
        }
        if (name.startsWith("_")) {
            name = name.substring(1);
        }
        if (name.startsWith("_")) {
            name = name.substring(1);
        } else if (fieldInfo.isForm || fieldInfo.isOptional || fieldInfo.isReadOnly) {
            name = Character.toLowerCase(name.charAt(0)) + name.substring(1);
        }
        if (list == null) {
            if (fieldInfo.type.isArray()) {
                list = new ArrayList();
            } else {
                list = (List<Object>)fieldInfo.type.newInstance();
            }
        } else {
            list.clear();
        }
        for (;;) {
            OMElement listElement = null;
            if (elementValue[0] != null) {
                listElement = elementValue[0];
                elementValue[0] = null;
            } else {
                if (!listElements.hasNext()) {
                    break;
                }
                listElement = (OMElement)listElements.next();
            }
            if (!listElement.getLocalName().equals(name)) {
                elementValue[0] = listElement;
                break;
            }
            Object value;
            if (fieldInfo.fieldInfoList != null) {
                value = convertElementsToObject(listElement, fieldInfo, true, true, false);
            } else {
                value = stringToTypedValue(fieldInfo, listElement.getText());
            }
            list.add(value);
        }
        return list;
    }

    protected Object convertElementsToObject(final OMElement element,
                                             ObjectView.FieldInfo fieldInfo,
                                             final boolean isListElement,
                                             final boolean isInList,
                                             final boolean isRoot)
        throws IllegalAccessException, InstantiationException, InvocationTargetException, URISyntaxException,
               ObjectViewWebService.ConversionException {
        final String name = element.getLocalName();
        final Iterator childElements = element.getChildElements();
        final OMElement[] elementValue = new OMElement[1];
        final String type
            = element.getAttributeValue(new QName(XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI, "type"));
        if (type != null) {
            String className = null;
            int colon = type.indexOf(':');
            if (colon != -1) {
                final String prefix = type.substring(0, colon),
                    typeName = type.substring(colon + 1),
                    namespace = element.findNamespaceURI(prefix).getNamespaceURI();
                className = getClassName(namespace, typeName);
            } else {
                className = getClassName(type);
            }
            fieldInfo = typeFieldInfoMap.get(className);
        }
        Object object = isListElement ? fieldInfo.elementType.newInstance() : isInList
            ? fieldInfo.type.newInstance() : fieldInfo.valueObject;
        ObjectView.FieldInfo elementFieldInfo = null;
        for (;;) {
            OMElement childElement = null;
            if (elementValue[0] != null) {
                childElement = elementValue[0];
                elementValue[0] = null;
            } else {
                if (!childElements.hasNext()) {
                    break;
                }
                childElement = (OMElement)childElements.next();
            }
            final String elementName = childElement.getLocalName();
            if (elementFieldInfo == null && isRoot) {
                elementFieldInfo = fieldInfo;
            } else {
                for (boolean isForm = false;; isForm = true) {
                    for (boolean isOptional = false;; isOptional = true) {
                        for (boolean isReadOnly = false;; isReadOnly = true) {
                            StringBuilder elementNameBuilder = new StringBuilder();
                            if (isForm) {
                                elementNameBuilder.append("form");
                            }
                            if (isOptional) {
                                if (elementNameBuilder.length() > 0) {
                                    elementNameBuilder.append("Optional");
                                } else {
                                    elementNameBuilder.append("optional");
                                }
                            }
                            if (isReadOnly) {
                                if (elementNameBuilder.length() > 0) {
                                    elementNameBuilder.append("ReadOnly");
                                } else {
                                    elementNameBuilder.append("readOnly");
                                }
                            }
                            String elementPrefix = elementNameBuilder.toString();
                            elementFieldInfo = fieldInfo.fieldInfoMap.get(elementPrefix + (elementPrefix.length() > 0 ? Character.toUpperCase(elementName.charAt(0)) + elementName.substring(1) : elementName));
                            if (elementFieldInfo == null) {
                                elementFieldInfo = fieldInfo.fieldInfoMap.get(elementPrefix + "_" + elementName);
                            }
                            if (elementFieldInfo != null
                                && (!isForm || elementFieldInfo.isForm)
                                && (!isOptional || elementFieldInfo.isOptional)
                                && (!isReadOnly || elementFieldInfo.isReadOnly)) {
                                isReadOnly = isOptional = isForm = true;
                            } else {
                                elementFieldInfo = null;
                            }
                            if (isReadOnly) {
                                break;
                            }
                        }
                        if (isOptional) {
                            break;
                        }
                    }
                    if (isForm) {
                        break;
                    }
                }
                if (elementFieldInfo == null) {
                    throw new ObjectViewWebService.ConversionException("Element definition cannot be found (name=" + name + ", elementName=" + elementName + ")", null);
                }
            }
            if (elementFieldInfo.isList || elementFieldInfo.isArray) {
                elementValue[0] = childElement;
                List<Object> valueList = null;
                if (elementFieldInfo.isArray) {
                    Object[] array = (Object[])elementFieldInfo.accessor.invoke(object);
                    if (array != null) {
                        valueList = new ArrayList(Arrays.asList(array));
                    }
                } else {
                    valueList = (List<Object>)elementFieldInfo.accessor.invoke(object);
                }
                valueList
                    = convertElementsToObjectList(valueList, elementValue, childElements, elementFieldInfo);
                if (object == null) {
                    return valueList;
                }
                Object value = null;
                if (elementFieldInfo.isArray) {
                    value = Array.newInstance(elementFieldInfo.elementType, valueList.size());
                    for (int index = 0; index < valueList.size(); index++) {
                        Array.set(value, index, valueList.get(index));
                    }
                } else {
                    value = valueList;
                }
                try {
                    elementFieldInfo.mutator.invoke(object, value);
                } catch (Exception exception) {
                    throw new ObjectViewWebService.ConversionException("Value cannot be set (mutator=" + elementFieldInfo.mutator + ", objectClass=" + object.getClass() + ", name=" + name + ", elementName=" + elementName + ", value=" + value + ", valueType=" + elementFieldInfo.type + ")", exception);
                }
                continue;
            }
            if (elementFieldInfo.fieldInfoList != null) {
                final Object value = convertElementsToObject(childElement, elementFieldInfo, false, isInList, false);
                elementFieldInfo.mutator.invoke(object, value);
                continue;
            }
            final Object typedValue = stringToTypedValue(elementFieldInfo, childElement.getText());
            try {
                elementFieldInfo.mutator.invoke(object, typedValue);
            } catch (Exception exception) {
                throw new ObjectViewWebService.ConversionException("Value cannot be set (mutator=" + elementFieldInfo.mutator + ", objectClass=" + object.getClass() + ", name=" + name + ", elementName=" + elementName + ", value=" + typedValue + ", valueType=" + elementFieldInfo.type + ")", exception);
            }
        }
        return object;
    }

    protected Object convertElementToObject(final OMElement element,
                                            ObjectView.FieldInfo fieldInfo,
                                            final boolean isListElement,
                                            final boolean isInList,
                                            final boolean isRoot)
        throws IllegalAccessException, InstantiationException, InvocationTargetException, URISyntaxException,
               ObjectViewWebService.ConversionException {
        Object object = null;
        if (fieldInfo.isList) {
            final Iterator childElements = element.getChildElements();
            final OMElement[] elementValue = new OMElement[1];
            object = convertElementsToObjectList(null, elementValue, childElements, fieldInfo);
        } else {
            object = convertElementsToObject(element, fieldInfo, false, false, isRoot);
        }
        return object;
    }

    protected OMNamespace getNamespace(final OMFactory factory,
                                       final String namespace,
                                       final NamespaceData namespaceData) {
        OMNamespace elementNamespace = namespaceData.namespaces.get(namespace);
        if (elementNamespace == null) {
            final String namespacePrefix = "ns" + namespaceData.namespacePrefixIndex++;
            elementNamespace = factory.createOMNamespace(namespace, namespacePrefix);
            namespaceData.namespaces.put(namespace, elementNamespace);
        }
        return elementNamespace;
    }

    protected String getNamespacePrefix(final String namespace,
                                        final NamespaceData namespaceData) {
        String namespacePrefix = namespaceData.namespacePrefixes.get(namespace);
        if (namespacePrefix == null) {
            namespacePrefix = "ns" + namespaceData.namespacePrefixIndex++;
            namespaceData.namespacePrefixes.put(namespace, namespacePrefix);
        }
        return namespacePrefix;
    }

    protected OMElement createElement(final OMFactory factory,
                                      final ObjectView.FieldInfo fieldInfo,
                                      final NamespaceData namespaceData,
                                      final boolean plain,
                                      final boolean header) {
        final OMNamespace elementNamespace
            = namespaceData != null && fieldInfo.namespace != null
            ? getNamespace(factory, fieldInfo.namespace, namespaceData) : null;
        OMElement element = null;
        String name = fieldInfo.name;
        if (fieldInfo.isForm) {
            name = name.substring("form".length());
        }
        if (fieldInfo.isOptional) {
            name = name.substring("optional".length());
        }
        if (fieldInfo.isReadOnly) {
            name = name.substring("readOnly".length());
        }
        if (name.startsWith("_")) {
            name = name.substring(1);
        }
        if (header) {
            final SOAPFactory soapFactory = OMAbstractFactory.getSOAP11Factory();
            element = soapFactory.createSOAPHeaderBlock(name, elementNamespace);
        } else {
            element = factory.createOMElement(plain ? name : fieldInfo.namePath,
                                              elementNamespace);
        }
        if (namespaceData != null && elementNamespace == null && namespace != null && !namespace.equals("")) {
            element.declareDefaultNamespace(namespace);
        }
        return element;
    }

    protected OMElement convertObjectToItem(final OMFactory factory,
                                            OMElement parent,
                                            final Object object,
                                            final ObjectView.FieldInfo fieldInfo,
                                            final ObjectView.FieldInfo parentFieldInfo,
                                            final boolean isListObject,
                                            final NamespaceData namespaceData,
                                            final boolean plain,
                                            final boolean header)
        throws IllegalAccessException, InvocationTargetException,
               ObjectViewWebService.ConversionException {
        Object item = null;
        if (!isListObject && fieldInfo.isList) {
            return convertObjectListToItems(factory, parent, (List<?>)object, fieldInfo, parentFieldInfo,
                                            namespaceData, plain, false);
        }
        if (fieldInfo.fieldInfoList != null) {
            item = convertListObjectToElement(factory, object, fieldInfo, namespaceData, plain, false);
        } else if (fieldInfo.isAttribute) {
            OMNamespace attributeNamespace = null;
            if (namespaceData != null && fieldInfo.namespace != null) {
                attributeNamespace = getNamespace(factory, fieldInfo.namespace, namespaceData);
            }
            final OMAttribute attribute
                = factory.createOMAttribute(fieldInfo.name, attributeNamespace, typedValueToString(fieldInfo, object));
            item = attribute;
        } else {
            final OMElement element = createElement(factory, fieldInfo, namespaceData, plain, header);
            if (fieldInfo.isSimple) {
                element.setText(typedValueToString(fieldInfo, object));
            }
            item = element;
        }
        if (item != null) {
            if (parent == null) {
                if (parentFieldInfo != null) {
                    parent = createElement(factory, parentFieldInfo, namespaceData, plain, header);
                } else {
                    parent = (OMElement)item;
                }
            }
            if (item instanceof OMElement) {
                parent.addChild((OMElement)item);
            } else {
                parent.addAttribute((OMAttribute)item);
            }
        }
        return parent;
    }

    protected OMElement convertObjectListToItems(final OMFactory factory,
                                                 OMElement parent,
                                                 final List<?> objectList,
                                                 final ObjectView.FieldInfo fieldInfo,
                                                 final ObjectView.FieldInfo parentFieldInfo,
                                                 final NamespaceData namespaceData,
                                                 final boolean plain,
                                                 final boolean header)
        throws IllegalAccessException, InvocationTargetException, ObjectViewWebService.ConversionException {
        if (parent == null) {
            if (parentFieldInfo != null) {
                parent = createElement(factory, parentFieldInfo, namespaceData, plain, header);
            } else {
                parent = createElement(factory, fieldInfo, namespaceData, plain, header);
            }
        }
        for (final Object object : objectList) {
            if (object == null) {
                continue;
            }
            parent = convertObjectToItem(factory, parent, object, fieldInfo, parentFieldInfo, true, namespaceData,
                                         true, false);
        }
        return parent;
    }

    protected OMElement convertListObjectToElement(final OMFactory factory,
                                                   final Object object,
                                                   final ObjectView.FieldInfo fieldInfo,
                                                   final NamespaceData namespaceData,
                                                   final boolean plain,
                                                   final boolean header)
        throws IllegalAccessException, InvocationTargetException, ObjectViewWebService.ConversionException {
        OMElement parent = !fieldInfo.isOptional
            ? createElement(factory, fieldInfo, namespaceData, plain, header) : null;
        for (final ObjectView.FieldInfo aFieldInfo : fieldInfo.fieldInfoList) {
            if (aFieldInfo.accessor == null) {
                continue;
            }
            final Object anObject = aFieldInfo.accessor.invoke(object);
            if (anObject == null) {
                continue;
            }
            parent = convertObjectToItem(factory, parent, anObject, aFieldInfo, fieldInfo,
                                         false, namespaceData, true, false);
        }
        return parent;
    }

    protected OMElement convertObjectToElements(final OMFactory factory,
                                                final Object object,
                                                final ObjectView.FieldInfo fieldInfo,
                                                final NamespaceData namespaceData,
                                                final boolean plain,
                                                final boolean header)
        throws IllegalAccessException, InvocationTargetException, ObjectViewWebService.ConversionException {
        final OMElement element
            = (OMElement)convertObjectToItem(factory, null, object, fieldInfo, null, false, namespaceData,
                                             plain, header);
        if (namespaceData != null) {
            for (final OMNamespace namespace : namespaceData.namespaces.values()) {
                element.declareNamespace(namespace);
            }
        }
        return element;
    }

    public Document getWSDL(final URL contextURL)
        throws AxisFault, ParserConfigurationException, SAXException, TransformerException, WSDLException {
        return toWSDL(contextURL.toExternalForm() + "services/" + serviceName);
    }

    public Document getWADL(final URL contextURL)
        throws ParserConfigurationException, SAXException, WSDLException {
        return toWADL(contextURL.toExternalForm() + "services/" + serviceName + "/");
    }

}
