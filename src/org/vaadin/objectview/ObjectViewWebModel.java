/**
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vaadin.objectview;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.transform.TransformerException;
import javax.wsdl.Binding;
import javax.wsdl.BindingInput;
import javax.wsdl.BindingOperation;
import javax.wsdl.Definition;
import javax.wsdl.Fault;
import javax.wsdl.Import;
import javax.wsdl.Input;
import javax.wsdl.Message;
import javax.wsdl.Operation;
import javax.wsdl.Output;
import javax.wsdl.Part;
import javax.wsdl.Port;
import javax.wsdl.PortType;
import javax.wsdl.Service;
import javax.wsdl.Types;
import javax.wsdl.WSDLException;
import javax.wsdl.extensions.ElementExtensible;
import javax.wsdl.extensions.schema.Schema;
import javax.wsdl.extensions.schema.SchemaImport;
import javax.wsdl.extensions.soap.SOAPAddress;
import javax.wsdl.extensions.soap.SOAPOperation;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLLocator;
import javax.wsdl.xml.WSDLReader;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.bcel.Constants;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Signature;
import org.apache.bcel.generic.ArrayType;
import org.apache.bcel.generic.ClassGen;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.FieldGen;
import org.apache.bcel.generic.InstructionConstants;
import org.apache.bcel.generic.InstructionFactory;
import org.apache.bcel.generic.InstructionList;
import org.apache.bcel.generic.MethodGen;
import org.apache.bcel.generic.ObjectType;
import org.apache.bcel.generic.PUSH;
import org.apache.bcel.generic.Type;
import org.apache.bcel.util.ClassLoaderRepository;
import org.apache.bcel.util.Repository;
import org.apache.xerces.impl.xs.XSImplementationImpl;
import org.apache.xerces.xs.StringList;
import org.apache.xerces.xs.XSAttributeDeclaration;
import org.apache.xerces.xs.XSAttributeUse;
import org.apache.xerces.xs.XSConstants;
import org.apache.xerces.xs.XSComplexTypeDefinition;
import org.apache.xerces.xs.XSSimpleTypeDefinition;
import org.apache.xerces.xs.XSElementDeclaration;
import org.apache.xerces.xs.XSImplementation;
import org.apache.xerces.xs.XSLoader; 
import org.apache.xerces.xs.XSModel;
import org.apache.xerces.xs.XSModelGroup;
import org.apache.xerces.xs.XSNamedMap;
import org.apache.xerces.xs.XSObject;
import org.apache.xerces.xs.XSObjectList;
import org.apache.xerces.xs.XSParticle;
import org.apache.xerces.xs.XSTerm;
import org.apache.xerces.xs.XSTypeDefinition;
import org.apache.xerces.xs.XSWildcard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSInput;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ObjectViewWebModel extends ObjectViewWebService implements Constants, WSDLLocator
{
    protected static final Logger logger = LoggerFactory.getLogger(ObjectViewWebModel.class);

    protected final static Map<String, XSModel> xsModels
        = new HashMap<String, XSModel>();
    protected final static XSImplementation xsImplementation
        = new XSImplementationImpl();

    protected static Map<String, Class<?>> typeClasses
        = new HashMap<String, Class<?>>();
    static {
        ObjectViewWebModel.typeClasses.put("anyType", Object.class);
        ObjectViewWebModel.typeClasses.put("anySimpleType", Object.class);
        // primitive data types
        ObjectViewWebModel.typeClasses.put("string", String.class);
        ObjectViewWebModel.typeClasses.put("boolean", Boolean.class);
        ObjectViewWebModel.typeClasses.put("decimal", BigDecimal.class);
        ObjectViewWebModel.typeClasses.put("float", Float.class);
        ObjectViewWebModel.typeClasses.put("double", Double.class);
        ObjectViewWebModel.typeClasses.put("duration", java.util.Date.class);
        ObjectViewWebModel.typeClasses.put("dateTime", java.sql.Timestamp.class);
        ObjectViewWebModel.typeClasses.put("time", java.sql.Time.class);
        ObjectViewWebModel.typeClasses.put("date", java.sql.Date.class);
        ObjectViewWebModel.typeClasses.put("gYearMonth", java.util.Date.class);
        ObjectViewWebModel.typeClasses.put("gYear", java.util.Date.class);
        ObjectViewWebModel.typeClasses.put("gMonthDay", java.util.Date.class);
        ObjectViewWebModel.typeClasses.put("gDay", java.util.Date.class);
        ObjectViewWebModel.typeClasses.put("gMonth", java.util.Date.class);
        ObjectViewWebModel.typeClasses.put("hexBinary", String.class);
        ObjectViewWebModel.typeClasses.put("base64Binary", String.class);
        ObjectViewWebModel.typeClasses.put("anyURI", String.class);
        ObjectViewWebModel.typeClasses.put("QName", String.class);
        ObjectViewWebModel.typeClasses.put("NOTATION", String.class);
        // derived data types
        ObjectViewWebModel.typeClasses.put("normalizedString", String.class);
        ObjectViewWebModel.typeClasses.put("token", String.class);
        ObjectViewWebModel.typeClasses.put("language", String.class);
        ObjectViewWebModel.typeClasses.put("NMTOKEN", String.class);
        ObjectViewWebModel.typeClasses.put("NMTOKENS", String.class);
        ObjectViewWebModel.typeClasses.put("Name", String.class);
        ObjectViewWebModel.typeClasses.put("NCName", String.class);
        ObjectViewWebModel.typeClasses.put("ID", String.class);
        ObjectViewWebModel.typeClasses.put("IDREF", String.class);
        ObjectViewWebModel.typeClasses.put("IDREFS", String.class);
        ObjectViewWebModel.typeClasses.put("ENTITY", String.class);
        ObjectViewWebModel.typeClasses.put("ENTITIES", String.class);
        ObjectViewWebModel.typeClasses.put("integer", BigInteger.class);
        ObjectViewWebModel.typeClasses.put("nonPositiveInteger", BigInteger.class);
        ObjectViewWebModel.typeClasses.put("negativeInteger", BigInteger.class);
        ObjectViewWebModel.typeClasses.put("long", Long.class);
        ObjectViewWebModel.typeClasses.put("int", Integer.class);
        ObjectViewWebModel.typeClasses.put("short", Short.class);
        ObjectViewWebModel.typeClasses.put("byte", Byte.class);
        ObjectViewWebModel.typeClasses.put("nonNegativeInteger", BigInteger.class);
        ObjectViewWebModel.typeClasses.put("unsignedLong", Long.class);
        ObjectViewWebModel.typeClasses.put("unsignedInt", Integer.class);
        ObjectViewWebModel.typeClasses.put("unsignedShort", Short.class);
        ObjectViewWebModel.typeClasses.put("unsignedByte", Byte.class);
        ObjectViewWebModel.typeClasses.put("positiveInteger", BigInteger.class);
    }

    protected String latestImportURI = null;
    protected String location = null;
    protected String wsdlURI = null;
    protected Class modelClass = null;
    protected Repository repository = null;
    protected ObjectViewClassLoader objectViewClassLoader = null;

    protected static class ObjectViewClassLoader extends ClassLoader {
        protected final String classDirectory;

        public ObjectViewClassLoader(final ClassLoader parent,
                                     final String classDirectory) {
            super(parent);
            this.classDirectory = classDirectory;
        }

        public Class<?> loadClass(final JavaClass javaClass,
                                  final boolean force)
            throws IOException {
            if (!force) {
                try {
                    return loadClass(javaClass.getClassName());
                } catch (final ClassNotFoundException classNotFoundException) {
                }
            }
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            javaClass.dump(out);
            final byte b[] = out.toByteArray();
            final File classFile = new File(classDirectory, javaClass.getClassName().replace('.', '/') + ".class");
            new File(classFile.getParent()).mkdirs();
            final OutputStream fileOut
                = new BufferedOutputStream(new FileOutputStream(classFile));
            fileOut.write(b);
            fileOut.close();
            final Class clazz = defineClass(javaClass.getClassName(), b, 0, b.length);
            resolveClass(clazz);
            return clazz;
        }

        public Class<?> loadClass(final JavaClass javaClass)
            throws IOException {
            return loadClass(javaClass, false);
        }

    }

    public static ObjectViewWebModel getObjectViewWebModel(final ObjectView objectView,
                                                           final String wsdlURI,
                                                           final String webServiceName,
                                                           final String webServicePortName,
                                                           final String webOperationName)
        throws ClassNotFoundException, IOException, SAXException, TransformerException, URISyntaxException,
               WSDLException {
        if (ObjectViewWebModel.logger.isTraceEnabled()) {
            logger.trace("wsdlURI=" + wsdlURI + ", webServiceName=" + webServiceName
                         + ", webServicePortName=" + webServicePortName + ", webOperationName=" + webOperationName);
        }
        final ServletContext servletContext = objectView.getServletContext();
        synchronized (objectView) {
            ObjectViewWebModel objectViewWebModel
                = (ObjectViewWebModel)servletContext.getAttribute(ObjectViewWebModel.class.getName() + "\\"
                                                                  + wsdlURI + "\\" + webServiceName + "\\"
                                                                  + webServicePortName + "\\" + webOperationName);
            if (objectViewWebModel == null) {
                objectViewWebModel = new ObjectViewWebModel(objectView, wsdlURI, webServiceName,
                                                            webServicePortName, webOperationName);
                servletContext.setAttribute(ObjectViewWebModel.class.getName(), objectViewWebModel);
            }
            return objectViewWebModel;
        }
    }

    public ObjectViewWebModel(final ObjectView objectView,
                              final String wsdlURI,
                              final String webServiceName,
                              final String webServicePortName,
                              final String webOperationName)
        throws ClassNotFoundException, IOException, SAXException, TransformerException, URISyntaxException,
               WSDLException {
        this.wsdlURI = wsdlURI;
        this.servletContext = objectView.getServletContext();
        final WSDLFactory wsdlFactory = WSDLFactory.newInstance();
        final WSDLReader wsdlReader = wsdlFactory.newWSDLReader();
        final Definition wsdlDefinition = wsdlReader.readWSDL(this);
        this.repository
            = new ClassLoaderRepository(getClass().getClassLoader());
        this.objectViewClassLoader
            = new ObjectViewClassLoader(getClass().getClassLoader(), servletContext.getRealPath("WEB-INF/classes"));
        this.modelClass
            = prepareModel(wsdlDefinition, wsdlURI,
                           webServiceName, webServicePortName, webOperationName,
                           objectView.getClass().getPackage().getName());
        if (ObjectViewWebModel.logger.isTraceEnabled()) {
            dump(this.modelClass);
        }
    }

    public Class getModelClass() {
        return modelClass;
    }

    public InputSource getBaseInputSource() {
        try {
            return getResource(wsdlURI);
        } catch (final IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public InputSource getImportInputSource(String parentLocation,
                                            String importLocation) {
        try {
            URL parentURL = null;
            if (parentLocation.indexOf(':') != -1) {
                parentURL = new URL(parentLocation);
            } else {
                parentURL = servletContext.getResource("/WEB-INF/" + parentLocation);
            }
            latestImportURI = new URL(parentURL, importLocation).toString();
            return getResource(latestImportURI);
        } catch (final IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public String getBaseURI() {
        return wsdlURI;
    }

    public String getLatestImportURI() {
        return latestImportURI;
    }

    public void close() {
    }

    protected InputSource getResource(final String wsdlURI)
        throws IOException {
        return new InputSource(resolveEntity(wsdlURI, this.wsdlURI));
    }

    protected ClassGen newClassGen(final String className,
                                   final String superClassName,
                                   final String name) {
        final ClassGen classGen
            = new ClassGen(className, superClassName, name + ".java", ACC_PUBLIC | ACC_SUPER,
                           new String[] {Serializable.class.getName()});
        classGen.addEmptyConstructor(ACC_PUBLIC);
        return classGen;
    }

    protected void initClassGen(final ClassGen classGen,
                                final InstructionList classInitInstructionList) {
        if (classInitInstructionList.getLength() > 0) {
            final InstructionFactory instructionFactory = new InstructionFactory(classGen);
            classInitInstructionList.append(instructionFactory.createReturn(Type.VOID));
            final MethodGen initMethod
                = new MethodGen(ACC_STATIC, Type.VOID, Type.NO_ARGS, new String[] {},
                                "<clinit>", classGen.getClassName(), classInitInstructionList,
                                classGen.getConstantPool());
            initMethod.setMaxStack();
            initMethod.setMaxLocals();
            classGen.addMethod(initMethod.getMethod());
            classInitInstructionList.dispose();
        }
    }

    protected void makeMethodGen(final String name,
                                 final String variableName,
                                 final String prefix,
                                 final String namespace,
                                 final Class clazz,
                                 final ClassGen classGen,
                                 final boolean isOptional,
                                 final boolean isReadOnly,
                                 final boolean isUnmutable,
                                 final boolean isMethodOnly,
                                 final boolean form,
                                 final Object value,
                                 final InstructionList initInstructionList) {
        final ConstantPoolGen constantPoolGen = classGen.getConstantPool();
        final InstructionFactory instructionFactory = new InstructionFactory(classGen);
        final Type type = Type.getType(clazz);
        if (!isMethodOnly && classGen.containsField(name) == null) {
            final FieldGen fieldGen = new FieldGen(ACC_PRIVATE, type, name, constantPoolGen);
            classGen.addField(fieldGen.getField());
            if (form) {
                initInstructionList.append(instructionFactory.createLoad(Type.OBJECT, 0));
                initInstructionList.append(instructionFactory.createNew(clazz.getName()));
                initInstructionList.append(InstructionConstants.DUP);
                initInstructionList.append(instructionFactory.createLoad(Type.OBJECT, 0));
                initInstructionList.append(InstructionConstants.ACONST_NULL);
                initInstructionList.append(instructionFactory.createInvoke(clazz.getName(), "<init>", Type.VOID, new Type[] { new ObjectType(classGen.getClassName()), type }, INVOKESPECIAL));
                initInstructionList.append(instructionFactory.createFieldAccess(classGen.getClassName(), name, type, PUTFIELD));
            }
        }
        final String argumentName = variableName != null ? variableName : name,
            fieldName = (form ? "Form" : "") 
            + (isOptional ? "Optional" : "") + (isReadOnly ? "ReadOnly" : "")
            + (prefix != null ? "" : "_") + argumentName;
        if (!isUnmutable) {
            final String methodName = (prefix != null ? prefix : "set") + fieldName;
            final InstructionList instructionList = new InstructionList();
            final MethodGen methodGen
                = new MethodGen(ACC_PUBLIC, Type.VOID, new Type[] {type}, new String[] {argumentName}, methodName,
                                classGen.getClassName(), instructionList, constantPoolGen);
            if (classGen.containsMethod(methodName, methodGen.getSignature()) == null) {
                instructionList.append(InstructionFactory.createThis());
                instructionList.append(instructionFactory.createLoad(type, 1));
                instructionList.append(instructionFactory.createPutField(classGen.getClassName(), name, type));
                instructionList.append(instructionFactory.createReturn(Type.VOID));
                methodGen.setMaxStack();
                methodGen.setMaxLocals();
                classGen.addMethod(methodGen.getMethod());
                instructionList.dispose();
            }
        }
        final String methodName = (prefix != null ? prefix : "get") + fieldName;
        final InstructionList instructionList = new InstructionList();
        final MethodGen methodGen
            = new MethodGen(ACC_PUBLIC, type, Type.NO_ARGS, new String[] {}, methodName,
                            classGen.getClassName(), instructionList, constantPoolGen);
        if (classGen.containsMethod(methodName, methodGen.getSignature()) == null) {
            if (isMethodOnly) {
                if (value != null) {
                    instructionList.append(instructionFactory.createConstant(value));
                } else {
                    instructionList.append(InstructionFactory.createNull(type));
                }
            } else {
                instructionList.append(InstructionFactory.createThis());
                instructionList.append(instructionFactory.createGetField(classGen.getClassName(), name, type));
            }
            instructionList.append(instructionFactory.createReturn(type));
            methodGen.setMaxStack();
            methodGen.setMaxLocals();
            classGen.addMethod(methodGen.getMethod());
            instructionList.dispose();
        }
        if (namespace != null) {
            makeMethodGen(fieldName, null, "namespace", null, String.class, classGen, false, false, true, true, false, namespace, null);
        }
    }

    protected void addSchemas(final Definition wsdlDefinition,
                              final List<Schema> schemas) {
        final Types types = wsdlDefinition.getTypes();
        final List extensibilityElements = types.getExtensibilityElements();
        for (final Object elementExtensible : extensibilityElements) {
            if (elementExtensible instanceof Schema) {
                schemas.add((Schema)elementExtensible);
            }
        }
    }

    protected Class prepareModel(final Definition wsdlDefinition,
                                 final String wsdlURI,
                                 final String webServiceName,
                                 final String webServicePortName,
                                 final String webOperationName,
                                 final String packageName)
        throws ClassNotFoundException, IOException, SAXException, TransformerException, URISyntaxException,
               WSDLException {
        ClassGen modelClassGen = null;
        final Map namespaces = wsdlDefinition.getNamespaces();
        final List<Schema> schemas = new ArrayList<Schema>();
        final Map imports = wsdlDefinition.getImports();
        final Iterator importValues = imports.values().iterator();
        while (importValues.hasNext()) {
            final Iterator importElements = ((List)importValues.next()).iterator();
            while (importElements.hasNext()) {
                Import importElement = (Import)importElements.next();
                addSchemas(importElement.getDefinition(), schemas);
            }
        }
        addSchemas(wsdlDefinition, schemas);
        LSInput lsInput = null;
        boolean started = false;
        for (final Schema schema : schemas) {
            final Element schemaElement = schema.getElement();
            final String targetNamespace = schemaElement.getAttribute("targetNamespace");
            if (!started) {
                final Node referenceNode = schemaElement.getFirstChild();
                final Document document = schemaElement.getOwnerDocument();
                final Map schemaImports = schema.getImports();
                for (final Schema aSchema : schemas) {
                    final Element aSchemaElement = aSchema.getElement();
                    final String aTargetNamespace = aSchemaElement.getAttribute("targetNamespace");
                    if (!targetNamespace.equals(aTargetNamespace)
                        && !schemaImports.containsKey(aTargetNamespace)) {
                        final Element importElement
                            = document.createElementNS(org.apache.axis2.namespace.Constants.URI_2001_SCHEMA_XSD,
                                                       "import");
                        importElement.setAttribute("namespace",
                                                   aTargetNamespace);
                        schemaElement.insertBefore(importElement,
                                                   referenceNode);
                    }
                }
                started = true;
            }
            for (final Object namespaceObject : namespaces.entrySet()) {
                final Map.Entry namespace = (Map.Entry)namespaceObject;
                final String namespacePrefix = (String)namespace.getKey(),
                    namespaceURI = (String)namespace.getValue();
                if (!schemaElement.hasAttributeNS(org.apache.axis2.namespace.Constants.NS_URI_XMLNS,
                                                  namespacePrefix)) {
                    if ("".equals(namespacePrefix)) {
                        schemaElement.setAttributeNS(org.apache.axis2.namespace.Constants.NS_URI_XMLNS,
                                                     "xmlns",
                                                     namespaceURI);
                    } else {
                        schemaElement.setAttributeNS(org.apache.axis2.namespace.Constants.NS_URI_XMLNS,
                                                     "xmlns:" + namespacePrefix,
                                                     namespaceURI);
                    }
                }
            }
            final byte[] resource = saveResource(schemaElement, targetNamespace);
            if (lsInput == null) {
                lsInput = ObjectViewWebModel.domImplementationLS.createLSInput();
                lsInput.setByteStream(new ByteArrayInputStream(resource));
            }
        }
        wsdlSchema = makeValidationSchema(schemas.get(0).getElement());
        XSModel xsModel = xsModels.get(wsdlURI);
        if (xsModel == null) {
            final XSLoader xsLoader = xsImplementation.createXSLoader(null);
            final DOMConfiguration configuration = xsLoader.getConfig();
            configuration.setParameter("resource-resolver", this);
            xsModel = xsLoader.load(lsInput);
            xsModels.put(wsdlURI, xsModel);
        }
        final Map allServices = wsdlDefinition.getAllServices();
        final Iterator serviceEntries = allServices.entrySet().iterator();
        while (serviceEntries.hasNext()) {
            final Map.Entry serviceEntry = (Map.Entry)serviceEntries.next();
            final Service service = (Service)serviceEntry.getValue();
            final String serviceName = service.getQName().getLocalPart();
            if (!serviceName.equals(webServiceName)) {
                continue;
            }
            final Map ports = service.getPorts();
            final Iterator portEntries = ports.entrySet().iterator();
            while (portEntries.hasNext()) {
                final Map.Entry portEntry = (Map.Entry)portEntries.next();
                final Port port = (Port)portEntry.getValue();
                if (!port.getName().equals(webServicePortName)) {
                    continue;
                }
                SOAPAddress soapAddress = null;
                List extensibilityElements
                    = port.getExtensibilityElements();
                for (final Object extensibilityElement
                         : extensibilityElements) {
                    if (extensibilityElement instanceof SOAPAddress) {
                        soapAddress = (SOAPAddress)extensibilityElement;
                        break;
                    }
                }
                if (soapAddress == null) {
                    continue;
                }
                if (location == null) {
                    location = soapAddress.getLocationURI();
                }
                final Binding binding = port.getBinding();
                final PortType portType = binding.getPortType();
                final List operations = portType.getOperations();
                final Iterator operationItems = operations.iterator();
                while (operationItems.hasNext()) {
                    final Operation operation
                        = (Operation)operationItems.next();
                    if (!operation.getName().equals(webOperationName)) {
                        continue;
                    }
                    modelClassGen
                        = newClassGen(packageName + "." + getClassName(webOperationName),
                                      Object.class.getName(), webOperationName);
                    final Input input = operation.getInput();
                    final QName action
                        = (QName)input.getExtensionAttribute(new QName(org.apache.axis2.addressing.AddressingConstants.Final.WSA_NAMESPACE, "Action"));
                    if (action != null) {
                        makeMethodGen("Action", null, "client", null, String.class, modelClassGen,
                                      false, false, true, true, false, action.getLocalPart(), null);
                    }
                    if (action == null) {
                        final BindingOperation bindingOperation
                            = binding.getBindingOperation(operation.getName(), input.getName(), operation.getOutput().getName());
                        extensibilityElements
                            = bindingOperation.getExtensibilityElements();
                        String soapAction = null;
                        for (final Object extensibilityElement
                                 : extensibilityElements) {
                            if (extensibilityElement instanceof SOAPOperation) {
                                soapAction = ((SOAPOperation)extensibilityElement).getSoapActionURI();
                                break;
                            }
                        }
                        if (soapAction != null) {
                            makeMethodGen("SOAPAction", null, "client", null, String.class, modelClassGen,
                                          false, false, true, true, false, soapAction, null);
                        }
                    }
                    final String name = operation.getName();
                    final Message inputMessage = input.getMessage();
                    final Class inputType = prepareModel(name, modelClassGen, inputMessage, xsModel, false, false);
                    makeMethodGen(name + "ClientCall", null, "_", null,
                                  Class.forName("[Ljava.lang.String;"), modelClassGen,
                                  false, false, true, true, false, null, null);
                    final Output output = operation.getOutput();
                    final Message outputMessage = output.getMessage();
                    final Class outputType = prepareModel(name, modelClassGen, outputMessage, xsModel, true, false);
                    final Map faults = operation.getFaults();
                    if (faults != null) {
                        for (final Object faultValue : faults.values()) {
                            final Fault fault = (Fault)faultValue;
                            final Message faultMessage = fault.getMessage();
                            final Class faultType = prepareModel(name, modelClassGen, faultMessage, xsModel, false, true);
                        }
                    }
                }
            }
        }
        if (location != null) {
            makeMethodGen("LocationURI", null, "client", null,
                          String.class, modelClassGen, false, false, true, true, false, location, null);
        }
        makeMethodGen("Plain", null, "client", null, Boolean.TYPE, modelClassGen,
                      false, false, true, true, false, Boolean.TRUE, null);
        return objectViewClassLoader.loadClass(modelClassGen.getJavaClass());
    }

    protected Class prepareModel(final String modelName,
                                 final ClassGen modelClassGen,
                                 final Message message,
                                 final XSModel xsModel,
                                 final boolean isOutput,
                                 final boolean isFault)
        throws IOException, URISyntaxException {
        final List orderedParts = message.getOrderedParts(null);
        if (orderedParts.isEmpty()) {
            return null;
        }
        String namespace = null;
        ClassGen classGen = null;
        for (final Object orderedPart : orderedParts) {
            final Part part = (Part)orderedPart;
            short objectType = 0;
            QName qName = part.getElementName();
            if (qName != null) {
                objectType = XSConstants.ELEMENT_DECLARATION;
            } else {
                qName = part.getTypeName();
                objectType = XSConstants.TYPE_DEFINITION;
            }
            if (namespace == null) {
                namespace = qName.getNamespaceURI();
            }
            final XSNamedMap xsNamedMap
                = xsModel.getComponents(objectType);
            final XSObject xsObject
                = xsNamedMap.itemByName(qName.getNamespaceURI(),
                                        qName.getLocalPart());
            final String name = qName.getLocalPart();
            classGen
                = newClassGen(getClassName(namespace, name), Object.class.getName(), name);
            final InstructionList classInitInstructionList = new InstructionList(),
                initInstructionList = new InstructionList();
            final Class<?> aClass
                = prepareModel(name, namespace, xsObject,
                               null, classGen, classInitInstructionList, initInstructionList, false, false, false,
                               null);
            initClassGen(classGen, classInitInstructionList);
            if (aClass != null) {
                makeMethodGen(name, null, null, namespace, aClass, classGen, false, false, false, false, false, null,
                              null);
            }
            if (!isFault) {
                break;
            }
        }
        final Class<?> type = objectViewClassLoader.loadClass(classGen.getJavaClass());
        String aName = null, variableName = null;
        if (isOutput) {
            aName = modelName + "Response";
            variableName = modelName + "Output";
        } else if (isFault) {
            aName = modelName + "Fault";
            variableName = modelName + "Error";
        } else {
            aName = modelName + "Request";
            variableName = modelName + "Input";
        }
        makeMethodGen(aName, null, null, namespace, type, modelClassGen,
                      false, false, false, false, false, null, null);
        makeMethodGen(aName, variableName, null, namespace, type, modelClassGen,
                      false, isOutput || isFault, false, false, false, null, null);
        return type;
    }

    protected Class getType(final XSSimpleTypeDefinition xsSimpleTypeDefinition) {
        XSSimpleTypeDefinition simpleType = null;
        switch (xsSimpleTypeDefinition.getVariety()) {
        case XSSimpleTypeDefinition.VARIETY_ATOMIC: {
            simpleType = xsSimpleTypeDefinition;
            if (simpleType.getName() == null) {
                simpleType = simpleType.getPrimitiveType();
            }
            break;
        }
        case XSSimpleTypeDefinition.VARIETY_LIST: {
            simpleType = xsSimpleTypeDefinition.getItemType();
        }
        }
        Class<?> type = null;
        while (simpleType != null) {
            final String typeName = simpleType.getName();
            type = ObjectViewWebModel.typeClasses.get(typeName);
            if (type == null) {
                simpleType = (XSSimpleTypeDefinition)simpleType.getBaseType();
                continue;
            }
            if (String.class.equals(type)) {
                final String lengthFacetValue
                    = xsSimpleTypeDefinition.getLexicalFacetValue(XSSimpleTypeDefinition.FACET_LENGTH);
                if ("1".equals(lengthFacetValue)) {
                    type = Character.class;
                }
            }
            break;
        }
        if (type == null) {
            type = Object.class;
        }
        return type;
    }

    protected void addSignature(final ClassGen classGen,
                                final String signature) {
        final ConstantPoolGen constantPoolGen = classGen.getConstantPool();
        final int signatureNameIndex = constantPoolGen.addUtf8("Signature"),
            signatureIndex = constantPoolGen.addUtf8(signature);
        classGen.addAttribute(new Signature(signatureNameIndex, 2, signatureIndex, constantPoolGen.getConstantPool()));
    }

    protected Class prepareModel(String name,
                                 String namespace,
                                 final XSObject xsObject,
                                 XSObjectList attributeUses,
                                 final ClassGen parentClassGen,
                                 final InstructionList classInitInstructionList,
                                 final InstructionList initInstructionList,
                                 final boolean isUnmutable,
                                 boolean isList,
                                 final boolean isOptional,
                                 boolean isComplex[])
        throws IOException, URISyntaxException {
        if (xsObject instanceof XSElementDeclaration) {
            final XSElementDeclaration xsElementDeclaration
                = (XSElementDeclaration)xsObject;
            final XSTypeDefinition xsTypeDefinition
                = xsElementDeclaration.getTypeDefinition();
            final String elementName = xsElementDeclaration.getName(),
                aNamespace = xsElementDeclaration.getNamespace();
            isComplex = new boolean[1];
            final Class<?> aClass
                = prepareModel(elementName, aNamespace,
                               xsTypeDefinition, null, parentClassGen, classInitInstructionList, initInstructionList,
                               isUnmutable, isList, isOptional, isComplex);
            if (aClass != null) {
                makeMethodGen(elementName, null, null, aNamespace, aClass, parentClassGen,
                              isOptional, false, isUnmutable, false, isList && isComplex[0], null,
                              initInstructionList);
            }
            return null;
        }
        if (xsObject instanceof XSComplexTypeDefinition) {
            if (isComplex != null) {
                isComplex[0] = true;
            }
            final XSComplexTypeDefinition xsComplexTypeDefinition
                = (XSComplexTypeDefinition)xsObject;
            attributeUses
                = xsComplexTypeDefinition.getAttributeUses();
            final XSParticle xsParticle = xsComplexTypeDefinition.getParticle();
            XSTerm xsTerm = null;
            if (xsParticle != null) {
                xsTerm = xsParticle.getTerm();
                if (xsParticle.getMaxOccursUnbounded()) {
                    isList = true;
                }
            }
            ClassGen classGen = null;
            InstructionList classInstructionList = null, instructionList = null;
            name = xsComplexTypeDefinition.getName();
            namespace = xsComplexTypeDefinition.getNamespace();
            if (name != null) {
                Class baseClass = null;
                final XSTypeDefinition xsBaseTypeDefinition
                    = xsComplexTypeDefinition.getBaseType();
                if (xsBaseTypeDefinition != null) {
                    final String baseName = xsBaseTypeDefinition.getName(),
                        baseNamespace = xsBaseTypeDefinition.getNamespace();
                    if (!baseNamespace.equals(XMLConstants.W3C_XML_SCHEMA_NS_URI)
                        || !baseName.equals("anyType")) {
                        baseClass
                            = prepareModel(baseName, baseNamespace, xsBaseTypeDefinition,
                                           null, classGen, classInstructionList, initInstructionList,
                                           false, false, false, null);
                    }
                }
                classGen
                    = newClassGen(getClassName(namespace, name),
                                  baseClass != null ? baseClass.getName() : Object.class.getName(), name);
                classInstructionList = new InstructionList();
                instructionList = new InstructionList();
            } else {
                classGen = parentClassGen;
                classInstructionList = classInitInstructionList;
                instructionList = initInstructionList;
            }
            prepareModel(name, namespace, xsTerm,
                         attributeUses, classGen, classInstructionList, initInstructionList,
                         isUnmutable, isList, isOptional, null);
            initClassGen(classGen, classInstructionList);
            Class<?> clazz
                = objectViewClassLoader.loadClass(classGen.getJavaClass());
            if (isList) {
                name += "List";
                final ClassGen listClassGen
                    = newClassGen(getClassName(namespace, name), ArrayList.class.getName(), name);
                addSignature(listClassGen, "Ljava/util/ArrayList<L" + clazz.getName().replace('.', '/') + ";>;");
                clazz = objectViewClassLoader.loadClass(listClassGen.getJavaClass());
            }
            return clazz;
        }
        if (xsObject instanceof XSSimpleTypeDefinition) {
            final XSSimpleTypeDefinition xsSimpleTypeDefinition
                = (XSSimpleTypeDefinition)xsObject;
            if (xsSimpleTypeDefinition.getVariety() == XSSimpleTypeDefinition.VARIETY_LIST) {
                isList = true;
            }
            if (isList) {
                final String listName = xsSimpleTypeDefinition.getName();
                final ClassGen listClassGen
                    = newClassGen(getClassName(namespace, name), ArrayList.class.getName(), listName);
                final Class<?> type = getType(xsSimpleTypeDefinition);
                addSignature(listClassGen, "Ljava/util/ArrayList<L" + type.getName().replace('.', '/') + ";>;");
                final Class<?> listClass = objectViewClassLoader.loadClass(listClassGen.getJavaClass());
                return listClass;
            }
            if (xsSimpleTypeDefinition.getVariety() == XSSimpleTypeDefinition.VARIETY_UNION) {
                final List<Class<?>> types = new ArrayList<Class<?>>();
                final XSObjectList memberTypes
                    = xsSimpleTypeDefinition.getMemberTypes();
                final int length = memberTypes.getLength();
                for (int index = 0; index < length; index++) {
                    final XSSimpleTypeDefinition xsSimpleTypeDefinitionMember
                        = (XSSimpleTypeDefinition)memberTypes.item(index);
                    final Class<?> type = getType(xsSimpleTypeDefinitionMember);
                    types.add(type);
                }
                int maxNumber = 0;
                Class<?> theType = null;
                for (final Class<?> type : types) {
                    int number = 0;
                    for (final Class<?> aType : types) {
                        if (type.isAssignableFrom(aType)) {
                            number++;
                        }
                    }
                    if (number > maxNumber) {
                        theType = type;
                        maxNumber = number;
                    }
                }
                return theType;
            }
            Class<?> type = null;
            final StringList lexicalEnumeration
                = xsSimpleTypeDefinition.getLexicalEnumeration();
            if (lexicalEnumeration != null
                && lexicalEnumeration.getLength() > 0) {
                final ConstantPoolGen constantPoolGen = parentClassGen.getConstantPool();
                final String valuesName = "values" + (isOptional ? "Optional" : "") + "_"
                    + Character.toUpperCase(name.charAt(0)) + name.substring(1);
                final Type valuesType = new ArrayType(Type.STRING, 1);
                final FieldGen field = new FieldGen(ACC_STATIC, valuesType,
                                                    valuesName, constantPoolGen);
                parentClassGen.addField(field.getField());
                final InstructionFactory instructionFactory = new InstructionFactory(parentClassGen);
                final int length = lexicalEnumeration.getLength();
                classInitInstructionList.append(new PUSH(constantPoolGen, length));
                classInitInstructionList.append(instructionFactory.createNewArray(Type.STRING, (short)1));
                for (int index = 0; index < length; index++) {
                    classInitInstructionList.append(InstructionConstants.DUP);
                    classInitInstructionList.append(new PUSH(constantPoolGen, index));
                    classInitInstructionList.append(new PUSH(constantPoolGen, lexicalEnumeration.item(index)));
                    classInitInstructionList.append(InstructionConstants.AASTORE);
                }
                classInitInstructionList.append(instructionFactory.createFieldAccess(parentClassGen.getClassName(),
                                                                                valuesName, valuesType, PUTSTATIC));
                final InstructionList instructionList = new InstructionList();
                final MethodGen method = new MethodGen(ACC_PUBLIC, valuesType, Type.NO_ARGS, new String[] {}, valuesName, parentClassGen.getClassName(), instructionList, constantPoolGen);
                instructionList.append(instructionFactory.createFieldAccess(parentClassGen.getClassName(), valuesName, valuesType, GETSTATIC));
                instructionList.append(instructionFactory.createReturn(valuesType));
                method.setMaxStack();
                method.setMaxLocals();
                parentClassGen.addMethod(method.getMethod());
                instructionList.dispose();
                return String.class;
            } else {
                type = getType(xsSimpleTypeDefinition);
            }
            return type;
        }
        if (xsObject == null || xsObject instanceof XSModelGroup) {
            if (xsObject != null) {
                final XSModelGroup xsModelGroup = (XSModelGroup)xsObject;
                final boolean isChoice = xsModelGroup.getCompositor() == XSModelGroup.COMPOSITOR_CHOICE;
                final XSObjectList particles = xsModelGroup.getParticles();
                final int length = particles.getLength();
                for (int index = 0; index < length; index++) {
                    final XSParticle xsParticle = (XSParticle)particles.item(index);
                    final XSTerm xsTerm = xsParticle.getTerm();
                    String termName = xsTerm.getName();
                    String aNamespace = xsTerm.getNamespace();
                    if (xsTerm instanceof XSElementDeclaration) {
                        final XSElementDeclaration xsElementDeclaration
                            = (XSElementDeclaration)xsTerm;
                        aNamespace = xsElementDeclaration.getNamespace();
                    }
                    if (aNamespace == null) {
                        aNamespace = namespace;
                    }
                    if (termName == null) {
                        termName = name;
                    }
                    final Class<?> aClass
                        = prepareModel(termName, aNamespace, xsTerm,
                                       null, parentClassGen, classInitInstructionList, initInstructionList,
                                       isUnmutable,
                                       xsParticle.getMaxOccursUnbounded(),
                                       xsParticle.getMinOccurs() == 0 || isChoice, null);
                    if (aClass != null) {
                        makeMethodGen(termName, null, null, aNamespace, aClass, parentClassGen,
                                      xsParticle.getMinOccurs() == 0 || isChoice, false, isUnmutable, false, false,
                                      null, null);
                    }
                }
            }
            if (attributeUses != null) {
                final int length = attributeUses.getLength();
                for (int index = 0; index < length; index++) {
                    final XSAttributeUse xsAttributeUse = (XSAttributeUse)attributeUses.item(index);
                    final XSAttributeDeclaration xsAttributeDeclaration = xsAttributeUse.getAttrDeclaration();
                    final String attributeName
                        = xsAttributeDeclaration.getName();
                    final XSSimpleTypeDefinition typeDefinition
                        = xsAttributeDeclaration.getTypeDefinition();
                    final Class<?> aClass
                        = prepareModel(attributeName, namespace,
                                       typeDefinition, null, parentClassGen,
                                       classInitInstructionList, initInstructionList,
                                       isUnmutable, false, true, null);
                    makeMethodGen(attributeName, null, null, namespace, aClass, parentClassGen,
                                  true, false, isUnmutable, false, false, null, null);
                }
            }
            return null;
        }
        if (xsObject instanceof XSWildcard) {
            makeMethodGen(name != null ? name : "", null, null, namespace, java.lang.Object.class, parentClassGen,
                          isOptional, false, isUnmutable, false, false, null, null);
            return java.lang.Object.class;
        }
        return null;
    }

    public static String dump(Class clazz) {
        final StringBuffer buffer = new StringBuffer();
        if (clazz.isArray()) {
            buffer.append(clazz.getComponentType().getName() + "[]");
        } else if (Iterable.class.isAssignableFrom(clazz)) {
            buffer.append(clazz.getName() + "()");
        } else if (Map.class.isAssignableFrom(clazz)) {
            buffer.append(clazz.getName() + "{}");
        } else {
            final Package objectPackage = clazz.getPackage();
            if (objectPackage != null) {
                final String packageName = objectPackage.getName();
                if (!packageName.startsWith("org.")) {
                    return clazz.getName();
                }
            }
            buffer.append(clazz.getName() + "{");
            boolean started = false;
            final Method methods[] = clazz.getMethods();
            for (final Method method : methods) {
                boolean hadIs = false;
                final Class declaringClass = method.getDeclaringClass();
                final Package classPackage = declaringClass.getPackage();
                if (classPackage != null) {
                    final String packageName = classPackage.getName();
                    if (!packageName.startsWith("org.")) {
                        continue;
                    }
                }
                final String methodName = method.getName();
                buffer.append(methodName + "(");
                boolean began = false;
                for (final Class parameterType : method.getParameterTypes()) {
                    if (began) {
                        buffer.append(", ");
                    }
                    buffer.append(parameterType.getName());
                    began = true;
                }
            }
            final Field fields[] = clazz.getFields();
            boolean began = false;
            for (final Field field : fields) {
                final Class declaringClass = field.getDeclaringClass();
                final Package classPackage = declaringClass.getPackage();
                if (classPackage != null) {
                    final String packageName = classPackage.getName();
                    if (!packageName.startsWith("org.")) {
                        continue;
                    }
                }
                if (began) {
                    buffer.append(", ");
                }
                final String name = field.getName();
                buffer.append(name);
                began = true;
            }
            buffer.append("}");
        }
        return buffer.toString();
    }

}
