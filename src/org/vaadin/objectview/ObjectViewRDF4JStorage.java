/**
 * Copyright 2011-2018 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vaadin.objectview;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.RDFCollections;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.algebra.BinaryTupleOperator;
import org.eclipse.rdf4j.query.algebra.Intersection;
import org.eclipse.rdf4j.query.algebra.Join;
import org.eclipse.rdf4j.query.algebra.QueryModelNode;
import org.eclipse.rdf4j.query.algebra.StatementPattern;
import org.eclipse.rdf4j.query.algebra.TupleExpr;
import org.eclipse.rdf4j.query.algebra.ValueConstant;
import org.eclipse.rdf4j.query.algebra.Var;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.util.Connections;

public class ObjectViewRDF4JStorage implements ObjectViewStorage<IRI> {

    Repository repository = null;

    public void close() {
        if (repository != null) {
            repository.shutDown();
        }
    }

    protected TupleExpr makeOp(final Model model,
                               final Object object,
                               final Map<String, ObjectView.FieldInfo> fieldInfoMap,
                               final boolean isQuery,
                               final boolean isRemove,
                               final boolean isRoot,
                               Var[] var)
        throws IllegalAccessException, InvocationTargetException {
        final List<TupleExpr> joinOps = new ArrayList<>();
        final List<StatementPattern> statementPatterns = new ArrayList<>();
        final Resource resource
            = makeRdfResource(model, statementPatterns, object, fieldInfoMap, isQuery, isRemove, isRoot, var, joinOps);
        if (resource == null) {
            return null;
        }
        TupleExpr op = null;
        for (TupleExpr statementPattern : statementPatterns) {
            if (op != null) {
                op = new Intersection(op, statementPattern);
            } else {
                op = statementPattern;
            }
        }
        for (TupleExpr anOp : joinOps) {
            op = new Join(op, anOp);
        }
        return op;
    }

    private Value createValue(ValueFactory valueFactory, ObjectView.FieldInfo fieldInfo, Object propertyValue) {
        if (propertyValue instanceof Date) {
            propertyValue = valueFactory.createLiteral((Date)propertyValue);
        } else if (fieldInfo != null && fieldInfo.type.equals(BigDecimal.class)) {
            propertyValue = valueFactory.createLiteral((BigDecimal)propertyValue);
        } else if (fieldInfo != null && fieldInfo.type.equals(BigInteger.class)) {
            propertyValue = valueFactory.createLiteral((BigInteger)propertyValue);
        } else if (fieldInfo != null && fieldInfo.type.isEnum()) {
            propertyValue = valueFactory.createLiteral((String)propertyValue);
        } else if (propertyValue instanceof Boolean) {
            propertyValue = valueFactory.createLiteral(((Boolean)propertyValue).booleanValue());
        } else if (propertyValue instanceof Byte) {
            propertyValue = valueFactory.createLiteral(((Byte)propertyValue).byteValue());
        } else if (propertyValue instanceof Double) {
            propertyValue = valueFactory.createLiteral(((Double)propertyValue).doubleValue());
        } else if (propertyValue instanceof Float) {
            propertyValue = valueFactory.createLiteral(((Float)propertyValue).floatValue());
        } else if (propertyValue instanceof Integer) {
            propertyValue = valueFactory.createLiteral(((Integer)propertyValue).intValue());
        } else if (propertyValue instanceof Long) {
            propertyValue = valueFactory.createLiteral(((Long)propertyValue).longValue());
        } else if (propertyValue instanceof Short) {
            propertyValue = valueFactory.createLiteral(((Short)propertyValue).shortValue());
        } else if (propertyValue instanceof String
                   && ((String)propertyValue).equals("")) {
            propertyValue = null;
        } else {
            propertyValue = valueFactory.createLiteral((String)propertyValue);
        }
        return (Value)propertyValue;
    }

    protected Resource makeRdfResource(final Model model,
                                       List<StatementPattern> statementPatterns,
                                       final Object object,
                                       final Map<String, ObjectView.FieldInfo> fieldInfoMap,
                                       final boolean isQuery,
                                       final boolean isRemove,
                                       final boolean isRoot,
                                       Var[] var,
                                       List<TupleExpr> joinOps)
        throws IllegalAccessException, InvocationTargetException {
        Class objectClass = object.getClass();
        String name = null;
        if (isQuery) {
            name = "jqv:" + objectClass.getName();
        } else {
            Method idMethod = ObjectView.getMethod(objectClass, "id");
            if (idMethod != null) {
                name = (String)idMethod.invoke(object);
            } else {
                name = objectClass.getName();
            }
        }
        boolean statementsAdded = false;
        ValueFactory valueFactory = repository.getValueFactory();
        final Resource resource
            = valueFactory.createIRI(name);
        if (isQuery && statementPatterns != null) {
            var[0] = new Var(objectClass.getName());
        }
        for (final ObjectView.FieldInfo fieldInfo : fieldInfoMap.values()) {
            if (fieldInfo.isVolatile) {
                continue;
            }
            final Method accessor = fieldInfo.accessor;
            if (accessor == null) {
                continue;
            }
            Object propertyValue = accessor.invoke(object);
            if (propertyValue == null) {
                continue;
            }
            Value rdfNode = null;
            if (fieldInfo.isSimple) {
                if (propertyValue != null) {
                    propertyValue = createValue(valueFactory, fieldInfo, propertyValue);
                    if (propertyValue != null) {
                        rdfNode = (Value)propertyValue;
                    }
                }
            } else if (fieldInfo.isArray) {
                if (propertyValue != null) {
                    final int length = Array.getLength(propertyValue);
                    if (length > 0) {
                        final List<Value> valueList = new ArrayList<>();
                        for (int index = 0; index < length; index++) {
                            final Object componentValue = Array.get(propertyValue, index);
                            if (fieldInfo.fieldInfoMap != null) {
                                final Resource aResource
                                    = makeRdfResource(model, statementPatterns, componentValue, fieldInfo.fieldInfoMap, false, isRemove, false, null, null);
                                if (aResource != null) {
                                    valueList.add(aResource);
                                }
                            } else {
                                valueList.add(createValue(valueFactory, null, componentValue));
                            }
                        }
                        Resource head = valueFactory.createBNode();
                        Model listModel = RDFCollections.asRDF(valueList, head, new LinkedHashModel());
                    }
                }
            } else if (fieldInfo.isList) {
                if (propertyValue != null && !((List)propertyValue).isEmpty()) {
                    final List<Value> valueList = new ArrayList<>();
                    for (final Object itemValue : (List)propertyValue) {
                        if (fieldInfo.fieldInfoMap != null) {
                            final Resource aResource
                                = makeRdfResource(model, statementPatterns, itemValue, fieldInfo.fieldInfoMap, false, isRemove, false, null, null);
                            if (aResource != null) {
                                valueList.add(aResource);
                            }
                        } else {
                            valueList.add(createValue(valueFactory, null, itemValue));
                        }
                    }
                    Resource head = valueFactory.createBNode();
                    Model listModel = RDFCollections.asRDF(valueList, head, new LinkedHashModel());
                }
            } else {
                if (isQuery) {
                    final Var[] var1 = new Var[1];
                    TupleExpr op
                        = makeOp(model, propertyValue, fieldInfo.fieldInfoMap, true, false, false, var1);
                    if (joinOps != null && statementPatterns != null) {
                        joinOps.add(op);
                        final StatementPattern statementPattern
                            = new StatementPattern(var != null ? var[0] : new Var("", resource),
                                                   new Var("", (IRI)fieldInfo.property), var1[0]);
                        statementPatterns.add(statementPattern);
                    }
                    continue;
                }
                rdfNode = makeRdfResource(model, statementPatterns, propertyValue, fieldInfo.fieldInfoMap, false, isRemove, false, null, null);
            }
            if (rdfNode != null) {
                if (statementPatterns != null) {
                    final StatementPattern statementPattern
                        = new StatementPattern(var != null ? var[0] : new Var("", resource),
                                               new Var("", (IRI)fieldInfo.property), new Var("", rdfNode));
                    statementPatterns.add(statementPattern);
                }
                if (isRemove) {
                    model.remove(resource, (IRI)fieldInfo.property, rdfNode);
                } else {
                    model.remove(resource, (IRI)fieldInfo.property, null);
                    model.add(resource, (IRI)fieldInfo.property, rdfNode);
                    statementsAdded = true;
                }
            }
        }
        if (!isQuery || !statementsAdded) {
            IRI classProperty
                = makeProperty("class", objectClass);
            if (isRemove) {
                Model aModel = model.filter(resource, (IRI)null, (Value)null);
                Iterator<Statement> statements
                    = aModel.iterator();
                if (statements.hasNext()) {
                    Statement statement = statements.next();
                    if (!statements.hasNext()
                        && statement.getPredicate().equals(classProperty)) {
                        model.remove(statement.getSubject(), statement.getPredicate(), statement.getObject());
                    }
                }
            } else {
                boolean found = false;
                if (!isQuery) {
                    Model aModel = model.filter(resource, classProperty,
                                                createValue(valueFactory, null, objectClass.getName()));
                    Iterator<Statement> statements
                        = aModel.iterator();
                    if (statements.hasNext()) {
                        found = true;
                    }
                }
                if (!found) {
                    if (statementPatterns != null) {
                        final StatementPattern statementPattern
                            = new StatementPattern(var != null ? var[0] : new Var("", resource),
                                                   new Var("", classProperty),
                                                   new Var("", createValue(valueFactory, null, objectClass.getName())));
                        statementPatterns.add(statementPattern);
                    }
                    model.add(resource, classProperty, createValue(valueFactory, null, objectClass.getName()));
                }
            }
            if (!isQuery || isRoot) {
                return resource;
            }
            if (!statementsAdded) {
                return null;
            }
        }
        return resource;
    }

    private Object getLiteralValue(final ObjectView.FieldInfo fieldInfo,
                                   final Literal literal) {
        Object propertyValue = null;
        if (fieldInfo.type.equals(Date.class)) {
            propertyValue = literal.calendarValue().toGregorianCalendar().getTime();
        } else if (fieldInfo.type.equals(BigDecimal.class)) {
            propertyValue = literal.decimalValue();
        } else if (fieldInfo.type.equals(BigInteger.class)) {
            propertyValue = literal.integerValue();
        } else if (fieldInfo.type.isEnum()) {
            propertyValue = Enum.valueOf(fieldInfo.enumType, (String)propertyValue);
        } else if (fieldInfo.type.equals(Boolean.class)) {
            propertyValue = literal.booleanValue();
        } else if (fieldInfo.type.equals(Byte.class)) {
            propertyValue = literal.byteValue();
        } else if (fieldInfo.type.equals(Double.class)) {
            propertyValue = literal.doubleValue();
        } else if (fieldInfo.type.equals(Float.class)) {
            propertyValue = literal.floatValue();
        } else if (fieldInfo.type.equals(Integer.class)) {
            propertyValue = literal.integerValue();
        } else if (fieldInfo.type.equals(Long.class)) {
            propertyValue = literal.longValue();
        } else if (fieldInfo.type.equals(Short.class)) {
            propertyValue = literal.shortValue();
        } else {
            propertyValue = literal.getLabel();
        }
        return propertyValue;
    }

    protected Object makeObject(final RepositoryConnection repositoryConnection,
                                final Class type,
                                final Map<String, ObjectView.FieldInfo> fieldInfoMap,
                                final Resource resource)
        throws IllegalAccessException, InstantiationException, InvocationTargetException {
        final Object object = type.newInstance();
        for (final ObjectView.FieldInfo fieldInfo : fieldInfoMap.values()) {
            if (fieldInfo.isVolatile) {
                continue;
            }
            final Method mutator = fieldInfo.mutator;
            if (mutator == null) {
                continue;
            }
            Object propertyValue = null;
            Optional<Statement> theStatement
                = Connections.getStatement(repositoryConnection, resource, (IRI)fieldInfo.property, null);
            if (theStatement.isPresent()) {
                Statement statement = theStatement.get();
                if (fieldInfo.isSimple) {
                    propertyValue = getLiteralValue(fieldInfo, (Literal)statement.getObject());
                } else if (fieldInfo.isArray) {
                    Model rdfCollection
                        = Connections.getRDFCollection(repositoryConnection, (Resource)statement.getObject(), new LinkedHashModel());
                    List<Value> rdfList = RDFCollections.asValues(rdfCollection, (Resource)statement.getObject(), new ArrayList<Value>());
                    final int size = rdfList.size();
                    final Object array = Array.newInstance(fieldInfo.elementType, size);
                    for (int index = 0; index < size; index++) {
                        final Value rdfNode = rdfList.get(index);
                        if (!(rdfNode instanceof Literal) && fieldInfo.fieldInfoMap != null) {
                            Array.set(array, index, makeObject(repositoryConnection,
                                                               fieldInfo.elementType, fieldInfo.fieldInfoMap,
                                                               (Resource)rdfNode));
                        } else {
                            Array.set(array, index, getLiteralValue(fieldInfo, (Literal)rdfNode));
                        }
                    }
                    propertyValue = array;
                } else if (fieldInfo.isList) {
                    Model rdfCollection
                        = Connections.getRDFCollection(repositoryConnection, (Resource)statement.getObject(), new LinkedHashModel());
                    List<Value> rdfList = RDFCollections.asValues(rdfCollection, (Resource)statement.getObject(), new ArrayList<Value>());
                    final int size = rdfList.size();
                    final List list = (List)fieldInfo.type.newInstance();
                    for (int index = 0; index < size; index++) {
                        final Value rdfNode = rdfList.get(index);
                        if (!(rdfNode instanceof Literal) && fieldInfo.fieldInfoMap != null) {
                            list.add(makeObject(repositoryConnection,
                                                fieldInfo.elementType, fieldInfo.fieldInfoMap,
                                                (Resource)rdfNode));
                        } else {
                            list.add(getLiteralValue(fieldInfo, (Literal)rdfNode));
                        }
                    }
                    propertyValue = list;
                } else {
                    propertyValue = makeObject(repositoryConnection,
                                               fieldInfo.type, fieldInfo.fieldInfoMap,
                                               (Resource)statement.getObject());
                }
            }
            if (mutator == null) {
                throw new RuntimeException("Mutator for '" + fieldInfo.namePath + "' not found");
            }
            try {
                mutator.invoke(object, propertyValue);
            } catch (final IllegalArgumentException illegalArgumentException) {
                throw new IllegalArgumentException("object: " + object + ", mutator: " + mutator + ", propertyValue: " + propertyValue + (propertyValue != null ? ", propertyValue class: " + propertyValue.getClass() : ""), illegalArgumentException);
            }
        }
        return object;
    }

    @Override
    public void searchData(final Object object,
                           final String name,
                           final Map<String, ObjectView.FieldInfo> fieldInfoMap,
                           final int begin,
                           final int end)
        throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        final RepositoryConnection repositoryConnection = repository.getConnection();
        final ObjectView.FieldInfo searchFieldInfo = fieldInfoMap.get(name);
        if (searchFieldInfo == null) {
            return;
        }
        final Object searchObject = searchFieldInfo.accessor.invoke(object);
        if (searchObject == null) {
            return;
        }
        final Map<String, ObjectView.FieldInfo> searchFieldInfoMap = searchFieldInfo.fieldInfoMap;
        Object resultObject = null;
        if (List.class.isAssignableFrom(searchObject.getClass())) {
            final List<?> searchObjectList = (List<?>)searchObject;
            resultObject = searchData(repositoryConnection, searchObjectList, searchFieldInfoMap, begin, end);
        } else {
            resultObject = searchObject.getClass().newInstance();
            for (final ObjectView.FieldInfo aSearchFieldInfo : searchFieldInfoMap.values()) {
                final List<?> searchObjectList = (List<?>)aSearchFieldInfo.accessor.invoke(searchObject),
                    resultObjectList = searchData(repositoryConnection, searchObjectList, aSearchFieldInfo.fieldInfoMap, begin, end);
                aSearchFieldInfo.mutator.invoke(resultObject, resultObjectList);
            }
        }
        searchFieldInfo.mutator.invoke(object, resultObject);
    }

    protected List<?> searchData(final RepositoryConnection repositoryConnection,
                                 final List<?> searchObjectList,
                                 final Map<String, ObjectView.FieldInfo> searchFieldInfoMap,
                                 final int begin,
                                 final int end)
        throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        final List<Object> resultObjectList = (List<Object>)searchObjectList.getClass().newInstance();
        for (final Object searchObject : searchObjectList) {
            final Class type = searchObject.getClass();
            final Model query = new LinkedHashModel();
            final Var[] var = new Var[1];
            final TupleExpr op
                = makeOp(query, searchObject, searchFieldInfoMap, true, false, true, var);
            final TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, op.toString());
            TupleQueryResult result = tupleQuery.evaluate();
            for (int index = 0; result.hasNext() && (end == -1 || index <= end); index++) {
                BindingSet bindingSet = result.next();
                if (index >= begin) {
                    Value value = bindingSet.getValue(var[0].getName());
                    resultObjectList.add(makeObject(repositoryConnection, type, searchFieldInfoMap, (Resource)value));
                }
            }
            result.close();
        }
        return resultObjectList;
    }

    @Override
    public void storeData(final Object object,
                          final String name,
                          final Map<String, ObjectView.FieldInfo> fieldInfoMap,
                          final boolean isRemove)
        throws IllegalAccessException, InvocationTargetException {
        final RepositoryConnection repositoryConnection = repository.getConnection();
        final ObjectView.FieldInfo storeFieldInfo = fieldInfoMap.get(name);
        repositoryConnection.begin();
        final Model model = new LinkedHashModel();
        if (storeFieldInfo != null) {
            final Object storeObject = storeFieldInfo.accessor.invoke(object);
            if (storeObject == null) {
                return;
            }
            final Map<String, ObjectView.FieldInfo> storeFieldInfoMap = storeFieldInfo.fieldInfoMap;
            if (List.class.isAssignableFrom(storeObject.getClass())) {
                final List<?> storeObjectList = (List<?>)storeObject;
                storeData(model, storeObjectList, storeFieldInfoMap, isRemove);
            } else {
                for (final ObjectView.FieldInfo aStoreFieldInfo : storeFieldInfoMap.values()) {
                    final List<?> storeObjectList = (List<?>)aStoreFieldInfo.accessor.invoke(storeObject);
                    storeData(model, storeObjectList, aStoreFieldInfo.fieldInfoMap, isRemove);
                }
            }
        } else {
            makeRdfResource(model, null, object, fieldInfoMap, false, isRemove, false, null, null);
        }
        repositoryConnection.commit();
    }

    protected void storeData(final Model model,
                             final List<?> storeObjectList,
                             final Map<String, ObjectView.FieldInfo> storeFieldInfoMap,
                             final boolean isRemove)
        throws IllegalAccessException, InvocationTargetException {
        for (final Object storeObject: storeObjectList) {
            makeRdfResource(model, null, storeObject, storeFieldInfoMap, false, isRemove, false, null, null);
        }
    }

    @Override
    public IRI makeProperty(final String localName,
                            final Class objectType) {
        return repository.getValueFactory().createIRI(objectType.getName(), localName);
    }

}
