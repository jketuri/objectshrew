/**
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vaadin.objectview;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.AddressingConstants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HTTPAuthenticator;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.axis2.transport.http.impl.httpclient4.HttpTransportPropertiesImpl.Authenticator;
import org.apache.rampart.RampartMessageData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.server.VaadinService;

public class ObjectViewWebClient {
    protected static final Logger logger = LoggerFactory.getLogger(ObjectViewWebClient.class);

    protected final ObjectViewWebService objectViewWebService;

    public ObjectViewWebClient(final ObjectViewWebService objectViewWebService) {
        this.objectViewWebService = objectViewWebService;
    }

    public static <T> T getClientParameter(final Object object,
                                           final String name)
        throws IllegalAccessException, InvocationTargetException {
        final Method method = object != null ? ObjectView.getMethod(object.getClass(), "client" + name) : null;
        return method != null ? (T)method.invoke(object) : null;
    }

    public void invokeService(final ObjectView.FieldInfo fieldInfo)
        throws AxisFault, IllegalAccessException, InstantiationException, InvocationTargetException,
               MalformedURLException, URISyntaxException, XMLStreamException,
               ObjectViewWebService.ConversionException {
        final ServiceClient serviceClient = new ServiceClient(objectViewWebService.configurationContext, null);
        String action = ObjectViewWebClient.getClientParameter(fieldInfo.object, "Action"),
            soapAction = ObjectViewWebClient.getClientParameter(fieldInfo.object, "SOAPAction"),
            locationURI = ObjectViewWebClient.getClientParameter(fieldInfo.object, "LocationURI"),
            username = ObjectViewWebClient.getClientParameter(fieldInfo.object, "Username"),
            password = ObjectViewWebClient.getClientParameter(fieldInfo.object, "Password"),
            messageType = ObjectViewWebClient.getClientParameter(fieldInfo.object, "MessageType"),
            requestType = ObjectViewWebClient.getClientParameter(fieldInfo.object, "RequestType");
        final boolean plain = Boolean.TRUE.equals(ObjectViewWebClient.getClientParameter(fieldInfo.object, "Plain")),
            preemptive = Boolean.TRUE.equals(ObjectViewWebClient.getClientParameter(fieldInfo.object, "Preemptive"));
        String method = null, verb = null;
        if (fieldInfo.namePath.endsWith("Retrieve")) {
            method = Constants.Configuration.HTTP_METHOD_GET;
            verb = "Retrieve";
        } else if (fieldInfo.namePath.endsWith("Update")) {
            method = Constants.Configuration.HTTP_METHOD_PUT;
            verb = "Update";
        } else if (fieldInfo.namePath.endsWith("Add")) {
            method = Constants.Configuration.HTTP_METHOD_POST;
            verb = "Add";
        } else if (fieldInfo.namePath.endsWith("Call")) {
            method = Constants.Configuration.HTTP_METHOD_POST;
            verb = "Call";
        } else if (fieldInfo.namePath.endsWith("Delete")) {
            method = Constants.Configuration.HTTP_METHOD_POST;
            verb = "Delete";
        }
        final String path = fieldInfo.namePath.substring(0, fieldInfo.namePath.length() - verb.length()
                                                         - "Client".length()) + verb;
        if (locationURI == null) {
            final HttpServletRequest request
                = (HttpServletRequest)VaadinService.getCurrentRequest();
            final URL requestURL
                = new URL(request.getRequestURL().toString());
            final String requestPath = request.getContextPath() + "/services/" + objectViewWebService.serviceName;
            if ("REST".equalsIgnoreCase(requestType)) {
                locationURI = new URL(requestURL, requestPath + "/" + path).toExternalForm();
            } else {
                locationURI = new URL(requestURL, requestPath).toExternalForm();
            }
        }
        final Object inObject = fieldInfo.requestFieldInfo != null
            ? fieldInfo.requestFieldInfo.accessor.invoke(fieldInfo.object) : null;
        final EndpointReference endpointReference = new EndpointReference(locationURI);
        serviceClient.setTargetEPR(endpointReference);
        final Options options = new Options();
        options.setTo(endpointReference);
        if (username != null) {
            options.setProperty(RampartMessageData.KEY_RAMPART_POLICY, objectViewWebService.policy);
            if (preemptive) {
                HTTPAuthenticator authenticator = new Authenticator();
                authenticator.setUsername(username);
                if (password != null) {
                    authenticator.setPassword(password);
                }
                authenticator.setPreemptiveAuthentication(true);
                options.setProperty(HTTPConstants.AUTHENTICATE, authenticator);
            } else {
                options.setUserName(username);
                if (password != null) {
                    options.setPassword(password);
                }
            }
            serviceClient.engageModule("rampart");
        }
        options.setTimeOutInMilliSeconds(2 * 60 * 1000);
        ObjectViewWebService.NamespaceData namespaceData = null;
        if (messageType != null) {
            options.setProperty(Constants.Configuration.MESSAGE_TYPE, messageType);
        }
        if (messageType == null || !messageType.toLowerCase().endsWith("/json")) {
            namespaceData = new ObjectViewWebService.NamespaceData();
        }
        final OMFactory factory = OMAbstractFactory.getOMFactory();
        if ("REST".equalsIgnoreCase(requestType)) {
            options.setProperty(Constants.Configuration.ENABLE_REST, Constants.VALUE_TRUE);
            options.setProperty(Constants.Configuration.HTTP_METHOD, method);
        } else {
            if (action == null) {
                action = soapAction != null ? soapAction : path;
            }
            options.setAction(action);
        }
        if (soapAction != null) {
            options.setSoapVersionURI(org.apache.axis2.Constants.URI_SOAP11_ENV);
            options.setProperty(AddressingConstants.DISABLE_ADDRESSING_FOR_OUT_MESSAGES, Boolean.TRUE);
        } else {
            options.setProperty(AddressingConstants.WS_ADDRESSING_VERSION,
                                AddressingConstants.Submission.WSA_NAMESPACE);
        }
        options.setProperty(HTTPConstants.CHUNKED, Boolean.FALSE);
        options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
        if (objectViewWebService.proxyName != null) {
            final HttpTransportProperties.ProxyProperties proxyProperties
                = new HttpTransportProperties.ProxyProperties();
            proxyProperties.setProxyName(objectViewWebService.proxyName);
            proxyProperties.setProxyPort(objectViewWebService.proxyPort);
            options.setProperty(HTTPConstants.PROXY, proxyProperties);
        }
        serviceClient.setOptions(options);
        final String name
            = fieldInfo.name.substring(0, fieldInfo.name.length()
                                       - "Client".length() - verb.length()) + "Header";
        if (fieldInfo.formFieldInfoMap != null) {
            final ObjectView.FieldInfo headerFieldInfo
                = fieldInfo.formFieldInfoMap.get(name);
            if (headerFieldInfo != null) {
                final Object headerObject = headerFieldInfo.accessor.invoke(headerFieldInfo.object);
                if (headerObject != null) {
                    final Iterator<ObjectView.FieldInfo> headerElementFieldInfos
                        = headerFieldInfo.fieldInfoList.iterator();
                    while (headerElementFieldInfos.hasNext()) {
                        final ObjectView.FieldInfo headerElementFieldInfo = headerElementFieldInfos.next();
                        final Object headerElementFieldObject
                            = headerElementFieldInfo.accessor.invoke(headerElementFieldInfo.object);
                        if (headerElementFieldObject != null) {
                            final OMElement headerElementField
                                = objectViewWebService.convertObjectToElements(factory, headerElementFieldObject,
                                                                               headerElementFieldInfo,
                                                                               namespaceData, true, true);
                            if (headerElementField != null) {
                                serviceClient.addHeader(headerElementField);
                            }
                        }
                    }
                }
            }
        }
        final OMElement request = inObject != null
            ? objectViewWebService.convertObjectToElements(factory, inObject, fieldInfo.requestFieldInfo,
                                                           namespaceData, plain, false) : null;
        Object outObject = null;
        try {
            final OMElement response = serviceClient.sendReceive(request);
            outObject = objectViewWebService.convertElementToObject(response, fieldInfo.responseFieldInfo,
                                                                    false, false, !plain);
        } finally {
            serviceClient.cleanupTransport();
            objectViewWebService.multiThreadedHttpConnectionManager.closeIdleConnections(0);
        }
        fieldInfo.responseFieldInfo.mutator.invoke(fieldInfo.object, outObject);
    }

}
