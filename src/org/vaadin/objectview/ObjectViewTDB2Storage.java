/**
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vaadin.objectview;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import org.apache.jena.system.Txn;
import org.apache.jena.tdb2.TDB2Factory;

public class ObjectViewTDB2Storage extends ObjectViewTDBStorage {

    public ObjectViewTDB2Storage(final String storageDirectory) {
        new File(storageDirectory).mkdirs();
        dataset = TDB2Factory.createDataset(storageDirectory);
        model = dataset.getDefaultModel();
    }

    @Override
    public void searchData(final Object object,
                           final String name,
                           final Map<String, ObjectView.FieldInfo> fieldInfoMap,
                           final int begin,
                           final int end)
        throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        Txn.executeRead(dataset, () -> {
                try {
                    super.searchData(object, name, fieldInfoMap, begin, end);
                } catch (IllegalAccessException ex) {
                } catch (InstantiationException ex) {
                } catch (InvocationTargetException ex) {
                } catch (NoSuchMethodException ex) {
                    throw new RuntimeException(ex);
                }
            });
    }

}
