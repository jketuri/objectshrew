/**
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vaadin.objectview;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.graph.Node ;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.Dataset ;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.sparql.algebra.Algebra;
import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.op.OpBGP;
import org.apache.jena.sparql.algebra.op.OpJoin;
import org.apache.jena.sparql.core.BasicPattern;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.QueryIterator;
import org.apache.jena.sparql.engine.binding.Binding;
//import org.apache.jena.util.ModelQueryUtil;
import org.apache.jena.util.iterator.ExtendedIterator;

public class ObjectViewTDBStorage implements ObjectViewStorage<Property> {

    Dataset dataset = null;
    Model model = null;

    public ObjectViewTDBStorage() {
    }

    @Override
    public void close() {
        if (dataset != null) {
            dataset.close();
        }
    }

    protected Op makeOp(final Model model,
                        final Object object,
                        final Map<String, ObjectView.FieldInfo> fieldInfoMap,
                        final boolean isQuery,
                        final boolean isRemove,
                        final boolean isRoot,
                        Var[] var)
        throws IllegalAccessException, InvocationTargetException {
            final BasicPattern bp = new BasicPattern();
            final List<Op> ops = new ArrayList<>();
            final org.apache.jena.rdf.model.Resource resource
                = makeRdfResource(model, bp, object, fieldInfoMap, isQuery, isRemove, isRoot, var, ops);
            if (resource == null) {
                return null;
            }
            Op op = new OpBGP(bp);
            for (Op anOp : ops) {
                op = OpJoin.create(op, anOp);
            }
            return op;
    }

    protected org.apache.jena.rdf.model.Resource makeRdfResource(final Model model,
                                                                 final BasicPattern pattern,
                                                                 final Object object,
                                                                 final Map<String, ObjectView.FieldInfo> fieldInfoMap,
                                                                 final boolean isQuery,
                                                                 final boolean isRemove,
                                                                 final boolean isRoot,
                                                                 Var[] var,
                                                                 List<Op> ops)
        throws IllegalAccessException, InvocationTargetException {
        Class objectClass = object.getClass();
        String name = null;
        if (isQuery) {
            name = "jqv:" + objectClass.getName();
        } else {
            Method idMethod = ObjectView.getMethod(objectClass, "id");
            if (idMethod != null) {
                name = (String)idMethod.invoke(object);
            } else {
                name = objectClass.getName();
            }
        }
        boolean statementsAdded = false;
        final org.apache.jena.rdf.model.Resource resource
            = model.createResource(name);
        if (isQuery && pattern != null) {
            var[0] = Var.alloc(objectClass.getName());
        }
        for (final ObjectView.FieldInfo fieldInfo : fieldInfoMap.values()) {
            if (fieldInfo.isVolatile) {
                continue;
            }
            final Method accessor = fieldInfo.accessor;
            if (accessor == null) {
                continue;
            }
            Object propertyValue = accessor.invoke(object);
            if (propertyValue == null) {
                continue;
            }
            RDFNode rdfNode = null;
            if (fieldInfo.isSimple) {
                if (propertyValue != null) {
                    if (propertyValue instanceof Date) {
                        final Calendar calendar = Calendar.getInstance();
                        calendar.setTime((Date)propertyValue);
                        propertyValue = calendar;
                    } else if (fieldInfo.type.isEnum()
                               || fieldInfo.type.equals(BigDecimal.class)
                               || fieldInfo.type.equals(BigInteger.class)) {
                        propertyValue = propertyValue.toString();
                    } else if (propertyValue instanceof String
                               && ((String)propertyValue).equals("")) {
                        propertyValue = null;
                    }
                    if (propertyValue != null) {
                        rdfNode = model.createTypedLiteral(propertyValue);
                    }
                }
            } else if (fieldInfo.isArray) {
                if (propertyValue != null) {
                    final int length = Array.getLength(propertyValue);
                    if (length > 0) {
                        RDFList rdfList = model.createList();
                        for (int index = 0; index < length; index++) {
                            final Object componentValue = Array.get(propertyValue, index);
                            if (fieldInfo.fieldInfoMap != null) {
                                final org.apache.jena.rdf.model.Resource aResource
                                    = makeRdfResource(model, pattern, componentValue, fieldInfo.fieldInfoMap, false, isRemove, false, null, null);
                                if (aResource != null) {
                                    rdfList = rdfList.with(aResource);
                                }
                            } else {
                                rdfList = rdfList.with(model.createTypedLiteral(componentValue));
                            }
                        }
                        rdfNode = rdfList;
                    }
                }
            } else if (fieldInfo.isList) {
                if (propertyValue != null && !((List)propertyValue).isEmpty()) {
                    RDFList rdfList = model.createList();
                    for (final Object itemValue : (List)propertyValue) {
                        if (fieldInfo.fieldInfoMap != null) {
                            final org.apache.jena.rdf.model.Resource aResource
                                = makeRdfResource(model, pattern, itemValue, fieldInfo.fieldInfoMap, false, isRemove, false, null, null);
                            if (aResource != null) {
                                rdfList = rdfList.with(aResource);
                            }
                        } else {
                            rdfList = rdfList.with(model.createTypedLiteral(itemValue));
                        }
                    }
                    rdfNode = rdfList;
                }
            } else {
                if (isQuery) {
                    final Var[] var1 = new Var[1];
                    Op op = makeOp(model, propertyValue, fieldInfo.fieldInfoMap, true, false, false, var1);
                    if (op != null && pattern != null) {
                        ops.add(op);
                        final Triple triple
                            = new Triple(var != null ? var[0] : resource.asNode(),
                                         ((Property)fieldInfo.property).asNode(), var1[0]);
                        pattern.add(triple);
                    }
                    continue;
                }
                rdfNode = makeRdfResource(model, pattern, propertyValue, fieldInfo.fieldInfoMap, false, isRemove, false, null, null);
            }
            if (rdfNode != null) {
                if (pattern != null) {
                    final Triple triple = new Triple(var != null ? var[0] : resource.asNode(),
                                                     ((Property)fieldInfo.property).asNode(), rdfNode.asNode());
                    pattern.add(triple);
                }
                final Statement statement = model.createStatement(resource, (Property)fieldInfo.property, rdfNode);
                if (isRemove) {
                    model.remove(statement);
                } else {
                    model.removeAll(resource, (Property)fieldInfo.property, null);
                    model.add(statement);
                    statementsAdded = true;
                }
            }
        }
        if (!isQuery || !statementsAdded) {
            Property classProperty
                = makeProperty("class", objectClass);
            if (isRemove) {
                StmtIterator statements
                    = model.listStatements(resource, null, (RDFNode)null);
                if (statements.hasNext()) {
                    Statement statement = statements.nextStatement();
                    if (!statements.hasNext()
                        && statement.getPredicate().equals(classProperty)) {
                        model.remove(statement);
                    }
                }
                statements.close();
            } else {
                boolean found = false;
                if (!isQuery) {
                    StmtIterator statements
                        = model.listStatements(resource, classProperty,
                                               objectClass.getName());
                    if (statements.hasNext()) {
                        found = true;
                    }
                    statements.close();
                }
                if (!found) {
                    if (pattern != null) {
                        final Triple triple
                            = new Triple(var != null ? var[0]
                                         : resource.asNode(),
                                         classProperty.asNode(),
                                         model.createTypedLiteral(objectClass.getName()).asNode());
                        pattern.add(triple);
                    }
                    final Statement statement
                        = model.createStatement(resource, classProperty,
                                                objectClass.getName());
                    model.add(statement);
                }
            }
            if (!isQuery || isRoot) {
                return resource;
            }
            if (!statementsAdded) {
                return null;
            }
        }
        return resource;
    }

    private Object getLiteralValue(final ObjectView.FieldInfo fieldInfo,
                                   final Literal literal) {
        Object propertyValue = literal != null ? literal.getValue() : null;
        if (propertyValue instanceof XSDDateTime) {
            propertyValue = ((XSDDateTime)propertyValue).asCalendar();
            if (Date.class.isAssignableFrom(fieldInfo.type)) {
                propertyValue = ((Calendar)propertyValue).getTime();
            }
        } else if (propertyValue instanceof String) {
            if (fieldInfo.type.isEnum()) {
                propertyValue = Enum.valueOf(fieldInfo.enumType, (String)propertyValue);
            } else if (fieldInfo.type.equals(BigDecimal.class)) {
                propertyValue = new BigDecimal((String)propertyValue);
            } else if (fieldInfo.type.equals(BigInteger.class)) {
                propertyValue = new BigInteger((String)propertyValue);
            }
        } else if (propertyValue instanceof Integer) {
            if (fieldInfo.type.equals(Byte.class) || fieldInfo.type.equals(Byte.TYPE)) {
                propertyValue = Byte.valueOf(((Integer)propertyValue).byteValue());
            } else if (fieldInfo.type.equals(Short.class) || fieldInfo.type.equals(Short.TYPE)) {
                propertyValue = Short.valueOf(((Integer)propertyValue).shortValue());
            }
        }
        return propertyValue;
    }

    protected Object makeObject(final Class type,
                                final Map<String, ObjectView.FieldInfo> fieldInfoMap,
                                final org.apache.jena.rdf.model.Resource resource)
        throws IllegalAccessException, InstantiationException, InvocationTargetException {
        final Object object = type.newInstance();
        for (final ObjectView.FieldInfo fieldInfo : fieldInfoMap.values()) {
            if (fieldInfo.isVolatile) {
                continue;
            }
            final Method mutator = fieldInfo.mutator;
            if (mutator == null) {
                continue;
            }
            Object propertyValue = null;
            final Statement statement = resource.getProperty((Property)fieldInfo.property);
            if (statement != null) {
                if (fieldInfo.isSimple) {
                    propertyValue = getLiteralValue(fieldInfo, statement.getLiteral());
                } else if (fieldInfo.isArray) {
                    if (statement.getObject() instanceof RDFList) {
                        final RDFList rdfList = statement.getObject().as(RDFList.class);
                        final int size = rdfList.size();
                        final Object array = Array.newInstance(fieldInfo.elementType, size);
                        for (int index = 0; index < size; index++) {
                            final RDFNode rdfNode = rdfList.get(index);
                            if (!rdfNode.isLiteral() && fieldInfo.fieldInfoMap != null) {
                                Array.set(array, index, makeObject(fieldInfo.elementType, fieldInfo.fieldInfoMap,
                                                                   (org.apache.jena.rdf.model.Resource)rdfNode));
                            } else {
                                Array.set(array, index, getLiteralValue(fieldInfo, rdfNode.asLiteral()));
                            }
                        }
                        propertyValue = array;
                    }
                } else if (fieldInfo.isList) {
                    final RDFList rdfList = statement.getObject().as(RDFList.class);
                    final int size = rdfList.size();
                    final List list = (List)fieldInfo.type.newInstance();
                    for (int index = 0; index < size; index++) {
                        final RDFNode rdfNode = rdfList.get(index);
                        if (!rdfNode.isLiteral() && fieldInfo.fieldInfoMap != null) {
                            list.add(makeObject(fieldInfo.elementType, fieldInfo.fieldInfoMap,
                                                (org.apache.jena.rdf.model.Resource)rdfNode));
                        } else {
                            list.add(getLiteralValue(fieldInfo, rdfNode.asLiteral()));
                        }
                    }
                    propertyValue = list;
                } else {
                    propertyValue = makeObject(fieldInfo.type, fieldInfo.fieldInfoMap,
                                               (org.apache.jena.rdf.model.Resource)statement.getObject());
                }
            }
            if (mutator == null) {
                throw new RuntimeException("Mutator for '" + fieldInfo.namePath + "' not found");
            }
            try {
                mutator.invoke(object, propertyValue);
            } catch (final IllegalArgumentException illegalArgumentException) {
                throw new IllegalArgumentException("object: " + object + ", mutator: " + mutator + ", propertyValue: " + propertyValue + (propertyValue != null ? ", propertyValue class: " + propertyValue.getClass() : ""), illegalArgumentException);
            }
        }
        return object;
    }

    @Override
    public void searchData(final Object object,
                           final String name,
                           final Map<String, ObjectView.FieldInfo> fieldInfoMap,
                           final int begin,
                           final int end)
        throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        final ObjectView.FieldInfo searchFieldInfo = fieldInfoMap.get(name);
        if (searchFieldInfo == null) {
            return;
        }
        final Object searchObject = searchFieldInfo.accessor.invoke(object);
        if (searchObject == null) {
            return;
        }
        final Map<String, ObjectView.FieldInfo> searchFieldInfoMap = searchFieldInfo.fieldInfoMap;
        Object resultObject = null;
        if (List.class.isAssignableFrom(searchObject.getClass())) {
            final List<?> searchObjectList = (List<?>)searchObject;
            resultObject = searchData(searchObjectList, searchFieldInfoMap, begin, end);
        } else {
            resultObject = searchObject.getClass().newInstance();
            for (final ObjectView.FieldInfo aSearchFieldInfo : searchFieldInfoMap.values()) {
                final List<?> searchObjectList = (List<?>)aSearchFieldInfo.accessor.invoke(searchObject),
                    resultObjectList = searchData(searchObjectList, aSearchFieldInfo.fieldInfoMap, begin, end);
                aSearchFieldInfo.mutator.invoke(resultObject, resultObjectList);
            }
        }
        searchFieldInfo.mutator.invoke(object, resultObject);
    }

    protected List<?> searchData(final List<?> searchObjectList,
                                 final Map<String, ObjectView.FieldInfo> searchFieldInfoMap,
                                 final int begin,
                                 final int end)
        throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        final List<Object> resultObjectList = (List<Object>)searchObjectList.getClass().newInstance();
        for (final Object searchObject : searchObjectList) {
            final Class type = searchObject.getClass();
            final Model query = ModelFactory.createDefaultModel();
            final Var[] var = new Var[1];
            final Op op
                = makeOp(query, searchObject, searchFieldInfoMap, true, false, true, var);
            QueryIterator qIter = Algebra.exec(op, model.getGraph()) ;
            for (int index = 0; qIter.hasNext() && (end == -1 || index <= end); index++) {
                Binding b = qIter.nextBinding() ;
                if (index >= begin) {
                    Node n = b.get(var[0]) ;
                    //System.out.println(NodeFmtLib.displayStr(n)) ;
                    //System.out.println(b) ; 
                    resultObjectList.add(makeObject(type, searchFieldInfoMap, model.createResource(n.getURI())));
                }
            }
            qIter.close() ;
            /*
            final ExtendedIterator<List<? extends RDFNode>> bindings
                = ModelQueryUtil.queryBindingsWith(model, query, new org.apache.jena.rdf.model.Resource[] {resource});
            while (bindings.hasNext()) {
                final List<? extends RDFNode> resources = bindings.next();
                resultObjectList.add(makeObject(type, searchFieldInfoMap, (org.apache.jena.rdf.model.Resource)resources.get(0)));
            }
            */
        }
        return resultObjectList;
    }

    @Override
    public void storeData(final Object object,
                          final String name,
                          final Map<String, ObjectView.FieldInfo> fieldInfoMap,
                          final boolean isRemove)
        throws IllegalAccessException, InvocationTargetException {
        final ObjectView.FieldInfo storeFieldInfo = fieldInfoMap.get(name);
        model.begin();
        if (storeFieldInfo != null) {
            final Object storeObject = storeFieldInfo.accessor.invoke(object);
            if (storeObject == null) {
                return;
            }
            final Map<String, ObjectView.FieldInfo> storeFieldInfoMap = storeFieldInfo.fieldInfoMap;
            if (List.class.isAssignableFrom(storeObject.getClass())) {
                final List<?> storeObjectList = (List<?>)storeObject;
                storeData(storeObjectList, storeFieldInfoMap, isRemove);
            } else {
                for (final ObjectView.FieldInfo aStoreFieldInfo : storeFieldInfoMap.values()) {
                    final List<?> storeObjectList = (List<?>)aStoreFieldInfo.accessor.invoke(storeObject);
                    storeData(storeObjectList, aStoreFieldInfo.fieldInfoMap, isRemove);
                }
            }
        } else {
            makeRdfResource(model, null, object, fieldInfoMap, false, isRemove, false, null, null);
        }
        model.commit();
    }

    protected void storeData(final List<?> storeObjectList,
                             final Map<String, ObjectView.FieldInfo> storeFieldInfoMap,
                             final boolean isRemove)
        throws IllegalAccessException, InvocationTargetException {
        for (final Object storeObject: storeObjectList) {
            makeRdfResource(model, null, storeObject, storeFieldInfoMap, false, isRemove, false, null, null);
        }
    }

    @Override
    public Property makeProperty(final String localName,
                                 final Class objectType) {
        return model.createProperty(objectType.getName(), localName);
    }

}
