/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vaadin.objectview;

import java.io.BufferedInputStream;;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListResourceBundle;
import java.util.Properties;

public class ObjectViewResourceBundle extends ListResourceBundle {
    protected Object[][] contents = null;

    public ObjectViewResourceBundle(final String name)
        throws IOException {
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            if (classLoader == null) {
                classLoader = getClass().getClassLoader();
            }
            final Properties properties = new Properties();
            final InputStream in = classLoader.getResourceAsStream(name);
            if (name.endsWith(".xml")) {
                final InputStream bufferedIn = new BufferedInputStream(in);
                properties.loadFromXML(bufferedIn);
                bufferedIn.close();
            } else {
                final Reader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                properties.load(reader);
                reader.close();
            }
            final List<Object[]> contentList = new ArrayList<Object[]>();
            final Iterator<String> propertyNames = properties.stringPropertyNames().iterator();
            while (propertyNames.hasNext()) {
                final String propertyName = propertyNames.next();
                contentList.add(new Object[] {propertyName, properties.getProperty(propertyName)});
            }
            contents = contentList.toArray(new Object[contentList.size()][]);
        } catch (final IOException ioException) {
            ioException.printStackTrace();
            throw ioException;
        }
    }

    protected Object[][] getContents() {
        return contents;
    }

}
