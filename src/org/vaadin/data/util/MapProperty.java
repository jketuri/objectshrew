/*
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vaadin.data.util;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;

import com.vaadin.v7.data.Property;
import com.vaadin.v7.util.SerializerHelper;

/**
 * MapProperty
 */
public class MapProperty implements Property, Property.ValueChangeNotifier,
                                    Property.ReadOnlyStatusChangeNotifier {
    private final static long serialVersionUID = 0L;

    /**
     * The map that includes the property the MapProperty is bound to.
     */
    private transient Map instance;

    /**
     * Is the MapProperty read-only?
     */
    private boolean readOnly;

    /**
     * The property name.
     */
    private transient Object key;

    /**
     * Type of the property.
     */
    private transient Class<?> type;

    /**
     * Target type of value.
     */
    private transient Class<?> targetType;

    /**
     * List of listeners who are interested in the read-only status changes of
     * the MapProperty
     */
    private LinkedList<ReadOnlyStatusChangeListener> readOnlyStatusChangeListeners = null;

    /**
     * List of listeners who are interested in the value changes of the
     * MapProperty
     */
    private LinkedList<ValueChangeListener> valueChangeListeners = null;

    /* Special serialization to handle method references */
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        SerializerHelper.writeClass(out, type);
        out.writeObject(instance);
    };

    /* Special serialization to handle method references */
    private void readObject(java.io.ObjectInputStream in) throws IOException,
                                                                 ClassNotFoundException {
        in.defaultReadObject();
        try {
            type = SerializerHelper.readClass(in);
            instance = (Map)in.readObject();
        } catch (SecurityException e) {
            System.err.println("Internal deserialization error");
            e.printStackTrace();
        }
    };

    /**
     * 
     * @param instance
     *            the map that includes the property.
     * @param key
     *            the key of the property to bind to.
     * @param type
     *            type of the property to bind to.
     * @param targetType
     *            target type of the value.
     */
    @SuppressWarnings("unchecked")
    public MapProperty(final Map instance,
                       final Object key,
                       final Class<?> type,
                       final Class<?> targetType) {
        this.instance = instance;
        this.key = key;
        this.type = type;
        this.targetType = targetType;
    }

    /**
     * Returns the type of the Property. The methods <code>getValue</code> and
     * <code>setValue</code> must be compatible with this type: one must be able
     * to safely cast the value returned from <code>getValue</code> to the given
     * type and pass any variable assignable to this type as an argument to
     * <code>setValue</code>.
     * 
     * @return type of the Property
     */
    @SuppressWarnings("unchecked")
    public final Class getType() {
        return type;
    }

    /**
     * Tests if the object is in read-only mode. In read-only mode calls to
     * <code>setValue</code> will throw <code>ReadOnlyException</code> and will
     * not modify the value of the Property.
     * 
     * @return <code>true</code> if the object is in read-only mode,
     *         <code>false</code> if it's not
     */
    public boolean isReadOnly() {
        return readOnly;
    }

    /**
     * Gets the value stored in the Property. The value is resolved by calling
     * the specified getter method with the argument specified at instantiation.
     * 
     * @return the value of the Property
     */
    public Object getValue() {
        Object value = instance.get(key);
        if (value != null && !value.getClass().equals(targetType)) {
            if (java.sql.Date.class.equals(targetType)) {
                return new java.sql.Date(((Date)value).getTime());
            }
            if (java.sql.Time.class.equals(targetType)) {
                return new java.sql.Time(((Date)value).getTime());
            }
            if (java.sql.Timestamp.class.equals(targetType)) {
                return new java.sql.Timestamp(((Date)value).getTime());
            }
        }
        return value;
    }

    /**
     * Returns the value of the <code>MapProperty</code> in human readable
     * textual format. The return value should be assignable to the
     * <code>setValue</code> method if the Property is not in read-only mode.
     * 
     * @return String representation of the value stored in the Property
     */
    @Override
    public String toString() {
        final Object value = getValue();
        if (value == null) {
            return null;
        }
        return value.toString();
    }

    /**
     * Sets the value of the property. This method supports setting from
     * <code>String</code>s if either <code>String</code> is directly assignable
     * to property type, or the type class contains a string constructor.
     * 
     * @param newValue
     *            the New value of the property.
     * @throws Property.ReadOnlyException if the object is in
     *         read-only mode.
     */
    @SuppressWarnings("unchecked")
    public void setValue(Object newValue) throws Property.ReadOnlyException {
        // Checks the mode
        if (isReadOnly()) {
            throw new Property.ReadOnlyException();
        }

        instance.put(key, newValue);
        fireValueChange();
    }

    /**
     * Sets the Property's read-only mode to the specified status.
     * 
     * @param newStatus
     *            the new read-only status of the Property.
     */
    public void setReadOnly(boolean newStatus) {
        final boolean prevStatus = readOnly;
        if (newStatus) {
            readOnly = true;
        } else {
            readOnly = false;
        }
        if (prevStatus != readOnly) {
            fireReadOnlyStatusChange();
        }
    }

    /* Events */

    /**
     * An <code>Event</code> object specifying the Property whose read-only
     * status has been changed.
     * 
     * @author IT Mill Ltd.
     * @version
     * 6.4.0
     * @since 3.0
     */
    private class ReadOnlyStatusChangeEvent extends java.util.EventObject
        implements Property.ReadOnlyStatusChangeEvent {
        private final static long serialVersionUID = 0L;

        /**
         * Constructs a new read-only status change event for this object.
         * 
         * @param source
         *            source object of the event.
         */
        protected ReadOnlyStatusChangeEvent(MapProperty source) {
            super(source);
        }

        /**
         * Gets the Property whose read-only state has changed.
         * 
         * @return source Property of the event.
         */
        public Property getProperty() {
            return (Property) getSource();
        }

    }

    /**
     * Registers a new read-only status change listener for this Property.
     * 
     * @param listener
     *            the new Listener to be registered.
     */
    public void addListener(Property.ReadOnlyStatusChangeListener listener) {
        if (readOnlyStatusChangeListeners == null) {
            readOnlyStatusChangeListeners = new LinkedList<ReadOnlyStatusChangeListener>();
        }
        readOnlyStatusChangeListeners.add(listener);
    }

    /**
     * Registers a new read-only status change listener for this Property.
     * 
     * @param listener
     *            the new Listener to be registered.
     */
    public void addReadOnlyStatusChangeListener(Property.ReadOnlyStatusChangeListener listener) {
        if (readOnlyStatusChangeListeners == null) {
            readOnlyStatusChangeListeners = new LinkedList<ReadOnlyStatusChangeListener>();
        }
        readOnlyStatusChangeListeners.add(listener);
    }

    /**
     * Removes a previously registered read-only status change listener.
     * 
     * @param listener
     *            the listener to be removed.
     */
    public void removeListener(Property.ReadOnlyStatusChangeListener listener) {
        if (readOnlyStatusChangeListeners != null) {
            readOnlyStatusChangeListeners.remove(listener);
        }
    }

    /**
     * Removes a previously registered read-only status change listener.
     * 
     * @param listener
     *            the listener to be removed.
     */
    public void removeReadOnlyStatusChangeListener(Property.ReadOnlyStatusChangeListener listener) {
        if (readOnlyStatusChangeListeners != null) {
            readOnlyStatusChangeListeners.remove(listener);
        }
    }

    /**
     * Sends a read only status change event to all registered listeners.
     */
    private void fireReadOnlyStatusChange() {
        if (readOnlyStatusChangeListeners != null) {
            final Object[] l = readOnlyStatusChangeListeners.toArray();
            final Property.ReadOnlyStatusChangeEvent event
                = new MapProperty.ReadOnlyStatusChangeEvent(this);
            for (int i = 0; i < l.length; i++) {
                ((Property.ReadOnlyStatusChangeListener) l[i])
                    .readOnlyStatusChange(event);
            }
        }
    }

    /**
     * An <code>Event</code> object specifying the Property whose value has been
     * changed.
     * 
     * @author IT Mill Ltd.
     * @version
     * 6.4.0
     * @since 5.3
     */
    private class ValueChangeEvent extends java.util.EventObject
        implements Property.ValueChangeEvent {
        private final static long serialVersionUID = 0L;

        /**
         * Constructs a new value change event for this object.
         * 
         * @param source
         *            source object of the event.
         */
        protected ValueChangeEvent(MapProperty source) {
            super(source);
        }

        /**
         * Gets the Property whose value has changed.
         * 
         * @return source Property of the event.
         */
        public Property getProperty() {
            return (Property) getSource();
        }

    }

    public void addListener(ValueChangeListener listener) {
        if (valueChangeListeners == null) {
            valueChangeListeners = new LinkedList<ValueChangeListener>();
        }
        valueChangeListeners.add(listener);
    }

    public void addValueChangeListener(ValueChangeListener listener) {
        if (valueChangeListeners == null) {
            valueChangeListeners = new LinkedList<ValueChangeListener>();
        }
        valueChangeListeners.add(listener);
    }

    public void removeListener(ValueChangeListener listener) {
        if (valueChangeListeners != null) {
            valueChangeListeners.remove(listener);
        }
    }

    public void removeValueChangeListener(ValueChangeListener listener) {
        if (valueChangeListeners != null) {
            valueChangeListeners.remove(listener);
        }
    }

    /**
     * Sends a value change event to all registered listeners.
     */
    public void fireValueChange() {
        if (valueChangeListeners != null) {
            final Object[] l = valueChangeListeners.toArray();
            final Property.ValueChangeEvent event
                = new MapProperty.ValueChangeEvent(this);
            for (int i = 0; i < l.length; i++) {
                ((Property.ValueChangeListener) l[i]).valueChange(event);
            }
        }
    }

}
