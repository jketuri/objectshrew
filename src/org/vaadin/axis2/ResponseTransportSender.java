
package org.vaadin.axis2;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.context.OperationContext;
import org.apache.axis2.transport.OutTransportInfo;
import org.apache.axis2.transport.base.AbstractTransportSender;
import org.apache.axis2.wsdl.WSDLConstants;

public class ResponseTransportSender extends AbstractTransportSender {

    @Override
    public void sendMessage(MessageContext msgContext, String targetEPR,
                            OutTransportInfo outTransportInfo)
        throws AxisFault {
        try {
            OperationContext operationContext = msgContext.getOperationContext();
            MessageContext inMessageContext
                = operationContext.getMessageContext(WSDLConstants.MESSAGE_LABEL_IN_VALUE);
            operationContext.setProperty(MessageContext.TRANSPORT_IN,
                                         new BufferedInputStream(new FileInputStream(targetEPR.substring("response:".length()))));
            inMessageContext.setDoingREST(true);
            inMessageContext.setProperty(Constants.Configuration.MESSAGE_TYPE,
                                         msgContext.getProperty(Constants.Configuration.MESSAGE_TYPE));
            inMessageContext.setProperty(Constants.Configuration.CONTENT_TYPE,
                                         msgContext.getProperty(Constants.Configuration.MESSAGE_TYPE));
            inMessageContext.setProperty(Constants.Configuration.CHARACTER_SET_ENCODING, "utf-8");
        } catch (FileNotFoundException ex) {
            throw AxisFault.makeFault(ex);
        }
    }

}

