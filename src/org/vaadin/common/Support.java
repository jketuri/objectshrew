/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.vaadin.common;

import java.lang.Package;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;

public abstract class Support {

    public static final char toHex(int number) {
        return Character.toUpperCase(Character.forDigit(number, 16));
    }

    public static final String toHexString(byte bytes[]) {
        StringBuilder builder = new StringBuilder();
        for (int index = 0; index < bytes.length; index++) {
            builder.append(Support.toHex((bytes[index] & 0xf0) >>> 4)).append(Support.toHex(bytes[index] & 0xf));
        }
        return builder.toString();
    }

    public static final byte fromHex(char hex) {
        return (byte)(Character.digit(hex, 16) & 0xff);
    }

    public static final byte[] fromHexString(final String hexString) {
        final int length = hexString.length();
        final byte[] bytes = new byte[length];
        for (int index = 0; index < length; index += 2) {
            bytes[index] = (byte)(((Support.fromHex(hexString.charAt(index)) & 0xff)) << 4 | (Support.fromHex(hexString.charAt(index)) & 0xf));
        }
        return bytes;
    }

    public static String toString(Object object) {
        if (object == null) {
            return "<null>";
        }
        final StringBuffer buffer = new StringBuffer();
        final Class objectClass = object.getClass();
        if (objectClass.isArray()) {
            buffer.append(objectClass.getComponentType().getName() + "[");
            final int length = java.lang.reflect.Array.getLength(object);
            for (int index = 0; index < length; index++) {
                Object item = java.lang.reflect.Array.get(object, index);
                if (index > 0) {
                    buffer.append(", ");
                }
                buffer.append(Support.toString(item));
            }
            buffer.append("]");
        } else if (object instanceof Iterable) {
            buffer.append(objectClass.getName() + "{");
            final Iterator elements = ((Iterable)object).iterator();
            boolean started = false;
            while (elements.hasNext()) {
                final Object element = elements.next();
                if (started) {
                    buffer.append(", ");
                } else {
                    started = true;
                }
                buffer.append(Support.toString(element));
            }
            buffer.append("}");
        } else if (object instanceof Map) {
            buffer.append(objectClass.getName() + "{");
            final Iterator<Map.Entry> entries = (Iterator<Map.Entry>)((Map)object).entrySet().iterator();
            boolean started = false;
            while (entries.hasNext()) {
                final Map.Entry entry = entries.next();
                if (started) {
                    buffer.append(", ");
                } else {
                    started = true;
                }
                if (object != entry.getKey() && object != entry.getValue()) {
                    buffer.append(Support.toString(entry.getKey())).append('=').append(Support.toString(entry.getValue()));
                }
            }
            buffer.append("}");
        } else if (object instanceof Enum) {
            buffer.append(object.toString());
        } else {
            final Package objectPackage = objectClass.getPackage();
            if (objectPackage != null) {
                final String packageName = objectPackage.getName();
                if (!packageName.startsWith("org.")) {
                    return object.toString();
                }
            }
            buffer.append(objectClass.getName() + "{");
            boolean started = false;
            final Method methods[] = objectClass.getMethods();
            for (final Method method : methods) {
                boolean hadIs = false;
                final Class declaringClass = method.getDeclaringClass();
                final Package classPackage = declaringClass.getPackage();
                if (classPackage != null) {
                    final String packageName = classPackage.getName();
                    if (!packageName.startsWith("org.")) {
                        continue;
                    }
                }
                final String methodName = method.getName();
                if (method.getParameterTypes().length == 0 &&
                    ((methodName.length() > 3 &&
                      methodName.startsWith("get") &&
                      Character.isUpperCase(methodName.charAt(3))) ||
                     (hadIs = methodName.length() > 2 &&
                      methodName.startsWith("is") &&
                      Character.isUpperCase(methodName.charAt(2))))) {
                    final Class returnType = method.getReturnType();
                    try {
                        final Object returnValue = method.invoke(object);
                        String name = methodName.substring(hadIs ? 2 : 3);
                        name = Character.toLowerCase(name.charAt(0)) + name.substring(1);
                        if (started) buffer.append(", ");
                        buffer.append(name + "=" + Support.toString(returnValue));
                        started = true;
                    } catch (IllegalAccessException ex) {}
                    catch (InvocationTargetException ex) {}
                }
            }
            final Field fields[] = objectClass.getFields();
            for (final Field field : fields) {
                final Class declaringClass = field.getDeclaringClass();
                final Package classPackage = declaringClass.getPackage();
                if (classPackage != null) {
                    final String packageName = classPackage.getName();
                    if (!packageName.startsWith("org.")) {
                        continue;
                    }
                }
                final String name = field.getName();
                try {
                    final Object value = field.get(object);
                    if (started) buffer.append(", ");
                    buffer.append(name + "=" + Support.toString(value));
                    started = true;
                } catch (IllegalAccessException ex) {}
            }
            buffer.append("}");
        }
        return buffer.toString();
    }

}
