/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.dummy;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.vaadin.objectview.ObjectView;

public class DummyFormClass implements Serializable {
    private final static long serialVersionUID = 0L;

    public static enum EnumValue {ENUM1, ENUM2, ENUM3, ENUM4};

    protected boolean invisible = false;
    protected boolean disabled = false;
    protected boolean booleanValue = false;
    protected char charValue = 'A';
    protected byte byteValue = 0;
    protected short shortValue = 0;
    protected int intValue = 1;
    protected long longValue = 2L;
    protected float floatValue = 1.0f;
    protected double doubleValue = 2.0;
    protected BigDecimal bigDecimalValue = BigDecimal.ZERO;
    protected BigInteger bigIntegerValue = BigInteger.ZERO;
    protected String stringValue = "String";
    protected String optionalStringValue = null;
    protected Date dateValue = null;
    protected DummyChildClass.EnumValue enumValue = DummyChildClass.EnumValue.ENUM2;
    protected DummyChildClass.EnumValue groupEnumValue = DummyChildClass.EnumValue.ENUM2;
    protected DummyChildClass dummyChildClass = new DummyChildClass();
    protected ObjectView.LayoutType layoutType = null;
    {
        long time = System.currentTimeMillis();
        time -= time % 1000;
        dateValue = new Date(time);
    }

    public DummyFormClass() {
    }

    public String id() {
        return String.valueOf(dateValue.getTime());
    }

    public boolean equals(final Object object) {
        return object != null && ((DummyFormClass)object).id().equals(id());
    }

    public ObjectView.LayoutType layoutType() {
        return layoutType;
    }

    public boolean invisible() {
        return invisible;
    }

    public boolean disabled() {
        return disabled;
    }

    public void setBooleanValue(final boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public boolean getBooleanValue() {
        return booleanValue;
    }

    public void setCharValue(final char charValue) {
        this.charValue = charValue;
    }

    public char getCharValue() {
        return charValue;
    }

    public void setByteValue(final byte byteValue) {
        this.byteValue = byteValue;
    }

    public byte getByteValue() {
        return byteValue;
    }

    public void setShortValue(final short shortValue) {
        this.shortValue = shortValue;
    }

    public short getShortValue() {
        return shortValue;
    }

    public void setIntValue(final int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return intValue;
    }

    public void setLongValue(final long longValue) {
        this.longValue = longValue;
    }

    public long getLongValue() {
        return longValue;
    }

    public void setFloatValue(final float floatValue) {
        this.floatValue = floatValue;
    }

    public float getFloatValue() {
        return floatValue;
    }

    public void setDoubleValue(final double doubleValue) {
        this.doubleValue = doubleValue;
    }

    public double getDoubleValue() {
        return doubleValue;
    }

    public void setBigDecimalValue(final BigDecimal bigDecimalValue) {
        this.bigDecimalValue = bigDecimalValue;
    }

    public BigDecimal getBigDecimalValue() {
        return bigDecimalValue;
    }

    public void setBigIntegerValue(final BigInteger bigIntegerValue) {
        this.bigIntegerValue = bigIntegerValue;
    }

    public BigInteger getBigIntegerValue() {
        return bigIntegerValue;
    }

    public void setStringValue(final String stringValue) {
        this.stringValue = stringValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setOptionalStringValue(final String optionalStringValue) {
        this.optionalStringValue = optionalStringValue;
    }

    public String getOptionalStringValue() {
        return optionalStringValue;
    }

    public void setDateValue(final Date dateValue) {
        this.dateValue = dateValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setEnumValue(final DummyChildClass.EnumValue enumValue) {
        this.enumValue = enumValue;
    }

    public DummyChildClass.EnumValue getEnumValue() {
        return enumValue;
    }

    public void setGroupEnumValue(final DummyChildClass.EnumValue groupEnumValue) {
        this.groupEnumValue = groupEnumValue;
    }

    public DummyChildClass.EnumValue getGroupEnumValue() {
        return groupEnumValue;
    }

    public void setDummyChildClass(final DummyChildClass dummyChildClass) {
        this.dummyChildClass = dummyChildClass;
    }

    public DummyChildClass getDummyChildClass() {
        return dummyChildClass;
    }

    public String[] execute() {
        setStringValue("Executed");
        return new String[] {"Dummy.tabList.DummyClass", "Dummy.tabList.DummyChildClass.stringValue", "Dummy.dummyWindow.stringValue", "Dummy.dummyApplicationWindow.stringValue"};
    }

}
