/*
 * Copyright 2011-2016 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.dummy.resources;

import java.io.IOException;

public class Dummy_fi extends Dummy {

    public Dummy_fi()
        throws IOException {
        super("/org/dummy/resources/Dummy_fi.properties");
    }

    public Dummy_fi(final String name)
        throws IOException {
        super(name);
    }

}
