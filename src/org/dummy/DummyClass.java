/*
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.dummy;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.vaadin.objectview.ObjectView;

public class DummyClass extends DummyChildClass {
    private final static long serialVersionUID = 0L;

    protected static enum WebClientType {REST_JSON, REST_XML, SOAP_XML};
    protected static String[] valuesLocale = new String[] {"en", "fi"};

    protected String locale = null;
    protected String[] themeValues = null;
    protected String theme = null;
    protected String stringReadOnlyValue = "Read only";
    protected DummyClass.DummyClassEnumSet enumSet = new DummyClass.DummyClassEnumSet();
    protected DummyClass.DummyClassEnumSet groupEnumSet = new DummyClass.DummyClassEnumSet();
    protected DummyChildClass child = new DummyChildClass();
    protected DummyChildClass requestChild = null;
    protected DummyChildClass booleanValueChild = new DummyChildClass();
    protected DummyChildClass stringValueChild = new DummyChildClass();
    protected DummyChildClass enumValueChild = new DummyChildClass();
    protected DummyClass.DummyChildClassList list = null;
    protected DummyClass.DummyChildClassList listReadOnly = new DummyClass.DummyChildClassList();
    protected DummyClass.DummyChildClassList responseList = new DummyClass.DummyChildClassList();
    protected DummyClass.DummyFormClassList formList = null;
    protected DummyClass.StringList stringList = null;
    protected boolean listInvisible = false;
    protected boolean listReadOnlyInvisible = false;
    protected int begin = 0;
    protected int end = -1;
    protected DummyClass.WebClientType webClientType = DummyClass.WebClientType.REST_JSON;
    protected DummyLink dummyWindowLink = new DummyLink("dummyWindow", "Dummy.dummyWindow");
    protected DummyLink dummyApplicationWindowLink = new DummyLink("dummyApplicationWindow", "Dummy.dummyApplicationWindow");
    protected DummyLink dummyPageApplicationWindowLink = new DummyLink("dummyPageApplicationWindow", "Dummy.dummyPageApplicationWindow");
    protected DummyLink dummyChildClassLink = null;
    {
        child.layoutType = ObjectView.LayoutType.HORIZONTAL_LAYOUT;
        booleanValueChild.invisible = true;
        stringValueChild.invisible = true;
        enumValueChild.invisible = true;
        enumSet.add(DummyChildClass.EnumValue.ENUM2);
        enumSet.add(DummyChildClass.EnumValue.ENUM4);
        groupEnumSet.add(DummyChildClass.EnumValue.ENUM1);
        groupEnumSet.add(DummyChildClass.EnumValue.ENUM3);
    }

    public static class DummyClassEnumSet extends HashSet<DummyChildClass.EnumValue> {
        private final static long serialVersionUID = 0L;
    }

    public static class DummyChildClassList extends ArrayList<DummyChildClass> {
        private final static long serialVersionUID = 0L;
    }

    public static class DummyFormClassList extends ArrayList<DummyFormClass> {
        private final static long serialVersionUID = 0L;
    }

    public static class StringList extends ArrayList<String> {
        private final static long serialVersionUID = 0L;
    }

    public DummyClass(final boolean usePage) {
        dummyChildClassLink = new DummyLink("DummyChildClass", usePage ? "Dummy.dummyPageApplicationWindow.pageList.DummyChildClass" : "Dummy.tabList.DummyChildClass");
    }

    public void themes(final String[] themeValues) {
        this.themeValues = themeValues;
    }

    public ObjectView.LayoutType layoutType() {
        return ObjectView.LayoutType.FORM_LAYOUT;
    }

    public void setLocale(final String locale) {
        if (locale != null) {
            final StringTokenizer tokens = new StringTokenizer(locale, "_");
            this.locale = tokens.nextToken();
        }
    }

    public String getLocale() {
        return locale;
    }

    public String[] editLocale(final String locale) {
        return new String[0];
    }

    public String[] valuesLocale() {
        return valuesLocale;
    }

    public void setTheme(final String theme) {
        this.theme = theme;
    }

    public String getTheme() {
        return theme;
    }

    public String[] editTheme(final String theme) {
        return new String[0];
    }

    public String[] valuesTheme() {
        return themeValues;
    }

    public String getTextValue() {
        return "ÖÄÅöäå";
    }

    public void setBooleanValue(final boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public boolean getBooleanValue() {
        return booleanValue;
    }

    public String[] editBooleanValue(final boolean value) {
        booleanValueChild.invisible = !value;
        return new String[] {"Dummy.tabList.DummyClass.booleanValueChild"};
    }

    public void setBooleanValueChild(final DummyChildClass booleanValueChild) {
        this.booleanValueChild = booleanValueChild;
    }

    public DummyChildClass getBooleanValueChild() {
        return booleanValueChild;
    }

    public void setCharValue(final char charValue) {
        this.charValue = charValue;
    }

    public char getCharValue() {
        return charValue;
    }

    public void setByteValue(final byte byteValue) {
        this.byteValue = byteValue;
    }

    public byte getByteValue() {
        return byteValue;
    }

    public void setShortValue(final short shortValue) {
        this.shortValue = shortValue;
    }

    public short getShortValue() {
        return shortValue;
    }

    public void setIntValue(final int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return intValue;
    }

    public void setLongValue(final long longValue) {
        this.longValue = longValue;
    }

    public long getLongValue() {
        return longValue;
    }

    public void setFloatValue(final float floatValue) {
        this.floatValue = floatValue;
    }

    public float getFloatValue() {
        return floatValue;
    }

    public void setDoubleValue(final double doubleValue) {
        this.doubleValue = doubleValue;
    }

    public double getDoubleValue() {
        return doubleValue;
    }

    public void setBigDecimalValue(final BigDecimal bigDecimalValue) {
        this.bigDecimalValue = bigDecimalValue;
    }

    public BigDecimal getBigDecimalValue() {
        return bigDecimalValue;
    }

    public void setBigIntegerValue(final BigInteger bigIntegerValue) {
        this.bigIntegerValue = bigIntegerValue;
    }

    public BigInteger getBigIntegerValue() {
        return bigIntegerValue;
    }

    public void setStringValue(final String stringValue) {
        this.stringValue = stringValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public String[] editStringValue(final String value) {
        stringValueChild.invisible = !"a".equals(value);
        return new String[] {"Dummy.tabList.DummyClass.stringValueChild"};
    }

    public void setStringValueChild(DummyChildClass stringValueChild) {
        this.stringValueChild = stringValueChild;
    }

    public DummyChildClass getStringValueChild() {
        return stringValueChild;
    }

    public void setOptionalStringValue(final String optionalStringValue) {
        this.optionalStringValue = optionalStringValue;
    }

    public String getOptionalStringValue() {
        return optionalStringValue;
    }

    public void setDateValue(final Date dateValue) {
        this.dateValue = dateValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setEnumValue(final DummyChildClass.EnumValue enumValue) {
        this.enumValue = enumValue;
    }

    public DummyChildClass.EnumValue getEnumValue() {
        return enumValue;
    }

    public String[] editEnumValue(final DummyChildClass.EnumValue value) {
        enumValueChild.invisible = !DummyChildClass.EnumValue.ENUM4.equals(value);
        return new String[] {"Dummy.tabList.DummyClass.enumValueChild"};
    }

    public void setEnumValueChild(DummyChildClass enumValueChild) {
        this.enumValueChild = enumValueChild;
    }

    public DummyChildClass getEnumValueChild() {
        return enumValueChild;
    }

    public void setGroupEnumValue(final DummyChildClass.EnumValue groupEnumValue) {
        this.groupEnumValue = groupEnumValue;
    }

    public DummyChildClass.EnumValue getGroupEnumValue() {
        return groupEnumValue;
    }

    public void setEnumSet(final Set<DummyChildClass.EnumValue> enumSet) {
        final DummyClass.DummyClassEnumSet newEnumSet = new DummyClass.DummyClassEnumSet();
        newEnumSet.addAll(enumSet);
        this.enumSet.clear();
        this.enumSet.addAll(newEnumSet);
    }

    public DummyClass.DummyClassEnumSet getEnumSet() {
        return enumSet;
    }

    public void setGroupEnumSet(final Set<DummyChildClass.EnumValue> groupEnumSet) {
        final DummyClass.DummyClassEnumSet newGroupEnumSet = new DummyClass.DummyClassEnumSet();
        newGroupEnumSet.addAll(groupEnumSet);
        this.groupEnumSet.clear();
        this.groupEnumSet.addAll(newGroupEnumSet);
    }

    public DummyClass.DummyClassEnumSet getGroupEnumSet() {
        return groupEnumSet;
    }

    public String getStringReadOnlyValue() {
        return stringReadOnlyValue;
    }

    public void setChild(final DummyChildClass child) {
        this.child = child;
    }

    public DummyChildClass getChild() {
        return child;
    }

    public DummyClass.DummyChildClassList getListReadOnly() {
        return listReadOnly;
    }

    public boolean invisibleListReadOnly() {
        return listReadOnlyInvisible;
    }

    public void setListReadOnlyInvisible(final boolean listReadOnlyInvisible) {
        this.listReadOnlyInvisible = listReadOnlyInvisible;
    }

    public String[] editListReadOnlyInvisible(final boolean listReadOnlyInvisible) {
        this.listReadOnlyInvisible = listReadOnlyInvisible;
        return new String[] {"Dummy.tabList.DummyClass.listReadOnly"};
    }

    public boolean getListReadOnlyInvisible() {
        return listReadOnlyInvisible;
    }

    public void setList(final DummyClass.DummyChildClassList list) {
        this.list = list;
    }

    public DummyClass.DummyChildClassList getList() {
        return list;
    }

    public boolean invisibleList() {
        return listInvisible;
    }

    public void setListInvisible(final boolean listInvisible) {
        this.listInvisible = listInvisible;
    }

    public String[] editListInvisible(final boolean listInvisible) {
        this.listInvisible = listInvisible;
        return new String[] {"Dummy.tabList.DummyClass.list"};
    }

    public boolean getListInvisible() {
        return listInvisible;
    }

    public void setFormList(final DummyClass.DummyFormClassList formList) {
        this.formList = formList;
    }

    public DummyClass.DummyFormClassList getFormList() {
        return formList;
    }

    public void setStringList(final DummyClass.StringList stringList) {
        this.stringList = stringList;
    }

    public DummyClass.StringList getStringList() {
        return stringList;
    }

    public DummyLink getLinkVolatileDummyChildClass() {
        return dummyChildClassLink;
    }

    public DummyLink getLinkVolatileDummyWindow() {
        return dummyWindowLink;
    }

    public DummyLink getLinkVolatileDummyApplicationWindow() {
        return dummyApplicationWindowLink;
    }

    public DummyLink getLinkVolatileDummyPageApplicationWindow() {
        return dummyPageApplicationWindowLink;
    }

    public DummyClass.DummyChildClassList getChildStoreSearch() {
        return getChildSearch();
    }

    public void setChildStoreSearch(final DummyClass.DummyChildClassList dummyChildClassList) {
        setChildSearch(dummyChildClassList);
    }

    public DummyClass.DummyChildClassList getChildStore() {
        if (responseList.size() > 0) {
            return responseList;
        }
        return getChildSearch();
    }

    public String[] childStore() {
        setStringValue("Stored");
        return new String[] {null, "Dummy.tabList.DummyClass.stringValue"};
    }

    public DummyClass.DummyChildClassList getChildSearch() {
        final DummyClass.DummyChildClassList dummyChildClassList = new DummyClass.DummyChildClassList();
        dummyChildClassList.add(child);
        return dummyChildClassList;
    }

    public void setChildSearch(final DummyClass.DummyChildClassList dummyChildClassList) {
        setChildResponse(dummyChildClassList);
    }

    public String[] childSearch() {
        return new String[] {null, "Dummy.tabList.DummyClass.listReadOnly"};
    }

    public int getChildSearchBegin() {
        return this.begin;
    }

    public int getChildSearchEnd() {
        return this.end;
    }

    public void setBegin(final int begin) {
        this.begin = begin;
    }

    public int getBegin() {
        return this.begin;
    }

    public void setEnd(final int end) {
        this.end = end;
    }

    public int getEnd() {
        return this.end;
    }

    public DummyClass.DummyChildClassList getChildRemove() {
        return getChildSearch();
    }

    public String[] childRemove() {
        setStringValue("Removed");
        return new String[] {null, "Dummy.tabList.DummyClass.stringValue"};
    }

    public String[] editWebClientType(final DummyClass.WebClientType webClientType) {
        return null;
    }

    public void setWebClientType(final DummyClass.WebClientType webClientType) {
        this.webClientType = webClientType;
    }

    public DummyClass.WebClientType getWebClientType() {
        return webClientType;
    }

    /**
     * Request which is sent with childClientRetrieve(), childClientUpdate(), childClientAdd(), childClientDelete()
     * in client side. Object is converted to JSON or XML and sent to network.
     **/
    public DummyChildClass getChildRequest() {
        return child;
    }

    /**
     * Request which is received with childRetrieve(), childUpdate(), childAdd(), childDelete()
     * in server side. Request is received from network as JSON or XML and it is converted
     * to object and set with this method for application by the framework.
     **/
    public void setChildRequest(final DummyChildClass child) {
        this.requestChild = child;
    }

    /**
     * Response which is sent with childRetrieve(), childUpdate(), childAdd(), childDelete()
     * in server side. Response is converted to JSON or XML and sent to network as response
     * to request which was received with method setChildRequest.
     **/
    public DummyClass.DummyChildClassList getChildResponse() {
        return responseList;
    }

    /**
     * Response which is received with childClientRetrieve(), childClientUpdate(), childClientAdd(),
     * childClientDelete() in client side. Response is received from network and converted from JSON or XML
     * to object and set with this method for application by the framework.
     **/
    public void setChildResponse(final DummyClass.DummyChildClassList dummyChildClassList) {
        listReadOnly.clear();
        listReadOnly.addAll(dummyChildClassList);
    }

    /**
     * Search criteria list used with childRetrieve() in server side to search from persistent storage.
     * This method is called before childRetrieve().
     **/
    public DummyClass.DummyChildClassList getChildRetrieveSearch() {
        final DummyClass.DummyChildClassList dummyChildClassList = new DummyClass.DummyChildClassList();
        if (requestChild != null) {
            dummyChildClassList.add(requestChild);
        }
        return dummyChildClassList;
    }

    /**
     * Receives search results from persistent storage with search criteria given in getChildRetrieveSearch().
     * This method is called after getChildRetrieveSearch() and before childRetrieve().
     **/
    public void setChildRetrieveSearch(final DummyClass.DummyChildClassList dummyChildClassList) {
        responseList.clear();
        responseList.addAll(dummyChildClassList);
    }

    /**
     * Service method in server side for REST GET-verb and SOAP-request with retrieve-action.
     */
    public String[] childRetrieve() {
        return null;
    }

    /**
     * Search criteria list used with childUpdate() in server side to search from persistent storage.
     * This method is called before childUpdate() by the framework.
     **/
    public DummyClass.DummyChildClassList getChildUpdateSearch() {
        return getChildRetrieveSearch();
    }

    /**
     * Receives search results from persistent storage with search criteria given in getChildUpdateSearch().
     * This method is called after getChildUpdateSearch() and before childUpdate() by the framework.
     **/
    public void setChildUpdateSearch(final DummyClass.DummyChildClassList dummyChildClassList) {
        setChildRetrieveSearch(dummyChildClassList);
    }

    /**
     * Object list from application used with childUpdate() in server side for storing to persistent storage.
     * This method is called after childUpdate() by the framework.
     **/
    public DummyClass.DummyChildClassList getChildUpdateStore() {
        return getChildAddStore();
    }

    /**
     * Service method in server side for REST PUT-verb and SOAP-request with update-action.
     */
    public void childUpdate() {
    }

    /**
     * Search criteria list used with childAdd() in server side to search from persistent storage.
     * This method is called before childAdd() by the framework.
     **/
    public DummyClass.DummyChildClassList getChildAddSearch() {
        return getChildRetrieveSearch();
    }

    /**
     * Receives search results from persistent storage with search criteria given in getChildAddSearch().
     * This method is called after getChildAddSearch() and before childAdd() by the framework.
     **/
    public void setChildAddSearch(final DummyClass.DummyChildClassList dummyChildClassList) {
        setChildRetrieveSearch(dummyChildClassList);
    }

    /**
     * Object list used with childAdd() in server side for storing to persistent storage.
     * This method is called after childAdd() by the framework.
     **/
    public DummyClass.DummyChildClassList getChildAddStore() {
        if (responseList.size() > 0) {
            return responseList;
        }
        return getChildRetrieveSearch();
    }

    /**
     * Service method in server side for REST POST-verb and SOAP-request with add-action.
     */
    public void childAdd() {
    }

    /**
     * Search criteria list used with childDelete() in server side to search from persistent storage.
     * This method is called before childDelete() by the framework.
     **/
    public DummyClass.DummyChildClassList getChildDeleteSearch() {
        return getChildRetrieveSearch();
    }

    /**
     * Receives search results from persistent storage with search criteria given in getChildDeleteSearch().
     * This method is called after getChildDeleteSearch() and before childDelete() by the framework.
     **/
    public void setChildDeleteSearch(final DummyClass.DummyChildClassList dummyChildClassList) {
        setChildRetrieveSearch(dummyChildClassList);
    }

    /**
     * Service method in server side for REST DELETE-verb and SOAP-request with delete-action.
     */
    public void childDelete() {
    }

    public String clientMessageType() {
        return WebClientType.REST_JSON.equals(webClientType) ? "application/json" : null;
    }

    public String clientRequestType() {
        return WebClientType.SOAP_XML.equals(webClientType) ? null : "REST";
    }

    /**
     * Action method in client side for REST GET-verb and SOAP-request with retrieve-action.
     */
    public String[] childClientRetrieve() {
        return new String[] {null, "Dummy.tabList.DummyClass.listReadOnly"};
    }

    /**
     * Action method in client side for REST PUT-verb and SOAP-request with update-action.
     */
    public void childClientUpdate() {
    }

    /**
     * Action method in client side for REST POST-verb and SOAP-request with add-action.
     */
    public void childClientAdd() {
    }

    /**
     * Action method in client side for REST DELETE-verb and SOAP-request with delete-action.
     */
    public void childClientDelete() {
    }

}
