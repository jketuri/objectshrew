/*
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.dummy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.vaadin.objectview.ObjectView.StorageType;

public class Dummy {
    final List<Object> tabList = new ArrayList<Object>();
    {
        tabList.add(new DummyClass(false));
        tabList.add(new DummyChildClass(false, false));
    }

    public StorageType storageType() {
        return StorageType.TDB2;
    }

    public String storageDirectory() {
        return new File(System.getProperty("user.home"), "dummyObjectView").getPath();
    }

    public String namespace() {
        return "http://dummy.org";
    }

    public List<Object> getTabList() {
        return tabList;
    }

    public DummyChildClass getDummyWindow() {
        return new DummyChildClass(false, true);
    }

    public DummyChildClass getDummyApplicationWindow() {
        return new DummyChildClass(false, false);
    }

    public DummyPageClass getDummyPageApplicationWindow() {
        return new DummyPageClass();
    }

}
