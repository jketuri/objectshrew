/*
 * Copyright 2011-2017 Joonas Keturi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.dummy;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.vaadin.common.Support;
import org.vaadin.objectview.ObjectView;

public class DummyChildClass implements Serializable {
    private final static long serialVersionUID = 0L;

    public static enum EnumValue {ENUM1, ENUM2, ENUM3, ENUM4};

    protected boolean usePage = false;
    protected boolean invisible = false;
    protected boolean disabled = false;
    protected boolean booleanValue = false;
    protected char charValue = 'A';
    protected byte byteValue = 0;
    protected short shortValue = 0;
    protected int intValue = 1;
    protected long longValue = 2L;
    protected float floatValue = 1.0f;
    protected double doubleValue = 2.0;
    protected BigDecimal bigDecimalValue = BigDecimal.ZERO;
    protected BigInteger bigIntegerValue = BigInteger.ZERO;
    protected String stringValue = "String";
    protected String optionalStringValue = null;
    protected Date dateValue = null;
    protected DummyChildClass.EnumValue enumValue = DummyChildClass.EnumValue.ENUM2;
    protected DummyChildClass.EnumValue groupEnumValue = DummyChildClass.EnumValue.ENUM2;
    protected DummyChildClass.DummyMenuItemList dummyMenu = null;
    protected ObjectView.LayoutType layoutType = null;
    {
        long time = System.currentTimeMillis();
        time -= time % 1000;
        dateValue = new Date(time);
    }

    public static class DummyMenuItemList extends ArrayList<DummyMenuItem> {
        private final static long serialVersionUID = 0L;

    }

    public class DummyMenuItem implements Serializable {
        private final static long serialVersionUID = 0L;

        private final String name;
        private final DummyChildClass.DummyMenuItemList menu;

        public DummyMenuItem(final String name,
                             final DummyChildClass.DummyMenuItemList menu) {
            this.name = name;
            this.menu = menu;
        }

        public String getName() {
            return name;
        }

        public DummyChildClass.DummyMenuItemList getMenu() {
            return menu;
        }

        public String[] execute() {
            setStringValue(name + " selected");
            return new String[] {null, "Dummy.dummyWindow.stringValue"};
        }

    }

    public DummyChildClass(final boolean usePage,
                           final boolean useMenu) {
        this.usePage = usePage;
        if (useMenu) {
            dummyMenu = new DummyChildClass.DummyMenuItemList();
            for (int menuNumber = 1; menuNumber <= 4; menuNumber++) {
                final DummyChildClass.DummyMenuItemList menu = new DummyChildClass.DummyMenuItemList();
                for (int itemNumber = 1; itemNumber <= 4; itemNumber++) {
                    menu.add(new DummyMenuItem("item" + itemNumber, null));
                }
                dummyMenu.add(new DummyMenuItem("menu" + menuNumber, menu));
            }
        }
    }

    public DummyChildClass() {
    }

    public String id() {
        return String.valueOf(dateValue.getTime());
    }

    public boolean equals(final Object object) {
        return object != null && ((DummyChildClass)object).id().equals(id());
    }

    public ObjectView.LayoutType layoutType() {
        return layoutType;
    }

    public boolean invisible() {
        return invisible;
    }

    public boolean disabled() {
        return disabled;
    }

    public DummyChildClass.DummyMenuItemList getOptionalMenu() {
        return dummyMenu;
    }

    public void setBooleanValue(final boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public boolean getBooleanValue() {
        return booleanValue;
    }

    public void setCharValue(final char charValue) {
        this.charValue = charValue;
    }

    public char getCharValue() {
        return charValue;
    }

    public void setByteValue(final byte byteValue) {
        this.byteValue = byteValue;
    }

    public byte getByteValue() {
        return byteValue;
    }

    public void setShortValue(final short shortValue) {
        this.shortValue = shortValue;
    }

    public short getShortValue() {
        return shortValue;
    }

    public void setIntValue(final int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return intValue;
    }

    public void setLongValue(final long longValue) {
        this.longValue = longValue;
    }

    public long getLongValue() {
        return longValue;
    }

    public void setFloatValue(final float floatValue) {
        this.floatValue = floatValue;
    }

    public float getFloatValue() {
        return floatValue;
    }

    public void setDoubleValue(final double doubleValue) {
        this.doubleValue = doubleValue;
    }

    public double getDoubleValue() {
        return doubleValue;
    }

    public void setBigDecimalValue(final BigDecimal bigDecimalValue) {
        this.bigDecimalValue = bigDecimalValue;
    }

    public BigDecimal getBigDecimalValue() {
        return bigDecimalValue;
    }

    public void setBigIntegerValue(final BigInteger bigIntegerValue) {
        this.bigIntegerValue = bigIntegerValue;
    }

    public BigInteger getBigIntegerValue() {
        return bigIntegerValue;
    }

    public void setStringValue(final String stringValue) {
        this.stringValue = stringValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setOptionalStringValue(final String optionalStringValue) {
        this.optionalStringValue = optionalStringValue;
    }

    public String getOptionalStringValue() {
        return optionalStringValue;
    }

    public void setDateValue(final Date dateValue) {
        this.dateValue = dateValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setEnumValue(final DummyChildClass.EnumValue enumValue) {
        this.enumValue = enumValue;
    }

    public DummyChildClass.EnumValue getEnumValue() {
        return enumValue;
    }

    public void setGroupEnumValue(final DummyChildClass.EnumValue groupEnumValue) {
        this.groupEnumValue = groupEnumValue;
    }

    public DummyChildClass.EnumValue getGroupEnumValue() {
        return groupEnumValue;
    }

    public String[] execute() {
        setStringValue("Executed");
        return new String[] {usePage ? "Dummy.dummyPageApplicationWindow.pageList.DummyClass" : "Dummy.tabList.DummyClass", "Dummy.tabList.DummyChildClass.stringValue", "Dummy.dummyWindow.stringValue", "Dummy.dummyApplicationWindow.stringValue"};
    }

    public String toString() {
        return Support.toString(this);
    }

}
