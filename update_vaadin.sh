set -e
cp -v $HOME/apps/vaadin/vaadin-*.jar lib/
mv -v lib/vaadin-client-compiler-?.?.?.jar compile/lib/
mv -v lib/vaadin-compatibility-client-?.?.?.jar compile/lib/
rm lib/vaadin-client-?.?.?.jar
cp -v $HOME/apps/vaadin/lib/*.jar lib/
